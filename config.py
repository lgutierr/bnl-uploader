import os
basedir = os.path.abspath(os.path.dirname(__file__))
class Config(object):
  SECRET_KEY = os.getenv('SECRET_KEY')
  SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")
  SQLALCHEMY_TRACK_MODIFICATIONS = False
  STORAGE_PATH = os.getenv('STORAGE_PATH')
  BNL_SSHKEY = os.getenv('BNL_SSHKEY')
  SSH_PRIVATE_KEY = os.getenv('SSH_PRIVATE_KEY')
  BNL_USER = os.getenv('BNL_USER') 
  BNL_GATEWAY = os.getenv('BNL_GATEWAY') 
  BNL_SPAR = os.getenv('BNL_SPAR') 
  BNL_STORAGE_PATH = os.getenv('BNL_STORAGE_PATH') 
  DB_KEY = os.getenv('DB_KEY') 
  BNL_PASSWORD = os.getenv('BNL_PASSWORD') 
  DROPZONE_PATH='/tmp'
  DROPZONE_UPLOAD_MULTIPLE = True
  DROPZONE_PARALLEL_UPLOADS = 5
  DROPZONE_ALLOWED_FILE_CUSTOM = True
  DROPZONE_ALLOWED_FILE_TYPE =  ".txt, .dat, .jpeg, .JPEG, .png, .json, .pdf, .jpg, .JPG, .xlsx, .xls, .csv, .tar, .cr2"
  DROPZONE_MAX_FILE_SIZE = 100
  SEND_FILE_MAX_AGE_DEFAULT = 0