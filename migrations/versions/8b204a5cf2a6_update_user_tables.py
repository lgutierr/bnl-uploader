"""Update user tables

Revision ID: 8b204a5cf2a6
Revises: cb348ccb089d
Create Date: 2025-01-14 11:35:39.346987

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8b204a5cf2a6'
down_revision = 'cb348ccb089d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('code1', sa.LargeBinary(length=256), nullable=True))
        batch_op.add_column(sa.Column('code2', sa.LargeBinary(length=256), nullable=True))
        batch_op.alter_column('submittedFiles',
               existing_type=sa.VARCHAR(length=10000),
               type_=sa.String(length=1000),
               existing_nullable=True)
        batch_op.drop_index('ix_user_accessCode1')
        batch_op.drop_index('ix_user_accessCode2')
        batch_op.create_index(batch_op.f('ix_user_code1'), ['code1'], unique=True)
        batch_op.create_index(batch_op.f('ix_user_code2'), ['code2'], unique=True)
        batch_op.drop_column('accessCode1')
        batch_op.drop_column('accessCode2')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('accessCode2', sa.VARCHAR(length=128), nullable=True))
        batch_op.add_column(sa.Column('accessCode1', sa.VARCHAR(length=128), nullable=True))
        batch_op.drop_index(batch_op.f('ix_user_code2'))
        batch_op.drop_index(batch_op.f('ix_user_code1'))
        batch_op.create_index('ix_user_accessCode2', ['accessCode2'], unique=1)
        batch_op.create_index('ix_user_accessCode1', ['accessCode1'], unique=1)
        batch_op.alter_column('submittedFiles',
               existing_type=sa.String(length=1000),
               type_=sa.VARCHAR(length=10000),
               existing_nullable=True)
        batch_op.drop_column('code2')
        batch_op.drop_column('code1')

    # ### end Alembic commands ###
