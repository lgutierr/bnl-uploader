# Recreate user database
python3 -m flask db upgrade

# Run application

# Dev server - switch on for development
# python3 -m flask run --host=0.0.0.0 --port=8080

#Production server - use when pushing to prod
gunicorn app:app --bind 0.0.0.0:8080
