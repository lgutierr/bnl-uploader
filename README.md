# Welcome to the BNL uploader repository
## Requirements
All the python module requirements for the BNL uploader to work can be found in the ```requirements.txt``` file. 
## Debugging state
* The recommended way of debugging is to create a virtual environment\
```python3 -m venv venv```

* Start an existing virtual environment\
```source venv/bin/activate```

* Install required libraries in virtual environment\
```python3 -m pip install -r requirements.txt```

## Running the software locally
* Start the virtual environment and run\
```source setup.sh```

If you are developing locally, you will need the .env file from another developer to access these variables and put that into the bnl-uploader directory. It is not stored on Gitlab.

## Deploying the software
This software uses CERN PaaS web hosting services: [https://paas.cern.ch/topology/ns/bnl-uploader](https://paas.cern.ch/topology/ns/bnl-uploader). Once a new commit is pushed, the CERN server builds a docker image of the newly committed software and updates public website with the latest version.
## Code arrangement
The code heavily relies on python's flask library. The main functionality code is inside the ```app``` folder which file structure is shown here:

```
bnl-uploader
│   README.md
│   __init__.py
│   config.py
│   requirements.txt
│   setup.sh
│   Dockerfile
│   .gitignore
│   .gitmodules
│   
└───documentation
│   └───example_files
│   │   .md files for maintainers
│   
└───migrations
└───app
│   │   routes.py
│   │   models.py
│   │   forms.py
│   │   diskActions.py
│   │   dbActions.py
│   │   classes.py
│   │   bkgTasks.py
│   └───bkgTaskMethods
│       │ errorchecks.py
│       │ commonFunctions.py
│       │ bkgFunction1_Methods
│       │ bkgFunction1_TestSchema
|       | ...
```

#### bnl-uploader folder 
General setup, configurations, and parts that run the app
* ```__init.py``` Sets up flask app on the docker image with certain settings
* ```config.py``` Contains Config class for ```__init__.py``` that defines the app settings and variables.
* ```requirements.txt``` Contains all python modules that the app uses. When the uploader starts, it ```pip installs``` the modules.
* ```setup.sh``` Creates the SQLLite (from SQLAlchemy) database of users
* ```Dockerfile``` Instructions for running the flask app on the docker image on the CERN server
* ```.gitignore``` Used for supplementary files that are not committed to the gitlab

#### documentation folder
* ```example_files``` Contains examples of files that are using for uploading for various functions
* ```other files``` Contains useful information for code maintainers

#### migrations folder
* This folder has the history of migrations of the db, it just needs to be kept to upgrade the db user tables. It's sort of like a .venv file.

#### app folder
* This folder has the history of migrations of the db, it just needs to be kept to upgrade the db user tables.

* ```routes.py``` Defines all the website URLs including the first interaction layer with the user inputs and the background job submissions.
* ```models.py``` Contains the User class that contains encrypted user information which gets stored on a SQLLite database needed for independent user interactions.
* ```forms.py``` Defines all the widgets the user will interact with such as buttons and forms.
* ```diskActions.py``` Simplified functions for interacting with the BNL backup disk.
* ```dbActions.py``` Simplified interactions with the ATLAS ITk production database using the ```itkdb``` python module: https://itkdb.docs.cern.ch/latest/
* ```classes.py``` Contains the component class which is used for monitoring 
* bkgTasks.py
* ```static folder``` Contains all the website cosmetics information (.css)
* ```bkgTaskMethods folder``` files in this folder are called from bkgTaskMethods, which contains:
    * ```errorchecks.py``` A file of common errors and error handling for bkgTasks
    * ```commonFunctions.py``` common functions that errorchecks and bkgTasks uses
    * ```bkgTask_Methods``` These files contain helper functions for each bkgTask
    * ```bkgTask_TestSchema``` These functions contain the itkdb test schema that bkgTask fills while uploading. 

## Contact information in case you have any requests
#### Supervisors: 
* Stefania Stucci: stucci@bnl.gov
* Gabriele D'Amen: gdamen@bnl.gov 
#### Contributors:
* Luis Felipe Gutierrez: luisfgz@sas.upenn.edu (Project creator/maintainer)
* Daniel House: danielhouse@brandeis.edu (Former maintainer)
* Ross Snyder: ross.snyder@duke.edu (Current maintainer)
* Maya Mancini: mayamancini@brandeis.edu (Current maintainer)

*Authors: Luis Gutierrez/Ross Snyder*<br>
*Last updated: 01/2025*