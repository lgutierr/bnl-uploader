#### Monitoring and debugging the app online

The application can be monitored here for debugging purposes. This is only for administrators of the BNL uploader and admin can be added here by creating a role binding:

https://paas.cern.ch/k8s/ns/bnl-uploader/rbac.authorization.k8s.io~v1~RoleBinding

Instances of the BNL uploader are contained in Pods:

https://paas.cern.ch/k8s/ns/bnl-uploader/core~v1~Pod

- On each Pod, uploader activity can be monitored to debug any issues with uploads by going to the "Logs" tab. The app logger prints out information on this log using app.logger defined in config.py. Print statements from the logger are shown as app-INFO
- The Metrics tab is used to monitor memory usage in the app.
- The terminal can also be accessed on the Terminal tab. The terminal is located in the bnl-uploader directory, and one can ```cd``` out to the /tmp folder where files and the database are stored. If things ever need to be moved to a different folder than \tmp, this terminal can be used to verify it.

If you navigate to the /tmp folder, the database tables in app.db can be accessed using python and SQL code. Type python3 in the /tmp directory and use the Python shell:

``` 
$ python3
Python 3.9.10 (main, Mar  2 2022, 04:23:34) 
[GCC 10.2.1 20210110] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

These commands in the shell can be used to see the user table in app.db
```
import sqlite3

# Connect to the SQLite database
connection = sqlite3.connect('app.db')
cursor = connection.cursor()

# Display all rows from the 'user' table
cursor.execute("SELECT * FROM user;")
rows = cursor.fetchall()
for row in rows:
    print(row)

# Close the connection
connection.close()

```