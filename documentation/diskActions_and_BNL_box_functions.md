# Uploading to the BNL Box

Note: There is an existing README in the BNL box if it needs to be referenced written by Luis.

## diskActions.py functions

This script has functions that connect via ssh to the BNL box and uploads data to three different folders. See folder structure below for more details.

1. connect(): Connects to the BNL Box via ssh. 
2. renameFile(connection, fileName, location)
3. uploadFile(connection, filePath, destination)

## BNL uploader folder structure

1. uploaded
    All files used to upload to the database.
    Only two types of files should exist here:
      a. Depending on the test, we create/use a .json file used to talk and upload to the ITk database
      b. Depending on the test, we need to upload the raw data file
2. notUploaded
    All files created/provided to the uploader that are not directly used to upload data to the database.
    This includes but is not limited to:
      a. Plots/graphs.
      b. Anything uploaded to the BNL dropboxes
      c. Raw data files not uploaded to the database.
      d. Excel files for analysis
      e. etc.
3. toUpload (currently not being used by uploader)
4. registration (this might be a bug on the code, should be created in the uploaded folder)
5. assembly (this might be a bug in the uploader code, should be created in the uploaded folder)

*Authors: Luis Guiterrez/Ross Snyder*<br>
*Last updated: 09/2024*