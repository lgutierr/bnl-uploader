[General]
StaveID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/4/2024 5:14:06 PM, Temperature =  "
Notes_Ideal = "Type your note here...."
Date_AfterGluing = "3/29/2024 2:34:55 PM, Temperature =  "
Notes_AfterGluing = "Type your note here...."
Date_BeforeBridgeRemoval = "4/1/2024 10:36:11 AM, Temperature =  "
Notes_BeforeBridgeRemoval = "Type your note here...."
Date_AfterBridgeRemoval = "4/1/2024 11:06:57 AM, Temperature =  "
Notes_AfterBridgeRemoval = "Type your note here...."

[CornerA]
X_Ideal = -41.504908
Y_Ideal = -30.548214
Z_Ideal = 16.207438
X_AfterGluing = -41.503841
Y_AfterGluing = -30.541942
Z_AfterGluing = 16.207442
X_BeforeBridgeRemoval = -41.504166
Y_BeforeBridgeRemoval = -30.538375
Z_BeforeBridgeRemoval = 16.207442
X_AfterBridgeRemoval = -41.505401
Y_AfterBridgeRemoval = -30.542927
Z_AfterBridgeRemoval = 16.207442

[CornerB]
X_Ideal = 55.479542
Y_Ideal = -27.922010
Z_Ideal = 16.207438
X_AfterGluing = 55.482101
Y_AfterGluing = -27.903769
Z_AfterGluing = 16.207442
X_BeforeBridgeRemoval = 55.481928
Y_BeforeBridgeRemoval = -27.899800
Z_BeforeBridgeRemoval = 16.207442
X_AfterBridgeRemoval = 55.479215
Y_AfterBridgeRemoval = -27.905189
Z_AfterBridgeRemoval = 16.207442

[CornerC]
X_Ideal = 52.833834
Y_Ideal = 69.782676
Z_Ideal = 16.207438
X_AfterGluing = 52.824560
Y_AfterGluing = 69.804392
Z_AfterGluing = 16.207442
X_BeforeBridgeRemoval = 52.825008
Y_BeforeBridgeRemoval = 69.808381
Z_BeforeBridgeRemoval = 16.207442
X_AfterBridgeRemoval = 52.822063
Y_AfterBridgeRemoval = 69.799057
Z_AfterBridgeRemoval = 16.207442

[CornerD]
X_Ideal = -44.150615    
Y_Ideal = 67.156471    
Z_Ideal = 16.207438    
X_AfterGluing = -44.157675
Y_AfterGluing = 67.164906
Z_AfterGluing = 16.207342
X_BeforeBridgeRemoval = -44.157128
Y_BeforeBridgeRemoval = 67.169567
Z_BeforeBridgeRemoval = 16.207442
X_AfterBridgeRemoval = -44.158194
Y_AfterBridgeRemoval = 67.160180
Z_AfterBridgeRemoval = 16.207442