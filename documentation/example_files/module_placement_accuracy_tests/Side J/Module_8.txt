[General]
StaveID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "2/9/2024 11:49:46 AM, Temperature =  "
Notes_Ideal = ""
Date_Testing = "2/8/2024 1:43:39 PM, Temperature =  "
Notes_Testing = "Type your note here...."
Date_AfterGluing = "2/8/2024 3:12:21 PM, Temperature =  "
Notes_AfterGluing = "Type your note here...."
Date_BeforeBridgeRemoval = "2/9/2024 9:47:58 AM, Temperature =  "
Notes_BeforeBridgeRemoval = "Type your note here...."
Date_AfterBridgeRemoval = "2/9/2024 11:37:34 AM, Temperature =  "
Notes_AfterBridgeRemoval = "Type your note here...."

[CornerA]
X_Ideal = -251.589701
Y_Ideal = -29.326840
Z_Ideal = 16.041118
X_Testing = -999.000000
Y_Testing = -999.000000
Z_Testing = -999.000000
X_AfterGluing = -251.592022
Y_AfterGluing = -29.332208
Z_AfterGluing = 16.041113
X_BeforeBridgeRemoval = -251.596259
Y_BeforeBridgeRemoval = -29.330009
Z_BeforeBridgeRemoval = 16.041113
X_AfterBridgeRemoval = -251.594243
Y_AfterBridgeRemoval = -29.329578
Z_AfterBridgeRemoval = 16.041113

[CornerB]
X_Ideal = -154.604663
Y_Ideal = -26.722463
Z_Ideal = 16.041118
X_Testing = -999.000000
Y_Testing = -999.000000
Z_Testing = -999.000000
X_AfterGluing = -154.600070
Y_AfterGluing = -26.717932
Z_AfterGluing = 16.041113
X_BeforeBridgeRemoval = -154.603073
Y_BeforeBridgeRemoval = -26.716179
Z_BeforeBridgeRemoval = 16.041113
X_AfterBridgeRemoval = -154.602961
Y_AfterBridgeRemoval = -26.715081
Z_AfterBridgeRemoval = 16.041113

[CornerC]
X_Ideal = -157.228381
Y_Ideal = 70.982815
Z_Ideal = 16.041118
X_Testing = -999.000000
Y_Testing = -999.000000
Z_Testing = -999.000000
X_AfterGluing = -157.229475
Y_AfterGluing = 70.988337
Z_AfterGluing = 16.041113
X_BeforeBridgeRemoval = -157.232206
Y_BeforeBridgeRemoval = 70.990129
Z_BeforeBridgeRemoval = 16.041113
X_AfterBridgeRemoval = -157.232555
Y_AfterBridgeRemoval = 70.989192
Z_AfterBridgeRemoval = 16.041113

[CornerD]
X_Ideal = -254.213419    
Y_Ideal = 68.378438    
Z_Ideal = 16.041118    
X_Testing = -999.000000
Y_Testing = -999.000000
Z_Testing = -999.000000
X_AfterGluing = -254.220854
Y_AfterGluing = 68.374029
Z_AfterGluing = 16.041113
X_BeforeBridgeRemoval = -254.223371
Y_BeforeBridgeRemoval = 68.374975
Z_BeforeBridgeRemoval = 16.041113
X_AfterBridgeRemoval = -254.223735
Y_AfterBridgeRemoval = 68.374415
Z_AfterBridgeRemoval = 16.041113