[General]
StaveID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "2/2/2024 10:50:48 AM, Temperature =  "
Notes_Ideal = ""
Date_AfterGluing = "2/1/2024 3:58:39 PM, Temperature =  "
Notes_AfterGluing = "Type your note here...."
Date_AfterBridgeRemoval = "2/2/2024 10:37:20 AM, Temperature =  "
Notes_AfterBridgeRemoval = "Type your note here...."

[CornerA]
X_Ideal = -447.913364
Y_Ideal = -29.622647
Z_Ideal = 16.049677
X_AfterGluing = -447.933161
Y_AfterGluing = -29.620094
Z_AfterGluing = 16.049715
X_BeforeBridgeRemoval = -999.
Y_BeforeBridgeRemoval = -999.
Z_BeforeBridgeRemoval = -999.
X_AfterBridgeRemoval = -447.933863
Y_AfterBridgeRemoval = -29.613761
Z_AfterBridgeRemoval = 16.049715

[CornerB]
X_Ideal = -350.928207
Y_Ideal = -27.022698
Z_Ideal = 16.049677
X_AfterGluing = -350.942119
Y_AfterGluing = -27.026212
Z_AfterGluing = 16.049715
X_BeforeBridgeRemoval = -999.
Y_BeforeBridgeRemoval = -999.
Z_BeforeBridgeRemoval = -999.
X_AfterBridgeRemoval = -350.943313
Y_AfterBridgeRemoval = -27.021462
Z_AfterBridgeRemoval = 16.049715

[CornerC]
X_Ideal = -353.547464
Y_Ideal = 70.682700
Z_Ideal = 16.049677
X_AfterGluing = -353.550585
Y_AfterGluing = 70.682728
Z_AfterGluing = 16.049715
X_BeforeBridgeRemoval = -999.
Y_BeforeBridgeRemoval = -999.
Z_BeforeBridgeRemoval = -999.
X_AfterBridgeRemoval = -353.552495
Y_AfterBridgeRemoval = 70.685024
Z_AfterBridgeRemoval = 16.049715

[CornerD]
X_Ideal = -450.532621    
Y_Ideal = 68.082751    
Z_Ideal = 16.049677    
X_AfterGluing = -450.539714
Y_AfterGluing = 68.089864
Z_AfterGluing = 16.049715
X_BeforeBridgeRemoval = -999.
Y_BeforeBridgeRemoval = -999.
Z_BeforeBridgeRemoval = -999.
X_AfterBridgeRemoval = -450.540772
Y_AfterBridgeRemoval = 68.090922
Z_AfterBridgeRemoval = 16.049715