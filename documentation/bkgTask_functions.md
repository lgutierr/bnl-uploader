# Table of contents

This table of contents is based on the main page of the uploader.
1. [Get Status](#Status)
   - [bkgComponentStatus()](#bkgcomponentstatus)
2. [Component Tests](#component-tests)
   - [bkgPullTest()](#bkgpulltest)
   - [bkgDropbox()](#bkgdropbox)
   - [bkgVisualInspection()](#bkgvisualinspection)
# Get Status

## bkgComponentStatus()

**General description:** Gets component general information and its test status. 

**Inputs:** Identifier (serial number)

**Routes:** 
loadComponent.html -> loadComponentEndpoint.html

**Inputs:** Identifier(Serial number)

**Outputs:**

Lists information about the component.
1) Component type
2) Serial number
3) Alternative ID
4) Local Name
5) Component Location
6) Stage
7) Test statuses

# Component Tests

## bkgPullTest()

**General description:** 

Uploads pull tests for asics, halfmoons, cicorels, hybrid flex arrays, powerboard carriers.

1. Uploads testSchema (dto sample) to ITk database. Test Schema = "PULL_TEST"
2. Uploads files to ITk database
3. Uploads to BNL dropbox

**Routes:** 

1. asicsPullTest.html
2. cicorelPullTest.html
3. halfmoonPullTest.html
4. hybridFlexArraryPullTest.html
5. powerboardCarrierPullTest.html

All route to successEndpoint.html

**Inputs:** 

1. identifier: Serial number
- If asic: SN of component type ["ABC", "HCC", "AMAC"]
- If cicorel: SN of component type ["CICOREL_CARD"]
- If halfmoon: SN of component type ["SENSOR_HALFMOONS"]
- If hybrid flex array: SN of component type ["HYBRID_FLEX_ARRAY"]
- If powerboard: The number on the powerboard test coupon from reception (Alternative ID) or the SN of ["PWB_CARRIER"]

2. compType = component type ["asic","cicorel","halfmoon","hybridflexarray","powerboard"]

3. files = .csv file with any name (see example file under Pull_Tests)

**Outputs:**

1. Database files uploaded:
- .csv file (e.g. CompSN_compType_Timestamp_PullTest.csv)
- .json file (e.g. CompSN_compType_Timestamp_PullTest.json)
2. BNL Box:
- Files that were uploaded to database in "uploaded/asic/pullTest"

## bkgDropbox()

**General description:** 

Uploads files to the BNL box with no error or name checks. Does not interact with the database. This is used for several different components:

1. ASIC
2. Cicorel
3. Halfmoon
4. hv tabs
5. sensors
6. powerboards
7. hybrix flex arrays
8. hybrids 
9. modules

**Routes:** 

1. asicsDropbox.html
2. cicorelDropbox.html
3. halfmoonDropbox.html
4. hvtabDropbox.html
5. sensorDropbox.html
6. powerboardDropbox.html
7. hybridFlexArrayDropbox.html
8. hybridDropbox.html
9. moduleDropbox.html

All route to successEndpoint.html

**Inputs:** 

To be determined which files are put into the dropbox.
1. ASICS -- 
2. Cicorel --
3. Halfmoon --
4. hvTabs -- 
5. Sensors --
6. Powerboards --
7. Hybrid Flex Arrays --
8. Hybrids --
9. Modules -- 

**Outputs:**

Goes to BNL box, files are accessed in: /usatlas/atlas_itk_sw/notUploaded/*component_name*/dropbox 

## bkgVisualInspection()

**General description:**  
Uploads visual inspection tests for HV tabs, modules, powerboards, and hybrids

1. Uploads json file with testSchema (dto sample) to ITk database. Test Schema =  "VISUAL_INSPECTION", except in a special case for reception modules, where  "VISUAL_INSPECTION_RECEPTION"
2. Uploads json file with testSchema to ITk database
3. Uploads to BNL dropbox

**Routes:** 

1. hvtabVisualInspection.html
2. moduleVisualInspection.html
3. powerboardVisualInspection.html
4. hybridVisualInspection.html

All route to successEndpoint.html

**Inputs:** 

For all components:
1. identifier
2. Pass/fail

For modules:
1. Assembled on-site/Reception

**Outputs:**

1. json file with test schema (under test results in the database)
2. In BNL box, files are accessed in: /usatlas/atlas_itk_sw/uploaded/*component_name*/visualInspection

*Authors: Ross Snyder/Luis Guiterrez*<br>
*Last updated: 09/2024*


