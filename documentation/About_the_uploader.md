# About the BNL uploader

This is a general guide to how the BNL uploader works.

- [Introduction](##introduction)
- [Routing](##Routing)
- [About handleBackgroundTask](##AboutBKGTask)
- [About bkgTasks](##about_bkgTasks)

## Layout

The BNL uploader is a Flask web application that is hosted on a Kubernetes Pod on the CERN servers:

https://paas.cern.ch/k8s/ns/bnl-uploader/core~v1~Pod

![Roadmap](images/BNL_Uploader_roadmap.png)

The Dockerfile has instructions to make the bnl-uploader directory with all of the relevant files run on the Pod. It creates the bnl-uploader directory and the shell script that runs the flask web app. 

The flask web app creates a SQLAlchemy database to store user information. The app also pulls in parameters needed for the application from the Secrets tab. The env. file given to developers (for local testing/development) contain these same secrets. The __init__.py file pulls info from Secrets, config.py to initialize the applications. 

When users login to the uploader, their information is immediately encrypted. Any database entry should be encrypted to prevent malicious outside parties from obtaining the information. This information is needed to use the *itkpd* module to create an itkClient object that uploads or fetches data from the database, so one way decryption from the database is done each time a new ITkClient is created. 

The various routes of the website connect html templates to each other, and a general function called handleBackgroundTask in routes calls individual functions that upload data to the ITk production database and the BNL box. 

The two differences between the developer version tested locally and the online version in the Pod are that:

1) Environment variables that should not be on Gitlab: The Pod uses info from the Secrets tab, developers use a local .env file
2) In setup.sh, the developer version uses flask run. Switch back and forth when developing. Do not push these changes to Gitlab as the online version uses the gunicorn module.

## Introduction

Below is how the BNL uploader uploads data to the database and BNL box:

1. Button routes to the webpage that contains the form where users enter needed inputs to the bkgTask.
2. Form contains all inputs needed for a background task.
3. html templates display the forms and buttons.
3. If the form is submitted, the background task that uploads data to the database/box starts. The background task fills entries in a dictionary and returns an output.
4. If there is an error in the background task, it brings up an error page that has a log of the traceback.
5. If an error is anticipated, like entering a SN that doesn't exist, the background task appends an error as part of its output. Both the error from the database and the error output from the error dictionary is shown.
6. If there isn't an error, it routes to an "endpoint". A common endpoint is the "successEndpoint". If all goes well, data was uploaded successfully.

![Introduction](images/BNLUploader_flowchart.png)
## Routing, forms

In order to add a button, you need to add code to these files:
1. The button form class to be called (forms.py)
1. Where page the button is on and where it needs to route (routes.py)
3. The html code that displays the webpage (webpage template)

### forms.py

An example is 
```
class ConfocalScanButton(FlaskForm):
  submitConfocalScanButton = SubmitField('Confocal Scan')
```
This makes a class (object) called submitConfocalScanButton. It is a submit field (there are many types of Flask form fields), and its title is 'Confocal Scan'.

### routes.py

The *clickButtonGoToURL* function basically states, "If the button is clicked, go to the next page. If it is not, return None. This function is used repeatedly for each button. 

function args:
1. button - name of button instance of class, which ends in ()
2. form - buttom form class
3. endpoint - the route you want the button to redirect to if it is clicked.

```
def clickButtonGoToURL(button, form, endpoint):
	if button.data and form.validate():
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for(endpoint)
		return redirect(next_page)
	return None
```
As an example, let's say I would like this button under the /staveTests route. The route is able to GET (retrieve data) or POST (send data). The := operator assigns the variable and does the if statement at the same time. It basically says, 

```
clickThenGoToNextPage = clickButtonGoToURL(button, form, endpoint)
if clickThenGoToNextPage is not None:
    return clickThenGoToNextPage
```
clickThenGoToNextPage then redirects to the next page.

The code below adds an instance of the button and form on the route, and then specifies where it goes if it is clicked. In this example, the ConfocalScanButton is initiated as an instance, and then it redirects to route /confocalScan if clicked:

```
@app.route('/staveTests', methods=['GET', 'POST'])
@login_required
def staveTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(ModulePlacementAccuracyButton().submitModulePlacementAccuracyButton, ModulePlacementAccuracyButton(), 'modulePlacementAccuracy_Side_J')):
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ConfocalScanButton().submitConfocalScanButton, ConfocalScanButton(), 'confocalScan')):
		return clickThenGoToNextPage
	return render_template('staveTests.html', title='Stave Tests', modulePlacementAccuracyButton=ModulePlacementAccuracyButton())
```
## About handleBackgroundTask

This function goes from a webpage where one uploads data to an endpoint while handling the function that uploads or plots data. 

Steps are the following after a form is submitted with "validate on submit":

- A task is created for the current user that specifies the user, beginning webpage, and ending webpage.
- It gathers parameters from the form automatically. It ignores submit fields and csrf tokens that are hidden in the form. 
- Any custom parameters are specified in the function and appended to the list of parameters in that order
- Hidden Flask fields can be used to put variables in a loop
- The function with parameters is sent to the executor to submit the task.
- The page goes to the /loading route. While the bacgkground task is executing, loading will check every 5 seconds to see if it is finished. After it is finished, the output dictionary from the background task goes to the \checkOutputErrors route.

The function arguments are shown below:

```
Gathers parameters from form inputs and appends custom parameters. Stores a "future" which runs in the background while it goes to the loading page.

args: 
1) formInstance = Instance of form class
2) bkgFunction = background function that uploads the data
3) bkgFunctionCustomParams = custom parameters not on the form that are specific to the function
4) webpage =  renders the webpage template/form
5) title = Title of webpage (seen on the tab of the website)
6) endPoint = which route it goes to after loading
7) functionType = Specifies whether files are being uploaded or not
returns:
1) If form is not submitted yet, render the webpage specified.
2) If form is submitted, go to loading.
```

## About bkgTasks

For more robust error handlings and uploads, each background task should use a common "checklist" that uses common uploading functions and dictionaries. Each background task is envisioned to do the following:

- Generate an output dictionary with common fields.
- Run through all possible error checks before upload (`errorChecks.py`). These error checks check correct file type, file content, component type, etc. If there is an error, the output is immediately returned with the common error.
- Parse through all files with helper functions under `bkgTaskMethods`.
- After running through the error checks and gathering necessary info, all relevant info is put into the uploading test schema. The test schema is filled and then put into the output.
- The test schema is uploaded to the ITk ATLAS database as a .json file using `dbActions.py`
- Files are also backed up in the BNL box using the spar gateway. Some are uploaded to the database but also backed up (uploaded folder), some are too large for the database like high resolution pictures (notUploaded folder). This is accomplished using `diskActions.py`.
- The output is returned and goes to the `/checkOutputErrors` route

## About checkErrors

The `/checkErrors` route does the following:

1) If there are no errors in the output, simply route to the next page. Most of the time, it should go to successEndpoint.html.
2) If there are known errors, route to the common error page. If nothing was uploaded, the common error from the error dictionary is displayed. If there was communication with the database, the database error will also be displayed. 
3) If the error is from programming, an error log used to be displayed, but this is no longer necessary because the developer can look at the logs on CERN OpenPaas. It currently says to contact the maintainer of the uploader.


*Authors: Ross Snyder*<br>
*Last updated: 03/2025*









































