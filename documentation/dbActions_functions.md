# Functions that communicate with the ITk PD

There is a list of functions that can be used to fetch and upload data to the database.They are shown here: [Database Commands](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=11425791)

## How they work in the uploader

The dbActions.py file contains a list of functions that create an object called itkClient that uses the username as input. 

*Authors: Ross Snyder*<br>
*Last updated: 09/2024*