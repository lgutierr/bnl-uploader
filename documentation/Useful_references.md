# Useful links

#### Debugging issues in the server/docker

One can monitor the BNL uploader running on the docker using this website. Memory and CPU usage is displayed. The logs display activities on the pod and one can diagnose them here.

https://paas.cern.ch/k8s/ns/bnl-uploader/core~v1~Pod

More about this can be found here:

https://docs.okd.io/latest/welcome/index.html

#### List of functions that can be used to fetch and upload data to the database.They are shown here: 

https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=11425791

#### General information on using the ITk production database. 

Especially using for examples on how to register components.

https://twiki.cern.ch/twiki/bin/viewauth/Atlas/StripsDatabase

#### ITk Production Database documentation

https://itk.docs.cern.ch/

#### Reference ITk production database scripts

https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts

#### Info about the python itkdb package

This contains examples on how to use the itkClient for user information.

https://itkdb.docs.cern.ch/v0.6/examples/

*Authors: Ross Snyder*<br>
*Last updated: 10/2024*