### Redefining user tables in the user database

The User class in models.py is used to store needed information to create an ITkClient that communicates with the database. 

The user tables are encrypted using a Fernet key that was generated outside the app:

```
from cryptography.fernet import Fernet
key = Fernet.generate_key()
```

Whenever any field in the Users table is updated, one needs to migrate the update:

flask db migrate -m "Your message here" 
flask db upgrade

