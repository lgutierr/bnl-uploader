from flask_login import current_user
from app import db
from app.models import User
from app.classes import Component
import app.dbActions as dbActions
import app.diskActions as diskActions
import time
import os
import io
import json
import statistics
from datetime import datetime
import tarfile
import re
from flask import send_file, url_for
from pandas.core.series import Series
from collections import OrderedDict
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import uproot
# For sensor IV crap
import pandas as pd
import ast
import csv
import numpy as np
import sys
import glob
import openpyxl
import mplhep as hep 
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import base64
import shutil
import inspect
import logging
from app.bkgTaskMethods import errorChecks
from app.bkgTaskMethods import commonFunctions
import mpld3

plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
# from apscheduler.schedulers.background import BackgroundScheduler
# from apscheduler.triggers.cron import CronTrigger
import pytz

#####################
# General functions #
#####################

from app.bkgTaskMethods import bkgTestUploaderMethods
def bkgTestUploader():
	''' Test functionality of uploader by going through functions'''
	functionDict = dict()

	#bkgPullTest
	functionDict = bkgTestUploaderMethods.testUploader_bkgPullTest_ASIC(functionDict) 
	functionDict = bkgTestUploaderMethods.testUploader_bkgPullTest_HybridFlexArray(functionDict) 
	#bkgVisualInspection
	functionDict = bkgTestUploaderMethods.testUploader_bkgVisualInspection_HVTab(functionDict) 
	functionDict = bkgTestUploaderMethods.testUploader_bkgVisualInspection_Module(functionDict) 
	functionDict = bkgTestUploaderMethods.testUploader_bkgVisualInspection_Powerboard(functionDict) 
	functionDict = bkgTestUploaderMethods.testUploader_bkgVisualInspection_Hybrid(functionDict) 
	return functionDict

''' Takes in a mathplotlib figure turns it into a base 64 string'''
def figureTo64(fig):
	pic_IObytes = io.BytesIO()
	fig.savefig(pic_IObytes, format='png')  # Save the figure using fig.savefig
	pic_IObytes.seek(0)
	pic_hash = base64.b64encode(pic_IObytes.read()).decode('ascii')
	return pic_hash

def bkgSendFile(file):
	# Output
	output = dict()
	output['errors'] = []
	output['sentFiles'] = []
	# Send files
	try:
		send_file(file, as_attachment=True)
		output['sentFiles'].append(file)
	except:
		output['errors'].append("Could not send files")
	return output

def readyForNextStage(identifier, desiredStage):
	''' Check if component with given identifier is ready to be moved to the next stage. '''
	return True, [] # Remove once approval is granted
	# Create component
	comp = Component(identifier, False)
	# Check if component is ready for next stage
	ready, missingTests = comp.ready_nextStage(desiredStage)
	return ready, missingTests

def renameFiles(files, serialNumbers, ext=""):
	newFileNames = []
	extStr = ""
	if ext != "":
		extStr = "_"+ext
	for i in range(len(files)):
		fParts = files[i].split("/")
		newName = ""
		if type(serialNumbers) == str:
			newName = str(serialNumbers)+extStr+"_"+fParts[-1]
		else:
			newName = str(serialNumbers[i])+extStr+"_"+fParts[-1]
		newPath = "/".join(fParts[:-1])+"/"+newName
		newFileNames.append(newPath)
		os.system("mv "+str(files[i])+" "+newPath)
	return newFileNames 

########################
# Get component status #
########################

def bkgComponentStatus(identifier):
	''' Get status of component requested by user. '''
	# Get component
	comp = Component(identifier, True)
	return comp.status()

#######################
# Pull test functions #
#######################

from app.bkgTaskMethods import bkgPullTestMethods
from app.bkgTaskMethods import bkgPullTestSchema
def bkgPullTest(identifier, compType, files):
	#Get common dictionary
	output = commonFunctions.commonOutputDict()
	#Get ITkClient for upload
	itkClient = dbActions.getITkClient(current_user)
	#Run through error checklist
	output = errorChecks.checkCompStatus(identifier, output, itkClient)
	if output['errors']!=[]: return output

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes("bkgPullTest")
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	acceptedTypes = commonFunctions.getAcceptedCompTypes("bkgPullTest", compType)
	output = errorChecks.checkCompType(identifier, acceptedTypes, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompLocation(identifier, output, itkClient)
	if output['errors']!=[]: return output

	#Get component info
	comp = dbActions.getCompStatus(identifier, itkClient)
	comp = comp[0]
	serialNumber = comp['serialNumber']
	stage = bkgPullTestMethods.defineStage(compType)
	currentStage = comp['currentStage']['code']
	compCode = comp['componentType']['code']

	#If powerboard, redefine and get identifier of powerboard test coupon (need to upload on that instead)
	if compCode == "PWB_CARRIER":
		identifier, comp, serialNumber, currentStage, compCode  = bkgPullTestMethods.getPowerboardInfo(serialNumber, output, itkClient)

	#Get input files
	inputFiles = bkgPullTestMethods.getInputFiles(files)
	# Loop through all the files and upload test schema/files
	output['inputFiles'].append(files)
	for file in inputFiles:
		#Fill out test schema
		if output['errors']!=[]: return output

		testData = bkgPullTestMethods.pullTestFileAnalysis(file)

		output = errorChecks.checkTestSchema('PULL_TEST', compCode, output, itkClient)
		if output['errors']!=[]: return output

		[testSchema, exception] = dbActions.getTestSchema('PULL_TEST', compCode, itkClient)
		testSchema = bkgPullTestSchema.fillTestSchema(comp, stage, testSchema, testData, itkClient)

		# Rename file
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgPullTest")
		renamedFilePath = f'/tmp/{fileName}.csv'
		shutil.copy(file, renamedFilePath)

		# Create json file
		jsonFilePath = f'/tmp/{fileName}.json'
		commonFunctions.createJsonFile(testSchema, jsonFilePath)

		# Check and set component in proper stage
		stage = bkgPullTestMethods.defineStage(compType)
		stageList = [stage]
		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output

		# Upload schema and file
		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema, itkClient)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		[uploadFileOut, exception] = dbActions.uploadFile(renamedFilePath, fileName, uploadTestRun, itkClient)
		output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
		if output['errors']!=[]: return output

		#Fill out output inside of loop for each file 
		output['createdFiles'].append(jsonFilePath)
		output['uploadedFiles'].append(jsonFilePath)
		output['uploadedFiles'].append(renamedFilePath)
		output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))
	
	#Finish filling output outside of loop
	output['identifier'] = identifier
	output['serialNumber'] = serialNumber
	output['compCode'] = compCode
	output['institution'] = commonFunctions.getInstitute()

	#Save files in BNL disk
	folderPath = bkgPullTestMethods.getBNLdiskFolder(compType)
	#Temporary, don't upload powerboard pull tests until the folder exists in the box
	# if compType.lower() != "powerboard":
		#diskActions.uploadToDisk(output, folderPath)
	return output

#####################
# Dropbox functions #
#####################

def bkgDropbox(location, files):
	
	output = commonFunctions.commonOutputDict()
	inputFiles = [f for f in files.split(";") if f!= '']
	output['inputFiles'] = inputFiles

	diskConnection = diskActions.connect()
	# for f in inputFiles:
	# 	try:
	# 		diskActions.uploadFile(diskConnection, f, os.path.join("notUploaded", location))
	# 		output['uploadedFiles'].append(f)
	# 	except:
	# 		output['errors'].append('Could not upload '+str(f))
	return output

########################
# Shear Test Functions #
########################

from app.bkgTaskMethods import bkgHVtabShearTest_TestSchema
def bkgHVtabShearTest(identifier, strengths, operator, reason, substrate):
	#Get output dictionary
	output = commonFunctions.commonOutputDict()

	itkClient = dbActions.getITkClient(current_user)
	#Error checks for component
	output = errorChecks.checkCompStatus(identifier, output, itkClient)
	if output['errors']!=[]: return output

	[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgHVtabShearTest")

	acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgHVtabShearTest")
	output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompLocation(identifier, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
	if output['errors']!=[]: return output
	
	output = errorChecks.checkTestSchema('SHEAR_TEST', compCode, output, itkClient)
	if output['errors']!=[]: return output

	#Upload test schema for every strength entered
	listStrengths = [float(x.replace(" ","")) for x in strengths.split(",")]
	for i in range(len(listStrengths)):
		strength = listStrengths[i]
		[testSchema, exception] = dbActions.getTestSchema('SHEAR_TEST', acceptedCompTypes[0])
		testSchema = bkgHVtabShearTest_TestSchema.fillTestSchema(comp, serialNumber,  strength, operator, reason, substrate, testSchema)
		
		# Rename file
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgHVtabShearTest")

		# Create json file
		jsonFilePath = f'/tmp/{fileName}.json'
		commonFunctions.createJsonFile(testSchema, jsonFilePath)

		# Upload schema
		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		#Fill out output inside of loop
		output['createdFiles'].append(jsonFilePath)
		output['uploadedFiles'].append(jsonFilePath)

	#Finish filling output outside of loop
	output['identifier'] = identifier
	output['serialNumber'] = serialNumber
	output['compCode'] = compCode
	output['institution'] = commonFunctions.getInstitute()
	output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	#Save files in BNL disk
	folderPath = commonFunctions.getBNLBoxFolderPath('bkgHVtabShearTest')
	#diskActions.uploadToDisk(output, folderPath)

	return output

#######################
# Sensor IV functions #
#######################

from app.bkgTaskMethods import bkgSensorIV_Methods
from app.bkgTaskMethods import bkgSensorIV_TestSchema
def bkgSensorIV(files):
	output = commonFunctions.commonOutputDict()
	itkClient = dbActions.getITkClient(current_user)
	#Check name, format, and type of files
	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgSensorIV')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	acceptedFileString = commonFunctions.getAcceptedFileFormatString('bkgSensorIV')
	output = errorChecks.checkFileContent(files, output, acceptedFileString)
	if output['errors']!=[]: return output

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgSensorIV')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	files = [x for x in files.split(';') if x!='']

	for file in files:
		identifier, df_header, df_data = bkgSensorIV_Methods.getFileData(file)

		#Error checks for component
		output = errorChecks.checkCompStatus(identifier, output, itkClient)
		if output['errors']!=[]: return output

		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgSensorIV")

		acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgSensorIV")
		output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkCompLocation(identifier, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output
		
		output = errorChecks.checkTestSchema("ATLAS18_IV_TEST_V1", compCode, output, itkClient)
		if output['errors']!=[]: return output

		[testSchema, exception] = dbActions.getTestSchema("ATLAS18_IV_TEST_V1", acceptedCompTypes[0])
		testSchema = bkgSensorIV_TestSchema.fillTestSchema(serialNumber, comp, currentStage, df_header, df_data, testSchema)
		
		# Rename file
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgSensorIV")

		# Create json file
		jsonFilePath = f'/tmp/{fileName}.json'
		commonFunctions.createJsonFile(testSchema, jsonFilePath)

		#Fill out output inside of loop for each file
		output['identifier'].append(identifier)
		output['serialNumber'].append(identifier)
		output['compCode'].append(compCode)
		output['inputFiles'].append(file)
		output['createdFiles'].append(jsonFilePath)
		output['testSchema'].append(testSchema)

	#Finish filling output outside of loop
	output['institution'] = commonFunctions.getInstitute()

	return output

def bkgIVGraph(output):
	#For now, this only uploads the first file
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	tbg=[]
	for file in output['createdFiles']:
		f = open(file)
		data = json.load(f)
		output['testSchema']=data
		#gets needed information from dictonary for graph
		time = data['date']
		if data['institution']: loc = data["institution"]
		else: loc = commonFunctions.getInstitute()
		current = data['results']['CURRENT']
		voltage = data['results']['VOLTAGE']
		#prevents error due to different sized arrays
		min_length = min(len(current), len(voltage))
		current=current[:min_length]
		voltage=voltage[:min_length]
		#creates dictonary for each inputed files that has needed information
		dictonary= {'current':current,'voltage':voltage, 'time':time, 'loc':loc}
		tbg.append(dictonary)
		f.close()
	#Graphs all IV curves
	plt.style.use(hep.style.ATLAS)
	for test in tbg:
		plt.plot(test.get('voltage'),test.get('current'), label = (file) )
		plt.title(str(file), fontsize = 15)
	ax.set_ylabel("Current [nA]")
	ax.set_xlabel("Voltage [V]")
	ax.set_xlim([-750,50])
	ax.set_xticks([0,-100,-200,-300,-400,-500,-600,-700])
	filePath=figureTo64(fig)
	output['fig']=filePath
	return output

def bkgUploadSensorIV(PF, action, output):
	#Can currently only upload one file
	if (PF=='Pass'):
		output['testSchema']['passed']=True
	else:
		output['testSchema']['passed']=False
	if (action=='Upload'):
		testSchema = output['testSchema']
		identifier = testSchema['component']
		file = output['inputFiles'][0]
		jsonFilePath=output['createdFiles'][0]
		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgSensorIV")
		
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgSensorIV")
		renamedFilePath = f'/tmp/{fileName}.dat'
		shutil.copy(file, renamedFilePath)

		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		[uploadFileOut, exception] = dbActions.uploadFile(renamedFilePath, fileName, uploadTestRun)
		output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
		if output['errors']!=[]: return output

		output['uploadedFiles'].append(jsonFilePath)
		output['uploadedFiles'].append(renamedFilePath)
		output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))
		
		folderPath = commonFunctions.getBNLBoxFolderPath("bkgSensorIV")
		#diskActions.uploadToDisk(output, folderPath)
	return output

######################################
# Sensor visual inspection functions #
######################################

from app.bkgTaskMethods import bkgSensorVisualInspection_TestSchema
from app.bkgTaskMethods import bkgSensorVisualInspectionReception_Methods
def bkgSensorVisualInspectionReception(files):
	#Get common dictionary
	output = commonFunctions.commonOutputDict()
	itkClient = dbActions.getITkClient(current_user)
	#Run through error checklist
	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes("bkgSensorVisualInspectionReception")
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output
	
	#Get input files
	inputFiles = bkgSensorVisualInspectionReception_Methods.getInputFiles(files)
	
	# Loop through all the files and upload test schema/files
	for file in inputFiles:
		data = bkgSensorVisualInspectionReception_Methods.readSensorVisualInspectionReceptionFile(file)
		for sensorData in data:
			# Get component serial number
			identifier = sensorData['SN']
			#Run through error checklist
			output = errorChecks.checkCompStatus(identifier, output, itkClient)
			if output['errors']!=[]: return output

			acceptedFileTypeList = commonFunctions.getAcceptedFileTypes("bkgSensorVisualInspectionReception")
			output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
			if output['errors']!=[]: return output

			acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgSensorVisualInspectionReception")
			output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
			if output['errors']!=[]: return output

			[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgSensorVisualInspectionReception")

			#Check user institution
			output = errorChecks.checkCompLocation(identifier, output, itkClient)
			if output['errors']!=[]: return output

			#Check Test Schema
			output = errorChecks.checkTestSchema("VIS_INSP_RES_MOD_V2", compCode, output, itkClient)
			if output['errors']!=[]: return output
			[testSchema, exception]= dbActions.getTestSchema("VIS_INSP_RES_MOD_V2", compCode)
			testSchema = bkgSensorVisualInspection_TestSchema.fillTestSchema(serialNumber, sensorData, testSchema)

			# Rename file
			fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgSensorVisualInspectionReception")

			# Create json file
			jsonFilePath = f'/tmp/{fileName}.json'
			commonFunctions.createJsonFile(testSchema, jsonFilePath)

			# Check and set component in proper stage
			output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
			if output['errors']!=[]: return output

			# Upload schema - if it fails, then return error
			[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
			output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
			if output['errors']!=[]: return output

			#Fill out output inside of loop for each file
			output['createdFiles'].append(jsonFilePath)
			output['uploadedFiles'].append(jsonFilePath)
			output['identifier'].append(identifier)
			output['inputFiles'].append(inputFiles)
			output['serialNumber'].append(serialNumber)
			output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))
			output['compCode'].append(compCode)

			#Save file in BNL disk
			# Creates error that needs to be looked into. Don't have time to look into it right now.
			# Line is most likely fileName = filePath.split("/")[-1] in #diskActions.py.
			# It trys to split the last part of the file. 
			# folderPath = commonFunctions.getBNLBoxFolderPath("bkgSensorVisualInspectionReception")
			# #diskActions.uploadToDisk(output, folderPath)

		#Finish filling output outside of loop
		output['institution'] = commonFunctions.getInstitute()

	return output

########################################
# Powerboard Electrical Test Functions #
########################################

def readPWBelectricalTestFile(fname):
	d = open(fname).read()
	data = json.loads(d)
	# Validate inputs
	if "config" not in data:
		print("Missing the AMACv2 configuration used during testing")
		return None
	if "component" not in data["config"]:
		print("Missing component name (via config)")
		return None
	if "runNumber" not in data:
		print("Missing runNumber")
		return None
	if "tests" not in data:
		print("Missing test results")
		return None
	# Update institution in the config
	data['config']['institution']="BNL"
	# Test results
	for test in data.get("tests",[]):
		test["runNumber"]= data["runNumber"]
		test["institution"]= data['config']['institution']
		test["component"]= data["config"]["component"]
		if 'properties' not in test: test["properties"]={}
		if 'runNumber' in data['config']:
			test["properties"]["CONFIG"]=data["config"]["runNumber"]
	return data  

def parsePWBerial(snum):
	version = int(snum[7])
	bnum = int(snum[8:10])
	pnum = int(snum[10:14])
	if version == 0:
		subType = "B2"
	elif version in [1,2,3]:
		subType = "B3"
	return version, bnum, pnum, subType

def bkgPowerboardCarrierVisualInspectionFetchChildren(identifier):
	isCarrier = dbActions.checkComponentType(identifier, 'PWB_CARRIER')
	fetched = ['']*10
	if isCarrier:
		children = dbActions.getChildren(identifier)
		for c in children:
			if (c['componentType']['code'] == 'PWB') and (c['component'] != None):
				fetched[c['order']-1] = c['component']['serialNumber']
	return fetched

def bkgPowerboardElectricalTests(tarFilesPaths):
	# Output
	output=dict()
	output['identifier'] = []
	output['inputFiles'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	# Variables based on inputs
	allFiles = [x for x in tarFilesPaths.split(';') if x!='']
	# Clear user files
	# user = User.query.get(current_user.id)
	# user.submittedFiles = ""
	# db.session.commit()
	# Loop through all tar files
	for tarFile in allFiles:
		# Untar file and get powerboards folders and serial numbers
		os.system("tar -xvf "+tarFile+" -C /tmp")
		panel = tarFile.split("/")[-1].split(".")[0]
		mainLocation = None
		location = None
		testDatesFolders = []
		try:
			mainLocation = "/".join(tarFile.split("/")[:-1])+"/Desktop/"
			location = mainLocation+panel
			testDatesFolders = os.listdir(location)
		except:
			mainLocation = "/".join(tarFile.split("/")[:-1])+"/"+panel
			location = mainLocation
			testDatesFolders = os.listdir(location)
		largestFolder = ""
		largestFolderSize = 0
		for testDate in testDatesFolders:
			tmpFolders = os.listdir(os.path.join(location,testDate))
			if len(tmpFolders) > largestFolderSize: # Earliest one will show up first
				largestFolderSize = len(tmpFolders)
				largestFolder = testDate
		pbFolders = os.listdir(os.path.join(location,largestFolder))
		pbSNs = [x.lstrip("pb") for x in pbFolders]
		output['identifier'] += pbSNs
		# Move powerboards files
		for i in range(len(pbFolders)):
			oldLocation = os.path.join(location,largestFolder+"/"+pbFolders[i])
			files = os.listdir(oldLocation)
			relevantFileParts = [x.split("_")[-1] for x in files]
			for j in range(len(files)):
				newFilePath = "/tmp/"+pbSNs[i]+"_"+relevantFileParts[j]
				os.system("mv "+oldLocation+"/"+files[j]+" "+newFilePath)
				output['inputFiles'].append(newFilePath)
		# Delete tar folder and untared folder
		os.system("rm "+tarFile)
		os.system("rm -r "+mainLocation)
		# Loop through files
		for fileToProcess in output['inputFiles']:
			# Extract relevant info
			f = readPWBelectricalTestFile(fileToProcess)
			# Get component
			comp = None
			serialNumber = fileToProcess.split("/")[-1].split("_")[0]
			compCode = None
			currentStage = None
			try: 
				comp = dbActions.getCompStatus(serialNumber)
				comp = comp[0]
				compCode=comp['code'] # needed for test runs list
				currentStage = comp['currentStage']['code']
			except:
				# deleteFiles(output)
				output['errors'].append('Component does not exist')
				return output
			if comp == None:
				# deleteFiles(output)
				output['errors'].append('Component does not exist')
				return output
			# Check component is of the correct type
			acceptedTypes = ["PWB"]
			correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
			if correctCompType == False:
				# deleteFiles(output)
				output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
				return output
			# Check component is in the correct location
			acceptedLocation = 'BNL'
			correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
			if not correctCompLocation:
				# deleteFiles(output)
				output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
				return output
			# Set component in proper stage
			stage="MODULE_RCP"
			if currentStage != stage:
				ready, missingTests = readyForNextStage(serialNumber, stage)
				if ready:
					[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
					if not setStageOut:
						# deleteFiles(output)
						output['errors'].append('Component not set to the proper stage.')
						return output
				else:
					# deleteFiles(output)
					output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
					return output
			# Check if the config exists, if not upload
			cfgNumber=None # Configuration runNumber
			uploadConfig = False
			if 'runNumber' in f['config']:
				# Final identifier for config (rn0-rn1)
				rn0=None # major, set to None if a match is found
				rn1=None # minor, set to None if a match is found
				maxrn0=0 # Known major version
				maxrn1=0 # Known minor version for found major version
				# Get latest runNumber
				for test in comp['tests']:
					if test['code'] == 'CONFIG':
						for testRun in test['testRuns']:
							majorTestRunNumber = -1
							minorTestRunNumber = -1
							try:
								majorTestRunNumber = int(testRun['runNumber'].split("-")[0])
								minorTestRunNumber = int(testRun['runNumber'].split("-")[1])
							except:
								continue
							if maxrn0 < majorTestRunNumber:
								maxrn0 = majorTestRunNumber
								maxrn1 = minorTestRunNumber
							elif maxrn0 == majorTestRunNumber and maxrn1 < minorTestRunNumber:
								maxrn1 = minorTestRunNumber
				# Determine runNumber of config
				if rn1==None:
					if rn0==None: # completely new
						rn0=maxrn0+1
						rn1=0
					else:
						rn1=maxrn1+1
					cfgNumber='{}-{}'.format(rn0,rn1)
					f['config']['runNumber']=cfgNumber
					f['config']['passed']=True
					uploadConfig = True
				else: #Found the entry!
					cfgNumber='{}-{}'.format(rn0,rn1)
			# Update cfgNumber in each schema
			for testSchema in f.get('tests',[]):
				if cfgNumber is not None:
					testSchema['properties']['CONFIG']=cfgNumber
			# Create a json file for the config file
			if uploadConfig == True:
				configJsonFilePath = os.path.join("/tmp/", serialNumber+"_CONFIG.json")
				commonFunctions.createJsonFile(f['config'], configJsonFilePath)
				output['createdFiles'].append(configJsonFilePath)
			# Create a json file for each test schema
			for testSchema in f.get('tests',[]):
				jsonFilePath = os.path.join("/tmp/", serialNumber+"_"+testSchema['testType']+".json")
				commonFunctions.createJsonFile(testSchema, jsonFilePath)
				output['createdFiles'].append(jsonFilePath)
			# Upload config file
			if uploadConfig == True:
				[uploadConfigOut, uploadConfigTestRun, exception] = dbActions.uploadSchema(f['config'])
				if not uploadConfigOut:
					# deleteFiles(output)
					output['errors'].append('Config file not uploaded')
					return output
			# Upload schemas
			for testSchema in f.get('tests',[]):
				[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
				if not uploadSchemaOut:
					# deleteFiles(output)
					output['errors'].append('Data not uploaded')
					return output
				else:
					jsonFilePath = os.path.join("/tmp/", serialNumber+"_"+testSchema['testType']+".json")
					output['uploadedFiles'].append(jsonFilePath)
	#Save files in BNL disk
	#diskActions.uploadToDisk(output, "powerboard/electricalTest")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output


################################
# Hybrid Glue Weight functions #
################################

def bkgHybridGlueWeight(identifier, hybridWeight, hybridAndGlueWeight, totalWeight):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	# General information
	testType = "ASIC_GLUE_WEIGHT"
	stage = "ASIC_ATTACHMENT"
	# Relevant information
	glueWeight = round(float(hybridAndGlueWeight) - float(hybridWeight), 5)
	asicsWeight = round(float(totalWeight) - float(hybridAndGlueWeight), 5)
	# Get component info
	comp = None
	serialNumber = None
	currentStage = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		currentStage = comp['currentStage']['code']
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["HYBRID_ASSEMBLY"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Get test schema
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedTypes[0])
	if testSchema == dict():
		# deleteFiles(output)
		output['errors'].append('Test schema for {} does not exist'.format(testType))
		return output
	# Update schema
	testSchema['component'] = serialNumber
	testSchema['institution'] = "BNL"
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stage)+1)
	date = datetime.now()
	testSchema['date'] = "{}-{}-{}T{}:{}:{}.000Z".format(date.year, str(date.month).zfill(2), str(date.day).zfill(2), str(date.hour).zfill(2), str(date.minute).zfill(2), str(date.second).zfill(2))
	testSchema['passed'] = glueWeight >= 0.0438-0.00438 #no longer an upper limit
	testSchema['problems'] = False # For the moment always false
	testSchema['results'] = dict()
	testSchema['results']['GW_GLUE_ASICS'] = glueWeight
	testSchema['results']['GW_HYBRID_HT'] = float(hybridWeight)
	testSchema['results']['GW_HYBRID_HTG'] = float(hybridAndGlueWeight)
	testSchema['results']['GW_HYBRID_HTGA'] = float(totalWeight)
	testSchema['results']['GW_ASIC'] = asicsWeight
	# Create a json file for each test schema
	jsonFilePath = os.path.join("/tmp/", serialNumber+"_hybridGlueWeight.json")
	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	# Set component in proper stage
	if currentStage != stage:
		ready, missingTests = readyForNextStage(serialNumber, stage)
		if ready:
			[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
			if not setStageOut:
				# deleteFiles(output)
				output['errors'].append('Component not set to the proper stage.')
				return output
		else:
			# deleteFiles(output)
			output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
			return output
	# Upload schema
	[uploadSchemaOut, testRun, exception]= dbActions.uploadSchema(testSchema)
	if not uploadSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Data not uploaded')
		return output
	else:
		output['uploadedFiles'].append(jsonFilePath)  
	#Save files in BNL disk
	#diskActions.uploadToDisk(output, "hybrid/glueWeight")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output


##############################
# Hybrid Metrology functions #
##############################
from app.bkgTaskMethods import bkgHybridMetrology_Methods
from app.bkgTaskMethods import bkgHybridMetrology_TestSchema
def bkgHybridMetrology(identifier, hybridThickness, check, files):
	output = commonFunctions.commonOutputDict()
	itkClient = dbActions.getITkClient(current_user)

	#Check file names,format, types
	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgHybridMetrology')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	#For checking file content, if needed in future
	# # acceptedFileString = commonFunctions.getAcceptedFileFormatString('bkgHybridMetrology')
	# # output = errorChecks.checkFileContent(files, output, acceptedFileString)
	# # if output['errors']!=[]: return output

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgHybridMetrology')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	files = [x for x in files.split(';') if x!='']
	output['inputFiles'] = files

	for file in files:
		output = errorChecks.checkCompStatus(identifier, output, itkClient)
		if output['errors']!=[]: return output

		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,'bkgHybridMetrology')

		# localName = bkgHybridMetrology_Methods.checkLocalName(identifier)
		# if localName == None:
		# 	output['errors'].append('Local name of the hybrid must have "BNL" or "GPD" as the first few characters')
		# 	return output
		
		acceptedCompTypes = commonFunctions.getAcceptedCompTypes('bkgHybridMetrology')
		output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkCompLocation(identifier, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output

		for file in output['inputFiles']:
			print("output in hybrid metrology is", output)
			output['createdFiles'] += bkgHybridMetrology_Methods.hybridMetrologyProcessing(serialNumber, file, float(hybridThickness))
			print("output after hybrid metrology processing is", output)
			tarFile = "/tmp/"+serialNumber+".tar"
			os.system("tar -cvf "+tarFile+" -P /tmp/"+serialNumber+"*")
			output["sendFiles"].append(tarFile)

		#Only upload if not checking
		if not check:
			output = errorChecks.checkTestSchema('ASIC_METROLOGY', compCode, output, itkClient)
			if output['errors']!=[]: return output

			[testSchema, exception] = dbActions.getTestSchema('ASIC_METROLOGY', acceptedCompTypes[0])
			for file in output['createdFiles']:
				if ".json" in file:
					jsonFilePath = file
					openedFile = open(file)
					testSchema = json.load(openedFile)
					openedFile.close()
			testSchema = bkgHybridMetrology_TestSchema.fillTestSchema(testSchema)

			[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
			output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
			if output['errors']!=[]: return output

			for file in output['createdFiles']:
				file_size = os.path.getsize(file)
				if file_size < 63000:
					[uploadFileOut, exception] = dbActions.uploadFile(file, file, uploadTestRun)
					output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
					if output['errors']!=[]: return output
					output['uploadedFiles'].append(jsonFilePath)

			#If not check, fill testRunLink
			output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))
			output['testSchema'].append(testSchema)
		
		output['institution'] = commonFunctions.getInstitute()
		output['identifier'].append(identifier)
		output['serialNumber'].append(identifier)
		output['compCode'].append(compCode)
		

	print(output)
	return output

		
	# 	# Rename file
	# 	fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgModuleIV")

	# 	# # Create json file
	# 	jsonFilePath = f'/tmp/{fileName}.json'
	# 	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	# 	#Fill out output inside of loop for each file
	# 	output['identifier'].append(identifier)
	# 	output['serialNumber'].append(identifier)
	# 	output['compCode'].append(compCode)
	# 	output['createdFiles'].append(jsonFilePath)
	# 	output['testSchema'].append(testSchema)
	# #Finish filling output outside of loop
	# output['institution'] = commonFunctions.getInstitute()
	# #Custom dict keys for this function to upload later
	# output['stage'] = stage
	
# def bkgHybridMetrology(identifier, hybridThickness, check, rawFileStr):
# 	# Output
# 	output=dict()
# 	output['inputFiles'] = []
# 	output['identifier'] = []
# 	output['uploadedFiles'] = []
# 	output['createdFiles'] = []
# 	output['errors'] = []
# 	output['sendFiles'] = []
# 	# General information
# 	stage = "ASIC_ATTACHMENT"
# 	# Variables based on inputs
# 	files = [x for x in rawFileStr.split(';') if x!='']
# 	# Clear user files
# 	# user = User.query.get(current_user.id)
# 	# user.submittedFiles = ""
# 	# db.session.commit()
# 	# Get good files
# 	inputFiles = []
# 	for f in files:
# 		found = False
# 		if "GPC" in f and ".txt" in f:
# 			found = True
# 		if not found:
# 			os.system("rm "+str(f))
# 		else:
# 			inputFiles.append(f)
# 			break
# 	if len(inputFiles) == 0:
# 		output['errors'].append(errorChecks.commonErrorDict['wrongFileError'])
# 		return output
# 	output['inputFiles'] = inputFiles
# 	# Get component info	#if the test is said to pass
# 	comp = None
# 	serialNumber = None
# 	localName = None
# 	currentStage = None
# 	try: 
# 		comp = dbActions.getCompStatus(identifier)
# 		comp = comp[0]
# 		serialNumber = comp['serialNumber']
# 		for i in range(len(comp['properties'])):
# 			if comp['properties'][i]['code'] == 'LOCAL_NAME':
# 				tmpLocalName = comp['properties'][i]['value']
# 				try:
# 					localNameParts = tmpLocalName.split("-")
# 					if len(localNameParts)>2 and ("GPC" in localNameParts[0] or ("BNL" in localNameParts[0])):
# 						localName = tmpLocalName
# 						break
# 				except:
# 					pass
# 		currentStage = comp['currentStage']['code']
# 	except:
# 		# deleteFiles(output)
# 		output['errors'].append('Component does not exist')
# 		return output
# 	if comp == None:
# 		# deleteFiles(output)
# 		output['errors'].append('Component does not exist')
# 		return output
# 	output['identifier'].append(serialNumber)
# 	# Update good input files name with serial number
# 	output['inputFiles'] = renameFiles(output['inputFiles'], serialNumber, "Raw")
# 	# Check component is of the correct type
# 	acceptedTypes = ["HYBRID_ASSEMBLY"]
# 	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
# 	if correctCompType == False:
# 		# deleteFiles(output)
# 		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
# 		return output
# 	# Check component is in the correct location
# 	acceptedLocation = 'BNL'
# 	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
# 	# if not correctCompLocation:
# 	# 	# deleteFiles(output)
# 	# 	output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
# 	# 	return output
# 	# Process information
# 	for file in output['inputFiles']:
# 		print("output in hybrid metrology is", output)
# 		output['createdFiles'] += hybridMetrologyProcessing(serialNumber, file, float(hybridThickness))
# 		print("output after hybrid metrology processing is", output)
# 	# Only create tar file if we are just checking
# 	if check:
# 		tarFile = "/tmp/"+serialNumber+".tar"
# 		os.system("tar -cvf "+tarFile+" -P /tmp/"+serialNumber+"*")
# 		output["sendFiles"].append(tarFile)
# 	if not check:
# 		# Create tar files
# 		tarFile = "/tmp/"+serialNumber+".tar"
# 		os.system("tar -cvf "+tarFile+" -P /tmp/"+serialNumber+"*")
# 		output["sendFiles"].append(tarFile)
# 		# Set component in proper stage
# 		if currentStage != stage:
# 			ready, missingTests = readyForNextStage(serialNumber, stage)
# 			if ready:
# 				[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
# 				if not setStageOut:
# 					# deleteFiles(output)
# 					output['errors'].append('Component not set to the proper stage.')
# 					return output
# 			else:
# 				# deleteFiles(output)
# 				output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
# 				return output
# 		# Upload test schema which is in .json file
# 		uploadTestRun = None
# 		for f in output['createdFiles']:
# 			if ".json" in f:
# 				openedFile = open(f)
# 				testSchema = json.load(openedFile)
# 				openedFile.close()
# 				[uploadSchemaOut, testRun, exception] = dbActions.uploadSchema(testSchema)
# 				if not uploadSchemaOut:
# 					# deleteFiles(output)
# 					output['errors'].append('Data not uploaded')
# 					return output
# 				else:
# 					output['uploadedFiles'].append(f)  
# 				break
# 		# Upload data file
# 		for f in output['createdFiles']:
# 			if "-X.txt" in f or "-Y.txt" in f:
# 				[uploadFileOut, exception] = dbActions.uploadFile(f, localName, uploadTestRun)
# 				if not uploadFileOut:
# 					# deleteFiles(output)
# 					output['errors'].append('File not uploaded')
# 					return output
# 				else:
# 					output['uploadedFiles'].append(f)
# 				break
# 		#Save files in BNL disk
# 		#diskActions.uploadToDisk(output, "hybrid/metrology")
# 		print("got to the end of the function")
# 		print("output at function end is", output)
# 	# Remove remaining files from tmp folder
# 	# deleteFiles(output)
# 	return output


#################################
# Hybrid Wire Bonding functions #
#################################

def bkgHybridWireBonding(identifier, operator, bonder, rawFailedAsicBond, rawFailedPanelBond, result):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	# General information
	testType = "WIRE_BONDING"
	stage = "WIRE_BONDING"
	# Get relevant information
	failedAsicBond = rawFailedAsicBond.split(",")
	failedPanelBond = rawFailedPanelBond.split(",")
	# Get component info
	comp = None
	serialNumber = None
	currentStage = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		currentStage = comp['currentStage']['code'] 
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["HYBRID_ASSEMBLY"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Get test schema
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedTypes[0])
	if testSchema == dict():
		# deleteFiles(output)
		output['errors'].append('Test schema for {} does not exist'.format(testType))
		return output
	# Update schema
	testSchema['component'] = serialNumber
	testSchema['institution'] = "BNL"
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stage)+1)
	date = datetime.now()
	testSchema['date'] = "{}-{}-{}T{}:{}:{}.000Z".format(date.year, str(date.month).zfill(2), str(date.day).zfill(2), str(date.hour).zfill(2), str(date.minute).zfill(2), str(date.second).zfill(2))
	if result == 'passed':
		testSchema['passed'] = True
	else:
		testSchema['passed'] = False
	testSchema['problems'] = False # For the moment always false
	testSchema['properties']['OPERATOR'] = operator
	testSchema['properties']['BONDER'] = bonder
	testSchema['results'] = dict()
	testSchema['results']['FAILED_ASIC_BACKEND'] = dict()
	testSchema['results']['REPAIRED_ASIC_BACKEND'] = dict()
	testSchema['results']['FAILED_HYBRID_TO_PANEL'] = dict()
	testSchema['results']['REPAIRED_HYBRID_TO_PANEL'] = dict()
	for bond in failedAsicBond:
		testSchema['results']['FAILED_ASIC_BACKEND'][str(bond).strip(" ")] = "Unknown"
		if result == 'passed': 
			testSchema['results']['REPAIRED_ASIC_BACKEND'][str(bond).strip(" ")] = "Rebonded"
	testSchema['results']['TOTAL_FAILED_ASIC_BACKEND'] = len(testSchema['results']['FAILED_ASIC_BACKEND'])
	for bond in failedPanelBond:
		testSchema['results']['FAILED_HYBRID_TO_PANEL'][str(bond).strip(" ")] = "Unknown"
		if result == 'passed':
			testSchema['results']['REPAIRED_HYBRID_TO_PANEL'][str(bond).strip(" ")] = "Rebonded"
	# Create a json file for each test schema
	jsonFilePath = os.path.join("/tmp/", serialNumber+"_hybridWireBonding.json")
	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	# Set component in proper stage
	if currentStage != stage:
		ready, missingTests = readyForNextStage(serialNumber, stage)
		if ready:
			[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
			if not setStageOut:
				# deleteFiles(output)
				output['errors'].append('Component not set to the proper stage.')
				return output
		else:
			# deleteFiles(output)
			output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
			return output
	# Upload schema
	[uploadSchemaOut, testRun, exception] = dbActions.uploadSchema(testSchema)
	if not uploadSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Data not uploaded')
		return output
	else:
		output['uploadedFiles'].append(jsonFilePath)  
	#Save files in BNL disk
	#diskActions.uploadToDisk(output, "hybrid/wireBonding")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output


#####################################
# Hybrid Electrical Tests functions #
#####################################

from app.bkgTaskMethods import bkgHybridElectricalTest_Methods
from app.bkgTaskMethods import bkgHybridElectricalTest_TestSchema
def bkgHybridElectricalTests(files):

	itkClient = dbActions.getITkClient(current_user)
	output = commonFunctions.commonOutputDict()

	#("USBHX" in f OR "USBHY" in f)
	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgHybridElectricalTests_1')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output
	
	#AND 

	#"_NO_" in f OR "PEDESTAL_TRIM" in f OR "STROBE_DELAY" in f OR "RESPONSE_CURVE" in f)
	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgHybridElectricalTests_2')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	#If filenames contain "NEXT", rename and change content from "NEXT" to "PPA"
	files = bkgHybridElectricalTest_Methods.updateFiles(files)

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgHybridElectricalTests')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	files = [x for x in files.split(';') if x!='']

	for file in files:
		output['inputFiles'].append(file)

		#Get identifier from testSchema
		f = open(file)
		fileSchema = json.load(f)
		f.close()
		identifier = fileSchema['component']

		output = errorChecks.checkCompStatus(identifier, output, itkClient)
		if output['errors']!=[]: return output

		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,'bkgHybridElectricalTests')

		acceptedCompTypes = commonFunctions.getAcceptedCompTypes('bkgHybridElectricalTests')
		output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkCompLocation(identifier, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output
		
		#Need to implement, check if fileSchema exists
		testType = fileSchema['testType']
		output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
		if output['errors']!=[]: return output

		[testSchema, exception] = dbActions.getTestSchema(fileSchema['testType'], acceptedCompTypes[0])
		testSchema = bkgHybridElectricalTest_TestSchema.fillTestSchema(fileSchema, testSchema, testType)
		# Keep jsonFilePath same as name of file
		jsonFilePath = file

		# Upload schema
		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		#Upload file - need to add >64 kB eOs option
		[uploadFileOut, exception] = dbActions.uploadFile(jsonFilePath, jsonFilePath, uploadTestRun, itkClient)
		output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
		if output['errors']!=[]: return output

		#Fill out output inside of loop for each file
		output['identifier'].append(identifier)
		output['serialNumber'].append(identifier)
		output['compCode'].append(compCode)
		output['createdFiles'].append(jsonFilePath)
		output['testSchema'].append(testSchema)
		output['uploadedFiles'].append(jsonFilePath)
		print(output)
		#File size is too large
		# output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	#Finish filling output outside of loop
	output['institution'] = commonFunctions.getInstitute()

	# #Save files in BNL disk
	# folderPath = commonFunctions.getBNLBoxFolderPath('bkgModuleElectricalTests')
	# #diskActions.uploadToDisk(output, folderPath)
	return output

#######################
# Module IV functions #
#######################

from app.bkgTaskMethods import bkgModuleIV_Methods
from app.bkgTaskMethods import bkgModuleIV_TestSchema
def bkgModuleIV(testForm, stage, files):
	output = commonFunctions.commonOutputDict()
	itkClient = dbActions.getITkClient(current_user)

	#Check file names,format, types
	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgModuleIV')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	# acceptedFileString = commonFunctions.getAcceptedFileFormatString('bkgModuleIV')
	# output = errorChecks.checkFileContent(files, output, acceptedFileString)
	# if output['errors']!=[]: return output

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgModuleIV')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	files = [x for x in files.split(';') if x!='']

	for file in files:
		if (".dat" in file and testForm == "PS") or (".json" in file and testForm == "AMAC"):
			pass
		else:
			output['errors'].append('Could not accept file type with test type. If test type is PS, please upload .dat files. If test type is AMAC, please upload .json files')
			return output
		
	for file in files:
		output['inputFiles'].append(file)
		identifier, df_header, df_data = bkgModuleIV_Methods.getFileData(file)
		output = errorChecks.checkCompStatus(identifier, output, itkClient)
		if output['errors']!=[]: return output

		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgModuleIV")

		acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgModuleIV")
		output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkCompLocation(identifier, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output

		testType = bkgModuleIV_Methods.getTestType(testForm, stage)
		output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
		if output['errors']!=[]: return output

		[testSchema, exception] = dbActions.getTestSchema(testType, acceptedCompTypes[0])
		testSchema = bkgModuleIV_TestSchema.fillTestSchema(file, serialNumber, comp, testType, stage, df_header, df_data, testSchema)
		
		# Rename file
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgModuleIV")

		# # Create json file
		jsonFilePath = f'/tmp/{fileName}.json'
		commonFunctions.createJsonFile(testSchema, jsonFilePath)
		#Fill out output inside of loop for each file
		output['identifier'].append(identifier)
		output['serialNumber'].append(identifier)
		output['compCode'].append(compCode)
		output['createdFiles'].append(jsonFilePath)
		output['testSchema'].append(testSchema)
	#Finish filling output outside of loop
	output['institution'] = commonFunctions.getInstitute()
	#Custom dict keys for this function to upload later
	output['stage'] = stage
	return output

def bkgUploadModuleIV(PF, action, output):
	if (PF=='Pass'):
		output['testSchema']['passed']=True
	else:
		output['testSchema']['passed']=False
	if (action=='Upload'):
		testSchema = output['testSchema']
		identifier = testSchema['component']
		file = output['inputFiles'][0]
		jsonFilePath=output['createdFiles'][0]
		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgModuleIV")
		
		fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgModuleIV")
		if '.dat' in output['inputFiles']: fileType = '.dat'
		else: fileType = '.json'

		renamedFilePath = f'/tmp/{fileName}{fileType}'
		shutil.copy(file, renamedFilePath)
		dt = datetime.strptime(testSchema['date'], "%Y-%m-%dT%H:%M:%S.%fZ")
		formatted_dt = dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
		testSchema['date'] = formatted_dt

		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		[uploadFileOut, exception] = dbActions.uploadFile(renamedFilePath, fileName, uploadTestRun)
		output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
		if output['errors']!=[]: return output

		output['uploadedFiles'].append(jsonFilePath)
		output['uploadedFiles'].append(renamedFilePath)
		output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))
		
		folderPath = commonFunctions.getBNLBoxFolderPath("bkgModuleIV")
		#diskActions.uploadToDisk(output, folderPath)
		return output
	
################################
# Module Glue Weight functions #
################################

def bkgModuleGlueWeight(identifier, gluetype, hybridxWeight, hybridyWeight, hybridxTabsWeight, hybridyTabsWeight, powerboardWeight, sensorWeight, sensorAndHybridWeight, totalWeight):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	# General information
	testType = "GLUE_WEIGHT"
	stage = "GLUED"
	# Relevant information
	hybridGlueWeight = -1
	try:
		hybridGlueWeight = round(float(sensorAndHybridWeight)+float(hybridxTabsWeight)+float(hybridyTabsWeight)-float(hybridxWeight)-float(hybridyWeight)-float(sensorWeight) , 5)
	except:
		hybridGlueWeight = round(float(sensorAndHybridWeight)+float(hybridxTabsWeight)-float(hybridxWeight)-float(sensorWeight) , 5)
	powerboardGlueWeight = round(float(totalWeight)-float(sensorAndHybridWeight)-float(powerboardWeight), 5) 
	# Get component info
	comp = None
	serialNumber = None
	mType = None
	currentStage = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		if comp['type']['code'] == 'BARREL_SS_MODULE':
			mType = 'SS' 
		else:
			mType = 'LS'
		currentStage = comp['currentStage']['code'] 
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["MODULE"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Get test schema
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedTypes[0])
	if testSchema == dict():
		# deleteFiles(output)
		output['errors'].append('Test schema for {} does not exist'.format(testType))
		return output
	# Update schema
	testSchema['component'] = serialNumber
	testSchema['institution'] = "BNL"
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stage)+1)
	date = datetime.now()
	testSchema['date'] = "{}-{}-{}T{}:{}:{}.000Z".format(date.year, str(date.month).zfill(2), str(date.day).zfill(2), str(date.hour).zfill(2), str(date.minute).zfill(2), str(date.second).zfill(2))
	if gluetype == "POLARIS":
		gluespec = 0.147
		tolerance = 0.02
	elif gluetype == "TRUE_BLUE":
		gluespec = 0.156
		tolerance = 0.02
	else:
		output['errors'].append('Glue type not recognized')
		return output
	if mType == 'LS':
		testSchema['passed'] = bool(hybridGlueWeight<=gluespec+tolerance and hybridGlueWeight>=gluespec-tolerance and powerboardGlueWeight<=0.097+0.013 and powerboardGlueWeight>=0.097-0.013)
	else:
		testSchema['passed'] = bool(hybridGlueWeight<=2*(gluespec+tolerance) and hybridGlueWeight>=2*(gluespec-tolerance) and powerboardGlueWeight<=0.097+0.013 and powerboardGlueWeight>=0.097-0.013)
	testSchema['problems'] = False # For the moment always false
	testSchema['properties'] = dict()
	testSchema['properties']['GW_METHOD'] = "Dispenser"
	testSchema['properties']['GLUE_METHOD_V_H1'] = "v1 - Hybrid to Sensor X (04)"
	if mType == 'LS':
		testSchema['properties']['GLUE_METHOD_V_H2'] = ""
	else:
		testSchema['properties']['GLUE_METHOD_V_H2'] = "v1 - Hybrid to Sensor Y (44)"
	testSchema['properties']['GLUE_METHOD_V_PB'] = "v1 - Powerboard to Sensor (14)"
	testSchema['results'] = dict()
	testSchema['results']['GW_SENSOR'] = float(sensorWeight)
	testSchema['results']['GW_PB'] = float(powerboardWeight)
	testSchema['results']['GW_GLUE_PB'] = float(powerboardGlueWeight)
	testSchema['results']['GW_HYBRID1'] = round(float(hybridxWeight) - float(hybridxTabsWeight),5)
	testSchema['results']['GW_HYBRID1T'] = float(hybridxWeight)
	testSchema['results']['GW_T1'] = float(hybridxTabsWeight)
	testSchema['results']['GW_MODULE_PB'] = None
	if mType == 'LS':
		testSchema['results']['GW_GLUE_H1'] = float(hybridGlueWeight)
		testSchema['results']['GW_MODULE_H1PB'] = float(totalWeight)
		testSchema['results']['GW_GLUE_H1PB'] = round(float(hybridGlueWeight) + float(powerboardGlueWeight),5)
		testSchema['results']['GW_MODULE_H1'] = float(sensorAndHybridWeight)
		testSchema['results']['GW_HYBRID2'] = None
		testSchema['results']['GW_HYBRID2T'] = None
		testSchema['results']['GW_T2'] = None
		testSchema['results']['GW_GLUE_H2'] = None
		testSchema['results']['GW_MODULE_H1H2'] = None
		testSchema['results']['GW_GLUE_H1H2'] = None
		testSchema['results']['GW_MODULE_H1H2PB'] = None
		testSchema['results']['GW_GLUE_H1H2PB'] = None
	else:
		testSchema['results']['GW_MODULE_H1PB'] = None
		testSchema['results']['GW_GLUE_H1PB'] = None
		testSchema['results']['GW_MODULE_H1'] = None
		testSchema['results']['GW_HYBRID2'] = round(float(hybridyWeight) - float(hybridyTabsWeight),5)
		testSchema['results']['GW_HYBRID2T'] = float(hybridyWeight)
		testSchema['results']['GW_T2'] = float(hybridyTabsWeight)
		testSchema['results']['GW_GLUE_H2'] = None
		testSchema['results']['GW_MODULE_H1H2'] = float(sensorAndHybridWeight)
		testSchema['results']['GW_GLUE_H1H2'] = float(hybridGlueWeight)
		testSchema['results']['GW_MODULE_H1H2PB'] = float(totalWeight)
		testSchema['results']['GW_GLUE_H1H2PB'] = round(float(hybridGlueWeight) + float(powerboardGlueWeight), 5)
	# Create a json file for each test schema
	jsonFilePath = os.path.join("/tmp/", serialNumber+"_moduleGlueWeight.json")
	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	# Set component in proper stage
	if currentStage != stage:
		ready, missingTests = readyForNextStage(serialNumber, stage)
		if ready:
			[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
			if not setStageOut:
				# deleteFiles(output)
				output['errors'].append('Component not set to the proper stage.')
				return output
		else:
			# deleteFiles(output)
			output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
			return output
	# Upload schema
	[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
	if not uploadSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Data not uploaded')
		return output
	else:
		output['uploadedFiles'].append(jsonFilePath)  
	#Save files in BNL disk
	#diskActions.uploadToDisk(output, "module/glueWeight")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output

##############################
# Module Metrology functions #
##############################

def moduleMetrologyProcessing(serialNumber, filePath, temperature=76, powerboardThickness=0.415, hybridThickness=0.38, check=False):
	# Output
	createdFiles = []
	# Get component info
	localName = ""
	#if Check = false then uses database to confirm information
	if not check:
		comp = dbActions.getCompStatus(serialNumber)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		for i in range(len(comp['properties'])):
			if comp['properties'][i]['code'] == 'LOCAL_NAME' or comp['properties'][i]['code']=='LOCALNAME':
				tmpLocalName = comp['properties'][i]['value']
				try:
					localNameParts = tmpLocalName.split("-")
					if len(localNameParts)==4:
						localName = tmpLocalName
						break
					else:
						localName = tmpLocalName
				except:
					localName = tmpLocalName
				print(localName)
	# if check is true, then does not use database to confirm is serial number and local name are correct to speed up how long it takes to process
	else:
		filename=filePath.split('/')[-1]
		localName = filename.split('.')[0][19:]
	mType = None
	if "MS" in serialNumber:
		mType = 'SS'
	else:
		mType = 'LS'
	# Relevant info
	hybrid_groups = np.arange(1, 34, 1).reshape((11,3)).T
	# Read file
	fileInfo = metrologyReadFile(filePath)
	data = fileInfo['data']
	data = data.drop(['Min.', 'Max.', 'Nominal Max.', 'Nominal Min.'], axis=1).dropna(how='all')
	# General header
	header = ["#---Header:\n"]
	header.append("{:<26}{}\n".format("EC or Barrel:", "SB"))
	header.append("{:<26}{}\n".format("Module Type:", mType))
	header.append("{:<26}{}\n".format("Module ref. number:", serialNumber))
	header.append("{:<26}{}\n".format("Date:", fileInfo['time']))
	header.append("{:<26}{}\n".format("Institute:", 'BNL'))
	header.append("{:<26}{}\n".format("Operator:", 'Meny Raviv Moshe'))
	header.append("{:<26}{}\n".format("Instrument Used:", 'OGP 320 Smartscope'))
	header.append("{:<26}{}\n".format("Test Run Number:", '1'))
	header.append("{:<26}{}\n".format("Measurement Program:", 'Module '+mType+" Metrology"))
	# General position scan data
	positionheaders = ['\n#---Position Scan:\n','{:<21}{:<21}Y[mm]\n'.format('#Location', 'X[mm]')]
	positionscandata = []
	hybrid_points = data.filter(regex=r'^H_\w_P\d_\w+_\w+$', axis=0).loc[:, ['X', 'Y']]
	powerboard_points = data.filter(regex=r'PB_P\d_\w+', axis=0).loc[:, ['X', 'Y']]
	positionscandata.append('{:<21}{:<21f}{:f}\n'.format('H_X_P1', hybrid_points.iloc[0,0], -hybrid_points.iloc[0,1]))
	positionscandata.append('{:<21}{:<21f}{:f}\n'.format('H_X_P2', hybrid_points.iloc[1,0], -hybrid_points.iloc[1,1]))
	if mType == 'SS':
		positionscandata.append('{:<21}{:<21f}{:f}\n'.format('H_Y_P1', hybrid_points.iloc[2,0], -hybrid_points.iloc[2,1]))
		positionscandata.append('{:<21}{:<21f}{:f}\n'.format('H_Y_P2', hybrid_points.iloc[3,0], -hybrid_points.iloc[3,1]))
	positionscandata.append('{:<21}{:<21f}{:f}\n'.format('PB_1', powerboard_points.iloc[1,0], -powerboard_points.iloc[1,1]))
	positionscandata.append('{:<21}{:<21f}{:f}\n'.format('PB_2', powerboard_points.iloc[0,0], -powerboard_points.iloc[0,1]))
	# General glue height data
	glueheightheaders = ['#---Glue Heights:\n', '{:<21}{:<21}{:<21}{:<21}{}\n'.format('# Location', 'Type', 'X [mm]', 'Y [mm]', 'Z [mm]')]
	glueheightdata = []
	sensor_scans = data.filter(regex=r'^Ls_Sensor_\d+$', axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
	for point in sensor_scans:
		glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('Sensor', '1', point[0], -point[1], round(point[2]-0.330,4)))
	xHybridXshift = round(hybrid_points.iloc[0,0],4)
	yHybridXshift = round(0.5*(-hybrid_points.iloc[0,1]-hybrid_points.iloc[1,1]), 4)
	if mType == 'SS':
		xHybridYshift = round(hybrid_points.iloc[2,0],4)
		yHybridYshift = round(0.5*(-hybrid_points.iloc[2,1]-hybrid_points.iloc[3,1]), 4)
	abc_scans = data.filter(regex=r'^HP\d\d?$', axis=0)
	for i in range(10):
		for j in hybrid_groups[:, i:i+2].flatten():
			glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('ABC_X_{}'.format(i), '2', round(abc_scans.loc['HP{}'.format(j), 'X']+xHybridXshift,4), round(-abc_scans.loc['HP{}'.format(j), 'Y']+yHybridXshift,4), abc_scans.loc['HP{}'.format(j), 'Z']))
				
	if mType == 'SS':
		abc_scans_ss = data.filter(regex=r'^[Hh][Pp]y\d\d?$', axis=0)
		rename_abc = lambda x: re.sub(r'[Hh][Pp]y(\d)?', r'HP\1', x)
		abc_scans_ss = abc_scans_ss.rename(index=rename_abc)
		for i in range(10):
			for j in hybrid_groups[:, i:i+2].flatten():
				glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('ABC_Y_{}'.format(i), '2', round(abc_scans_ss.loc['HP{}'.format(j), 'X']+xHybridYshift,4), round(-abc_scans_ss.loc['HP{}'.format(j), 'Y']+yHybridYshift,4), abc_scans_ss.loc['HP{}'.format(j), 'Z']))
	hcc_scans = data.filter(regex=r'^H_Hcc\w\w$', axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
	for point in hcc_scans:
		glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('HCC_X_0', '2', round(point[0]+xHybridXshift,4), round(-point[1]+yHybridXshift,4), point[2]))
	if mType == 'SS':
		hcc_scans = data.filter(regex=r'^H_Hcc\w\wy$', axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
		for point in hcc_scans:
			glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('HCC_Y_0', '2', round(point[0]+xHybridYshift,4), round(-point[1]+yHybridYshift,4), point[2]))
	for i in range(3):
		hybrid_scans = data.filter(regex=r'^HX{}_P\d'.format(i), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
		isEmpty = not hybrid_scans.any()
		if i == 2 or isEmpty:
			hybrid_scans = data.filter(regex=r'^HX{}'.format(i), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
		for j in range(hybrid_scans.shape[0]):
			point = hybrid_scans[j]
			glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('H_X_{}_P{}'.format(i, j+1), '2', round(point[0]+xHybridXshift,4), round(-point[1]+yHybridXshift,4), point[2]))
	if mType == 'SS':
		for i in range(3):
			hybrid_scans = data.filter(regex=r'^Hy{}_P\d'.format(i), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
			isEmpty = not hybrid_scans.any()
			if i == 2 or isEmpty:
				hybrid_scans = data.filter(regex=r'^Hy{}'.format(i), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
			for j in range(hybrid_scans.shape[0]):
				point = hybrid_scans[j]
				glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('H_Y_{}_P{}'.format(i, j+1), '2', round(point[0]+xHybridYshift,4), round(-point[1]+yHybridYshift,4), point[2]))
	xPBshift = round(powerboard_points.iloc[0,0], 4)
	yPBshift = round(0.5*(-powerboard_points.iloc[0,1]-powerboard_points.iloc[1,1]),4)
	for i in range(7):
		powerboard_scans = data.filter(regex=r'^PB{}_P\d$'.format(i+1), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
		isEmpty = not powerboard_scans.any()
		if i+1 in [2,5,6,7] or isEmpty:
			powerboard_scans = data.filter(regex=r'^PB{}$'.format(i+1), axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
		for j in range(powerboard_scans.shape[0]):
			glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('PB_{}_P{}'.format(i+1, j+1), '2', round(powerboard_scans[j,0]+xPBshift,4), round(-powerboard_scans[j,1]+yPBshift,4), powerboard_scans[j,2]))
		#glueheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('PB_{}_P1'.format(i+1), '2', round(powerboard_scans[j,0]+xPBshift,4), round(-powerboard_scans[j,1]+yPBshift,4), powerboard_scans[j,2]))
	# General other height data
	otherheightheaders =  ['\n#---Other heights:\n', '{:<21}{:<21}{:<21}{:<21}{}\n'.format('# Location', 'Type', 'X [mm]', 'Y [mm]', 'Z [mm]')]
	otherheightdata = []
	xPBshift = round(powerboard_points.iloc[0,0],4)
	yPBshift = round(0.5*(-powerboard_points.iloc[0,1]-powerboard_points.iloc[1,1]),4)
	capacitor_scans = data.filter(regex=r'^C\d$', axis=0).loc[:, ['X', 'Y', 'Z']]
	for i in range(capacitor_scans.shape[0]):
		otherheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format(capacitor_scans.index[i], '4', round(capacitor_scans.iloc[i,0]+xPBshift,4), round(-capacitor_scans.iloc[i,1]+yPBshift,4), capacitor_scans.iloc[i,2]))      
	shield_scans = data.filter(regex=r'^Shield_\d$', axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
	for point in shield_scans:
		otherheightdata.append('{:<21}{:<21}{:<21}{:<21}{}\n'.format('Shield', '4', round(point[0]+xPBshift,4), round(-point[1]+yPBshift,4), point[2]))
	# Generate metrology raw file
	rawMetrologyFilePath = os.path.join("/tmp",serialNumber+"_"+localName+".txt")
	rawMetrologyFile = open(rawMetrologyFilePath, 'w')
	for item in header:
		rawMetrologyFile.write(item)
	for item in positionheaders:
		rawMetrologyFile.write(item)
	for item in positionscandata:
		rawMetrologyFile.write(item)
	for item in glueheightheaders:
		rawMetrologyFile.write(item)
	for item in glueheightdata:
		rawMetrologyFile.write(item)
	for item in otherheightheaders:
		rawMetrologyFile.write(item)
	for item in otherheightdata:
		rawMetrologyFile.write(item)
	rawMetrologyFile.close()
	createdFiles.append(rawMetrologyFilePath)
	# Collect hybrid positions
	hybrid_points = data.filter(regex=r'^H_\w_P\d_\w+_\w+$', axis=0).loc[:, ['X', 'Y', 'Nominal X', 'Nominal Y']].sort_index()
	rename_scheme = lambda x: re.sub(r'H_(\w)_P(\d)_\w+_\w+', r'H_\1_P\2', x)
	hybrid_points = hybrid_points.rename(index=rename_scheme)
	xposdeviations = (1000*(hybrid_points.loc[:, 'Nominal X'] - hybrid_points.loc[:, 'X'])).round(3)
	yposdeviations = (1000*(hybrid_points.loc[:, 'Nominal Y'] - hybrid_points.loc[:, 'Y'])).round(3)
	passedHybridPositions = (xposdeviations.abs()<250).all() and (yposdeviations.abs()<250).all()
	hybridpositions = OrderedDict()
	for i in hybrid_points.index:
		hybridpositions[i] = [xposdeviations[i], yposdeviations[i]]
	# Collect powerboard positions
	powerboard_points = data.filter(regex=r'PB_P\d_\w+', axis=0).loc[:, ['X', 'Y', 'Nominal X', 'Nominal Y']].sort_index()
	rename_scheme = lambda x: re.sub(r'PB_P(\d)_\w+', r'PB_P\1', x)
	powerboard_points = powerboard_points.rename(index=rename_scheme)
	xposdeviations = (1000*(powerboard_points.loc[:, 'Nominal X'] - powerboard_points.loc[:, 'X'])).round(3)
	yposdeviations = (1000*(powerboard_points.loc[:, 'Nominal Y'] - powerboard_points.loc[:, 'Y'])).round(3)
	passedPowerboardPositions = (xposdeviations.abs() < 250).all() and (yposdeviations.abs() < 250).all()
	pbpositions = OrderedDict()
	for i in powerboard_points.index:
		pbpositions[i] = [xposdeviations[i], yposdeviations[i]]
	# Collect hybrid glue thickness
	abc_heights = data.filter(regex=r'^ABC\w\d$', axis=0).loc[:, 'Z'].sort_index()
	abc_heights = abc_heights - hybridThickness
	rename_abc = lambda x: re.sub(r'ABC(\w)(\d)', r'ABC_\1_\2', x).upper()
	abc_heights = abc_heights.rename(index=rename_abc)*1000
	conditionalVal = (1/float(len(abc_heights)))*sum(abc_heights)
	passedHybridGlueThickness = conditionalVal > 40 and conditionalVal < 170
	problemsHybridGlueThickness = conditionalVal > 40 and conditionalVal < 70
	hcc_scans = data.filter(regex=r'^H_Hcc\w\w$', axis=0).loc[:, 'Z'].mean() - hybridThickness
	hybrid_scans = data.filter(regex=r'^H[Xy]\d$', axis=0).loc[:, 'Z'] - hybridThickness
	rename_hybrid = lambda x: re.sub(r'H(\w)(\d)', r'H_\1_\2', x).upper()
	hybrid_scans = hybrid_scans.rename(index=rename_hybrid)*1000
	abc_heights['HCC_X_0'] = hcc_scans*1000
	if mType == 'SS':
		hcc_y_scans = data.filter(regex=r'^H_Hcc\w\wy$', axis=0).loc[:, 'Z'].mean() - hybridThickness
		abc_heights['HCC_Y_0'] = hcc_y_scans*1000
		abc_heights = abc_heights._append(hybrid_scans)
	hybridgluethickness = abc_heights.round(3).to_dict(OrderedDict)
	# Collect powerboard glue thickness
	powerboard_scans = data.filter(regex=r'^PB\d$', axis=0).loc[:, 'Z']
	powerboard_glue = powerboard_scans - powerboardThickness
	rename_powerboard = lambda x: re.sub(r'PB(\d)', r'PB_\1', x)
	powerboard_glue = powerboard_glue.rename(index=rename_powerboard)*1000
	conditionalVal = 0.25*sum(powerboard_glue[0:4])
	passedPowerboardGlueThickness = conditionalVal > 40 and conditionalVal < 170
	problemsPowerboardGlueThickness = conditionalVal > 40 and conditionalVal < 70
	pbgluethickness = powerboard_glue.round(3).to_dict(OrderedDict)
	# Collect capacitor height
	capacitor_scans = data.filter(regex=r'^C\d$', axis=0).loc[:, 'Z']*1000
	capacitorheight = capacitor_scans.round(3).to_dict(OrderedDict)
	# Collect shieldbox height
	shield_scans = data.filter(regex=r'^Shield_\d$', axis=0).loc[:, 'Z']
	shieldboxheight = shield_scans.max()*1000
	passedShieldboxHeight = shieldboxheight <= 5700
	# Generate metrology json file
	jsonMetrologyData = dict()
	jsonMetrologyData['component'] = serialNumber 
	jsonMetrologyData['institution'] = 'BNL' 
	jsonMetrologyData['runNumber'] = '1'
	jsonMetrologyData['testType'] = 'MODULE_METROLOGY'
	jsonMetrologyData['date'] = fileInfo['time']
	jsonMetrologyData['passed'] = bool(passedHybridPositions and passedPowerboardPositions and passedHybridGlueThickness and passedPowerboardGlueThickness and passedShieldboxHeight)
	jsonMetrologyData['problems'] = bool(problemsHybridGlueThickness and problemsPowerboardGlueThickness)
	jsonMetrologyData['results'] = OrderedDict()
	jsonMetrologyData['results']['HYBRID_POSITION'] = hybridpositions 
	jsonMetrologyData['results']['PB_POSITION'] = pbpositions
	jsonMetrologyData['results']['HYBRID_GLUE_THICKNESS'] = hybridgluethickness
	jsonMetrologyData['results']['PB_GLUE_THICKNESS'] = pbgluethickness
	jsonMetrologyData['results']['CAP_HEIGHT'] = capacitorheight
	jsonMetrologyData['results']['SHIELDBOX_HEIGHT'] = shieldboxheight
	jsonMetrologyData['properties'] = OrderedDict()
	jsonMetrologyData['properties']['OPERATOR'] = 'Meny Raviv Moshe'
	jsonMetrologyData['properties']['MACHINE'] = 'OGP 320 Smartscope'
	jsonMetrologyData['properties']['SCRIPT_VERSION'] = 'readmodule.py'
	jsonMetrologyFilePath = os.path.join("/tmp/", serialNumber+"_"+localName+"_moduleMetrology.json")
	commonFunctions.createJsonFile(jsonMetrologyData, jsonMetrologyFilePath)
	createdFiles.append(jsonMetrologyFilePath)
	# Collect bow scan
	bowscan = ['#---Bow\n', '{:<21}{:<21}{:<21}{}\n'.format('# Location', 'X [mm]', 'Y [mm]', 'Z [mm]')]
	sensor_scans = data.filter(regex=r'^Ls_\d\d?$', axis=0).loc[:, ['X', 'Y', 'Z']].to_numpy()
	for point in sensor_scans:
		bowscan.append('{:<21}{:<21}{:<21}{}\n'.format('Sensor', point[0], point[1], point[2]))
	# Generate bow raw file
	rawBowFilePath = os.path.join("/tmp",serialNumber+"_"+localName+"Bow.txt")
	rawBowFile = open(rawBowFilePath, 'w')
	for item in header:
		rawBowFile.write(item)
	for item in bowscan:
		rawBowFile.write(item)
	rawBowFile.close()
	createdFiles.append(rawBowFilePath)
	# Collect bow height
	sensor_scans = data.filter(regex=r'^Ls_\d\d?$', axis=0).loc[:, ['X', 'Y', 'Z', 'Nominal Z']]
	sensor_scans = sensor_scans.drop(index=["Ls_" + str(i) for i in []])
	sensor_scans['Diff Z'] = sensor_scans['Z'] - sensor_scans['Nominal Z']
	centroid = sensor_scans.mean(axis=0)
	max_index = sensor_scans.idxmax()['Diff Z']
	min_index = sensor_scans.idxmin()['Diff Z']
	distance_max = np.sqrt(np.square(sensor_scans.loc[max_index, 'X'] - centroid['X']) + np.square(sensor_scans.loc[max_index, 'Y'] - centroid['Y']))
	distance_min = np.sqrt(np.square(sensor_scans.loc[min_index, 'X'] - centroid['X']) + np.square(sensor_scans.loc[min_index, 'Y'] - centroid['Y']))
	diff_height = np.abs(sensor_scans.loc[max_index, 'Diff Z'] - sensor_scans.loc[min_index, 'Diff Z'])*1000
	bowheight = None
	passedBowHeight = True
	if distance_max < distance_min:
		if diff_height > 50:
			passedBowHeight = False
		bowheight = -diff_height.round(3)
	else:
		bowheight = diff_height.round(3)
		if diff_height > 150:
			passedBowHeight = False
	# Generate bow json file
	jsonBowData = dict()
	jsonBowData['component'] = serialNumber 
	jsonBowData['institution'] = 'BNL' 
	jsonBowData['runNumber'] = '1'
	jsonBowData['testType'] = 'MODULE_BOW'
	jsonBowData['date'] = fileInfo['time']
	jsonBowData['passed'] = bool(passedBowHeight)
	jsonBowData['problems'] = False
	jsonBowData['properties'] = OrderedDict()
	jsonBowData['properties']['OPERATOR'] = 'Meny Raviv Moshe'
	jsonBowData['properties']['JIG'] = 'Module Assembly Jig'
	jsonBowData['properties']['USED_SETUP'] = 'OGP 320 Smartscope'
	jsonBowData['properties']['SCRIPT_VERSION'] = 'readmodule.py'
	jsonBowData['results'] = OrderedDict()
	jsonBowData['results']['BOW'] = bowheight
	tempInKelvin = (5/9.0) * (temperature + 459.67)
	tempInCelsius = (5/9.0) * (temperature - 32.0)
	jsonBowData['results']['TEMPERATURE'] = tempInCelsius
	jsonBowFilePath = os.path.join("/tmp/", serialNumber+"_"+localName+"_moduleBow.json")
	commonFunctions.createJsonFile(jsonBowData, jsonBowFilePath)
	createdFiles.append(jsonBowFilePath)
	# Generate plots
	pos_data = pd.DataFrame({**jsonMetrologyData['results']['HYBRID_POSITION'], **jsonMetrologyData['results']['PB_POSITION']}, index=['X','Y']).transpose()
	hybrid_glue = pd.DataFrame(jsonMetrologyData['results']['HYBRID_GLUE_THICKNESS'], index=['Thickness']).transpose()
	pb_glue = pd.DataFrame(jsonMetrologyData['results']['PB_GLUE_THICKNESS'], index=['Thickness']).transpose()
	cap_height = pd.DataFrame(jsonMetrologyData['results']['CAP_HEIGHT'], index=['Height']).transpose()
	max_vals = pos_data.max(axis=0)
	min_vals = pos_data.min(axis=0)
	# Plot position deviation with x
	pxfig, pxax = plt.subplots(figsize=(10,6))
	for i in range(pos_data.shape[0]):
		if pos_data.iloc[i, 0] > 250:
			pxax.arrow(i, 80, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if pos_data.iloc[i, 0] < -250:
			pxax.arrow(i, -80, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	pxax.plot(pos_data.loc[:,'X'], marker='o', color='tab:blue', linestyle='None')
	pxax.set_xticks(range(pos_data.shape[0]))
	pxax.set_xticklabels(pos_data.index.to_list(), rotation=60)
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=-250
	Max=250
	if (Max<max_vals.loc['X']):
		Max=max_vals.loc['X']+25
	if (Min>min_vals.loc['X']):
		Min=min_vals.loc['X']-25
	pxax.set_ylim(Min, Max)
	pxax.set_xlabel('Component')
	pxax.set_ylabel(r'Position Deviation $[\mu m]$')
	pxax.set_title(r'X Position Deviation ($\pm 250 \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(max_vals.loc['X'], min_vals.loc['X']))
	pxax.grid(True)
	plt.tight_layout()
	xdevFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-XDev.png")
	plt.savefig(xdevFileName)
	plt.close()
	createdFiles.append(xdevFileName)
	# Plot position deviations with y
	pyfig, pyax = plt.subplots(figsize=(10, 6))
	for i in range(pos_data.shape[0]):
		if pos_data.iloc[i, 1] > 250:
			pyax.arrow(i, 80, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if pos_data.iloc[i, 1] < -250:
			pyax.arrow(i, -80, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	pyax.plot(pos_data.loc[:, 'Y'], marker='o', color='tab:blue', linestyle='None')
	pyax.set_xticks(range(pos_data.shape[0]))
	pyax.set_xticklabels(pos_data.index.to_list(), rotation=60)
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=-250
	Max=250
	if (Max<max_vals.loc['Y']):
		Max=max_vals.loc['Y']+25
	if (Min>min_vals.loc['Y']):
		Min=min_vals.loc['Y']-25
	pyax.set_ylim(Min, Max)
	pyax.set_xlabel('Component')
	pyax.set_ylabel(r'Position Deviation $[\mu m]$')
	pyax.set_title(r'Y Position Deviation ($\pm 250 \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(max_vals.loc['Y'], min_vals.loc['Y']))
	pyax.grid(True)
	plt.tight_layout()
	ydevFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-YDev.png")
	plt.savefig(ydevFileName)
	plt.close()
	createdFiles.append(ydevFileName)
	# Plot the X Hybrid glue thickness
	hgfig, hgax = plt.subplots(figsize=(10, 6))
	X_hyb = hybrid_glue.filter(like='X', axis=0)
	hgax.plot(X_hyb, marker='o', color='tab:blue', linestyle='None')
	hgax.set_xticks(range(X_hyb.shape[0]))
	hgax.set_xticklabels(X_hyb.index.to_list(), rotation=60)
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=0
	Max=210
	if (Min>X_hyb.min()[0]):
		Min=X_hyb.min()[0]-20
	if (Max<X_hyb.max()[0]):
		Max=X_hyb.max()[0]+20
	hgax.set_ylim(Min, Max)
	hgax.set_xlabel('Component')
	hgax.set_ylabel(r'Glue Thickness $[\mu m]$')
	hgax.set_title(r'X Hybrid Glue Thickness ($[40,170] \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(X_hyb.max()[0], X_hyb.min()[0]))
	hgax.grid(True)
	plt.tight_layout()
	xhybridthicknessFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-XHybridThickness.png")
	plt.savefig(xhybridthicknessFileName)
	plt.close()
	createdFiles.append(xhybridthicknessFileName)
	# Plot Y hybrid if it is a SS module
	if mType == 'SS':
		hgyfig, hgyax = plt.subplots(figsize=(10, 6))
		Y_hyb = hybrid_glue.filter(like='Y', axis=0)
		hgyax.plot(Y_hyb, marker='o', color='tab:blue', linestyle='None')
		hgyax.set_xticks(range(Y_hyb.shape[0]))
		hgyax.set_xticklabels(Y_hyb.index.to_list(), rotation=60)
		#if all datapoints are not in orginal window, changes window size to have all datapoints included
		Min=0
		Max=210
		if (Min>Y_hyb.min()[0]):
			Min=Y_hyb.min()[0]-20
		if (Max<Y_hyb.max()[0]):
			Max=Y_hyb.max()[0]+20
		hgyax.set_ylim(Min, Max)
		hgyax.set_xlabel('Component')
		hgyax.set_ylabel(r'Glue Thickness $[\mu m]$')
		hgyax.set_title(r'Y Hybrid Glue Thickness ($[40,170] \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(Y_hyb.max()[0], Y_hyb.min()[0]))
		hgyax.grid(True)
		plt.tight_layout()
		yhybridthicknessFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-YHybridThickness.png")
		plt.savefig(yhybridthicknessFileName)
		plt.close()
		createdFiles.append(yhybridthicknessFileName)
	# Plot the powerboard glue thickness
	pbfig, pbax = plt.subplots(figsize=(10, 6))
	pbax.plot(pb_glue, marker='o', color='tab:blue', linestyle='None')
	pbax.set_xticks(range(pb_glue.shape[0]))
	pbax.set_xticklabels(pb_glue.index.to_list(), rotation=60)
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=0
	Max=210
	if (Min>pb_glue.min()[0]):
		Min=pb_glue.min()[0]-20
	if (Max<pb_glue.max()[0]):
		Max=pb_glue.max()[0]+20
	pbax.set_ylim(Min, Max)
	pbax.set_xlabel('Component')
	pbax.set_ylabel(r'Glue Thickness $[\mu m]$')
	pbax.set_title(r'Thickness ($[40,170] \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(pb_glue.max()[0], pb_glue.min()[0]))
	pbax.grid(True)
	plt.tight_layout()
	pbthicknessFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-PBThickness.png")
	plt.savefig(pbthicknessFileName)
	plt.close()
	createdFiles.append(pbthicknessFileName)
	# Plot the capacitor height
	capfig, capax = plt.subplots(figsize=(10, 6))
	capax.plot(cap_height, marker='o', color='tab:blue', linestyle='None')
	capax.set_xticks(range(cap_height.shape[0]))
	capax.set_xticklabels(cap_height.index.to_list(), rotation=60)
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=2000
	Max=2750
	if (Min>cap_height.min()[0]):
		Min=cap_height.min()[0]-200
	if (Max<cap_height.max()[0]):
		Max=cap_height.max()[0]+200
	capax.set_ylim(Min, Max)
	capax.set_xlabel('Component')
	capax.set_ylabel(r'Capacitor Height $[\mu m]$')
	capax.set_title(r'Height ($[2000,2750] \mu$m)' + '\nMax: {:0.1f}, Min: {:0.1f}'.format(cap_height.max()[0], cap_height.min()[0]))
	capax.grid(True)
	plt.tight_layout()
	capHeightFileName = os.path.join("/tmp", serialNumber+"_"+localName+"-CapacitorHeight.png")
	plt.savefig(capHeightFileName)
	plt.close()
	createdFiles.append(capHeightFileName)
	return createdFiles

def bkgModuleMetrology(identifier, temperature, powerboardThickness, hybridThickness, check, rawFileStr):
	# Output
	output=dict()
	output['inputFiles'] = []
	output['identifier'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	output['sendFiles'] = []
	# General information
	stage = "GLUED"
	# Variables based on inputs
	files = [x for x in rawFileStr.split(';') if x!='']
	# Clear user files
	# user = User.query.get(current_user.id)
	# user.submittedFiles = ""
	# db.session.commit()
	# Get good files
	inputFiles = []
	for f in files:
		found = False
		if ".txt" in f:
			oF = open(f)
			lines = oF.readlines()
			if "Step Name: ls_ch_1   Feature Name: ls_ch_1\n" in lines:
				found = True
		if not found:
			os.system("rm "+str(f))
		else:
			inputFiles.append(f)
			break
	if len(inputFiles) == 0:
		output['errors'].append(errorChecks.commonErrorDict['wrongFileError'])
		return output
	output['inputFiles'] = inputFiles 
	# Get component info
	comp = None
	serialNumber = None
	localName = None
	currentStage = None
	try: 
		print("tried to get comp status within function")
		comp = dbActions.getCompStatus(identifier)
		print("Got comp status. comp is", comp)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		print(serialNumber)
		for i in range(len(comp['properties'])):
			if comp['properties'][i]['code'] == 'LOCAL_NAME' or comp['properties'][i]['code'] == 'LOCALNAME':
				tmpLocalName = comp['properties'][i]['value']
				localName = tmpLocalName
				try:
					localNameParts = tmpLocalName.split("-")
					if len(localNameParts)==4:
						localName = tmpLocalName
						break
				except:
					localName = tmpLocalName
					pass
		currentStage = comp['currentStage']['code'] 
	except:
		# deleteFiles(output)
		print("got to exception within function")
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		print("got to exception within function, component was none")
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Update good input files name with serial number
	output['inputFiles'] = renameFiles(output['inputFiles'], serialNumber, "Raw")
	# Check component is of the correct type
	acceptedTypes = ["MODULE"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location only if uploading will happen
	# if not check:
	# 	acceptedLocation = 'BNL'
	# 	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	# 	if not correctCompLocation:
	# 		# deleteFiles(output)
	# 		output['errors'].append('Component is not in the accepted location: ' + acceptedLocation)
	# 		return output
	# Process information
	for file in output['inputFiles']:
		output['createdFiles'] += moduleMetrologyProcessing(serialNumber, file, float(temperature), float(powerboardThickness), float(hybridThickness),check = check)
	# Only create tar file if we are just checking
	if check:
		tarFile = "/tmp/"+serialNumber+".tar"
		os.system("tar -cvf "+tarFile+" -P /tmp/"+serialNumber+"*")
		output["sendFiles"].append(tarFile)
	if not check:
		# Create tar file
		tarFile = "/tmp/"+serialNumber+".tar"
		os.system("tar -cvf "+tarFile+" -P /tmp/"+serialNumber+"*")
		output["sendFiles"].append(tarFile)
		# Set component in proper stage
		if currentStage != stage:
			ready, missingTests = readyForNextStage(serialNumber, stage)
			if ready:
				[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
				if not setStageOut:
					# deleteFiles(output)
					output['errors'].append('Component not set to the proper stage.')
					return output
			else:
				# deleteFiles(output)
				output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
				return output
		# Upload bow test schema which is in .json file
		uploadTestRun = None
		for f in output['createdFiles']:
			if "Bow.json" in f:
				openedFile = open(f)
				testSchema = json.load(openedFile)
				openedFile.close()
				[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
				if not uploadSchemaOut:
					# deleteFiles(output)
					output['errors'].append('Data not uploaded')
					return output
				else:
					output['uploadedFiles'].append(f)  
				break
		# Upload bow data file
		print(localName)
		for f in output['createdFiles']:
			if "Bow.txt" in f:
				[uploadFileOut, exception] = dbActions.uploadFile(f, localName+"_Bow", uploadTestRun)
				if not uploadFileOut:
					# deleteFiles(output)
					output['errors'].append('File not uploaded')
					return output
				else:
					output['uploadedFiles'].append(f)
				break
		# Upload metrology test schema which is in .json file
		uploadTestRun = None
		for f in output['createdFiles']:
			if ".json" in f and "Bow.json" not in f:
				openedFile = open(f)
				testSchema = json.load(openedFile)
				openedFile.close()
				[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
				if not uploadSchemaOut:
					# deleteFiles(output)
					output['errors'].append('Data not uploaded')
					return output
				else:
					output['uploadedFiles'].append(f)  
				break
		# Upload metrology data file
		for f in output['createdFiles']:
			if ".txt" in f and "Bow.txt" not in f:
				[uploadFileOut, exception] = dbActions.uploadFile(f, localName, uploadTestRun)
				if not uploadFileOut:
					# deleteFiles(output)
					output['errors'].append('File not uploaded')
					return output
				else:
					output['uploadedFiles'].append(f)
				break
		#Save files in BNL disk
		#diskActions.uploadToDisk(output, "module/metrology")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output

def metrologyReadFile(filePath):
	# Output
	output = dict()
	output['data'] = None
	# Open metrology file and extract information
	f = open(filePath)
	lines = f.readlines()
	reportlines = []
	single_points = []
	latestStepName = ""
	for i in range(len(lines)):
		attributes = []
		actualvals = []
		if 'Step Name' in lines[i]:
			name = lines[i].rstrip().split()[2]
			# Skip lines and move towards data
			i += 4
			# Read data
			readline = lines[i]
			while readline != '\n':
				splitline = readline.rstrip().split()
				attributes.append(splitline[0])
				actualvals.append(splitline[3])
				attributes.append("Nominal {}".format(splitline[0]))
				actualvals.append(splitline[2])
				i += 1
				readline = lines[i]
			single_points.append(pd.DataFrame(columns=attributes, index=[name]))
			single_points[-1].loc[name] = actualvals
			# Store latest step name
			latestStepName = name
		elif 'Finder Name' in lines[i]:
			# Skip line and move towards data
			i += 2
			# Read data
			readline = lines[i]
			while 'Focus' in readline:
				splitline = readline.rstrip().split()
				name = latestStepName + "_P"+splitline[1]
				attributes.append("X")
				actualvals.append(splitline[2]) 
				attributes.append("Y")
				actualvals.append(splitline[3]) 
				attributes.append("Z")
				actualvals.append(splitline[4]) 
				single_points.append(pd.DataFrame(columns=attributes, index=[name]))
				single_points[-1].loc[name] = actualvals
				attributes = []
				actualvals = []
				i += 1
				readline = lines[i]
		elif lines[i] != '\n':
			reportlines.append(lines[i])
	# Convert file information into a DataFrame
	data = pd.concat(single_points, axis=0).astype(np.float64)
	output['data'] = data
	# Instrument information
	runtime = [s for s in reportlines if "Date/Time" in s][0].strip()
	dateTime = datetime.strptime(runtime, "Run Date/Time: %m:%d:%y %H:%M:%S")
	output['time'] = "{}-{}-{}T{}:{}:{}.000Z".format(str(dateTime.year), str(dateTime.month).zfill(2), str(dateTime.day).zfill(2), str(dateTime.hour).zfill(2), str(dateTime.minute).zfill(2), str(dateTime.second).zfill(2))
	return output
	
def calculateTilt(p1, p2):
	numerator = np.abs(p1.loc['Z'] - p2.loc['Z'])
	denominator = np.sqrt((p1.loc['X'] - p2.loc['X']) ** 2 + (p1.loc['Y'] - p2.loc['Y']) ** 2)
	tilt = numerator / denominator
	return tilt

#################################
# Module Wire Bonding functions #
#################################

def bkgModuleWireBonding(identifier, operator, bonder, maxUnconChans, maxConUnconChansRow1, FailedRow1, RepairedRow1, maxConUnconChansRow2, FailedRow2, RepairedRow2, maxConUnconChansRow3, FailedRow3, RepairedRow3, maxConUnconChansRow4, FailedRow4, RepairedRow4, NumUnconChans, result):
	# Output
	print(operator)
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = []
	output['createdFiles'] = []
	output['errors'] = []
	# General information
	testType = "MODULE_WIRE_BONDING"
	stage = "BONDED"
	# Get component info
	comp = None
	serialNumber = None
	currentStage = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		currentStage = comp['currentStage']['code'] 
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["MODULE"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Get test schema
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedTypes[0])
	if testSchema == dict():
		# deleteFiles(output)
		output['errors'].append('Test schema for {} does not exist'.format(testType))
		return output
	# Update schema
	testSchema['component'] = serialNumber
	testSchema['institution'] = "BNL"
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stage)+1)
	date = datetime.now()
	testSchema['date'] = "{}-{}-{}T{}:{}:{}.000Z".format(date.year, str(date.month).zfill(2), str(date.day).zfill(2), str(date.hour).zfill(2), str(date.minute).zfill(2), str(date.second).zfill(2))
	if result == 'passed':
		testSchema['passed'] = True
	else:
		testSchema['passed'] = False
	testSchema['problems'] = False # For the moment always false
	testSchema['properties']['OPERATOR'] = operator
	testSchema['properties']['BOND_MACHINE'] = bonder
	if "MS" in serialNumber:
		totalchannels=256*10
	else:
		totalchannels=256*20
	try:
		testSchema['results']['TOTAL_PERC_UNCON_SENSOR_CHAN'] = 100*float(NumUnconChans)/totalchannels
	except Exception as error:
		testSchema['results']['TOTAL_PERC_UNCON_SENSOR_CHAN'] = None
	try:
		testSchema['results']['MAX_UNCON_SENSOR_CHAN'] = int(maxUnconChans)
	except:
		testSchema['results']['MAX_UNCON_SENSOR_CHAN'] = None
	try:
		testSchema['results']['MAX_CONT_UNCON_ROW1'] = int(maxConUnconChansRow1)
	except:
		testSchema['results']['MAX_CONT_UNCON_ROW1'] = None
	try:
		testSchema['results']['FAILED_FRONTEND_ROW1'] = {'Failed bonds': FailedRow1}
	except:
		testSchema['results']['FAILED_FRONTEND_ROW1'] = None
	try:
		testSchema['results']['REPAIRED_FRONTEND_ROW1'] = {'Repaired bonds': RepairedRow1}
	except:
		testSchema['results']['REPAIRED_FRONTEND_ROW1'] = None
	try:
		testSchema['results']['MAX_CONT_UNCON_ROW2'] = int(maxConUnconChansRow2)
	except:
		testSchema['results']['MAX_CONT_UNCON_ROW2'] = None
	try:
		testSchema['results']['FAILED_FRONTEND_ROW2'] = {'Failed bonds': FailedRow2}
	except:
		testSchema['results']['FAILED_FRONTEND_ROW2'] = None
	try:
		testSchema['results']['REPAIRED_FRONTEND_ROW2'] = {'Repaired bonds': RepairedRow2}
	except:
		testSchema['results']['REPAIRED_FRONTEND_ROW2'] = None
	try:
		testSchema['results']['MAX_CONT_UNCON_ROW3'] = int(maxConUnconChansRow3)
	except:
		testSchema['results']['MAX_CONT_UNCON_ROW3'] = None
	try:
		testSchema['results']['FAILED_FRONTEND_ROW3'] = {'Failed bonds': FailedRow3}
	except:
		testSchema['results']['FAILED_FRONTEND_ROW3'] = None
	try:
		testSchema['results']['REPAIRED_FRONTEND_ROW3'] = {'Repaired bonds': RepairedRow3}
	except:
		testSchema['results']['REPAIRED_FRONTEND_ROW3'] = None
	try:
		testSchema['results']['MAX_CONT_UNCON_ROW4'] = int(maxConUnconChansRow4)
	except:
		testSchema['results']['MAX_CONT_UNCON_ROW4'] = None
	try:
		testSchema['results']['FAILED_FRONTEND_ROW4'] = {'Failed bonds': FailedRow4}
	except:
		testSchema['results']['FAILED_FRONTEND_ROW4'] = None
	try:
		testSchema['results']['REPAIRED_FRONTEND_ROW4'] = {'Repaired bonds': RepairedRow4}
	except:
		testSchema['results']['REPAIRED_FRONTEND_ROW4'] = None
	# Create a json file for each test schema
	jsonFilePath = os.path.join("/tmp/", serialNumber+"_moduleWireBonding.json")
	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	# Set component in proper stage
	if currentStage != stage:
		ready, missingTests = readyForNextStage(serialNumber, stage)
		if ready:
			[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
			if not setStageOut:
				# deleteFiles(output)
				output['errors'].append('Component not set to the proper stage.')
				return output
		else:
			# deleteFiles(output)
			output['errors'].append('Component stage not changed due to missing tests in current stage. Missing tests: {}'.format(missingTests))
			return output
	# Upload schema
	[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
	if not uploadSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Data not uploaded')
		return output
	else:
		output['uploadedFiles'].append(jsonFilePath)  
	#Save files in BNL disk
	#diskActions.uploadToDisk(output, "module/wireBonding")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output


#####################################
# Module Electrical Tests functions #
#####################################

from app.bkgTaskMethods import bkgModuleElectricalTest_Methods
from app.bkgTaskMethods import bkgModuleElectricalTest_TestSchema
def bkgModuleElectricalTests(thermalCycled, files):

	itkClient = dbActions.getITkClient(current_user)
	output = commonFunctions.commonOutputDict()

	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgModuleElectricalTests_1')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	acceptedFileNameList = commonFunctions.getAcceptedFileNames('bkgModuleElectricalTests_2')
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	#If filenames contain "NEXT", rename and change content from "NEXT" to "PPA"
	files = bkgModuleElectricalTest_Methods.updateFiles(files)

	#Need to implement to double check file content
	# acceptedFileString = commonFunctions.getAcceptedFileFormatString('bkgModuleElectricalTests')
	# output = errorChecks.checkFileContent(files, output, acceptedFileString)
	# if output['errors']!=[]: return output

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgModuleElectricalTests')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	files = [x for x in files.split(';') if x!='']

	for file in files:
		output['inputFiles'].append(file)

		#Get identifier from testSchema
		f = open(file)
		fileSchema = json.load(f)
		f.close()
		identifier = fileSchema['component']

		output = errorChecks.checkCompStatus(identifier, output, itkClient)
		if output['errors']!=[]: return output

		[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,'bkgModuleElectricalTests')

		acceptedCompTypes = commonFunctions.getAcceptedCompTypes('bkgModuleElectricalTests')
		output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkCompLocation(identifier, output, itkClient)
		if output['errors']!=[]: return output

		output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
		if output['errors']!=[]: return output
		
		#Need to implement, check if fileSchema exists
		testType = bkgModuleElectricalTest_TestSchema.getTestType(fileSchema, thermalCycled)
		output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
		if output['errors']!=[]: return output

		[testSchema, exception] = dbActions.getTestSchema(fileSchema['testType'], acceptedCompTypes[0])
		testSchema = bkgModuleElectricalTest_TestSchema.fillTestSchema(fileSchema, testSchema, testType)
		print("Test schema was ")
		# Keep jsonFilePath same as name of file
		jsonFilePath = file

		# Upload schema
		[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
		output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
		if output['errors']!=[]: return output

		#Upload file - need to add >64 kB eOs option
		# [uploadFileOut, exception] = dbActions.uploadFile(jsonFilePath, jsonFilePath, uploadTestRun, itkClient)
		# output = errorChecks.checkUploadFileOut(uploadFileOut, output, exception)
		# if output['errors']!=[]: return output

		#Fill out output inside of loop for each file
		output['identifier'].append(identifier)
		output['serialNumber'].append(identifier)
		output['compCode'].append(compCode)
		output['createdFiles'].append(jsonFilePath)
		output['testSchema'].append(testSchema)
		output['uploadedFiles'].append(jsonFilePath)
		output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	#Finish filling output outside of loop
	output['institution'] = commonFunctions.getInstitute()

	#Save files in BNL disk
	folderPath = commonFunctions.getBNLBoxFolderPath('bkgModuleElectricalTests')
	#diskActions.uploadToDisk(output, folderPath)

	return output	

###############################
# Visual Inspection functions #
###############################

def bkgMultipleVisualInspection(identifiers, results, stage, compType):
	outputs = {'outputs': [], 'errors': []} 
	
	reception = 'false'
	for identifier, result in zip(identifiers, results):
		if result:
			result_status = 'passed'
		else:
			result_status = 'failed'
		print(identifier, result_status, stage, compType)
		out = bkgVisualInspection(identifier, result, stage, reception, compType)
		
		outputs['outputs'].append(out)
		
		if out['errors']:  # Check if 'errors' exists in out
			outputs['errors'].append(f"{identifier} has error {out['errors']}. ")
	
	return outputs

from app.bkgTaskMethods import bkgVisualInspection_TestSchema
def bkgVisualInspection(identifier, result, stage, reception, compType):
	#Get common dictionary
	output = commonFunctions.commonOutputDict()
	itkClient = dbActions.getITkClient(current_user)
	#Check comp status
	output = errorChecks.checkCompStatus(identifier,output, itkClient)
	if output['errors']!=[]: return output

	#Get component
	[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgVisualInspection")

	#Run error checklist
	acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgVisualInspection",compType)
	output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompLocation(identifier, output, itkClient)
	if output['errors']!=[]: return output

	#Note that this checks stage based on form
	output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, [stage], output, itkClient)
	if output['errors']!=[]: return output
	
	#Get test schema and fill, dependent on whether or not it is module reception test
	testType = 'VISUAL_INSPECTION'

	output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
	if output['errors']!=[]: return output
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedCompTypes[0])

	testSchema = bkgVisualInspection_TestSchema.fillTestSchema(comp, testType, stage, serialNumber, testSchema, result)

	#Rename and create json file
	fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgVisualInspection")
	jsonFilePath = f'/tmp/{fileName}.json'
	commonFunctions.createJsonFile(testSchema, jsonFilePath)

	#Upload file and end to box
	[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
	output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
	if output['errors']!=[]: return output

	#Fill out output
	output['identifier'].append(identifier)
	output['serialNumber'].append(serialNumber)
	output['compCode'].append(compCode)
	output['institution'].append(commonFunctions.getInstitute())
	output['createdFiles'].append(jsonFilePath)
	output['testSchema'].append(testSchema)
	output['uploadedFiles'].append(jsonFilePath)
	output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	folderPath = commonFunctions.getBNLBoxFolderPath("bkgVisualInspection", compType)
	#diskActions.uploadToDisk(output, folderPath)

	return output
	
###############################
# Stave functions #
###############################

from app.bkgTaskMethods import bkgConfocalScan_Methods
from app.bkgTaskMethods import bkgConfocalScan_TestSchema
def bkgConfocalScan(files):
	#Create output dict, get ITk Client
	output = commonFunctions.commonOutputDict()
	allFiles = [x for x in files.split(';') if x != '']
	output['inputFiles'] = allFiles
	#Get identifier from first file name
	identifier = allFiles[0].split('_')[0]
	identifier = identifier.split('/')[2]
	itkClient = dbActions.getITkClient(current_user)
	#Check file types and name
	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes("bkgConfocalScan")
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output
	#Check comp status from identifier
	output = errorChecks.checkCompStatus(identifier, output, itkClient)
	if output['errors']!=[]: return output
	#Get component
	[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,"bkgConfocalScan")
	#Run error checklist
	acceptedFileNameList = commonFunctions.getAcceptedFileNames("bkgConfocalScan")
	output = errorChecks.checkFileName(files, output, acceptedFileNameList)
	if output['errors']!=[]: return output

	acceptedCompTypes = commonFunctions.getAcceptedCompTypes("bkgConfocalScan",compCode)
	output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompLocation(identifier, output, itkClient)
	if output['errors']!=[]: return output

	#Note that this checks stage based on form
	output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
	if output['errors']!=[]: return output
	
	#Get test schema and fill it
	confocal_survey = bkgConfocalScan_Methods.csv_to_array(allFiles)
	testType = 'XYZ_METROLOGY'
	output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
	if output['errors']!=[]: return output
	[testSchema, exception] = dbActions.getTestSchema(testType, acceptedCompTypes[0])

	testSchema = bkgConfocalScan_TestSchema.fillTestSchema(testSchema, identifier, comp, testType, stageList, confocal_survey)

	#Rename and create json file
	fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgConfocalScan")
	jsonFilePath = f'/tmp/{fileName}.json'
	commonFunctions.createJsonFile(testSchema, jsonFilePath)
	#Upload file and end to box
	[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
	output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
	if output['errors']!=[]: return output

	#Fill out output
	output['identifier'].append(identifier)
	output['serialNumber'].append(serialNumber)
	output['compCode'].append(compCode)
	output['institution'].append(commonFunctions.getInstitute())
	output['createdFiles'].append(jsonFilePath)
	output['testSchema'].append(testSchema)
	output['uploadedFiles'].append(jsonFilePath)
	output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	#Not implemented yet
	# folderPath = commonFunctions.getBNLBoxFolderPath("bkgConfocalScan", compType)
	# #diskActions.uploadToDisk(output, folderPath)

	return output

from app.bkgTaskMethods import bkgModulePlacementAccuracy_Methods
from app.bkgTaskMethods import bkgModulePlacementAccuracy_TestSchema
def bkgModulePlacementAccuracy_Side_J(identifier, files):
	#Get all files from Side J, save in a folder for later retrieval
	output = commonFunctions.commonOutputDict()
	allFiles = [x for x in files.split(';') if x != '']
	#Make side J directory file
	dir = bkgModulePlacementAccuracy_Methods.mkDirJ()
	#Write identifier file
	bkgModulePlacementAccuracy_Methods.writeIdentifierFile(identifier, dir)
	#Custom error checks for number of files and file content
	bkgModulePlacementAccuracy_Methods.fileCheckErrors_saveToDir(allFiles, output, dir)
	return output

def bkgModulePlacementAccuracy(files):
	output = commonFunctions.commonOutputDict()

	#Get identifier from previous function (Side J)
	identifier = bkgModulePlacementAccuracy_Methods.getIdentifier()
	#Make directory L and check L files
	dir = bkgModulePlacementAccuracy_Methods.mkDirL()

	allFiles = [x for x in files.split(';') if x != '']
	output = bkgModulePlacementAccuracy_Methods.fileCheckErrors_saveToDir(allFiles,output, dir)
	output = bkgModulePlacementAccuracy_Methods.sort_append_files(output)

	# Sort the files based on the extracted numbers, and create fids array for each type of run
	sorted_files = sorted(output['inputFiles'], key=lambda f: bkgModulePlacementAccuracy_Methods.extract_number_and_side(f))
	fidsIdeal_all = []
	fidsAfterGluing_all = []
	fidsBeforeBridgeRemoval_all = []
	fidsAfterBridgeRemoval_all = []

	for file in sorted_files:
		fidsIdeal = bkgModulePlacementAccuracy_Methods.parseFile(file, 'X_Ideal', 'Y_Ideal', 'Z_Ideal')
		fidsAfterGluing = bkgModulePlacementAccuracy_Methods.parseFile(file, 'X_AfterGluing', 'Y_AfterGluing', 'Z_AfterGluing')
		fidsBeforeBridgeRemoval = bkgModulePlacementAccuracy_Methods.parseFile(file, 'X_BeforeBridgeRemoval', 'Y_BeforeBridgeRemoval', 'Z_BeforeBridgeRemoval')
		fidsAfterBridgeRemoval = bkgModulePlacementAccuracy_Methods.parseFile(file, 'X_AfterBridgeRemoval', 'Y_AfterBridgeRemoval', 'Z_AfterBridgeRemoval')

		fidsIdeal_all.append(fidsIdeal)
		fidsAfterGluing_all.append(fidsAfterGluing)
		fidsBeforeBridgeRemoval_all.append(fidsBeforeBridgeRemoval)
		fidsAfterBridgeRemoval_all.append(fidsAfterBridgeRemoval)

	#Generate excel file in Dave-friendly format
	allFids_Side_J = [fidsIdeal_all[:14],fidsAfterGluing_all[:14],fidsBeforeBridgeRemoval_all[:14],fidsAfterBridgeRemoval_all[:14]]
	allFids_Side_L = [fidsIdeal_all[14:28],fidsAfterGluing_all[14:28],fidsBeforeBridgeRemoval_all[14:28],fidsAfterBridgeRemoval_all[14:28]]
	output = bkgModulePlacementAccuracy_Methods.generateExcelFile(identifier, allFids_Side_J, allFids_Side_L, output)

	# Stack all arrays of fids[i][corner][XYZ]
	fidsIdeal_all = np.vstack(fidsIdeal_all)
	fidsAfterGluing_all = np.vstack(fidsAfterGluing_all)
	fidsBeforeBridgeRemoval_all = np.vstack(fidsBeforeBridgeRemoval_all)
	fidsAfterBridgeRemoval_all = np.vstack(fidsAfterBridgeRemoval_all)
	fids = [fidsIdeal_all,fidsAfterGluing_all,fidsBeforeBridgeRemoval_all,fidsAfterBridgeRemoval_all]
	itkClient = dbActions.getITkClient(current_user)

	acceptedFileTypeList = commonFunctions.getAcceptedFileTypes('bkgModulePlacementAccuracy')
	output = errorChecks.checkFileType(files, output, acceptedFileTypeList)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompStatus(identifier, output, itkClient)
	if output['errors']!=[]: return output

	[comp, serialNumber, stageList, currentStage, compCode] = commonFunctions.getComponentInfo(identifier,'bkgModulePlacementAccuracy')

	print(serialNumber, stageList, currentStage, compCode)
	acceptedCompTypes = commonFunctions.getAcceptedCompTypes('bkgModulePlacementAccuracy')
	output = errorChecks.checkCompType(identifier, acceptedCompTypes, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkCompLocation(identifier, output, itkClient)
	if output['errors']!=[]: return output

	output = errorChecks.checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient)
	if output['errors']!=[]: return output
		
	#Need to implement, check if fileSchema exists
	testType = "MODULE_PLACEMENT_ACCURACY_STAVE"
	print(testType, compCode, output, itkClient)
	output = errorChecks.checkTestSchema(testType, compCode, output, itkClient)
	if output['errors']!=[]: return output
	print("Test schema was checked")
	[testSchema, exception] = dbActions.getTestSchema(testType, compCode)

	#For now, only upload final results (previously went from 0 to 3 for before/after bridge loading, after gluing, and ideals)
	# for runNumber in range(4):
	runNumber = 3
	testSchema = bkgModulePlacementAccuracy_TestSchema.fillTestSchema(testSchema,identifier, fids, itkClient, runNumber)
	print(testSchema)
	#Rename and create json file
	fileName = commonFunctions.getFileName(serialNumber, compCode, "bkgModulePlacementAccuracy")
	jsonFilePath = f'/tmp/{fileName}.json'
	commonFunctions.createJsonFile(testSchema, jsonFilePath)

	#Upload file and end to box
	[uploadSchemaOut, uploadTestRun, exception] = dbActions.uploadSchema(testSchema)
	output = errorChecks.checkUploadSchema(uploadSchemaOut, output, exception)
	if output['errors']!=[]: return output

	#Fill out output for each runNumber inside loop
	output['createdFiles'].append(jsonFilePath)
	output['testSchema'].append(testSchema)
	output['uploadedFiles'].append(jsonFilePath)
	output['testRunLink'].append("https://itkpd-test.unicorncollege.cz/testRunView?id="+str(uploadTestRun))

	output['institution'].append(commonFunctions.getInstitute())
	output['identifier'].append(identifier)
	output['serialNumber'].append(serialNumber)
	output['compCode'].append(compCode)
	# Note implemented yet
	# folderPath = commonFunctions.getBNLBoxFolderPath("bkgModulePlacementAccuracy")
	# #diskActions.uploadToDisk(output, folderPath)
	
	return output

##########################
# Registration functions #
##########################
def bkgHybridRegistration(localName, asicAssemblyJig, asicAssemblyChipTray, asicAssemblyPickupTool, glueSyringe, hccBondsCrossed, hType):
	# Output
	output = dict()
	output['inputs'] = dict()
	output['inputs']['hType'] = hType
	output['inputs']['localName'] = localName
	output['inputs']['asicAssemblyJig'] = asicAssemblyJig
	output['inputs']['asicAssemblyChipTray'] = asicAssemblyChipTray
	output['inputs']['asicAssemblyPickuypTool'] = asicAssemblyPickupTool
	output['inputs']['glueSyringe'] = glueSyringe
	output['outputs'] = dict()
	output['outputs']['serialNumber'] = ''
	output['outputs']['componentLink'] = 'https://itkpd-test.unicorncollege.cz/componentView?code='
	output['outputs']['flex'] = ''
	output['inputFiles'] = [] # Keep empty
	output['createdFiles'] = []
	output['uploadedFiles'] = []
	output['errors'] = []
	# Get registration schema
	regSchemaOut, regSchema = dbActions.getRegistrationSchema('HYBRID_ASSEMBLY')
	if not regSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Could not get registration schema')
		return output
	# Fill schema
	regSchema['subproject'] = 'SB'
	regSchema['institution'] = 'BNL'
	regSchema['type'] = hType
	regSchema['properties']['LOCAL_NAME'] = localName
	regSchema['properties']['ASSEMBLY_JIG'] = asicAssemblyJig
	regSchema['properties']['ASSEMBLY_CHIPTRAY'] = asicAssemblyChipTray
	regSchema['properties']['ASSEMBLY_PICKUP_TOOL'] = asicAssemblyPickupTool
	regSchema['properties']['GLUE_SYRINGE'] = glueSyringe
	if hccBondsCrossed == 'yes':  
		regSchema['properties']['HCC_BONDS_CROSSED'] = True
	else:
		regSchema['properties']['HCC_BONDS_CROSSED'] = False
	# Register component
	regRetOut, regRetVal = dbActions.registerComponent(regSchema)
	print(regRetOut)
	print(regRetVal)
	if not regRetOut:
		# deleteFiles(output)
		output['errors'].append("Could not register component")
		return output
	output['outputs']['serialNumber'] = regRetVal['component']['serialNumber']
	output['outputs']['componentLink'] += regRetVal['component']['code']
	# Create .json file
	jsonFilePath = os.path.join("/tmp", str(output['outputs']['serialNumber'])+"_HybridRegistration.json")
	commonFunctions.createJsonFile(regSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	output['uploadedFiles'].append(jsonFilePath)
	# Obtain flex using alternative ID (this should not fail registration)
	localNameParts = localName.split("-")
	flexAlternativeID = ""
	try:
		if len(localNameParts[1]) == 4:
			flexAlternativeID = localNameParts[0]+"_"+localNameParts[3]+"_"+localNameParts[1][1:]+"_"+localNameParts[2][0]+"_H"+localNameParts[2][1:]
		else: #elif len(localNameParts[1] == 3):
			flexAlternativeID = localNameParts[0]+"_"+localNameParts[3]+"_"+localNameParts[1]+"_"+localNameParts[2][0]+"_H"+localNameParts[2][1:]
		print(flexAlternativeID)
		output['outputs']['flex'] = flexAlternativeID
	except:
		output['errors'].append("No flex ID built, none attached")
	flexComp = None
	flexCompSN = None
	try:
		flexComp = dbActions.getCompStatus(flexAlternativeID)
		flexComp = flexComp[0]
		flexCompSN = flexComp['serialNumber']
	except:
		output['errors'].append('Flex component does not exist')
	if flexComp == None:
		output['errors'].append('Flex component does not exist')
	# Associate hybrid flex using alternative ID to freshly assembled hybrid
	if flexCompSN != "":
		assembleRetOut = dbActions.assembleComponent(output['outputs']['serialNumber'], flexCompSN)
		if not assembleRetOut:
			output['errors'].append('Flex component not assembled') 
	# Save files in BNL disk
	#diskActions.uploadToDisk(output, "registration/hybrid")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output

def bkgHVtabSheetRegistration(sheetLabel, unusableTabs):
	# Output
	output = dict()
	output['inputs'] = dict()
	output['inputs']['unusableTabs'] = unusableTabs
	output['inputs']['sheetLabel'] = sheetLabel
	output['outputs'] = dict()
	output['outputs']['serialNumber'] = ''
	output['outputs']['componentLink'] = 'https://itkpd-test.unicorncollege.cz/componentView?code='
	output['inputFiles'] = [] # Keep empty
	output['createdFiles'] = []
	output['uploadedFiles'] = []
	output['errors'] = []
	# Get registration schema
	regSchemaOut, regSchema = dbActions.getRegistrationSchema('HV_TAB_SHEET')
	if not regSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Could not get registration schema')
		return output
	# Fill schema
	regSchema['subproject'] = 'SB'
	regSchema['institution'] = 'BNL'
	regSchema['type'] = 'BARREL'
	regSchema['properties']['LABEL'] = sheetLabel
	regSchema['properties']['NUMBER_TABS_FOR_MODULES'] = None
	regSchema['properties']['NUMBER_TABS_FOR_OTHER'] = None
	try:
		regSchema['properties']['NUMBER_BAD_TABS'] = int(unusableTabs)
	except:
		regSchema['properties']['NUMBER_BAD_TABS'] = 0
	# Register component
	regRetOut, regRetVal = dbActions.registerComponent(regSchema)
	if not regRetOut:
		# deleteFiles(output)
		output['errors'].append("Could not register component")
		return output
	output['outputs']['serialNumber'] = regRetVal['component']['serialNumber']
	output['outputs']['componentLink'] += regRetVal['component']['code']
	# Create .json file
	jsonFilePath = os.path.join("/tmp", str(output['outputs']['serialNumber'])+"_HybridRegistration.json")
	commonFunctions.createJsonFile(regSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	output['uploadedFiles'].append(jsonFilePath)
	# Save files in BNL disk
	#diskActions.uploadToDisk(output, "registration/hvtabSheet")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output

def bkgHybridTestPanelRegistration(panelName, localName, rfid):
	# Output
	output = dict()
	output['inputs'] = dict()
	output['inputs']['panelName'] = panelName
	output['inputs']['localName'] = localName
	output['inputs']['rfid'] = rfid
	output['outputs'] = dict()
	output['outputs']['serialNumber'] = ''
	output['outputs']['componentLink'] = 'https://itkpd-test.unicorncollege.cz/componentView?code='
	output['inputFiles'] = [] # Keep empty
	output['createdFiles'] = []
	output['uploadedFiles'] = []
	output['errors'] = []
	# Get registration schema
	regSchemaOut, regSchema = dbActions.getRegistrationSchema('HYBRID_TEST_PANEL')
	if not regSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Could not get registration schema')
		return output
	# Fill schema
	regSchema['subproject'] = 'SB'
	regSchema['institution'] = 'BNL'
	regSchema['type'] = 'BARREL'
	regSchema['properties']['RFID'] = rfid
	regSchema['properties']['PANEL_NAME'] = panelName
	regSchema['properties']['LOCAL_NAME'] = localName
	regSchema['properties']['NO_CYCLES'] = 0
	# Register component
	regRetOut, regRetVal = dbActions.registerComponent(regSchema)
	if not regRetOut:
		# deleteFiles(output)
		output['errors'].append("Could not register component")
		return output
	output['outputs']['serialNumber'] = regRetVal['component']['serialNumber']
	output['outputs']['componentLink'] += regRetVal['component']['code']
	# Create .json file
	jsonFilePath = os.path.join("/tmp", str(output['outputs']['serialNumber'])+"_HybridRegistration.json")
	commonFunctions.createJsonFile(regSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	output['uploadedFiles'].append(jsonFilePath)
	# Save files in BNL disk
	#diskActions.uploadToDisk(output, "registration/hybridTestPanel")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output

def bkgModuleRegistration(localName, sensorSN, hvtabJig, mType):
	# Output
	output = dict()
	output['inputs'] = dict()
	output['inputs']['mType'] = mType
	output['inputs']['localName'] = localName
	output['inputs']['sensorSN'] = sensorSN
	output['inputs']['hvtabJig'] = hvtabJig
	output['outputs'] = dict()
	output['outputs']['serialNumber'] = ''
	output['outputs']['componentLink'] = 'https://itkpd-test.unicorncollege.cz/componentView?code='
	output['inputFiles'] = [] # Keep empty
	output['createdFiles'] = []
	output['uploadedFiles'] = []
	output['errors'] = []
	# Get registration schema
	regSchemaOut, regSchema = dbActions.getRegistrationSchema('MODULE')
	if not regSchemaOut:
		# deleteFiles(output)
		output['errors'].append('Could not get registration schema')
		return output
	# Fill schema
	regSchema['subproject'] = 'SB'
	regSchema['institution'] = 'BNL'
	regSchema['type'] = 'BARREL_'+mType+'_MODULE'
	regSchema['properties']['LOCALNAME'] = localName
	regSchema['properties']['HV_TAB_ASSEMBLY_JIG'] = hvtabJig
	# Register component
	regRetOut, regRetVal = dbActions.registerComponent(regSchema)
	if not regRetOut:
		# deleteFiles(output)
		output['errors'].append("Could not register component")
		return output
	output['outputs']['serialNumber'] = regRetVal['component']['serialNumber']
	output['outputs']['componentLink'] += regRetVal['component']['code']
	# Create .json file
	jsonFilePath = os.path.join("/tmp", str(output['outputs']['serialNumber'])+"_HybridRegistration.json")
	commonFunctions.createJsonFile(regSchema, jsonFilePath)
	output['createdFiles'].append(jsonFilePath)
	output['uploadedFiles'].append(jsonFilePath)
	# Associate sensor
	sensorComp = None
	try:
		sensorComp = dbActions.getCompStatus(sensorSN)
		sensorComp = sensorComp[0]
	except:
		output['errors'].append('Sensor component does not exist')
	if sensorComp == None:
		output['errors'].append('Sensor component does not exist')
	# Associate hybrid flex using alternative ID to freshly assembled hybrid
	assembleRetOut = dbActions.assembleComponent(output['outputs']['serialNumber'], sensorSN)
	if not assembleRetOut:
		output['errors'].append('Sensor component not associated') 
	# Save files in BNL disk
	#diskActions.uploadToDisk(output, "registration/module")
	# Remove remaining files from tmp folder
	# deleteFiles(output)
	return output


######################
# Assemble functions #
######################

def bkgHybridAssemble(panelIdentifier, filePath):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = [] # Should remain empty
	output['createdFiles'] = [] # Should remain empty
	output['errors'] = []
	# Make sure provided file is a root file
	f = filePath.split(";")[0]
	output['inputFiles'].append(f)
	if ".root" in f:
		pass
	else:
		os.system("rm "+str(f))
	if len(output['inputFiles']) == 0:
		output['errors'].append(errorChecks.commonErrorDict['wrongFileError'])
		return output
	# Go through file
	for file in output['inputFiles']:
		# Read chip fuse IDs from ROOT file
		f = uproot.open(file+":configuration")
		fuseIDKeys = sorted([key for key in f.keys() if 'module_' in key and 'fuseid_readback' in key]) 
		modules = sorted(list(set([key.split('/')[0] for key in fuseIDKeys])))
		fuseJSON = {}
		for module in modules:
			thisModuleDict = {}
			for key in fuseIDKeys:
				if module not in key:
					continue
				chipKey = key.split('/')[-1].split(';')[0] # from e.g. 'module_1/fuseid_readback_chan_10;1', drop 'module_1/' and the cycle (';1')
				chipValue = f[key]
				thisModuleDict[chipKey] = chipValue
			fuseJSON[module] = thisModuleDict
		# Get panel information
		panelInfo = None
		try:
			panelInfo = dbActions.getCompStatus(panelIdentifier)
			panelInfo = panelInfo[0]
		except:
			# deleteFiles(output)
			output['errors'].append('Panel does not exist')
			return output
		if panelInfo == None:
			# deleteFiles(output)
			output['errors'].append('Panel does not exist')
			return output
		# Get hybrid and powerboard information
		hybridDict = {}
		pbDict = {}
		for child in panelInfo['children']:
			if not child['component']:
				continue
			if not child['component']['serialNumber']:
				continue
			if child['componentType']['code']=="HYBRID_ASSEMBLY":
				childPosition = [prop['value'] for prop in child['properties'] if prop['code']=="HYBRID_POSITION"][0]
				# Try to parse this.
				try:
					childPosition = str(int(childPosition)) # check that the hybrid position on the panel property is a str of an int (e.g. '0')
				except:
					try:
						childPosition = str(int(childPosition[0])) # if it isn't, hopefully it's e.g. '0X'.  Check if the first character is an int.
					except:
						# deleteFiles(output)
						output['errors'].append("Hybrid panel does not have expected HYBRID_POSITION property: {}".format(childPosition))
						return output
				childSerial = child['component']['serialNumber']
				hybridDict[childPosition] = childSerial
			elif child['componentType']['code']=="PWB":
				childPosition = [prop['value'] for prop in child['properties'] if prop['code']=="POWERBOARD_POSITION"][0]
				childSerial = child['component']['serialNumber']
				pbDict[childPosition] = childSerial
		# Check number of hybrids assembled to the panel according to ITSDAQ matches the DB
		if len(fuseJSON) > len(hybridDict):
			# deleteFiles(output)
			output['errors'].append("More hybrids physically assembled to this panel than virtually in the DB")
			return output
		# Assembling
		for slot,fuseDict in fuseJSON.items():
			# Get slot number
			slot = slot.split("_")[-1]
			# Check if hybrid in this panel slot according to the DB
			try:
				hybridSerial = hybridDict[slot]
			except:
				# deleteFiles(output)
				output['errors'].append("No hybrids in this slot according to DB")
				return output
			# Pull the hybrid's info from the DB
			try:
				hybridInfo = dbActions.getCompStatus(hybridSerial)
				hybridInfo = hybridInfo[0]
			except:
				# deleteFiles(output)
				output['errors'].append('Hybrid does not exist')
				return output
			if hybridInfo == None:
				# deleteFiles(output)
				output['errors'].append('Hybrid does not exist')
				return output
			# Iterate through chips
			for chip, fuse in fuseDict.items():
				# abc
				if 'chan' in chip:
					# Get just the slot number
					chipPosition = chip.split('_')[-1]
					# Check fuse ID length
					if not len(fuse)==8:
						# deleteFiles(output)
						output['errors'].append("The length of this chip's fuseID is unexpected.  Expect a length of 8 when in hex.")
						return output
					# Get this chip's info from the DB
					shortFuse = fuse[2:]
					altID = "ABC"+shortFuse
					try:
						chipInfo = dbActions.getCompStatus(altID)
						chipInfo = chipInfo[0]
					except:
						# deleteFiles(output)
						output['errors'].append('Failed to find chip in DB')
						return output
					if chipInfo == None:
						# deleteFiles(output)
						output['errors'].append('Chip does not exist')
						return output
					# Check institute.
					hybridInstitution = hybridInfo['institution']['code']
					hybridLocation = hybridInfo['currentLocation']['code']
					chipLocation = chipInfo['currentLocation']['code']
					if not chipLocation==hybridInstitution and not chipLocation==hybridLocation:
						# deleteFiles(output)
						output['errors'].append('Chip location must match hybrid institution')
						return output
					# Check if this chip is already assembled to a hybrid.
					parentHybrids = [parent for parent in chipInfo['parents'] if parent['componentType']['code']=='HYBRID_ASSEMBLY' and parent['component'] ]
					if len(parentHybrids)==1: # this chip is already assembled to a hybrid.
						parentHybrid = parentHybrids[0]
						parentHybridSerial = parentHybrid['component']['serialNumber']
						# Check if the hybrid to which this chip is already assembled matches the hybrid you are trying to assemble.
						if parentHybridSerial == hybridSerial: # yes it is already assembled to the right hybrid.
							# Check if it is assembled in the correct position as well.
							assembledInfo = [child for child in hybridInfo['children'] if child['component']] # get all assembled children
							assembledInfo = [child for child in assembledInfo if child['component']['serialNumber']==chipInfo['serialNumber']][0] # get assembled chip matching this one.
							assembledPosition = [prop['value'] for prop in assembledInfo['properties'] if prop['code']=='ABC_POSITION'][0]                        
							try:
								assembledPosition = str(int(assembledPosition)) # checking that this is just a string of an integer.
							except:
								try:
									assembledPosition = str(int(assembledPosition.split('_')[-1])) # in case the positions are e.g. ABC_X_9
								except:
									pass
						# No more assembly to do right now, so continue to the next chip.
						continue
					# Chip is not already assembled to a hybrid.
					# But should also check if this hybrid has another chip in this slot already.  After previous check, it should not be this chip.
					abcstarv1Children = [child for child in hybridInfo['children'] if child['type']['code']=="ABCSTARV1" and child['component']]
					alreadyFilledPositions = [([prop['value'] for prop in child['properties'] if prop['code']=='ABC_POSITION'][0],child['component']['serialNumber']) for child in abcstarv1Children ]
					if len(alreadyFilledPositions)>0:
						if chipPosition in np.array(alreadyFilledPositions)[:,0]:
							continue
					# Check if this chip is still assembled to an ASIC_SHIPPING_CONTAINER/ABC_GELPACK.
					parentCont = [parent for parent in chipInfo['parents'] if parent['componentType']['code'] in ['ASIC_SHIPPING_CONTAINER', 'ABC_GELPACK'] and parent['component'] ]
					if len(parentCont)==1: # this chip is still assembled to a shipping container or gelpack.
						parentCont = parentCont[0]
						parentContSerial = parentCont['component']['serialNumber']
						disassembleRetVal = dbActions.disassembleComponent(parentContSerial, chipInfo['serialNumber'])
						if not disassembleRetVal:
							# deleteFiles(output)
							output['errors'].append('Could not disassemble '+str(chipInfo['serialNumber'])+" from "+parentContSerial)
							return output
					# Need to pick a slot on the hybrid.  Should probably try to match the slot's 'order' property to the ABC position...
					#    But 'order' starts at 0, but for PPB, chip positions start at 1.
					#    Maybe pick order = position -1.
					order = int(chipPosition) - 1
					slotID = [child['id'] for child in hybridInfo['children'] if child['type']['code']=='ABCSTARV1' and child['order']==order][0]
					# Build properties dict for upload
					childProps = {
						"ABC_POSITION": chipPosition,
						"WAFER_NUMBER": None, # skip for now.  "WAFER_NUMBER" is only in the history of the ABC.  It is not a property of the ABC itself...
						"POSITION_ON_WAFER": None # skip for now.  "POSITION_ON_WAFER" is only in the history of the ABC.  It is not a property of the ABC itself...
					}
					# Assemble 
					assembleRetVal = dbActions.assembleComponent(hybridSerial,chipInfo['serialNumber'],childProps, slotID)
					if not assembleRetVal:
						# deleteFiles(output)
						output['errors'].append('Could not assemble ASICs on hybrid')
						return output
				# HCC
				else:
					# Check fuse ID length
					if not len(fuse)==8 and not len(fuse)==6:
						# deleteFiles(output)
						output['errors'].append("The length of HCC fuseID is unexpected.  Expect a length of 6 or 8 when in hex.")
						return output
					# Get this chip's info from the DB
					if len(fuse)==8:
						shortFuse = fuse[2:]
					else:
						shortFuse = fuse
					altID = "HCC"+shortFuse
					try: 
						chipInfo = dbActions.getCompStatus(altID)
						chipInfo = chipInfo[0]
					except:
						# deleteFiles(output)
						output['errors'].append('Failed to find chip in DB')
						return output
					if chipInfo == None:
						# deleteFiles(output)
						output['errors'].append('Chip does not exist')
						return output
					# Check institute.
					hybridInstitution = hybridInfo['institution']['code']
					hybridLocation = hybridInfo['currentLocation']['code']
					chipLocation = chipInfo['currentLocation']['code']
					if not chipLocation==hybridInstitution and not chipLocation==hybridLocation:
						# deleteFiles(output)
						output['errors'].append('Chip location must match hybrid institution')
						return output
					# Check if this chip is already assembled to a hybrid.
					parentHybrids = [parent for parent in chipInfo['parents'] if parent['componentType']['code']=='HYBRID_ASSEMBLY' and parent['component']]
					if len(parentHybrids)==1: # this chip is already assmebled to a hybrid.
						parentHybrid = parentHybrids[0]
						parentHybridSerial = parentHybrid['component']['serialNumber']
						#No more assembly to do right now, so continue to the next chip.
						continue
					# Chip is not already assembled to a hybrid.
					# But should also check if this hybrid has a chip in this slot already.  After previous check, it should not be this chip.
					hccv1Children = [child for child in hybridInfo['children'] if child['type']['code']=="HCCSTARV1" and child['component']]
					# Check if this chip is still assembled to an ASIC_SHIPPING_CONTAINER/HCC_GELPACK.
					parentCont = [parent for parent in chipInfo['parents'] if parent['componentType']['code'] in ['ASIC_SHIPPING_CONTAINER','HCC_GELPACK'] and parent['component'] ]
					if len(parentCont)==1: # this chip is still assembled to a shipping container or gelpack.
						parentCont = parentCont[0]
						parentContSerial = parentCont['component']['serialNumber']
						disassembleRetVal = dbActions.disassembleComponent(parentContSerial, chipInfo['serialNumber'])
						if not disassembleRetVal:
							# deleteFiles(output)
							output['errors'].append('Could not disassemble '+str(chipInfo['serialNumber'])+" from "+parentContSerial)
							return output
					# Build properties dict for upload
					childProps = {
						"FUSE_ID": shortFuse,
						"WAFER_NUMBER": [prop['value'] for prop in chipInfo['properties'] if prop['code']=="WAFER_NAME"][0], # use WAFER_NAME property of HCC?
						"POSITION_ON_WAFER": [prop['value'] for prop in chipInfo['properties'] if prop['code']=="DIE_INDEX"][0] # use DIE_INDEX property of HCC?
					}
					# Finally, if this point is reached, the ABC should be assembled to the hybrid in the position written in the fuseID JSON.
					# Assemble 
					assembleRetVal = dbActions.assembleComponent(hybridSerial,chipInfo['serialNumber'],childProps)
					if not assembleRetVal:
						# deleteFiles(output)
						output['errors'].append('Could not assemble ASICs on hybrid')
						return output
	return output

def bkgModuleAssemble(identifier, assemblyJig, glueType, pbSN, pbAlignmentJig, pbPickupTool, pbGlueDate, pbGlueSample, hybridxSN, hybridxAlignmentJig, hybridxPickupTool, hybridxGlueDate, hybridxGlueSample, hybridySN, hybridyAlignmentJig, hybridyPickupTool, hybridyGlueDate, hybridyGlueSample):
	# #Redefine variables
	pbGlue = "{}_{}_{}".format(glueType, pbGlueDate, pbGlueSample)
	hybridxGlue = "{}_{}_{}".format(glueType, hybridxGlueDate, hybridxGlueSample)
	hybridyGlue = "{}_{}_{}".format(glueType, hybridyGlueDate, hybridyGlueSample)
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = [] # Should remain empty
	output['createdFiles'] = [] # Should remain empty
	output['errors'] = []
	# Group properties
	pbProperties = {'JIG_POWERBOARD_ALIGNMENT':pbAlignmentJig, 'PICKUPTOOL_POWERBOARD':pbPickupTool, 'POWERBOARD_GLUE_SAMPLE': pbGlue, 'MODULE_ASSEMBLY_JIG':assemblyJig}
	hybridxProperties = {'JIG_HYBRID_ALIGNMENT':hybridxAlignmentJig, 'PICKUPTOOL_HYBRID':hybridxPickupTool, 'HYBRID_GLUE_SAMPLE': hybridxGlue, 'MODULE_ASSEMBLY_JIG':assemblyJig}
	hybridyProperties = {'JIG_HYBRID_ALIGNMENT':hybridyAlignmentJig, 'PICKUPTOOL_HYBRID':hybridyPickupTool, 'HYBRID_GLUE_SAMPLE': hybridyGlue, 'MODULE_ASSEMBLY_JIG':assemblyJig}
	# Get component
	comp = None
	serialNumber = None
	mType = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		if comp['type']['code'] == 'BARREL_SS_MODULE':
			mType = 'SS'
		else:
			mType = 'LS'
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["MODULE"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Check pb and hybrids are completed
	pbReady = False
	hybridxReady = False
	hybridyReady = False
	if pbSN != None or pbSN != "":
		pbReady, missingPbTests = readyForNextStage(pbSN, 'LOADED')
	if hybridxSN != None or hybridxSN != "":
		hybridxReady, missingHybridxTests = readyForNextStage(hybridxSN, 'ON_MODULE')
	if hybridySN != None or hybridySN != "":
		hybridyReady, missingHybridyTests = readyForNextStage(hybridySN, 'ON_MODULE')
	if not pbReady:
		output['error'].append('Powerboard not ready. Missing tests in current stage: {}'.format(missingPbTests))
		return output
	if not hybridxReady:
		output['error'].append('Hybridx not ready. Missing tests in current stage: {}'.format(missingHybridxTests))
		return output
	if mType == 'SS' and not hybridyReady:
		output['error'].append('Hybridy not ready. Missing tests in current stage: {}'.format(missingHybridyTests))
		return output
	# Assemble pb
	if pbSN != None or pbSN != "":
		print(serialNumber, pbSN, pbProperties)
		assembleRetVal = dbActions.assembleComponent(serialNumber, pbSN, pbProperties)
		print(assembleRetVal)
		if not assembleRetVal:
			output['errors'].append('Could not assemble powerboard '+str(pbSN))
	# Get pb db component current stage
	comp = dbActions.getCompStatus(pbSN)
	comp = comp[0]
	currentStage = comp['currentStage']['code'] 
	# Set powerboard to loaded stage
	if currentStage != 'LOADED':
		[setStageOut, exception] = dbActions.setCompStage(pbSN, "LOADED")
		print('powerboard current stage:', currentStage)
		print(setStageOut)
		if not setStageOut:
			output['errors'].append('Powerboard not set to the proper stage.')
	# Assemble hybrid X
	if hybridxSN != None or hybridxSN != "":
		print(serialNumber, hybridxSN, hybridxProperties)
		assembleRetVal = dbActions.assembleComponent(serialNumber, hybridxSN, hybridxProperties)
		print(assembleRetVal)
		if not assembleRetVal:
			output['errors'].append('Could not assemble hybrid X '+str(hybridxSN))
	# Get hybridx db component current stage
	comp = dbActions.getCompStatus(hybridxSN)
	comp = comp[0]
	currentStage = comp['currentStage']['code'] 
	# Set hybrid X to loaded stage
	if currentStage != 'ON_MODULE':
		print('hybridX current stage:', currentStage)
		[setStageOut, exception] = dbActions.setCompStage(hybridxSN, "ON_MODULE")
		if not setStageOut:
			output['errors'].append('Hybrid X not set to the proper stage.')
	# Assemble hybrid Y
	if mType == 'SS':
		if hybridySN != None or hybridySN != "":
			assembleRetVal = dbActions.assembleComponent(serialNumber, hybridySN, hybridyProperties)
			if not assembleRetVal:
				output['errors'].append('Could not assemble hybrid Y '+str(hybridySN))
			else:
				# Get hybridx db component current stage
				comp = dbActions.getCompStatus(hybridySN)
				comp = comp[0]
				currentStage = comp['currentStage']['code'] 
				# Set hybrid Y to loaded stage
				if currentStage != 'ON_MODULE':
					[setStageOut, exception] = dbActions.setCompStage(hybridySN, "ON_MODULE")
					if not setStageOut:
						output['errors'].append('Hybrid Y not set to the proper stage.')
		else:
			output['errors'].append('Expected hybrid Y for SS module, no hybrid Y assembled')
	return output

def bkgHybridTestPanelAssemble(identifier, placementJig, location0SN, location0PickupTool, location1SN, location1PickupTool, location2SN, location2PickupTool, location3SN, location3PickupTool, location4SN, location4PickupTool, location5SN, location5PickupTool):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = [] # Should remain empty
	output['createdFiles'] = [] # Should remain empty
	output['errors'] = []
	# Group properties
	location0properties = {'HYBRID_POSITION': "0", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location0PickupTool}
	location1properties = {'HYBRID_POSITION': "1", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location1PickupTool}
	location2properties = {'HYBRID_POSITION': "2", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location2PickupTool}
	location3properties = {'HYBRID_POSITION': "3", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location3PickupTool}
	location4properties = {'HYBRID_POSITION': "4", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location4PickupTool}
	location5properties = {'HYBRID_POSITION': "5", 'JIG_FOR_HYBRID_PLACEMENT':placementJig, 'PICKUPTOOL_FOR_HYBRID_PLACEMENT':location5PickupTool}
	# Get component
	comp = None
	serialNumber = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["HYBRID_TEST_PANEL"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Assemble location0
	if location0SN != None or location0SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location0SN, location0properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location0SN)+' to location 0')
	# Assemble location1
	if location1SN != None or location1SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location1SN, location1properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location1SN)+' to location 1')
	# Assemble location2
	if location2SN != None or location2SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location2SN, location2properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location2SN)+' to location 2')
	# Assemble location3
	if location3SN != None or location3SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location3SN, location3properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location3SN)+' to location 3')
	# Assemble location4
	if location4SN != None or location4SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location4SN, location4properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location4SN)+' to location 4')
	# Assemble location5
	if location5SN != None or location5SN != "":
		assembleRetVal = dbActions.assembleComponent(serialNumber, location5SN, location5properties)
		if not assembleRetVal:
			output['errors'].append('Could not assemble '+str(location5SN)+' to location 5')
	return output


def bkgStaveAssemble(identifier, assembler, glueBatchID, glueType, calibTime, glueTime, mod0identifier,mod1identifier,mod2identifier,mod3identifier,mod4identifier,mod5identifier,mod6identifier,mod7identifier,mod8identifier,mod9identifier,mod10identifier,mod11identifier,mod12identifier,mod13identifier,mod14identifier,mod15identifier,mod16identifier,mod17identifier,mod18identifier,mod19identifier,mod20identifier,mod21identifier,mod22identifier,mod23identifier,mod24identifier,mod25identifier,mod26identifier,mod27identifier,coreidentifier,EOSJidentifier,EOSLidentifier,DCDCJ,DCDCL):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = [] # Should remain empty
	output['createdFiles'] = [] # Should remain empty
	output['errors'] = []
	#not reading the files yet

	#assembly properties
	moduleproperties = {"SIDE": None, "POSITION": None, "ASSEMBLER": assembler,"GLUE-ID": glueBatchID, "GLUE": glueType, "CALIBRATION": calibTime,"GLUE-TIME": glueTime}

	# Get component
	comp = None
	serialNumber = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["STAVE"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	#could do a check that stave stage is RECEPTION here, but not sure if it's important.
	# Assemble components
	#28 modules + core + 2 EOS cards + 2 DCDCs = 33
	for slot_id in range(33):
		if slot_id<28:
			modulevar = f"mod{slot_id}identifier"
		elif slot_id == 28:
			modulevar = "coreidentifier"
		elif slot_id == 29:
			modulevar = "EOSJidentifier"
		elif slot_id == 30:
			modulevar = "EOSLidentifier"
		elif slot_id == 31:
			modulevar = "DCDCJ"
		elif slot_id == 32:
			modulevar = "DCDCL"
		SN=vars()[modulevar]
		if SN != "":
			if comp['children'][slot_id]['component'] != None:
				output['errors'].append("Slot " + str(slot_id) + " is already filled!")
				return output
			these_properties = moduleproperties
			DCDCproperties = {"SIDE": None}
			slot_code=comp['children'][slot_id]['id']
			if slot_id < 14:
				these_properties["SIDE"] = "Main"
				these_properties["POSITION"] = str(slot_id)
			elif slot_id < 28:
				these_properties["SIDE"] = "Secondary"
				these_properties["POSITION"] = str(slot_id-14)
			elif slot_id == 31:
				DCDCproperties["SIDE"] = "Main"
			elif slot_id == 32:
				DCDCproperties["SIDE"] = "Secondary"
			if slot_id < 28:
				assembleRetVal = dbActions.assembleComponent(serialNumber, SN, these_properties, slot_code)
			elif 27 < slot_id < 31:
				assembleRetVal = dbActions.assembleComponent(serialNumber, SN, slot=slot_code)
			elif slot_id>30:
				assembleRetVal = dbActions.assembleComponent(serialNumber, SN, DCDCproperties, slot_code)
			if not assembleRetVal:
				output['errors'].append('Could not assemble '+SN+' to location ' + str(slot_id))
	return output


#########################
# Disassemble functions #
#########################

def bkgASICsGelpackDisassemble(identifier):
	# Output
	output=dict()
	output['inputFiles'] = [] # Should remain empty
	output['identifier'] = []
	output['uploadedFiles'] = [] # Should remain empty
	output['createdFiles'] = [] # Should remain empty
	output['errors'] = []
	# Get component
	comp = None
	serialNumber = None
	currentStage = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		serialNumber = comp['serialNumber']
		currentStage = comp['currentStage']['code']
	except:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	if comp == None:
		# deleteFiles(output)
		output['errors'].append('Component does not exist')
		return output
	output['identifier'].append(serialNumber)
	# Check component is of the correct type
	acceptedTypes = ["HCC_GELPACK", "ABC_GELPACK", "AMAC_GELPACK", "ASIC_SHIPPING_CONTAINER"]
	correctCompType = dbActions.checkComponentType(serialNumber, acceptedTypes)
	if correctCompType == False:
		# deleteFiles(output)
		output['errors'].append('Provided component is not an accepted type: '+", ".join(acceptedTypes))
		return output
	# Check component is in the correct location
	acceptedLocation = 'BNL'
	correctCompLocation = dbActions.checkComponentLocation(serialNumber, acceptedLocation)
	if not correctCompLocation:
		# deleteFiles(output)
		output['errors'].append('Component is not in the accepted location: '+acceptedLocation)
		return output
	# Get all components to disassemble
	children = comp['children']
	childrenForDisassembly = []
	for i in range(len(children)):
		try:
			childSN = children[i]['component']['serialNumber']
			childrenForDisassembly.append(childSN)
		except:
			pass # Some locations might have missing ASICs
	# Disassemble
	for childSN in childrenForDisassembly:
		disassembleRetVal = dbActions.disassembleComponent(serialNumber, childSN) 
		if not disassembleRetVal:
			output['errors'].append('Could not disassemble '+str(childSN))
	# Send gelpack to final stage
	stage = 'EMPTY'
	if currentStage != stage:
		[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage)
		if not setStageOut:
			# deleteFiles(output)
			output['errors'].append('Component not set to final stage.')
	return output


#takes in an array of abc star chip data and returns them in order of channel number
def seperator(A):
	X=[]
	for i in A:
		for j in i:
			X.append(j)
	return X

def getElectrical_test(identifier):
	output = dbActions.getCompStatus(identifier)
	output = output[0]
	#Find needed id's and run numbers
	tests=output['tests']
	TestIDs=[] #array of touples that contain th test runs ID and the run number
	run_nums=[] #array of all run numbers
	for test in tests:
		if (test['code']=="RESPONSE_CURVE_PPA"): # only gets the response curves
			for run in test['testRuns']:# looks at all response curves
				if (run['state']=='ready'): #if the test was not deleated
					TestIDs.append([run['id'],run["runNumber"]])
					run_nums.append(run["runNumber"])

  #find all 10 point gains by removing all run numbers that have the same number before the dash only keeps that largest number after the dash
  #this will get only the 10 point gains
	max_after_dash = {}
	for item in run_nums:
		before_dash, after_dash = map(int, item.split('-'))
		if before_dash not in max_after_dash or after_dash > max_after_dash[before_dash]:
			max_after_dash[before_dash] = after_dash
	run_nums = [item for item in run_nums if int(item.split('-')[1]) == max_after_dash[int(item.split('-')[0])]]

	nonBNL=0 # checks on a non bnl test has been saved 
	tbg=[] #array of dictonaries to be graphed
	for run,run_num in TestIDs:
		if run_num in run_nums:
			data = dbActions.getCompTestRun(run)
			stage = data["components"][-1]['testedAtStage']['code']
			time = data['date']
			loc = data["institution"]['name']
			results = data['results']
			if (loc=="Brookhaven National Laboratory"):
				loc='BNL'
			elif(loc=="Lawrence Berkeley National Lab - strip modules"):
				loc='LBNL'
			if (loc=="BNL"):
				if (stage=='ON_MODULE'):
					gain_away = seperator(results[0]['value'])
					gain_under = seperator(results[5]['value'])
					innse_away = seperator(results[6]['value'])
					innse_under = seperator(results[11]['value'])
					vt50_away = seperator(results[20]['value'])
					vt50_under = seperator(results[25]['value'])
					dictonary= {'gain_away':gain_away,'gain_under':gain_under,'innse_away':innse_away,'innse_under':innse_under,'vt50_away':vt50_away,'vt50_under':vt50_under,'stage':stage, 'time':time, 'run_num':run_num,'loc':loc}
					tbg.append(dictonary)
			elif(nonBNL==0):
				gain_away = seperator(results[0]['value'])
				gain_under = seperator(results[5]['value'])
				innse_away = seperator(results[6]['value'])
				innse_under = seperator(results[11]['value'])
				vt50_away = seperator(results[20]['value'])
				vt50_under = seperator(results[25]['value'])
				dictonary= {'gain_away':gain_away,'gain_under':gain_under,'innse_away':innse_away,'innse_under':innse_under,'vt50_away':vt50_away,'vt50_under':vt50_under,'stage':stage, 'time':time, 'run_num':run_num,'loc':loc}
				tbg.append(dictonary)
				nonBNL=1
	return tbg

def bkgAMACIVScans(identifier):
	output=dict()
	output['fig'] = []
	output['errors'] = []
	comp = dbActions.getCompStatus(identifier)
	print(comp)
	comp = comp[0]
	output['fig'] = comp
	if (output['fig']==None):
		output['errors'].append('Component does not exist')
		return output
	if (output['fig']['componentType']['code']!='MODULE'):
		output['errors'].append('Component is not a Module')
		return output
  #Find needed data in comp
	tests=output['fig']['tests']
	TestIDs=[]
	run=0
	for test in tests:
		if (test['code']=="MODULE_IV_AMAC"):
			for run in test['testRuns']:
				if (run['state']=='ready'):
					TestIDs.append(run['id'])
					run=1
		if (test['code']=='MODULE_IV_PS_V1'):
			for run in test['testRuns']:
				if (run['state']=='ready'):
					TestIDs.append(run['id'])
					run=1
	if (run==0):
		output['errors'].append('No AMAC IV Test Runs')
		return output
	HVtabed=0
	tbg=[] #array of dictonaries to be graphed
	for run in TestIDs:
		data = dbActions.getCompTestRun(run)
		stage = data["components"][-1]['testedAtStage']['code']
		time = data['date']
		loc = data["institution"]['name']
		if (loc=="Brookhaven National Laboratory"):
			loc='BNL'
		elif(loc=="Lawrence Berkeley National Lab - strip modules"):
			loc='LBNL'
		if (stage == 'TESTED' or stage=="FINISHED"):
			current = data['results'][0]['value']
			voltage = data['results'][5]['value']
			min_length = min(len(current), len(voltage))
			current=current[:min_length]
			voltage=voltage[:min_length]
			dictonary= {'current':current,'voltage':voltage,'stage':stage, 'time':time, 'loc':loc}
			tbg.append(dictonary)
		if (stage=="HV_TAB_ATTACHED" and HVtabed==0):
			current = data['results'][0]['value']
			voltage = data['results'][-1]['value']
			min_length = min(len(current), len(voltage))
			current=current[:min_length]
			voltage=voltage[:min_length]
			dictonary= {'current':current,'voltage':voltage,'stage':stage, 'time':time, 'loc':loc}
			tbg.append(dictonary)
			HVtabed=1
	fig = plt.figure("identifer")
	ax = fig.add_subplot(1,1,1)
	plt.style.use(hep.style.ATLAS)
	for test in tbg:
		plt.plot(test.get('voltage'),test.get('current'), label = (test.get('loc')+ " - " + test.get('time') + " - " + test.get('stage')))
	ax.legend(loc="lower left")
	ax.set_ylabel("Current [nA]")
	ax.set_xlabel("Voltage [V]")
	ax.set_title(identifier + " : IV Scans")
	ax.set_xlim([-750,50])
	fig.savefig("/tmp/figforprez.png")
	html_str = mpld3.fig_to_html(fig)
	output['fig']=html_str
	return output

def bkgElectricalTestGraph(identifier,testType):
	output=dict()
	output['fig'] = []
	output['errors'] = []
	#get module data
	comp = dbActions.getCompStatus(identifier)
	comp = comp[0]
	output['fig'] = comp
	print(output['fig'])
	#check if the component is a module and if it does exist
	if (output['fig']==None):
		output['errors'].append('Component does not exist')
		return output
	if (output['fig']['componentType']['code']!='MODULE'):
		output['errors'].append('Component is not a Module')
		return output
  #gets all hybrids
	hybrids=[]
	for child in output['fig']['children']:
		if (child['componentType']['name']=='STAR Hybrid Assembly'):
			if (child['component']!=None):
				hybrids.append(child['component']['serialNumber'])
	print(hybrids)
  #gets the needed test data for each hybrid
	tests_data=[]
	for hbd in hybrids:
		try:
			data=getElectrical_test(hbd)
		except:
			# deleteFiles(output)
			output['errors'].append('Component does not exist')
			return output
		tests_data.append([data,hbd])

  #creates the x coordinates for graphing
	Xs=np.linspace(0,1280,1280)

  #Depending on what type of graphing is wanted creates needed graphs
	if (testType=='Gain'):
		if (len(hybrids)==1): #if the module is a LS
			fig, axs = plt.subplots(2)
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs
				axs[0].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
	  #set all titles,axies,sizes, and legends
			axs[0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0].set_title(hybrids[0] + " : gain away")
			axs[1].set_title(hybrids[0] + " : gain under") 
			axs[0].set_ylabel("gain (mV/fC)")
			axs[1].set_ylabel("gain (mV/fC)")
			axs[0].set_xlabel("Channel")
			axs[1].set_xlabel("Channel")
			axs[0].legend(loc="lower left")
			axs[1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				axs[i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(12)
		if (len(hybrids)==2):#if the module is a SS
			fig, axs = plt.subplots(2,2)
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs for hybrid 1
				axs[0,0].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,0].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			for dic in tests_data[1][0]:  #plots data from all selected tests on the graphs for hybrid 2
				axs[0,1].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,1].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))  
	  #set all titles,axies,sizes, and legends
			axs[0,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0,0].set_title(hybrids[0] + " : gain away")
			axs[1,0].set_title(hybrids[0] + " : gain under")
			axs[0,1].set_title(hybrids[1] + " : gain away")
			axs[1,1].set_title(hybrids[1] + " : gain under")
			axs[0,0].set_ylabel("gain (mV/fC)")
			axs[1,0].set_ylabel("gain (mV/fC)")
			axs[0,1].set_ylabel("gain (mV/fC)")
			axs[1,1].set_ylabel("gain (mV/fC)")
			axs[0,0].set_xlabel("Channel")
			axs[1,0].set_xlabel("Channel")
			axs[0,1].set_xlabel("Channel")
			axs[1,1].set_xlabel("Channel")
			axs[0,0].legend(loc="lower left")
			axs[1,0].legend(loc="lower left")
			axs[0,1].legend(loc="lower left")
			axs[1,1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				for j in range(2):
					axs[j,i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(20)
	
	elif(testType=='Noise'):
		if (len(hybrids)==1):#if the module is a LS
			fig, axs = plt.subplots(2) 
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs
				axs[0].plot(Xs, dic.get('innse_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1].plot(Xs, dic.get('innse_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			axs[0].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[1].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[0].set_title(hybrids[0] + " : innse away")
			axs[1].set_title(hybrids[0] + " : innse under")
			axs[0].set_ylabel("innse (ENC)")
			axs[1].set_ylabel("innse (ENC)")
			axs[0].set_xlabel("Channel")
			axs[1].set_xlabel("Channel")
			axs[0].legend(loc="lower left")
			axs[1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				axs[i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(12)
		if (len(hybrids)==2): #if the module is a SS
			fig, axs = plt.subplots(2,2)
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs for hybrid 1
				axs[0,0].plot(Xs, dic.get('innse_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,0].plot(Xs, dic.get('innse_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			for dic in tests_data[1][0]: #plots data from all selected tests on the graphs for hybrid 2
				axs[0,1].plot(Xs, dic.get('innse_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,1].plot(Xs, dic.get('innse_under'),label=(dic.get('loc')+ " - " + dic.get('time')))  
			#set all titles,axies,sizes, and legends
			axs[0,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[1,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[0,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[1,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[0,0].set_title(hybrids[0] + " : innse away")
			axs[1,0].set_title(hybrids[0] + " : innse under")
			axs[0,1].set_title(hybrids[1] + " : innse away")
			axs[1,1].set_title(hybrids[1] + " : innse under")
			axs[0,0].set_ylabel("innse (ENC)")
			axs[1,0].set_ylabel("innse (ENC)")
			axs[0,1].set_ylabel("innse (ENC)")
			axs[1,1].set_ylabel("innse (ENC)")
			axs[0,0].set_xlabel("Channel")
			axs[1,0].set_xlabel("Channel")
			axs[0,1].set_xlabel("Channel")
			axs[1,1].set_xlabel("Channel")
			axs[0,0].legend(loc="lower left")
			axs[1,0].legend(loc="lower left")
			axs[0,1].legend(loc="lower left")
			axs[1,1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				for j in range(2):
					axs[j,i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(20)

	elif(testType=='vt50'):
		if (len(hybrids)==1):#if the module is a LS
			fig, axs = plt.subplots(2)
			for dic in tests_data[0][0]:  #plots data from all selected tests on the graphs
				axs[0].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			#set all titles,axies,sizes, and legends
			axs[0].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[1].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[0].set_title(hybrids[0] + " : vt50 away")
			axs[1].set_title(hybrids[0] + " : vt50 under") 
			axs[0].set_ylabel("vt50 (mV)")
			axs[1].set_ylabel("vt50 (mV)")
			axs[0].set_xlabel("Channel")
			axs[1].set_xlabel("Channel")
			axs[0].legend(loc="lower left")
			axs[1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				axs[i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(12)
		if (len(hybrids)==2): #if the module is a SS
			fig, axs = plt.subplots(2,2)
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs for hybrid 1
				axs[0,0].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,0].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			for dic in tests_data[1][0]: #plots data from all selected tests on the graphs for hybrid 2
				axs[0,1].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,1].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))  
			#set all titles,axies,sizes, and legends
			axs[0,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[1,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[0,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[1,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[0,0].set_title(hybrids[0] + " : vt50 away")
			axs[1,0].set_title(hybrids[0] + " : vt50 under")
			axs[0,1].set_title(hybrids[1] + " : vt50 away")
			axs[1,1].set_title(hybrids[1] + " : vt50 under")
			axs[0,0].set_ylabel("vt50 (mV)")
			axs[1,0].set_ylabel("vt50 (mV)")
			axs[0,1].set_ylabel("vt50 (mV)")
			axs[1,1].set_ylabel("vt50 (mV)")
			axs[0,0].set_xlabel("Channel")
			axs[1,0].set_xlabel("Channel")
			axs[0,1].set_xlabel("Channel")
			axs[1,1].set_xlabel("Channel")
			axs[0,0].legend(loc="lower left")
			axs[1,0].legend(loc="lower left")
			axs[0,1].legend(loc="lower left")
			axs[1,1].legend(loc="lower left")
			plt.style.use(hep.style.ATLAS)
			gridlines=np.linspace(0,1280,11)
			for i in range (2):
				for j in range(2):
					axs[j,i].set_xticks(gridlines)
			fig.set_figheight(12)
			fig.set_figwidth(20)

	else: #if testType = all
		print("Went to all")
		if (len(hybrids)==1):#if the module is a LS
			print("Was a long strip")
			fig, axs = plt.subplots(6)
			plt.style.use(hep.style.ATLAS)
			hep.atlas.text("ATLAS ITk")    
			for dic in tests_data[0][0]:  #plots data from all selected tests on the graphs
				axs[0].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[2].plot(Xs, dic.get('innse_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[3].plot(Xs, dic.get('innse_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[4].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[5].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			#set all titles,axies,sizes, and legends
			axs[0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0].set_title(hybrids[0] + " : gain away")
			axs[1].set_title(hybrids[0] + " : gain under") 
			axs[0].set_ylabel("Gain (mV/fC)")
			axs[1].set_ylabel("Gain (mV/fC)")
			axs[0].set_xlabel("Channel")
			axs[1].set_xlabel("Channel")
			axs[2].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[3].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[2].set_title(hybrids[0] + " : Input Noise Away")
			axs[3].set_title(hybrids[0] + " : Input Noise Under") 
			axs[2].set_ylabel("Input Noise (ENC)")
			axs[3].set_ylabel("Input Noise (ENC)")
			axs[2].set_xlabel("Channel")
			axs[3].set_xlabel("Channel")
			axs[4].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[5].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[4].set_title(hybrids[0] + " : vt50 away")
			axs[5].set_title(hybrids[0] + " : vt50 under") 
			axs[4].set_ylabel("vt50 (mV)")
			axs[5].set_ylabel("vt50 (mV)")
			axs[4].set_xlabel("Channel")
			axs[5].set_xlabel("Channel")
			axs[0].legend(loc="lower left")
			axs[1].legend(loc="lower left")
			axs[2].legend(loc="lower left")
			axs[3].legend(loc="lower left")
			axs[4].legend(loc="lower left")
			axs[5].legend(loc="lower left")
			gridlines=np.linspace(0,1280,11)
			for j in range(6):
				axs[j].set_xticks(gridlines)
			fig.set_figheight(40)
			fig.set_figwidth(12)
			fig.savefig("/tmp/figforprez.png")

		if (len(hybrids)==2):#if the module is a SS
			print("Was a long strip")
			fig, axs = plt.subplots(6,2)
			for dic in tests_data[0][0]: #plots data from all selected tests on the graphs for hybrid 1
				axs[0,0].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,0].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[2,0].plot(Xs, dic.get('innse_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[3,0].plot(Xs, dic.get('innse_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[4,0].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[5,0].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			for dic in tests_data[1][0]: #plots data from all selected tests on the graphs for hybrid 2
				axs[0,1].plot(Xs, dic.get('gain_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[1,1].plot(Xs, dic.get('gain_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[2,1].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[3,1].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[4,1].plot(Xs, dic.get('vt50_away'),label=(dic.get('loc')+ " - " + dic.get('time')))
				axs[5,1].plot(Xs, dic.get('vt50_under'),label=(dic.get('loc')+ " - " + dic.get('time')))
			#set all titles,axies,sizes, and legends
			axs[0,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0,0].set_title(hybrids[0] + " : gain away")
			axs[1,0].set_title(hybrids[0] + " : gain under") 
			axs[0,0].set_ylabel("gain (mV/fC)")
			axs[1,0].set_ylabel("gain (mV/fC)")
			axs[0,0].set_xlabel("Channel")
			axs[1,0].set_xlabel("Channel")
			axs[2,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[3,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[2,0].set_title(hybrids[0] + " : innse away")
			axs[3,0].set_title(hybrids[0] + " : innse under") 
			axs[2,0].set_ylabel("innse (ENC)")
			axs[3,0].set_ylabel("innse (ENC)")
			axs[2,0].set_xlabel("Channel")
			axs[3,0].set_xlabel("Channel")
			axs[4,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[5,0].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[4,0].set_title(hybrids[0] + " : vt50 away")
			axs[5,0].set_title(hybrids[0] + " : vt50 under") 
			axs[4,0].set_ylabel("vt50 (mV)")
			axs[5,0].set_ylabel("vt50 (mV)")
			axs[4,0].set_xlabel("Channel")
			axs[5,0].set_xlabel("Channel")
			axs[0,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[1,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=120)
			axs[0,1].set_title(hybrids[0] + " : gain away")
			axs[1,1].set_title(hybrids[0] + " : gain under") 
			axs[0,1].set_ylabel("gain (mV/fC)")
			axs[1,1].set_ylabel("gain (mV/fC)")
			axs[0,1].set_xlabel("Channel")
			axs[1,1].set_xlabel("Channel")
			axs[2,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[3,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=1500)
			axs[2,1].set_title(hybrids[0] + " : innse away")
			axs[3,1].set_title(hybrids[0] + " : innse under") 
			axs[2,1].set_ylabel("innse (ENC)")
			axs[3,1].set_ylabel("innse (ENC)")
			axs[2,1].set_xlabel("Channel")
			axs[3,1].set_xlabel("Channel")
			axs[4,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[5,1].axis(xmin=0,xmax= 1280,ymin=0,ymax=250)
			axs[4,1].set_title(hybrids[0] + " : vt50 away")
			axs[5,1].set_title(hybrids[0] + " : vt50 under") 
			axs[4,1].set_ylabel("vt50 (mV)")
			axs[5,1].set_ylabel("vt50 (mV)")
			axs[4,1].set_xlabel("Channel")
			axs[5,1].set_xlabel("Channel")
			axs[0,0].legend(loc="lower left")
			axs[1,0].legend(loc="lower left")
			axs[0,1].legend(loc="lower left")
			axs[1,1].legend(loc="lower left")
			axs[2,0].legend(loc="lower left")
			axs[3,0].legend(loc="lower left")
			axs[2,1].legend(loc="lower left")
			axs[3,1].legend(loc="lower left")
			axs[4,0].legend(loc="lower left")
			axs[5,0].legend(loc="lower left")
			axs[4,1].legend(loc="lower left")
			axs[5,1].legend(loc="lower left")
			fig.set_figheight(40)
			fig.set_figwidth(20)
	html_str = mpld3.fig_to_html(fig)
	output['fig']=html_str
	return output

'''
gets data from most recent test of serial number with the given test type
'''
def bkgGetFirstTests(serialNumber, testType):
	output = dbActions.getCompStatus(serialNumber)
	output = output[0]
	try:  #if serial number does not exist
		tests=output['tests']
	except:
		testResults = dict()
		testResults['serialNumber'] = serialNumber
		return testResults
	TestID=[] #Finds the Glue_weight test data
	for test in tests:
		if (testType in test['code']): # only gets the test type
			for run in test['testRuns']:# looks at all tests
				if (run['state']=='ready'): #if the test was not deleated
					TestID.append(run['id'])
	testResults = dict()
	testResults['serialNumber'] = serialNumber
	if len(TestID)!=0: # checks to see if there is a glue weight
		#get the data from the tests
		data = dbActions.getCompTestRun(TestID[0])
		testResults['date'] = data['date']
		results = data['results']
		for part in results:
			testResults[part['code']]=part['value']
	return testResults

'''
Gets all data from glue weight tests for all module ID's in the file
'''
def bkgGlueWeightRetrieval(files):
	output = dict()	
	output['inputFiles'] = []
	output['createdFiles'] = []
	output['outputFiles'] = []
	output['errors'] = []
	# check if input if good
	if ".txt" not in files:
		output['errors'].append('Could not upload because of wrong file type')
	allFiles = [x for x in files.split(';') if x!='']
	if (len(allFiles)!=1):
		output['errors'].append('Too many files imputed')
		return output
	output['inputFiles'] = allFiles

	# Get all read all lines from file
	mods=[]
	with open(allFiles[0] , 'r' ) as f:
		for line in f:	
			if "\n" in line:
				mods.append(line[:-1])
			else:
				mods.append(line)

	# get all infromation from each module
	data=[]
	for serialNumber in mods:
		data.append(bkgGetFirstTests(serialNumber, "GLUE_WEIGHT"))
	
	# If any keys are removed from the list then they will not be displayed
	keys=['serialNumber', 'GW_GLUE_H2', 'GW_GLUE_PB', 'GW_HYBRID1', 'GW_HYBRID2', \
	   'GW_SENSOR', 'GW_GLUE_H1', 'GW_MODULE_H1H2', 'GW_GLUE_H1H2','GW_MODULE_PB', 'GW_PB', 'GW_MODULE_H1PB', 'GW_GLUE_H1PB', \
	   'GW_MODULE_H1H2PB', 'GW_GLUE_H1H2PB', 'GW_HYBRID1T', 'GW_MODULE_H1', 'GW_HYBRID2T', 'GW_T1', 'GW_T2']
	
	# creates and writes all information into the file
	rawFilePath = os.path.join("/tmp","GlueWeights" + ".txt")
	rawFile = open(rawFilePath, 'w')
	#print the headings
	for key in keys:
		rawFile.write(key + '\t')
	rawFile.write('\n')
	
	#print the keys
	for part in data:
		for key in keys:
			try:
				rawFile.write(str(part[key]) + '\t')
			except:
				rawFile.write('None\t')
		rawFile.write('\n')
	rawFile.close()
	output['createdFiles'].append(rawFilePath)
	# tarFile = "/tmp/"+serialNumber+".tar"
	# os.system("tar -cvf "+tarFile+" -P /tmp/"+ rawFilePath)
	# output['outputFiles'].append(tarFile)
	return output

def bkgCreateIVFile(data, folder):
	rawFilePath = os.path.join(folder , data['serialNumber'] + ".txt")
	rawFile = open(rawFilePath, 'w')
	rawFile.write('Serial Number : ' + data['serialNumber'] + '\n')
	rawFile.write('Test Date : ' + data['date'] + '\n')
	rawFile.write('Voltage		Current' + '\n')
	for i in range(min(len(data['CURRENT']),len(data['VOLTAGE']))):
		rawFile.write(str(data['VOLTAGE'][i]) +'\t' + str(data['CURRENT'][i]) + '\n')
	return rawFilePath

def bkgMenyModIV(files):
	output = dict()	
	output['inputFiles'] = []
	output['createdFiles'] = []
	output['outputFiles'] = []
	output['errors'] = []
	if ".txt" not in files:
		output['erorrs'].append('Could not upload because of wrong file type')
	allFiles = [x for x in files.split(';') if x!='']
	if (len(allFiles)!=1):
		output['errors'].append('Too many files imputed')
		return output
	output['inputFiles'] = allFiles
	mods=[]
	with open(allFiles[0] , 'r' ) as f:
		for line in f:	
			if "\n" in line:
				mods.append(line[:-1])
			else:
				mods.append(line)
	newdir = '/tmp/multiModIV'
	if not os.path.isdir(newdir):
		os.makedirs(newdir)
	else:
		for filename in os.listdir(newdir):
			file_path = os.path.join(newdir, filename)
			try:
				if os.path.isfile(file_path) or os.path.islink(file_path):
					os.unlink(file_path)
			except:
				print('opsie')
	counter = 0
	badIDs = []
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	ax.set_ylabel("Current [nA]")
	ax.set_xlabel("Voltage [V]")
	ax.set_xlim([-750,50])
	ax.set_xticks([0,-100,-200,-300,-400,-500,-600,-700])
	ttle = 'Modules: '
	for serialNumber in mods:
		print(serialNumber)
		data = (bkgGetFirstTests(serialNumber, "IV"))
		try:
			newfile = bkgCreateIVFile(data,newdir)	
			output['createdFiles'].append(newfile)
			min_length = min(len(data['VOLTAGE']), len(data['CURRENT']))
			ax.plot(data['VOLTAGE'][:min_length],data['CURRENT'][:min_length])
			print('graphed')
			ttle += serialNumber + " , "
			counter+=1
			if (counter%3==0):
				ax.set_title(ttle[:-2], fontsize = 10)
				FilePath = os.path.join(newdir , "IVGraphs" + str(counter//3) +".png")
				print(FilePath)
				fig.savefig(FilePath)
				print("i still work")
				output['createdFiles'].append(FilePath)
				print('noProbHere')
				fig.clear()
				ax = fig.add_subplot(1,1,1)
				ax.set_ylabel("Current [nA]")
				ax.set_xlabel("Voltage [V]")
				ax.set_xlim([-750,50])
				ax.set_xticks([0,-100,-200,-300,-400,-500,-600,-700])
				ttle = 'Modules: '
				print("reset")
		except:
			badIDs.append(serialNumber) 
			print("badID")
	print(badIDs)
	print(ttle[:-2])
	ax.set_title(ttle[:-2])
	FilePath = os.path.join(newdir , "IVGraphs" + str((counter//3)+1) +".png")
	print(FilePath)
	fig.savefig(FilePath)
	output['createdFiles'].append(FilePath)
	zip_file = "/tmp/mutliModIV"
	shutil.make_archive(zip_file, 'zip', newdir)
	zip_file+='.zip'
	output['zipfile'] = zip_file
	if os.path.exists(zip_file):
		print('existS')
	# os.system(f'zip -r {zip_file} {newdir}')
	# print(badIDs)
	# print(mods)
	return output