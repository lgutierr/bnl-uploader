import app.dbActions as dbActions
from datetime import datetime
from datetime import timezone
from app.bkgTaskMethods import commonFunctions
'''DTO Sample
{
	"component": "...",
	"testType": "SHEAR_TEST",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-08-29T19:56:42.411Z",
	"passed": true,
	"problems": false,
	"properties": {
		"OPERATOR": null,
		"TEST_SUBSTRATE": null,
		"TEST_REASON": null
	},
	"results": {
		"SHEAR_STRENGTH": 0
	}
}
'''
def fillTestSchema(comp, serialNumber,  strength, operator, reason, substrate, testSchema):
		# Modify test schema properties
		testSchema['component'] = serialNumber
		testSchema['testType'] = "SHEAR_TEST"
		testSchema['institution'] = commonFunctions.getInstitute()
		testSchema['date'] = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
		testSchema['runNumber'] = str(dbActions.getRunNumber(comp, "SHEAR_TEST", "SHEET_WITH_TABS")+1)
		testSchema['problems'] = False
		testSchema['passed'] = (strength < 1000)
		# Modify test schema properties
		testSchema['properties']['OPERATOR'] = operator
		testSchema['properties']['TEST_SUBSTRATE'] = substrate
		testSchema['properties']['TEST_REASON'] = reason
		# Modify test schema results
		testSchema['results']['SHEAR_STRENGTH'] = round(strength,3)
		return testSchema