
import pandas as pd
from app.bkgTaskMethods import bkgModuleIV_Methods_Analysis
from app.bkgTaskMethods import bkgSensorIV_Methods_CommonConfig
import json

def getTestType(testForm, stage):
	print(testForm, stage)
	if testForm == "AMAC":
		testType = "MODULE_IV_AMAC"
	elif testForm == "PS":
		if stage not in ['HV_TAB_ATTACHED', 'GLUED', 'STITCH_BONDING']:
			testType = "MODULE_IV_PS_BONDED"
		else:
			testType = "MODULE_IV_PS_V1"
	print(testType)
	return testType

def getFileData(file):
	f = open(file, 'r')

	if ".dat" in file:
		# Get header and data
		headerList, dataList = GetGleanList(f)
		f.close()
		# Set header
		df_header = pd.DataFrame(headerList, columns=['key','value'])
		df_header['key']=df_header['key'].str.replace(':','')
		df_header.fillna('', inplace=True)
		df_data = pd.DataFrame(dataList, columns=['voltage', 'current', 'shunt'])
		serialNumber = df_header.query('key=="Module_SN"')['value'].values[0]
		# Get component sn
	elif ".json" in file:
		info = json.load(f)
		serialNumber = info['component']
		df_header, df_data = None, None
	f.close()
	return serialNumber, df_header, df_data

def GetGleanList(fileObj):
	header=[]
	data=[]
	change = False

	for raw_line in fileObj.readlines():
		#st.write("raw line:",line)
		line = ""
		if type(raw_line) != str:
			line=raw_line.decode("ISO-8859-1")
		else:
			line=raw_line
		#st.write(line)
		if line == "":
			break

		if change==False:
			if "Shunt" in line:
				change=True
				continue
			header.append([x.strip() for x in line.split(': ')])
		elif change==True:
			data.append([x.strip() for x in line.split('\t')])
		#st.write("Line{}: {}".format(count, line.strip()))
	return header, data

def getResDict(df_header, df_data):
	# Load up input dictionary
	anaDict={k:None for k in bkgSensorIV_Methods_CommonConfig.datafields}
	for c in df_header['key'].to_list():
		for k in anaDict.keys():
			if c.lower() in k.lower():
				anaDict[k]=df_header.query('key=="'+c+'"')['value'].values[0]
	anaDict['Filepath']="someString"
	anaDict['Filename']="someString"
	anaDict['TABLE_Current[nA]']=df_data['current'].astype(float).to_list()
	anaDict['TestType']="ATLAS18_IV_TEST_V1"
	anaDict['Type']=df_header.query('key=="'+"Type"+'"')['value'].values[0]
	anaDict['TABLE_Voltage[V]']=[round(float(x)) for x in df_data['voltage'].astype(float).to_list()]
	# Get test info
	resDict=bkgModuleIV_Methods_Analysis.Analyze_IV(anaDict)
	return resDict

