import json
'''
{
  "component": "...",
  "testType": "ASIC_METROLOGY",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-01-06T19:26:18.770Z",
  "passed": true,
  "problems": false,
  "properties": {
	"USER": null,
	"SETUP": null,
	"SCRIPT_VERSION": null
  },
  "results": {
	"POSITION": {},
	"HEIGHT": {},
	"TOTAL_HEIGHT": {},
	"TILT": {}
  }
}
'''
def fillTestSchema(testSchema):
	#already filled out from json file
	return testSchema