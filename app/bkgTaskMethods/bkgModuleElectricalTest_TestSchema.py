from app.bkgTaskMethods import commonFunctions
import json

def getTestType(fileSchema, thermalCycled):
	testType = fileSchema['testType']
	if testType == "STROBE_DELAY_PPA":
		if thermalCycled == 'TRUE': testType = "STROBE_DELAY_TC"
		else: pass
	if testType == "RESPONSE_CURVE_PPA":
		if thermalCycled == 'TRUE': testType = "RESPONSE_CURVE_TC"
		else: pass
	if testType == "PEDESTAL_TRIM_PPA":
		if thermalCycled == 'TRUE': testType = "PEDESTAL_TRIM_TC"
		else: pass
	if testType == "NO_PPA":
		if thermalCycled == 'TRUE': testType = "NO_TC"
		else: pass
	return testType

def fillTestSchema(fileSchema, testSchema, testType):
	if testSchema['testType']=="STROBE_DELAY_PPA": 
		testSchema = fillStrobeDelay(fileSchema, testSchema, testType)
	elif testSchema['testType']=="RESPONSE_CURVE_PPA": 
		testSchema = fillResponseCurve(fileSchema, testSchema, testType)
	elif testSchema['testType']=="PEDESTAL_TRIM_PPA": 
		testSchema = fillPedestalTrim(fileSchema, testSchema, testType)
	elif testSchema['testType']=="NO_PPA": 
		testSchema = fillNoise(fileSchema, testSchema, testType)
	if testSchema['testType']=="STROBE_DELAY_TC": 
		testSchema = fillTestTC(fileSchema, testSchema, testType)
	elif testSchema['testType']=="RESPONSE_CURVE_TC": 
		testSchema = fillTestTC(fileSchema, testSchema, testType)
	elif testSchema['testType']=="PEDESTAL_TRIM_TC": 
		testSchema = fillTestTC(fileSchema, testSchema, testType)
	elif testSchema['testType']=="NO_TC": 
		testSchema = fillTestTC(fileSchema, testSchema, testType)
	return testSchema
'''
{
  "component": "...",
  "testType": "STROBE_DELAY_PPA",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:24:22.269Z",
  "passed": true,
  "problems": false,
  "properties": {
	"DCS": null,
	"FRACTION": null,
	"det_info": null,
	"system_info": null,
	"scan_info": null
  },
  "results": {
	"StrobeDelay_away": [
	  0,
	  0
	],
	"StrobeDelay_under": [
	  0,
	  0
	]
  }
}

{
  "component": "...",
  "testType": "STROBE_DELAY_TC",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:39:17.203Z",
  "passed": true,
  "problems": false,
  "properties": {
	"itsdaq_test_info": null,
	"DCS": null,
	"det_info": null,
	"scan_info": null,
	"system_info": null,
	"FRACTION": null
  },
  "results": {
	"StrobeDelay_under": [
	  0,
	  0
	],
	"StrobeDelay_away": [
	  0,
	  0
	]
  }
}
'''
def fillStrobeDelay(fileSchema, testSchema, testType):
	testSchema['component'] = fileSchema['component']
	testSchema["testType"] = testType
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema["runNumber"] = fileSchema["runNumber"]
	testSchema['date'] = fileSchema['date']
	testSchema['passed'] = fileSchema['passed']
	testSchema['problems'] = fileSchema['problems']
	testSchema['properties']['DCS'] = fileSchema['properties']['DCS']
	if testType == "STROBE_DELAY_TC":
		testSchema['properties']["itsdaq_test_info"] = fileSchema['properties']["itsdaq_test_info"]
	testSchema['properties']["det_info"] = fileSchema['properties']["det_info"]
	testSchema['properties']["scan_info"] = fileSchema['properties']["scan_info"]
	testSchema['properties']["system_info"] = fileSchema['properties']["system_info"]
	testSchema['properties']["FRACTION"] = fileSchema['properties']["FRACTION"]
	testSchema['results']["StrobeDelay_under"] = fileSchema['results']["StrobeDelay_under"]
	testSchema['results']["StrobeDelay_away"] = fileSchema['results']["StrobeDelay_away"]
	return testSchema
'''
{
  "component": "...",
  "testType": "NO_PPA",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:32:35.696Z",
  "passed": true,
  "problems": false,
  "properties": {
	"DCS": null,
	"det_info": null,
	"system_info": null,
	"scan_info": null
  },
  "results": {
	"enc_est_away": [
	  0,
	  0
	],
	"enc_est_under": [
	  0,
	  0
	],
	"occupancy_mean_away": [
	  0,
	  0
	],
	"occupancy_mean_under": [
	  0,
	  0
	],
	"occupancy_rms_away": [
	  0,
	  0
	],
	"occupancy_rms_under": [
	  0,
	  0
	],
	"offset_away": [
	  0,
	  0
	],
	"offset_under": [
	  0,
	  0
	]
  }
}

{
  "component": "...",
  "testType": "NO_TC",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:37:23.168Z",
  "passed": true,
  "problems": false,
  "properties": {
	"itsdaq_test_info": null,
	"DCS": null,
	"det_info": null,
	"scan_info": null,
	"system_info": null
  },
  "results": {
	"enc_est_under": [
	  0,
	  0
	],
	"enc_est_away": [
	  0,
	  0
	],
	"offset_under": [
	  0,
	  0
	],
	"offset_away": [
	  0,
	  0
	],
	"occupancy_rms_under": [
	  0,
	  0
	],
	"occupancy_rms_away": [
	  0,
	  0
	],
	"occupancy_mean_under": [
	  0,
	  0
	],
	"occupancy_mean_away": [
	  0,
	  0
	]
  }
}
'''

def fillNoise(fileSchema, testSchema, testType):
	testSchema['component'] = fileSchema['component']
	testSchema["testType"] = testType
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema["runNumber"] = fileSchema["runNumber"]
	testSchema['date'] = fileSchema['date']
	testSchema['passed'] = fileSchema['passed']
	testSchema['problems'] = fileSchema['problems']
	testSchema['properties']['DCS'] = fileSchema['properties']['DCS']
	if testType == "NO_TC":
		testSchema['properties']["itsdaq_test_info"] = fileSchema['properties']["itsdaq_test_info"]
	testSchema['properties']["det_info"] = fileSchema['properties']["det_info"]
	testSchema['properties']["scan_info"] = fileSchema['properties']["scan_info"]
	testSchema['properties']["system_info"] = fileSchema['properties']["system_info"]
	testSchema['results']["enc_est_under"] = fileSchema['results']["enc_est_under"]
	testSchema['results']["enc_est_away"] = fileSchema['results']["enc_est_away"]
	testSchema['results']["offset_under"] = fileSchema['results']["offset_under"]
	testSchema['results']["offset_away"] = fileSchema['results']["offset_away"]
	testSchema['results']["occupancy_rms_under"] = fileSchema['results']["occupancy_rms_under"]
	testSchema['results']["occupancy_rms_away"] = fileSchema['results']["occupancy_rms_away"]
	testSchema['results']["occupancy_mean_under"] = fileSchema['results']["occupancy_mean_under"]
	testSchema['results']["occupancy_mean_away"] = fileSchema['results']["occupancy_mean_away"]
	return testSchema
'''
{
  "component": "...",
  "testType": "RESPONSE_CURVE_PPA",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:43:32.248Z",
  "passed": true,
  "problems": false,
  "properties": {
	"det_info": null,
	"fit_type_code": null,
	"scan_info": null,
	"special_point_index": null,
	"system_info": null,
	"DCS": null
  },
  "results": {
	"gain_away": [
	  0,
	  0
	],
	"gain_mean_away": [
	  0,
	  0
	],
	"gain_mean_under": [
	  0,
	  0
	],
	"gain_rms_away": [
	  0,
	  0
	],
	"gain_rms_under": [
	  0,
	  0
	],
	"gain_under": [
	  0,
	  0
	],
	"innse_away": [
	  0,
	  0
	],
	"innse_mean_away": [
	  0,
	  0
	],
	"innse_mean_under": [
	  0,
	  0
	],
	"innse_rms_away": [
	  0,
	  0
	],
	"innse_rms_under": [
	  0,
	  0
	],
	"innse_under": [
	  0,
	  0
	],
	"offset_mean_away": [
	  0,
	  0
	],
	"offset_mean_under": [
	  0,
	  0
	],
	"offset_rms_away": [
	  0,
	  0
	],
	"offset_rms_under": [
	  0,
	  0
	],
	"outnse_away": [
	  0,
	  0
	],
	"outnse_under": [
	  0,
	  0
	],
	"rc_fit_away": [
	  0,
	  0
	],
	"rc_fit_under": [
	  0,
	  0
	],
	"vt50_away": [
	  0,
	  0
	],
	"vt50_mean_away": [
	  0,
	  0
	],
	"vt50_mean_under": [
	  0,
	  0
	],
	"vt50_rms_away": [
	  0,
	  0
	],
	"vt50_rms_under": [
	  0,
	  0
	],
	"vt50_under": [
	  0,
	  0
	]
  }

  {
  "component": "...",
  "testType": "RESPONSE_CURVE_TC",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:44:11.867Z",
  "passed": true,
  "problems": false,
  "properties": {
	"itsdaq_test_info": null,
	"DCS": null,
	"det_info": null,
	"scan_info": null,
	"system_info": null,
	"fit_type_code": null,
	"special_point_index": null
  },
  "results": {
	"innse_under": [
	  0,
	  0
	],
	"vt50_under": [
	  0,
	  0
	],
	"vt50_rms_under": [
	  0,
	  0
	],
	"vt50_rms_away": [
	  0,
	  0
	],
	"vt50_mean_under": [
	  0,
	  0
	],
	"vt50_mean_away": [
	  0,
	  0
	],
	"vt50_away": [
	  0,
	  0
	],
	"innse_mean_under": [
	  0,
	  0
	],
	"innse_mean_away": [
	  0,
	  0
	],
	"innse_rms_under": [
	  0,
	  0
	],
	"innse_rms_away": [
	  0,
	  0
	],
	"innse_away": [
	  0,
	  0
	],
	"gain_under": [
	  0,
	  0
	],
	"gain_mean_under": [
	  0,
	  0
	],
	"gain_mean_away": [
	  0,
	  0
	],
	"gain_rms_under": [
	  0,
	  0
	],
	"gain_rms_away": [
	  0,
	  0
	],
	"gain_away": [
	  0,
	  0
	],
	"offset_rms_under": [
	  0,
	  0
	],
	"offset_rms_away": [
	  0,
	  0
	],
	"offset_mean_under": [
	  0,
	  0
	],
	"offset_mean_away": [
	  0,
	  0
	]
  }
}
'''

def fillResponseCurve(fileSchema, testSchema, testType):
	testSchema['component'] = fileSchema['component']
	testSchema["testType"] = testType
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema["runNumber"] = fileSchema["runNumber"]
	testSchema['date'] = fileSchema['date']
	testSchema['passed'] = fileSchema['passed']
	testSchema['problems'] = fileSchema['problems']

	if testType == "RESPONSE_CURVE_TC":
		testSchema['properties']["itsdaq_test_info"] = fileSchema['properties']["itsdaq_test_info"]
	testSchema['properties']['DCS'] = fileSchema['properties']['DCS']
	testSchema['properties']["det_info"] = fileSchema['properties']["det_info"]
	testSchema['properties']["scan_info"] = fileSchema['properties']["scan_info"]
	testSchema['properties']["system_info"] = fileSchema['properties']["system_info"]
	testSchema['properties']["fit_type_code"] = fileSchema['properties']["fit_type_code"]
	testSchema['properties']["special_point_index"] = fileSchema['properties']["special_point_index"]

	if testType == "RESPONSE_CURVE_PPA":
		testSchema['results']["outnse_away"] = fileSchema['results']["outnse_away"]
		testSchema['results']["outnse_under"] = fileSchema['results']["outnse_under"]
		testSchema['results']["rc_fit_away"] = fileSchema['results']["rc_fit_away"]
		testSchema['results']["rc_fit_under"] = fileSchema['results']["rc_fit_under"]		
	testSchema['results']['innse_under'] = fileSchema['results']['innse_under']
	testSchema['results']["vt50_under"] = fileSchema['results']["vt50_under"]
	testSchema['results']["vt50_rms_under"] = fileSchema['results']["vt50_rms_under"]
	testSchema['results']["vt50_rms_away"] = fileSchema['results']["vt50_rms_away"]
	testSchema['results']["vt50_mean_away"] = fileSchema['results']["vt50_mean_away"]
	testSchema['results']["vt50_mean_under"] = fileSchema['results']["vt50_mean_under"]
	testSchema['results']["vt50_away"] = fileSchema['results']["vt50_away"]
	testSchema['results']["innse_mean_under"] = fileSchema['results']["innse_mean_under"]
	testSchema['results']["innse_mean_away"] = fileSchema['results']["innse_mean_away"]
	testSchema['results']["innse_rms_under"] = fileSchema['results']["innse_rms_under"]
	testSchema['results']["innse_rms_away"] = fileSchema['results']["innse_rms_away"]
	testSchema['results']["innse_away"] = fileSchema['results']["innse_away"]
	testSchema['results']["gain_under"] = fileSchema['results']["gain_under"]
	testSchema['results']["gain_mean_under"] = fileSchema['results']["gain_mean_under"]
	testSchema['results']["gain_mean_away"] = fileSchema['results']["gain_mean_away"]
	testSchema['results']["gain_rms_under"] = fileSchema['results']["gain_rms_under"]
	testSchema['results']["gain_rms_away"] = fileSchema['results']["gain_rms_away"]
	testSchema['results']["gain_away"] = fileSchema['results']["gain_away"]
	testSchema['results']["offset_rms_under"] = fileSchema['results']["offset_rms_under"]
	testSchema['results']["offset_rms_away"] = fileSchema['results']["offset_rms_away"]
	testSchema['results']["offset_mean_under"] = fileSchema['results']["offset_mean_under"]
	testSchema['results']["offset_mean_away"] = fileSchema['results']["offset_mean_away"]
	return testSchema

'''
{
  "component": "...",
  "testType": "PEDESTAL_TRIM_PPA",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:44:46.941Z",
  "passed": true,
  "problems": false,
  "properties": {
	"DCS": null,
	"det_info": null,
	"system_info": null,
	"trim_offset": null,
	"trim_range": null,
	"scan_info": null
  },
  "results": {
	"trim_away": [
	  0,
	  0
	],
	"trim_under": [
	  0,
	  0
	]
  }
}

{
  "component": "...",
  "testType": "PEDESTAL_TRIM_TC",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-04T19:45:08.281Z",
  "passed": true,
  "problems": false,
  "properties": {
	"itsdaq_test_info": null,
	"DCS": null,
	"det_info": null,
	"scan_info": null,
	"system_info": null,
	"trim_offset": null,
	"trim_range": null
  },
  "results": {
	"trim_under": [
	  0,
	  0
	],
	"trim_away": [
	  0,
	  0
	]
  }
}
'''

def fillPedestalTrim(fileSchema, testSchema, testType):
	print('###################################################################################################')
	print(testSchema)
	print(fileSchema)
	print('###################################################################################################')
	testSchema['component'] = fileSchema['component']
	testSchema["testType"] = testType
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema["runNumber"] = fileSchema["runNumber"]
	testSchema['date'] = fileSchema['date']
	testSchema['passed'] = fileSchema['passed']
	testSchema['problems'] = fileSchema['problems']

	if testType == "PEDESTAL_TRIM_TC":
		testSchema['properties']["itsdaq_test_info"] = fileSchema['properties']["itsdaq_test_info"]
	testSchema['properties']['DCS'] = fileSchema['properties']['DCS']
	testSchema['properties']["det_info"] = fileSchema['properties']["det_info"]
	testSchema['properties']["scan_info"] = fileSchema['properties']["scan_info"]
	testSchema['properties']["system_info"] = fileSchema['properties']["system_info"]
	testSchema['properties']["trim_offset"] = fileSchema['properties']["trim_offset"]
	testSchema['properties']["trim_range"] = fileSchema['properties']["trim_range"]

	testSchema['results']["trim_under"] = fileSchema['results']["trim_under"]
	testSchema['results']["trim_away"] = fileSchema['results']["trim_away"]
	print('###################################################################################################')
	print(testSchema)
	print(fileSchema)
	print('###################################################################################################')
	return testSchema

'''
{
  "component": "...",
  "testType": "STROBE_DELAY_TC",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-02-04T18:28:54.276Z",
  "passed": true,
  "problems": false,
  "properties": {
    "itsdaq_test_info": null,
    "DCS": null,
    "det_info": null,
    "scan_info": null,
    "system_info": null,
    "FRACTION": null
  },
  "results": {
    "StrobeDelay_under": [
      0,
      0
    ],
    "StrobeDelay_away": [
      0,
      0
    ]
  }
}'''

def fillTestTC(fileSchema, testSchema, testType):
	testSchema.update(fileSchema)
	return testSchema
