
import pandas as pd
from app.bkgTaskMethods import bkgSensorIV_Methods_CommonConfig
from app.bkgTaskMethods import bkgSensorIV_Methods_Analysis
import json

def updateFiles(files):
    files = [x for x in files.split(';') if x!='']
    newFiles = []
    for f in files:
        newFileName = f
        if "NEXT.json" in f:
            # Update file name
            newFileName = newFileName.replace("NEXT.json", "PPA.json")
            # Update testType in file
            openedFile = open(f)
            testSchema = json.load(openedFile)
            openedFile.close()
            testSchema["testType"] = testSchema["testType"].replace("NEXT", "PPA")
            jsonObject = json.dumps(testSchema, indent=2)
            outputJsonFile = open(newFileName, "w")
            outputJsonFile.write(jsonObject)
            outputJsonFile.close()
            newFiles.append(newFileName)
        else:
            newFiles.append(f)
    return ';'.join(newFiles)