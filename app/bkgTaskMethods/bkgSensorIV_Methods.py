
import pandas as pd
from app.bkgTaskMethods import bkgSensorIV_Methods_CommonConfig
from app.bkgTaskMethods import bkgSensorIV_Methods_Analysis

def getFileData(file):
	# Get file header and data
	print("The file is")
	print(file)
	f = open(file, 'r')
	headerList, dataList = GetGleanList(f)
	f.close()
	# Set header
	df_header = pd.DataFrame(headerList, columns=['key','value'])
	df_header['key']=df_header['key'].str.replace(':','')
	df_header.fillna('', inplace=True)
	df_data = pd.DataFrame(dataList, columns=['voltage', 'current', 'shunt'])
	# Get component sn
	serialNumber = df_header.query('key=="Component"')['value'].values[0]
	return serialNumber, df_header, df_data

def GetGleanList(fileObj):
	header=[]
	data=[]
	change = False

	for raw_line in fileObj.readlines():
		#st.write("raw line:",line)
		line = ""
		if type(raw_line) != str:
			line=raw_line.decode("ISO-8859-1")
		else:
			line=raw_line
		#st.write(line)
		if line == "":
			break

		if change==False:
			if "Shunt" in line:
				change=True
				continue
			header.append([x.strip() for x in line.split(': ')])
		elif change==True:
			data.append([x.strip() for x in line.split('\t')])
		#st.write("Line{}: {}".format(count, line.strip()))
	return header, data

def getResDict(df_header, df_data):
	# Load up input dictionary
	anaDict={k:None for k in bkgSensorIV_Methods_CommonConfig.datafields}
	for c in df_header['key'].to_list():
		for k in anaDict.keys():
			if c.lower() in k.lower():
				anaDict[k]=df_header.query('key=="'+c+'"')['value'].values[0]
	anaDict['Filepath']="someString"
	anaDict['Filename']="someString"
	anaDict['TABLE_Current[nA]']=df_data['current'].astype(float).to_list()
	anaDict['TestType']="ATLAS18_IV_TEST_V1"
	anaDict['Type']=df_header.query('key=="'+"Type"+'"')['value'].values[0]
	anaDict['TABLE_Voltage[V]']=[round(float(x)) for x in df_data['voltage'].astype(float).to_list()]
	# Get test info
	resDict=bkgSensorIV_Methods_Analysis.Analyze_IV(anaDict)
	return resDict

