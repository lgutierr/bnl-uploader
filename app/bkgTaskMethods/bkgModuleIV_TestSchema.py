import app.dbActions as dbActions
from datetime import datetime
from app.bkgTasks import bkgModuleIV_Methods
import pandas as pd
from app.bkgTaskMethods.bkgSensorIV_Methods_CommonConfig import datafields
import json
'''
{
	"component": "...",
	"testType": "MODULE_IV_AMAC",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-11-06T20:02:40.461Z",
	"passed": true,
	"problems": false,
	"properties": {
		"system_info": null,
		"AMAC_READINGS": null,
		"DCS": null,
		"det_info": null,
		"AMAC_CURRENT_RANGE": [
			null,
			null
		],
		"scan_info": null,
		"sensor_type": null
	},
	"results": {
		"TEMPERATURE": 0,
		"I_500V": 0,
		"HUMIDITY": 0,
		"VBD": -1,
		"CURRENT": [
			0,
			0
		],
		"VOLTAGE": [
			0,
			0
		],
		"CURRENT_RMS": [
			0,
			0
		],
		"PS_CURRENT": [
			0,
			0
		]
	}
}
'''

'''
{
	"component": "...",
	"testType": "MODULE_IV_PS_BONDED",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-11-06T20:04:06.420Z",
	"passed": true,
	"problems": false,
	"properties": {
		"VBIAS_SMU": null,
		"RSERIES": null,
		"TEST_DMM": null,
		"RSHUNT": null,
		"RUNNUMBER": null,
		"COMMENTS": null,
		"ALGORITHM_VERSION": null,
		"SOFTWARE_TYPE_VERSION": null,
		"MODULE_STAGE": null
	},
	"results": {
		"TEMPERATURE": -1,
		"I_500V": -1,
		"SHUNT_VOLTAGE": [
			-1,
			-1
		],
		"HUMIDITY": -1,
		"VBD": -1,
		"CURRENT": [
			-1,
			-1
		],
		"RMS_STABILITY": -1,
		"VOLTAGE": [
			-1,
			-1
		]
	}
}
'''
'''
{
	"component": "...",
	"testType": "MODULE_IV_PS_V1",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-11-06T20:04:29.440Z",
	"passed": true,
	"problems": false,
	"properties": {
		"VBIAS_SMU": null,
		"RSERIES": null,
		"TEST_DMM": null,
		"RSHUNT": null,
		"RUNNUMBER": null,
		"COMMENTS": null,
		"ALGORITHM_VERSION": null,
		"SOFTWARE_TYPE_VERSION": null,
		"MODULE_STAGE": null
	},
	"results": {
		"TEMPERATURE": 0,
		"I_500V": 0,
		"SHUNT_VOLTAGE": [
			0,
			0
		],
		"HUMIDITY": 0,
		"VBD": 0,
		"CURRENT": [
			0,
			0
		],
		"RMS_STABILITY": 0,
		"VOLTAGE": [
			0,
			0
		]
	}
}
'''
def fillTestSchema(file, serialNumber, comp, testType, stage, df_header, df_data, testSchema):
	print("In test Schema")
	print(file, serialNumber, comp, testType, stage, df_header, df_data, testSchema) 
	if (testType == "MODULE_IV_PS_V1") or (testType == "MODULE_IV_PS_BONDED"):
		# Modify test schema properties
		testSchema['component']= serialNumber
		testSchema['institution']= df_header.query('key=="Institute"')['value'].values[0]
		testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stage)+1)
		dateStr=df_header.query('key=="Date"')['value'].values[0]+"@"+df_header.query('key=="Time"')['value'].values[0]
		testSchema['date']= datetime.strptime(dateStr, "%d %b %Y@%H:%M:%S").strftime("%Y-%m-%dT%H:%M:%S.000Z")
		#Intentionally don't filled out "testSchema[passed]" for now
		#Intentionally don't filled out "testSchema[problems]" for now	
		resDict = bkgModuleIV_Methods.getResDict(df_header, df_data)

		testSchema['properties']['VBIAS_SMU']=df_header.query('key=="Vbias_SMU"')['value'].values[0]
		testSchema['properties']['RSERIES']=df_header.query('key=="Rseries"')['value'].values[0]
		testSchema['properties']['TEST_DMM']=df_header.query('key=="Test_DMM"')['value'].values[0]
		testSchema['properties']['RSHUNT']=df_header.query('key=="Rshunt"')['value'].values[0]
		testSchema['properties']['RUNNUMBER']=df_header.query('key=="RunNumber"')['value'].values[0]
		testSchema['properties']['COMMENTS']=df_header.query('key=="Comments"')['value'].values[0]
		testSchema['properties']['ALGORITHM_VERSION']=resDict['AlgorithmVersion']
		try: testSchema['properties']['SOFTWARE_TYPE_VERSION']=df_header.query('key=="Software type and version, fw version"')['value'].values[0]
		except: testSchema['properties']['SOFTWARE_TYPE_VERSION']=df_header.query('key=="Software type and version/ fw version"')['value'].values[0]

		testSchema['results']['TEMPERATURE']=float(df_header.query('key=="Temperature"')['value'].values[0])
		testSchema['results']['I_500V']=resDict['IDataAtTestV']
		testSchema['results']['SHUNT_VOLTAGE']=df_data['shunt'].astype(float).to_list()
		testSchema['results']['HUMIDITY']=float(df_header.query('key=="Humidity"')['value'].values[0])
		testSchema['results']['VBD']=float(resDict['MicroDischargeV'])
		testSchema['results']['CURRENT']=df_data['current'].astype(float).to_list()
		testSchema['results']['RMS_STABILITY']=resDict['RMS_Stability[nA]']
		testSchema['results']['VOLTAGE']=df_data['voltage'].astype(float).to_list()

	else: #testType == "MODULE_IV_AMAC"
		# Get file header
		f = open(file, 'r')
		testSchema = json.load(f)
		testSchema['testType']="MODULE_IV_AMAC"
		f.close()
		# #Intentionally don't fill out "testSchema[passed]" for now
		# #Intentionally don't fill out "testSchema[problems]" for now	

	return testSchema