import os
import shutil
import logging
import pytz
from datetime import datetime
from flask import url_for

def bkgTestDatabaseLog(exception):
	# Create unique log by timestamping it
	ny_tz = pytz.timezone('America/New_York')
	ny_time = datetime.now(ny_tz)
	timestamp = ny_time.strftime("%Y-%m-%d_%H-%M-%S")
	log_file_name = f'error_log_{timestamp}.txt'
	log_directory = '/tmp'
	log_file_path = os.path.join(log_directory, log_file_name)
	logging.basicConfig(filename=log_file_path, level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
	logging.exception(str(exception))
	return log_file_path

def testFunction(functionDict, key, file, bkgFunction, *bkgFunctionParams):
	functionDict[key] = []
	
	if file != '':
		new_file_path = os.path.join('/tmp', os.path.basename(file))
		shutil.copy(file, new_file_path)
	try:
		output = bkgFunction(*bkgFunctionParams)

		if not output['errors']:
			functionDict[key] = {
				'status': 'Pass', 
				'message': 'Upload successful', 
				'checkmark': True}
		else:
			error_messages = "<br>".join([f"{number + 1}. {error}" for number, error in enumerate(output['errors'])])
			functionDict[key] = {
				'status': 'Fail', 
				'message': f'Upload failed - the errors were the following:\n{error_messages}', 
				'checkmark': False}
	except Exception as exception:
			log_file_name = bkgTestDatabaseLog(exception)
			log_file_url = url_for('download_log', filename=log_file_name)
			functionDict[key]= {
				'status': 'Fail',
				'message': f'Upload failed - please see log: <a href="{log_file_url}" style="color: white;">Download Log</a> ',
				'checkmark': False
			}

'''

Uploads on a test module

Module SN = 20USBML1234687
Star Hybrid Assmebly = 20USBHX2000757
	HCC = 20USGAH0012095
	ABC = 20USGAA1004159
	Hybrid Flex Array = 20USB100000149
		Star Hybrid Flex = GPC1938_X_006_B_H3
Sensor = 20USBSL0000171
Powerboard = 20USBP03000133
	Powerboard AMAC = 20USG000501145
'''

def testUploader_bkgPullTest_ASIC(functionDict):
	from app.bkgTasks import bkgPullTest
	key = 'PullTest_ASIC'
	file = 'Documentation/Example_Files/Pull_Tests/PositionA.csv'
	functionParams = ['20USGAA1004159', 'asic', '/tmp/PositionA.csv']
	testFunction(functionDict, key, file, bkgPullTest, *functionParams)
	return functionDict

# def testUploader_bkgPullTest_Cicorel(functionDict):
# 	from app.bkgTasks import bkgPullTest
# 	key = 'PullTest_Cicorel'
# 	file = 'Documentation/Example_Files/Pull_Tests/PositionA.csv'
# 	functionParams = ['', 'cicorel', '/tmp/PositionA.csv']
# 	testFunction(functionDict, key, file, bkgPullTest, *functionParams)
# 	return functionDict

# def testUploader_bkgPullTest_Halfmoon(functionDict):
# 	from app.bkgTasks import bkgPullTest
# 	key = 'PullTest_Halfmoon'
# 	file = 'Documentation/Example_Files/Pull_Tests/PositionA.csv'
# 	functionParams = ['', "halfmoon", '/tmp/PositionA.csv']
# 	testFunction(functionDict, key, file, bkgPullTest, *functionParams)
# 	return functionDict

def testUploader_bkgPullTest_HybridFlexArray(functionDict):
	from app.bkgTasks import bkgPullTest
	key = 'PullTest_HybridFlexArray'
	file = 'Documentation/Example_Files/Pull_Tests/PositionA.csv'
	functionParams = ['20USB100000149', "hybridflexarray", '/tmp/PositionA.csv']
	testFunction(functionDict, key, file, bkgPullTest, *functionParams)
	return functionDict

# def testUploader_bkgPullTest_Powerboard(functionDict):
# 	from app.bkgTasks import bkgPullTest
# 	key = 'PullTest_Powerboard'
# 	file = 'Documentation/Example_Files/Pull_Tests/PositionA.csv'
# 	functionParams = ['5000086', "powerboard", '/tmp/PositionA.csv']
# 	testFunction(functionDict, key, file, bkgPullTest, *functionParams)
# 	return functionDict

# def testUploader_bkgHVtabShearTest(functionDict):
# 	from app.bkgTasks import bkgHVtabShearTest
# 	key = 'HVtabShearTest'
# 	file = ''
# 	functionParams = ['','3.4','Test guy','Before module tabbing','half-moon']
# 	testFunction(functionDict, key, file, bkgHVtabShearTest, *functionParams)
# 	return functionDict

def testUploader_bkgVisualInspection_HVTab(functionDict):
	from app.bkgTasks import bkgVisualInspection
	key = 'VisualInspection_HVTab'
	functionParams = ['', 'passed', stage, reception, "HV_TAB_SHEET"]
	testFunction(functionDict, key, bkgVisualInspection, *functionParams)
	return functionDict