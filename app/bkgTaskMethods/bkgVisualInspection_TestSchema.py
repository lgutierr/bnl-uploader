import time
from datetime import datetime
from app.bkgTaskMethods import commonFunctions
import app.dbActions as dbActions
from datetime import datetime, timezone

'''
For on-site visual inspection:
{
  "component": "...",
  "testType": "VISUAL_INSPECTION",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-10-24T16:58:56.491Z",
  "passed": true,
  "problems": false
}

For module visual inspection
{
  "component": "...",
  "testType": "VISUAL_INSPECTION_RECEPTION",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-10-24T17:05:10.486Z",
  "passed": true,
  "problems": false
}
'''

def fillTestSchema(comp, testType, stage, serialNumber, testSchema, result):
  print(comp, testType, stage, serialNumber, testSchema, result)
  testSchema["component"] = serialNumber
  testSchema["testType"] = testType
  testSchema["institution"] = commonFunctions.getInstitute()
  testSchema["runNumber"] = str(dbActions.getRunNumber(comp, testType, stage)+1)
  testSchema["date"] = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
  if result == 'passed':
    testSchema["passed"] = True
  else:
    testSchema["passed"] = False
  testSchema["problems"] = False
  return testSchema