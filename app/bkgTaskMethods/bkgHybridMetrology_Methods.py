import app.dbActions as dbActions
import os
from datetime import datetime
import re
from collections import OrderedDict
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from app.bkgTaskMethods import commonFunctions

def checkLocalName(identifier):
	localName = None
	try: 
		comp = dbActions.getCompStatus(identifier)
		comp = comp[0]
		for i in range(len(comp['properties'])):
			if comp['properties'][i]['code'] == 'LOCAL_NAME':
				tmpLocalName = comp['properties'][i]['value']
				try:
					localNameParts = tmpLocalName.split("-")
					if len(localNameParts)>2 and ("GPC" in localNameParts[0] or ("BNL" in localNameParts[0])):
						localName = tmpLocalName
						break
				except:
					pass
	except:
		pass
	return localName
	
def metrologyReadFile(filePath):
	# Output
	output = dict()
	output['data'] = None
	# Open metrology file and extract information
	f = open(filePath)
	lines = f.readlines()
	reportlines = []
	single_points = []
	latestStepName = ""
	for i in range(len(lines)):
		attributes = []
		actualvals = []
		if 'Step Name' in lines[i]:
			name = lines[i].rstrip().split()[2]
			# Skip lines and move towards data
			i += 4
			# Read data
			readline = lines[i]
			while readline != '\n':
				splitline = readline.rstrip().split()
				attributes.append(splitline[0])
				actualvals.append(splitline[3])
				attributes.append("Nominal {}".format(splitline[0]))
				actualvals.append(splitline[2])
				i += 1
				readline = lines[i]
			single_points.append(pd.DataFrame(columns=attributes, index=[name]))
			single_points[-1].loc[name] = actualvals
			# Store latest step name
			latestStepName = name
		elif 'Finder Name' in lines[i]:
			# Skip line and move towards data
			i += 2
			# Read data
			readline = lines[i]
			while 'Focus' in readline:
				splitline = readline.rstrip().split()
				name = latestStepName + "_P"+splitline[1]
				attributes.append("X")
				actualvals.append(splitline[2]) 
				attributes.append("Y")
				actualvals.append(splitline[3]) 
				attributes.append("Z")
				actualvals.append(splitline[4]) 
				single_points.append(pd.DataFrame(columns=attributes, index=[name]))
				single_points[-1].loc[name] = actualvals
				attributes = []
				actualvals = []
				i += 1
				readline = lines[i]
		elif lines[i] != '\n':
			reportlines.append(lines[i])
	# Convert file information into a DataFrame
	data = pd.concat(single_points, axis=0).astype(np.float64)
	output['data'] = data
	# Instrument information
	runtime = [s for s in reportlines if "Date/Time" in s][0].strip()
	dateTime = datetime.strptime(runtime, "Run Date/Time: %m:%d:%y %H:%M:%S")
	output['time'] = "{}-{}-{}T{}:{}:{}.000Z".format(str(dateTime.year), str(dateTime.month).zfill(2), str(dateTime.day).zfill(2), str(dateTime.hour).zfill(2), str(dateTime.minute).zfill(2), str(dateTime.second).zfill(2))
	return output
	
def calculateTilt(p1, p2):
	numerator = np.abs(p1.loc['Z'] - p2.loc['Z'])
	denominator = np.sqrt((p1.loc['X'] - p2.loc['X']) ** 2 + (p1.loc['Y'] - p2.loc['Y']) ** 2)
	tilt = numerator / denominator
	return tilt

def hybridMetrologyProcessing(serialNumber, filePath, hybridThickness=0.38):
	# Output
	createdFiles = []
	print("got to hybrid metrology processing")
	# Get component info
	comp = dbActions.getCompStatus(serialNumber)
	comp = comp[0]
	serialNumber = comp['serialNumber']
	localName = None
	for i in range(len(comp['properties'])):
		if comp['properties'][i]['code'] == 'LOCAL_NAME':
			tmpLocalName = comp['properties'][i]['value']
			print(tmpLocalName)
			try:
				print("now under try")
				localNameParts = tmpLocalName.split("-")
				print(localNameParts )
				if len(localNameParts) == 1:
					localNameParts = tmpLocalName.split("_")
					print(localNameParts)
				if len(localNameParts) > 2 and ("GPC" in localNameParts[0] or ("BNL" in localNameParts[0])):
					localName = tmpLocalName
					print(localName)
					break
			except:
				pass
	print(localName)
	localName = 'BNL_PP2_HX-242'
	hType = comp['type']['code']
	# Read file
	fileInfo = metrologyReadFile(filePath)
	data = fileInfo['data']
	# Variables for processing information
	heightscangroups = [
		[10, 11, 32, 33, 21, 22], [9, 10, 31, 32, 20, 21], [8, 9, 30, 31, 19, 20],
		[7, 8, 29, 30, 18, 19], [6, 7, 28, 29, 17, 18], [5, 6, 27, 28, 16, 17],
		[4, 5, 26, 27, 15, 16], [3, 4, 25, 26, 14, 15], [2, 3, 24, 25, 13, 14],
		[1, 2, 23, 24, 12, 13]
	]
	heightscangroups2 = [
		[10, 11, 21, 22], [9, 10, 20, 21], [8, 9, 19, 20],
		[7, 8, 18, 19], [6, 7, 17, 18], [5, 6, 16, 17],
		[4, 5, 15, 16], [3, 4, 14, 15], [2, 3, 13, 14],
		[1, 2, 12, 13]
	]
	heightgroups = ['TL', 'TR', 'BL', 'BR']
	heighthccgroups = ['TL', 'TR', 'BL', 'BR', 'TM', 'BM']
	# General header
	header = ["#---Header:\n"]
	header.append("{:<26}{}\n".format("EC or Barrel:", "SB"))
	header.append("{:<26}{}\n".format("Hybrid Type", hType))
	header.append("{:<26}{}\n".format("Hybrid ref. number", serialNumber))
	header.append("{:<26}{}\n".format("Date:", fileInfo['time']))
	header.append("{:<26}{}\n".format("Institute:", 'BNL'))
	header.append("{:<26}{}\n".format("Operator:", 'Meny Raviv Moshe'))
	header.append("{:<26}{}\n".format("Instrument Used:", 'OGP 320 Smartscope'))
	header.append("{:<26}{}\n".format("Test Run Number:", '1'))
	header.append("{:<26}{}\n".format("Measurement Program:", 'Hybrid-'+hType+"-ASIC-Metrology"))
	# General position scan data
	positionheaders = ['\n#---Position Scan:\n','{:<21}{:<21}Y[mm]\n'.format('#Location', 'X[mm]')]
	positionscandata = []
	print("got to hType = X")
	if hType == "X":
		# Hybrid points
		positionscandata = ["{:<21}{:<21.3f}{:0.3f}\n".format('H_{}_P1'.format(hType), 
								data.loc['H_{}_P1'.format(hType), 'X'], 
								data.loc['H_{}_P1'.format(hType), 'Y']),
								"{:<21}{:<21.3f}{:0.3f}\n".format('H_{}_P2'.format(hType), 
								data.loc['H_{}_P2'.format(hType), 'X'], 
								data.loc['H_{}_P2'.format(hType), 'Y'])]
		print("Made it past positionscandata")
		# ASIC data
		for i in range(9,-1,-1):
			positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format('ABC_{}_{}_P1'.format(hType, i),
																									data.loc['LAsic_{}'.format(i), 'X'],
																									data.loc['LAsic_{}'.format(i), 'Y']))
			positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format('ABC_{}_{}_P2'.format(hType, i),
																									data.loc['RAsic_{}'.format(i), 'X'],
																									data.loc['RAsic_{}'.format(i), 'Y']))
		# HCC data
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P1".format(hType),
																													 data.loc['Hcc_XY_TL', 'X'],
																													 data.loc['Hcc_XY_TL', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P2".format(hType),
																													 data.loc['Hcc_XY_TR', 'X'],
																													 data.loc['Hcc_XY_TR', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P3".format(hType),
																													 data.loc['Hcc_XY_BL', 'X'],
																													 data.loc['Hcc_XY_BL', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P4".format(hType),
																													 data.loc['Hcc_XY_BR', 'X'],
																													 data.loc['Hcc_XY_BR', 'Y']))
	elif hType == "Y":
		# Hybrid points
		positionscandata = ["{:<21}{:<21.3f}{:0.3f}\n".format('H_{}_P1'.format(hType), 
								data.loc['H_{}_P1'.format(hType), 'X'], 
								-data.loc['H_{}_P1'.format(hType), 'Y']),
								"{:<21}{:<21.3f}{:0.3f}\n".format('H_{}_P2'.format(hType), 
								data.loc['H_{}_P2'.format(hType), 'X'], 
								-data.loc['H_{}_P2'.format(hType), 'Y'])]
		# ASIC data
		for i in range(9,-1,-1):
			positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format('ABC_{}_{}_P1'.format(hType, i),
																									data.loc['ABC_Y_{}_P1'.format(i), 'X'],
																									-data.loc['ABC_Y_{}_P1'.format(i), 'Y']))
			positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format('ABC_{}_{}_P2'.format(hType, i),
																									data.loc['ABC_Y_{}_P2'.format(i), 'X'],
																									-data.loc['ABC_Y_{}_P2'.format(i), 'Y']))
		# HCC data
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P1".format(hType),
																													 data.loc['Hcc_XY_TL', 'X'],
																													 -data.loc['Hcc_XY_TL', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P2".format(hType),
																													 data.loc['Hcc_XY_TR', 'X'],
																													 -data.loc['Hcc_XY_TR', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P3".format(hType),
																													 data.loc['Hcc_XY_BL', 'X'],
																													 -data.loc['Hcc_XY_BL', 'Y']))
		positionscandata.append("{:<21}{:<21.3f}{:0.3f}\n".format("HCC_{}_0_P4".format(hType),
																													 data.loc['Hcc_XY_BR', 'X'],
																													 -data.loc['Hcc_XY_BR', 'Y']))
	# General height scan data
	heightheaders = ['\n#---Height Scan:\n',
									 '{:<21}{:<21}{:<21}{:<21}Z[mm]\n'.format('#Location', 'Type', 'X[mm]', 'Y[mm]')]
	heightscandata = []
	if hType == "X":
		# ASIC data
		for i in range(9, -1, -1):
			# Type 1 data
			for j in heightscangroups[i]:
				heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("ABC_{}_{}".format(hType, i), '1',
																																							data.loc['Hybrid{}'.format(j), 'X'],
																																							data.loc['Hybrid{}'.format(j), 'Y'],
																																							data.loc['Hybrid{}'.format(j), 'Z'] - hybridThickness))
			# Type 2 data
			for j in heightgroups:
				heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("ABC_{}_{}".format(hType, i), '2',
																																							data.loc['Asic_{}{}'.format(j,i), 'X'],
																																							data.loc['Asic_{}{}'.format(j,i), 'Y'],
																																							data.loc['Asic_{}{}'.format(j,i), 'Z']))
		# HCC data type 1 data
		for i in heighthccgroups:
			heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("HCC_{}_0".format(hType), '1',
																																						data.loc['Hcc_base_{}'.format(i), 'X'],
																																						data.loc['Hcc_base_{}'.format(i), 'Y'],
																																						data.loc['Hcc_base_{}'.format(i), 'Z'] - hybridThickness))
		# HCC data type 2 data
		for i in heightgroups:
			heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("HCC_{}_0".format(hType), '2',
																																							data.loc['HCC_{}'.format(i), 'X'],
																																							data.loc['HCC_{}'.format(i), 'Y'],
																																							data.loc['HCC_{}'.format(i), 'Z']))
	elif hType == "Y":
		# ASIC data
		for i in range(9, -1, -1):
			# Type 1 data
			for j in heightscangroups[-(i+1)]:
				heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("ABC_{}_{}".format(hType, i), '1',
																																							data.loc['Hybrid{}'.format(j), 'X'],
																																							-data.loc['Hybrid{}'.format(j), 'Y'],
																																							data.loc['Hybrid{}'.format(j), 'Z'] - hybridThickness))
			# Type 2 data
			for j in heightgroups:
				heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("ABC_{}_{}".format(hType, i), '2',
																																							data.loc['Asic_{}{}'.format(j,i), 'X'],
																																							-data.loc['Asic_{}{}'.format(j,i), 'Y'],
																																							data.loc['Asic_{}{}'.format(j,i), 'Z']))
		# HCC data type 1 data
		for i in heighthccgroups:
			heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("HCC_{}_0".format(hType), '1',
																																						data.loc['Hcc_base_{}'.format(i), 'X'],
																																						-data.loc['Hcc_base_{}'.format(i), 'Y'],
																																						data.loc['Hcc_base_{}'.format(i), 'Z'] - hybridThickness))
		# HCC data type 2 data
		for i in heightgroups:
			heightscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("HCC_{}_0".format(hType), '2',
																																							data.loc['HCC_{}'.format(i), 'X'],
																																							-data.loc['HCC_{}'.format(i), 'Y'],
																																							data.loc['HCC_{}'.format(i), 'Z']))
	# JIG scan
	jigdata = data.filter(regex=r'Jig_\d', axis=0)[['X','Y','Z']].to_numpy()
	jigscandata = []
	if hType == "X":
		for i in jigdata:
			jigscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("JIG", "0", i[0], i[1], i[2]))
	elif hType == "Y":
		for i in jigdata:
			jigscandata.append("{:<21}{:<21}{:<21.3f}{:<21.3f}{:0.3f}\n".format("JIG", "0", i[0], -i[1], i[2]))
	# Generate raw file
	rawFilePath = os.path.join("/tmp",serialNumber+"_"+localName+".txt")
	rawFile = open(rawFilePath, 'w')
	for item in header:
		rawFile.write(item)
	for item in positionheaders:
		rawFile.write(item)
	for item in positionscandata:
		rawFile.write(item)
	for item in heightheaders:
		rawFile.write(item)
	for item in heightscandata:
		rawFile.write(item)
	for item in jigscandata:
		rawFile.write(item)
	rawFile.close()
	createdFiles.append(rawFilePath)
	# Position deviation
	positionDeviationData = OrderedDict()
	deviationDataPoints = None
	if hType == "X":
		deviationDataPoints = data.filter(regex=r'[L,R]Asic_\d', axis=0)._append(
																	data.filter(regex=r'Hcc_XY_B[L,R]', axis=0))
	elif hType == "Y":
		deviationDataPoints = data.filter(regex=r'ABC_Y_\d_P\d', axis=0)._append(
																	data.filter(regex=r'Hcc_XY_B[L,R]', axis=0))
	xposdeviations = (1000*(deviationDataPoints.loc[:, 'Nominal X']-deviationDataPoints.loc[:, 'X'])).round(3)
	yposdeviations = (1000*(deviationDataPoints.loc[:, 'Nominal Y']-deviationDataPoints.loc[:, 'Y'])).round(3)
	passedPositionDeviation = xposdeviations.abs().le(100).any() and yposdeviations.abs().le(100).any()
	# ASIC deviation
	for i in range(10):
		if hType == 'X':
			p1data = [xposdeviations.loc['LAsic_{}'.format(i)], yposdeviations.loc['LAsic_{}'.format(i)]]
			p2data = [xposdeviations.loc['RAsic_{}'.format(i)], yposdeviations.loc['RAsic_{}'.format(i)]]
		elif hType == 'Y':
			p1data = [xposdeviations.loc['ABC_Y_{}_P1'.format(i)], yposdeviations.loc['ABC_Y_{}_P1'.format(i)]]
			p2data = [xposdeviations.loc['ABC_Y_{}_P2'.format(i)], yposdeviations.loc['ABC_Y_{}_P2'.format(i)]]
		positionDeviationData['ABC_'+hType+'_{}'.format(i)] = [p1data, p2data]
	# HCC deviation
	p1data = [xposdeviations.loc['Hcc_XY_BL'], yposdeviations.loc['Hcc_XY_BL']]
	p2data = [xposdeviations.loc['Hcc_XY_BR'], yposdeviations.loc['Hcc_XY_BR']]
	positionDeviationData['HCC_'+hType+'_0'] = [p1data, p2data]
	passedPosDevX = True
	passedPosDevY = True
	for k,v in positionDeviationData.items():
		for i in range(len(v)):
			if v[i][0] > 200 or v[i][0] < -200:
				passedPosDevX = passedPosDevX and False
			if v[i][1] > 200 or v[i][1] < -200:
				passedPosDevY = passedPosDevY and False
	passedPositionDeviation = passedPosDevX and passedPosDevY
	# Glue height calculation
	glueData = None
	averageGlueHeight = None
	if hType == 'X':
		# Get the Asic data for glue height and append HCC data
		glueData = data.filter(regex=r'Plane_Asic_\d', axis=0)['Z'].sort_index()
		averageGlueHeight = ((glueData.abs()-0.3)*1000).mean()
		glueData = glueData._append(pd.Series(data.loc['Plane____Hcc', 'Z'], index=['Plane____Hcc']))
		# Rename index values for the JSON file
		# There are two cases: one for ASIC and the else is for HCC
		renamescheme = lambda x: re.sub(r'Plane_Asic_(\d)(\w+)', r'ABC_\2_\1', x + hType) if 'Asic' in x \
				else 'HCC_' + hType + '_0'
	elif hType == 'Y':
		# Get the Asic data for glue height and append HCC data
		glueData = data.filter(regex=r'Plane_420um_\d', axis=0)['Z'].sort_index()
		averageGlueHeight = ((glueData.abs()-0.3)*1000).mean()
		glueData = glueData._append(pd.Series(data.loc['Plane_420um_Hcc', 'Z'], index=['Plane_420um_Hcc']))
		# Rename index values for the JSON file
		# There are two cases: one for ASIC and the else is for HCC
		renamescheme = lambda x: re.sub(r'Plane_420um_(\d)(\w+)', r'ABC_\2_\1', x + hType) if 'Hcc' not in x \
				else 'HCC_' + hType + '_0'
	glueData = (glueData.abs() - 0.3) * 1000
	glueData = glueData.rename(index=renamescheme)
	glueData = glueData.round(3).to_dict(OrderedDict)
	passedGlueHeight = averageGlueHeight < 160 and averageGlueHeight > 60
	#problemsGlueHeight = averageGlueHeight < 70 and averageGlueHeight > 40
	# Package height
	packageHeightData = None
	if hType == 'X':
		# Get the Asic data for glue height and append HCC data
		packageHeightData = data.filter(regex=r'Plane_Asic__\d\d', axis=0)['Z'].sort_index()
		packageHeightData = packageHeightData._append(pd.Series(data.loc['Plane__Hcc1', 'Z'], index=['Plane__Hcc1']))
		# Rename index values for the JSON file
		# There are two cases: one for ASIC and the else is for HCC
		renamescheme = lambda x: re.sub(r'Plane_Asic__(\d)(\d)(\w+)', r'ABC_\3_\2', x + hType) if 'Asic' in x \
				else 'HCC_' + hType + '_0'
	elif hType == 'Y':
		# Get the Asic data for glue height and append HCC data
		packageHeightData = data.filter(regex=r'Plane_800um_[\d,Hcc]', axis=0)['Z'].sort_index()
		# Rename index values for the JSON file
		# There are two cases: one for ASIC and the else is for HCC
		renamescheme = lambda x: re.sub(r'Plane_800um_(\d)\d?(\w)', r'ABC_\2_\1', x + hType) if 'Hcc' not in x \
				else 'HCC_' + hType + '_0'
		# Convert to microns
	packageHeightData = packageHeightData * 1000
	packageHeightData = packageHeightData.rename(index=renamescheme)
	passedPackageHeight = packageHeightData.le(880).any() and packageHeightData.ge(800).any()
	packageHeightData = packageHeightData.round(3).to_dict(OrderedDict)
	# Tilt calculation
	tiltData = OrderedDict()
	passedAsicTilt = True
	for i in range(10):
		backtilt = calculateTilt(data.loc['Asic_TR{}'.format(i)], data.loc['Asic_TL{}'.format(i)])
		fronttilt = calculateTilt(data.loc['Asic_BR{}'.format(i)], data.loc['Asic_BL{}'.format(i)])
		tiltdata_name = 'ABC_' + hType + '_{}'.format(i)
		tiltData[tiltdata_name] = [backtilt, fronttilt]
		passedAsicTilt = passedAsicTilt and backtilt <= 0.025 and fronttilt <= 0.025
	backtilt = calculateTilt(data.loc['HCC_TL'], data.loc['HCC_BL'])
	fronttilt = calculateTilt(data.loc['HCC_BL'], data.loc['HCC_BR'])
	tiltData['HCC_'+hType+"_0"] = [backtilt, fronttilt]
	passedHccTilt = backtilt <= 0.025 and fronttilt <= 0.025
	passedTilt = passedAsicTilt and passedHccTilt 
	# Generate JSON file
	jsondata = dict()
	jsondata['component'] = serialNumber 
	jsondata['institution'] = 'BNL' 
	jsondata['runNumber'] = '1'
	jsondata['testType'] = 'ASIC_METROLOGY'
	jsondata['date'] = fileInfo['time']
	jsondata['passed'] = bool(passedPositionDeviation and passedGlueHeight and passedPackageHeight and passedTilt)
	jsondata['problems'] = not bool(passedPositionDeviation and passedGlueHeight and passedPackageHeight and passedTilt)
	jsondata['results'] = dict()
	jsondata['results']['POSITION'] = positionDeviationData
	jsondata['results']['HEIGHT'] = glueData
	jsondata['results']['TOTAL_HEIGHT'] = packageHeightData
	jsondata['results']['TILT'] = tiltData
	jsondata['properties'] = dict()
	jsondata['properties']['USER'] = 'Meny Raviv Moshe'
	jsondata['properties']['SETUP'] = 'OGP 320 Smartscope'
	jsondata['properties']['SCRIPT_VERSION'] = 'readmetrology.py'
	jsonFilePath = os.path.join("/tmp/", serialNumber+"_"+localName+"_asicMetrology.json")
	commonFunctions.createJsonFile(jsondata, jsonFilePath)
	createdFiles.append(jsonFilePath)
	# Generate plots
	results = pd.DataFrame(jsondata['results'])
	n_data = len(results)
	data_labels = ["%s" % item for item in results.index]
	rename_p1 = lambda x: x + '_P1'
	rename_p2 = lambda x: x + '_P2'
	positions = pd.DataFrame(results.loc[:, 'POSITION'].to_list(), index=results.index, columns=('P1', 'P2'))
	p1 = pd.DataFrame(positions.loc[:, 'P1'].to_dict(), index=('X', 'Y')).rename(columns=rename_p1)
	p2 = pd.DataFrame(positions.loc[:, 'P2'].to_dict(), index=('X', 'Y')).rename(columns=rename_p2)
	position_sep = pd.concat((p1, p2), axis=1).T.sort_index()
	rename_front = lambda x: x + '_front'
	rename_back = lambda x: x + '_back'
	tilt = pd.DataFrame(results.loc[:, 'TILT'].to_list(), index=results.index, columns=('Front', 'Back'))
	front_tilt = tilt['Front'].rename(index=rename_front)
	back_tilt = tilt['Back'].rename(index=rename_back)
	tilt_concat = pd.concat((front_tilt, back_tilt)).sort_index()
	max_vals = position_sep.max(axis=0)
	min_vals = position_sep.min(axis=0)
	max_tilt = tilt_concat.max()
	min_tilt = tilt_concat.min()
	# Plot position deviations starting with x
	pxfig, pxax = plt.subplots(figsize=(10, 6))
	for i in range(position_sep.shape[0]):
		if position_sep.iloc[i, 0] > 250:
			pxax.arrow(i, 80, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if position_sep.iloc[i, 0] < -250:
			pxax.arrow(i, -80, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	pxax.plot(position_sep.loc[:, 'X'], marker='o', color='tab:blue', linestyle='None', label='P1')
	pxax.set_xticks(range(position_sep.shape[0]))
	pxax.set_xticklabels(position_sep.index.to_list(), rotation=90)
	Min=-250
	Max=250
	if (Max<max_vals.loc['X']):
		Max=max_vals.loc['X']+25
	if (Min>min_vals.loc['X']):
		Min=min_vals.loc['X']-25
	pxax.set_ylim(Min, Max)
	pxax.set_xlabel('Component')
	pxax.set_ylabel(r'Position Deviation $[\mu m]$')
	pxax.set_title(r'X Position Deviation ($\pm250\mu$m)' + 
		'\nMax: {:0.1f}, Min: {:0.1f}'.format(max_vals.loc['X'], min_vals.loc['X']))
	pxax.grid(True)
	plt.tight_layout()
	xDevPlotName = "/tmp/"+serialNumber+"_"+localName+'-XDev.png'
	plt.savefig(xDevPlotName)
	plt.close()
	createdFiles.append(xDevPlotName)
	# Plot position deviations with y
	pyfig, pyax = plt.subplots(figsize=(10, 6))
	for i in range(position_sep.shape[0]):
		if position_sep.iloc[i, 1] > 250:
			pyax.arrow(i, 80, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if position_sep.iloc[i, 1] < -250:
			pyax.arrow(i, -80, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	pyax.plot(position_sep.loc[:, 'Y'], marker='o', color='tab:blue', linestyle='None', label='P1')
	pyax.set_xticks(range(position_sep.shape[0]))
	pyax.set_xticklabels(position_sep.index.to_list(), rotation=90)
	Min=-250
	Max=250
	if (Max<max_vals.loc['Y']):
		Max=max_vals.loc['Y']+25
	if (Min>min_vals.loc['Y']):
		Min=min_vals.loc['Y']-25
	pyax.set_ylim(Min, Max)
	pyax.set_xlabel('Component')
	pyax.set_ylabel(r'Position Deviation $[\mu m]$')
	pyax.set_title(r'Y Position Deviation ($\pm250\mu$m)' + 
		'\nMax: {:0.1f}, Min: {:0.1f}'.format(max_vals.loc['Y'], min_vals.loc['Y']))
	pyax.grid(True)
	plt.tight_layout()
	yDevPlotName = "/tmp/"+serialNumber+"_"+localName+'-YDev.png'
	plt.savefig(yDevPlotName)
	plt.close()
	createdFiles.append(yDevPlotName)
	# Plotting glue thickness
	glfig, glax = plt.subplots(figsize=(10, 6))
	for i in range(11):
		if results.iloc[i, 1] > 170:
			glax.arrow(i, 145, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if results.iloc[i, 1] < 70:
			glax.arrow(i, 95, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	glax.plot(results.loc[:, 'HEIGHT'], marker='o', color='tab:blue', linestyle='None')
	glax.set_xticks(range(n_data))
	glax.set_xticklabels(data_labels, rotation=45)
	glax.set_xlabel('Component')
	glax.set_ylabel(r'Glue Thickness [$\mu$m]')
	glax.set_title(r'Glue Height (120 $\pm$ 50) $\mu$m' + 
		'\nMax: {:0.1f}, Min: {:0.1f}'.format(results.max(axis=0).loc['HEIGHT'], results.min(axis=0).loc['HEIGHT']))
	Min=70
	Max=170
	if (Max<results.max(axis=0).loc['HEIGHT']):
		Max=results.max(axis=0).loc['HEIGHT']+20
	if (Min>results.min(axis=0).loc['HEIGHT']):
		Min=results.min(axis=0).loc['HEIGHT']-20
	glax.set_ylim(Min, Max)
	glax.grid(True)
	plt.tight_layout()
	gluePlotName = "/tmp/"+serialNumber+"_"+localName+'-Glue.png'
	plt.savefig(gluePlotName)
	plt.close()
	createdFiles.append(gluePlotName)
	# Plotting package height
	pkfig, pkgax = plt.subplots(figsize=(10, 6))
	for i in range(11):
		if results.iloc[i, 2] > 840:
			pkgax.arrow(i, 825, 0, 10, width=0.075, head_length=2.5, color='tab:red')
		if results.iloc[i, 2] < 760:
			pkgax.arrow(i, 775, 0, -10, width=0.075, head_length=2.5, color='tab:red')
	pkgax.plot(results.loc[:, 'TOTAL_HEIGHT'], marker='o', color='tab:blue', linestyle='None')
	pkgax.set_xticks(range(n_data))
	pkgax.set_xticklabels(data_labels, rotation=45)
	pkgax.set_xlabel('Component')
	pkgax.set_ylabel(r'Package Height [$\mu$m]')
	pkgax.set_title(r'Package Height (800 $\pm$ 40) $\mu$m'+
		'\nMax: {:0.1f}, Min: {:0.1f}'.format(results.max(axis=0).loc['TOTAL_HEIGHT'], results.min(axis=0).loc['TOTAL_HEIGHT']))
	#if all datapoints are not in orginal window, changes window size to have all datapoints included
	Min=760
	Max=840
	if (Min>results.min(axis=0).loc['TOTAL_HEIGHT']):
		Min=results.min(axis=0).loc['TOTAL_HEIGHT']-10
	if (Max<results.max(axis=0).loc['TOTAL_HEIGHT']):
		Max=results.max(axis=0).loc['TOTAL_HEIGHT']+10
	pkgax.set_ylim(Min, Max)
	pkgax.grid(True)
	plt.tight_layout()
	packagePlotName = "/tmp/"+serialNumber+"_"+localName+'-Package.png'
	plt.savefig(packagePlotName)
	plt.close()
	createdFiles.append(packagePlotName)
	# Plotting tilt
	tltfig, tltax = plt.subplots(figsize=(10, 6))
	for i in range(tilt_concat.shape[0]):
		if tilt_concat.iloc[i] > 0.025:
			tltax.arrow(i, 0.0225, 0, 0.001, width=0.075, head_length=0.0005, color='tab:red')
	tltax.plot(tilt_concat, marker='o', color='tab:blue', linestyle='None', label='ASIC Front/HCC Y')
	tltax.set_xticks(range(tilt_concat.shape[0]))
	tltax.set_xticklabels(tilt_concat.index.tolist(), rotation=90)
	tltax.set_xlabel('Component')
	tltax.set_ylabel(r'Tilt')
	tltax.set_title('Tilt of Components (Max 0.025)' +
		'\nMax: {:0.2e}, Min: {:0.2e}'.format(max_tilt, min_tilt))
	tltax.set_ylim(-0.0005, 0.025)
	tltax.grid(True)
	tltax.legend(loc='best')
	plt.tight_layout()
	tiltPlotName = "/tmp/"+serialNumber+"_"+localName+'-Tilt.png'
	plt.savefig(tiltPlotName)
	plt.close()
	createdFiles.append(tiltPlotName)
	return createdFiles

