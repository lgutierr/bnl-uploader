import statistics
import os
import app.dbActions as dbActions
from app.bkgTaskMethods import errorChecks

def defineStage(compType):
	if compType.lower() == "asic": stage = "BOND_PULLING"
	elif compType.lower() == "cicorel": stage = "NEW"
	elif compType.lower() == "halfmoon": stage = "RECEIVED"
	elif compType.lower() == "hybridflexarray": stage = "AT_HYBRID_ASSEMBLY_SITE"
	elif compType.lower() == "powerboard": stage = "MODULE_RCP"
	return stage

def getFileNameString(compType):
	if compType.lower() == "asic": str = "ASIC"
	elif compType.lower() == "cicorel": str = "Cicorel"
	elif compType.lower() == "halfmoon": str = "Halfmoon"
	elif compType.lower() == "hybridflexarray": str = "HybridFlexArray"
	elif compType.lower() == "powerboard": str = "PowerboardCarrier"
	return str

def getInputFiles(files):
	allFiles = [x for x in files.split(';') if x!='']
	inputFiles = []
	for f in allFiles:
		if ".csv" in f:
			oF = open(f)
			lines = oF.readlines()
			oF.close()
			found = False
			for l in lines:
				if "TESTLOAD" in l:
					found = True
					break
			if not found:
				os.system("rm "+str(f))
			else:
				inputFiles.append(f)
		else:
			os.system("rm "+str(f))
	return inputFiles

def pullTestFileAnalysis(file):
	# Gather file data
	machine, bonder, data, testTime, operator = "", "", "", "", ""
	testload = 0
	wires, grades, strengths = [], [], []
	f = open(file, 'r')
	for l in f.readlines():
		parts = l.split("\t")
		if len(parts) == 1:
			parts = l.split(",")
		for i in range(len(parts)):
			parts[i] = parts[i].replace('\n','').replace('"','')
		if "TESTER" in l and len(parts) > 1:
			machine = parts[1]
		elif "BONDER" in l and len(parts) > 1:
			bonder = parts[1]
		elif "DATE" in l and len(parts) > 1:
			date = parts[1]
		elif "TIME" in l and len(parts) > 1:
			testTime = parts[1]
		elif "OPERATOR" in l and len(parts) > 1:
			operator = parts[1]
		elif "TESTLOAD" in l and len(parts) > 1:
			testload = float(parts[1])
		elif "TESTSPEED" not in l and "TEST" in l and len(parts)>3:
			wires.append(int(parts[1]))
			grades.append(int(parts[2]))
			strengths.append(float(parts[3]))
	f.close()
	# Get relevant calculations
	maxPullStrength = max(strengths)
	minPullStrength = min(strengths)
	meanPullStrength = sum(strengths)/float(len(strengths))
	numWires = len(wires)
	numWiresLess5g = len([st for st in strengths if st < 5])
	heelbreaks = 100*(numWires - len([gr for gr in grades if gr == 4]))/float(numWires)
	std = statistics.pstdev(strengths)
	passed = (testload < minPullStrength)
	# Output
	info = dict()
	info["machine"] = machine
	info["bonder"] = bonder
	info["date"] = date.replace("/", "-") + "T" + testTime + ".000Z"
	info["operator"] = operator
	info["grades"] = grades
	info["strengths"] = strengths
	info["heelbreaks"] = heelbreaks
	info["maxPull"] = maxPullStrength
	info["meanPull"] = meanPullStrength
	info["minPull"] = minPullStrength
	info["std"] = std
	info["numWires"] = numWires
	info["numWiresLess5g"] = numWiresLess5g
	info["passed"] = passed
	return info

def getPowerboardInfo(serialNumber, output, itkClient):
	powerboardChildren = dbActions.getChildren(serialNumber)
	for child in powerboardChildren:
		if child['componentType']['code']=='PB_TC':
			childIdentifier = child['component']['serialNumber']

			#Check if Powerboardtest coupon has SN
			output = errorChecks.checkCompStatus(childIdentifier, output, itkClient)
			if output['errors']!=[]: return output

			childComp = dbActions.getCompStatus(childIdentifier)
			childComp = childComp[0]
			childSerialNumber = childComp['serialNumber']
			childCurrentStage = childComp['currentStage']['code']
			childCompCode = childComp['componentType']['code']
			
	return childIdentifier, childComp, childSerialNumber, childCurrentStage, childCompCode


def getBNLdiskFolder(compType):
	if compType.lower() == "asic": folderPath = "asic/pullTest"
	elif compType.lower() == "cicorel": folderPath = "cicorel/pullTest"
	elif compType.lower() == "halfmoon": folderPath = "halfmoon/pullTest"
	elif compType.lower() == "hybridflexarray": folderPath = "hybridFlexArray/pullTest"
	#Folder needs to be created in the box
	elif compType.lower() == "powerboard": folderPath = "powerboard/pullTest"
	return folderPath 

