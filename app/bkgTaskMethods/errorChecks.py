import app.dbActions as dbActions
import inspect
from flask_login import current_user

commonErrorDict = {
	'compRetrieveError': 'Could not find component in the database based on the identifier (such as SN) that was provided.',
	'compTypeError': 'Component provided is the wrong type.',
	'compLocationError': 'Location of component does not match the institutions of the user.',

	'wrongFileError': 'Could not upload because of wrong file type. The correct file types are: {}',
	'fileNameError': 'Could not upload because of file name. The file name must contain ONE of the words: {}',
	'fileContentError': 'Please check the file contents and template used for upload. Could not find "{}" in the file.',

	'compMissingTestError': 'Component could not be staged due to missing tests in current stage',
	'compStageError': 'Could not set stage of component. Component may not be in the proper stage, or user is not authorized to set stage.',

	'testSchemaError': 'Test schema does not exist for this test, contact maintainer of uploader: https://gitlab.cern.ch/lgutierr/bnl-uploader',
	'uploadSchemaError': 'Test could not be uploaded to the database, contact maintainer of uploader: https://gitlab.cern.ch/lgutierr/bnl-uploader',
	'uploadFileOutError': 'File could not be uploaded to the database, contact maintainer of uploader: https://gitlab.cern.ch/lgutierr/bnl-uploader',

}

def checkFileType(files, output, fileTypes):
	# Makes sure the file type(s) uploaded are correct for the script
	allFiles = [x for x in files.split(';') if x != '']
	fileTypesStr = ', '.join(fileTypes)
	for file in allFiles:
		if not any(file.endswith(fileType) for fileType in fileTypes):
			error_message = commonErrorDict['wrongFileError'].format(fileTypesStr)
			output['errors'].append(error_message)
	return output

#Checks list to see if any accepted (OR statement for a list)
def checkFileName(files, output, fileNames):
    allFiles = [x for x in files.split(';') if x != '']
    fileNamesStr = ', '.join(fileNames)
    # Flag to check if any acceptedFileName is found in the uploaded files
    found = False
    for file in allFiles:
        if any(fileName in file for fileName in fileNames):
            found = True
            break
    # If none of the fileNames are found in any file, add the error message
    if not found:
        error_message = commonErrorDict['fileNameError'].format(fileNamesStr)
        output['errors'].append(error_message)
    return output

def checkFileContent(files, output, fileString):
	allFiles = [x for x in files.split(';') if x != '']
	for file in allFiles:
		oF = open(file)
		lines = oF.readlines()
		oF.close()
		found = False
		for l in lines:
			if fileString in l:
				found = True
				break
		if not found:
			error_message = commonErrorDict['fileContentError'].format(fileString)
			output['errors'].append(error_message)		
	return output

def checkCompStatus(identifier, output, itkClient):
	#Makes sure the component exists
	[comp, exception] = dbActions.getCompStatus(identifier, itkClient)
	if comp == None:
		output['errors'].append(commonErrorDict['compRetrieveError'])
	if exception != None:
		output['dBerrors'].append(exception)
	return output

#Dependent on whether or not comp exists using checkCompstatus
def checkCompType(identifier, acceptedTypes, output, itkClient):
	[comp, exception] = dbActions.getCompStatus(identifier, itkClient)
	assert(exception==None)
	compCode = comp['componentType']['code']
	print(compCode)
	print(acceptedTypes)
	if compCode in acceptedTypes: correctCompType = True
	else: correctCompType = False
	print(correctCompType)
	acceptedTypesStr = ', '.join(acceptedTypes)
	if correctCompType == False:
		print("appending to error dictionary")
		output['errors'].append(f"{commonErrorDict['compTypeError']} Component type provided is: {compCode}. Component types needed for this function: {acceptedTypesStr}")
		print(output)
	return output

#Dependent on whether or not comp exists using checkCompstatus
def checkCompLocation(identifier, output, itkClient):
	[institutionList, exception] = dbActions.getUserInstitutionList(itkClient)
	if exception:
		error_message = "Could not find list of institutions for user"
		output['errors'].append(error_message)
		output['dBerrors'].append(exception)
		return output
	else:
		institution_names = ', '.join(institutionList)  
		for institution in institutionList:
			[comp, exception] = dbActions.getCompStatus(identifier, itkClient)
			assert(exception==None)
			compLocation = comp['currentLocation']['code']
			if compLocation == institution: return output
		error_message = f"{commonErrorDict['compLocationError']} User locations are : {institution_names}. Component location is {compLocation}"
		output['errors'].append(error_message)
		return output

def checkTestSchema(testType, compCode, output, itkClient):
	#Make sure test schema (dto sample) exists
	print("dbActions.getTestSchema")
	[testSchema, exception] = dbActions.getTestSchema(testType, compCode, itkClient)
	if testSchema == dict():
		output['errors'].append(commonErrorDict['testSchemaError'])
		output['dBerrors'].append(exception)
	return output

def readyForNextStage(identifier, desiredStage):
	'''Old method, keeping for now'''
	''' Check if component with given identifier is ready to be moved to the next stage. '''
	return True, [] # Remove once approval is granted
	# Create component
	comp = Component(identifier, False)
	# Check if component is ready for next stage
	ready, missingTests = comp.ready_nextStage(desiredStage)
	return ready, missingTests

def checkAndSetCompStageList(serialNumber, currentStage, stageList, output, itkClient):
	#If stage is at current stage, pass
	for stage in stageList:
		print("stage and currentStage are")
		print(stage)
		print(currentStage)
		if stage == currentStage:
			print("passed current stage")
			print("In check And set CompStage List, passed")
			return output
	#If stage != current stage, see if ready for next stage
	ready, missingTests = readyForNextStage(serialNumber, stage)
	#If ready, check if able to set to next stage
	if not ready:
		output['errors'].append(commonErrorDict['compMissingTestError'])
	[setStageOut, exception] = dbActions.setCompStage(serialNumber, stage, itkClient)
	print("Was ready")
	print([setStageOut, exception])
	if setStageOut: 
		print("able to set stage")
		return output
	#If not able to set stage give error
	else:
		output['errors'].append(commonErrorDict['compStageError'])
		output['dBerrors'].append(exception)	
	return output

def checkUploadSchema(uploadSchemaOut, output, exception):
	if not uploadSchemaOut:
		output['errors'].append(commonErrorDict['uploadSchemaError'])
		output['dBerrors'].append(exception)
	return output

def checkUploadFileOut(uploadFileOut, output, exception):
	if not uploadFileOut:
		output['errors'].append(commonErrorDict['uploadFileOutError'])
		output['dBerrors'].append(exception)
	return output
