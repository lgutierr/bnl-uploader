def fillTestSchema(fileSchema, testSchema, testType):
	testSchema.update(fileSchema)
	return testSchema

'''
{
  "component": "...",
  "testType": "NO_BURNIN",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-02-11T15:52:50.375Z",
  "passed": true,
  "problems": false,
  "properties": {
    "DCS": null,
    "det_info": null,
    "system_info": null,
    "scan_info": null,
    "itsdaq_test_info": null
  },
  "results": {
    "enc_est_away": [
      0,
      0
    ],
    "enc_est_under": [
      0,
      0
    ],
    "occupancy_mean_away": [
      0,
      0
    ],
    "occupancy_mean_under": [
      0,
      0
    ],
    "occupancy_rms_away": [
      0,
      0
    ],
    "occupancy_rms_under": [
      0,
      0
    ],
    "offset_away": [
      0,
      0
    ],
    "offset_under": [
      0,
      0
    ]
  }
}
'''

'''
{
  "component": "...",
  "testType": "RESPONSE_CURVE_BURNIN",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-02-11T15:53:31.967Z",
  "passed": true,
  "problems": false,
  "properties": {
    "itsdaq_test_info": null,
    "DCS": null,
    "det_info": null,
    "fit_type_code": null,
    "scan_info": null,
    "special_point_index": null,
    "system_info": null
  },
  "results": {
    "innse_mean_under": [
      0,
      0
    ],
    "offset_rms_under": [
      0,
      0
    ],
    "innse_mean_away": [
      0,
      0
    ],
    "innse_rms_under": [
      0,
      0
    ],
    "innse_rms_away": [
      0,
      0
    ],
    "gain_mean_under": [
      0,
      0
    ],
    "gain_mean_away": [
      0,
      0
    ],
    "gain_rms_under": [
      0,
      0
    ],
    "gain_rms_away": [
      0,
      0
    ],
    "offset_rms_away": [
      0,
      0
    ],
    "offset_mean_under": [
      0,
      0
    ],
    "offset_mean_away": [
      0,
      0
    ]
  }
}
'''

'''
{
  "component": "...",
  "testType": "PEDESTAL_TRIM_BURNIN",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-02-11T15:54:15.374Z",
  "passed": true,
  "problems": false,
  "properties": {
    "DCS": null,
    "det_info": null,
    "system_info": null,
    "trim_offset": null,
    "trim_range": null,
    "scan_info": null,
    "itsdaq_test_info": null
  },
  "results": {
    "trim_away": [
      0,
      0
    ],
    "trim_under": [
      0,
      0
    ]
  }
}
'''

'''
{
  "component": "...",
  "testType": "STROBE_DELAY_BURNIN",
  "institution": "...",
  "runNumber": "...",
  "date": "2025-02-11T15:54:41.549Z",
  "passed": true,
  "problems": false,
  "properties": {
    "DCS": null,
    "FRACTION": null,
    "det_info": null,
    "system_info": null,
    "scan_info": null,
    "itsdaq_test_info": null
  },
  "results": {
    "StrobeDelay_away": [
      0,
      0
    ],
    "StrobeDelay_under": [
      0,
      0
    ]
  }
}
'''