import app.dbActions as dbActions
from flask_login import current_user
'''DTO Sample

{
	"component": "...",
	"testType": "PULL_TEST",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-08-19T17:38:55.091Z",
	"passed": true,
	"problems": false,
	"properties": {
		"OPERATOR": null,
		"PULL_TEST_MACHINE": null,
		"NUMBER_WIRES": null,
		"WIRE_BOND_MACHINE": null
	},
	"results": {
		"MEAN": -1,
		"PULL_STRENGTH": [
			-1,
			-1
		],
		"PULL_GRADE": [
			-1,
			-1
		],
		"STANDARD_DEVIATION": -1,
		"TOO_LOW": -1,
		"MINIMUM_STRENGTH": -1,
		"MAXIMUM_STRENGTH": -1,
		"HEEL_BREAKS": -1,
		"FILE": null
	}
}
		'''

def fillTestSchema(comp, stage, testSchema, testData, itkClient):
	testSchema['component'] = comp['serialNumber']
	print("1")
	testSchema['testType'] = 'PULL_TEST'
	print("2")
	testSchema['institution'] = 'BNL'
	print("3")
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, 'PULL_TEST', stage, itkClient)+1)
	print("4")
	testSchema['problems'] = False
	print("5")
	testSchema['passed'] = testData['passed']
	print("6")
	testSchema['date'] = testData['date']
	print("7")

	testSchema['properties']['OPERATOR'] = testData['operator']
	print("8")
	testSchema['properties']['PULL_TEST_MACHINE'] = testData['machine']
	print("9")
	testSchema['properties']['NUMBER_WIRES'] = testData['numWires']
	print("10")
	testSchema['properties']['WIRE_BOND_MACHINE'] = testData['bonder']
	print("11")

	testSchema['results']['MEAN'] = testData['meanPull']
	print("12")
	testSchema['results']['PULL_STRENGTH'] = testData['strengths']
	print("13")
	testSchema['results']['PULL_GRADE'] = testData['grades']
	print("14")
	
	testSchema['results']['STANDARD_DEVIATION'] = testData['std']
	print("15")
	testSchema['results']['TOO_LOW'] = testData['numWiresLess5g']
	print("16")
	testSchema['results']['MINIMUM_STRENGTH'] = testData['minPull']
	print("17")
	testSchema['results']['MAXIMUM_STRENGTH'] = testData['maxPull']
	print("18")
	testSchema['results']['HEEL_BREAKS'] = testData['heelbreaks']
	print("19")

	return testSchema