import statistics
import os
import app.dbActions as dbActions
from app.bkgTaskMethods import errorChecks

# Get good files
def getInputFiles(files):
	allFiles = [x for x in files.split(';') if x!='']
	inputFiles = []
	for f in allFiles:
		# found = False
		# if "sensor" in f and "visualInspection" in f:
		# 	found = True
		# if not found:
		# 	os.system("rm "+str(f))
		# else:
		inputFiles.append(f)
	return inputFiles

def readSensorVisualInspectionReceptionFile(filePath):
	''' Returns a dictionary with the file information
	'''
	f = open(filePath)
	lines = f.readlines()
	necessaryInfo = {"Institute":"", "RunNumber": ""}
	keys = ["BatchWaferNo", "SN", "Result", "Date", "Time",  "Comments", "ScratchPadURL", "Location1", "DamageType1", "URLs1", "Location2", "DamageType2", "URLs2", "Location3", "DamageType3", "URLs3", "Location4", "DamageType4", "URLs4", "Location5", "DamageType5", "URLs5", "Location6", "DamageType6", "URLs6"]
	sensorsInfo = []
	sensors = []
	for line in lines:
		line = line.strip()
		try:
			if line[0] == "#" or "BatchWaferNo" in line:
				continue
		except:
			continue
		# Relevant information
		parts = line.split(":")
		if len(parts) > 1:
			key = parts[0].strip()
			value = parts[1].strip()
			if key == "Institute":
				necessaryInfo[key] = value
				continue
			if key == "RunNumber":
				necessaryInfo[key] = value
				continue
		splittedLine = line.strip().split(";")
		if len(splittedLine) < 4:
			continue
		else:
			sensorsInfo.append(line.strip().split(";"))
	for sensorInfo in sensorsInfo:
		sensorDict = dict()
		for i in range(len(keys)):
			try:
				if "URLs" in keys[i]:
					sensorDict[keys[i]] = sensorInfo[i].strip().split(",")
				else:
					sensorDict[keys[i]] = sensorInfo[i].strip()
			except:
				if "URLs" in keys[i]:
					sensorDict[keys[i]] = [""]
				else:
					sensorDict[keys[i]] = ""
		for key in necessaryInfo.keys():
			sensorDict[key] = necessaryInfo[key]
		sensors.append(sensorDict)
	return sensors