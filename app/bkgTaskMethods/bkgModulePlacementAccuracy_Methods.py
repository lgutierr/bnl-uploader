import re
import numpy as np
import openpyxl
import os
import shutil

def mkDirJ():
	# Create a directory under /tmp to save the files
	dir = f"/tmp/Side_J"

	# Check if the directory exists and clean it up
	if os.path.exists(dir):
		shutil.rmtree(dir)

	# Create the directory
	os.makedirs(dir, exist_ok=True)
	return dir

def writeIdentifierFile(identifier, dir):
	#Save the identifier
	identifier_file_path = os.path.join(dir, "identifier.txt")
	with open(identifier_file_path, 'w') as f:
		f.write(identifier)
	f.close
	return

def mkDirL():
	# Create a directory under /tmp to save the files
	dir = f"/tmp/Side_L"

	# Check if the directory exists and clean it up
	if os.path.exists(dir):
		shutil.rmtree(dir)

	# Create the directory
	os.makedirs(dir, exist_ok=True)
	return dir

def getIdentifier():
	identifier_file = '/tmp/Side_J/identifier.txt'
	with open(identifier_file, 'r') as f:
		identifier = f.readline().strip()
	os.remove(identifier_file)
	return identifier

def sort_append_files(output):
	files_Side_J = os.listdir('/tmp/Side_J')
	files_Side_L = os.listdir('/tmp/Side_L')
	allFiles = []

	def natural_sort_key(filename):
		return [int(text) if text.isdigit() else text for text in re.split(r'(\d+)', filename)]

	files_Side_J = sorted(files_Side_J, key=natural_sort_key)
	files_Side_L = sorted(files_Side_L, key=natural_sort_key)
	for num, filename in enumerate(files_Side_J):
		if filename != 'identifier.txt':
			new_filename = f'/tmp/Side_J_Module_{num}.txt'
			os.rename(os.path.join('/tmp/Side_J', filename), os.path.join('/tmp/Side_J', new_filename))
			allFiles.append(new_filename)
	for num, filename in enumerate(files_Side_L):
		new_filename = f'/tmp/Side_L_Module_{num}.txt'
		os.rename(os.path.join('/tmp/Side_L', filename), os.path.join('/tmp/Side_L', new_filename))
		allFiles.append(new_filename)

	filePattern = re.compile(r"/tmp/Side_[JL]_Module_(?:[0-9]|1[0-3])\.txt")
	output['inputFiles'] = []
	for file in allFiles:
		# Get files that only match pattern "Module_j.txt", where j goes from 0 to 13
		if filePattern.match(file):
			output['inputFiles'].append(file)
		else:
			output['errors'].append('Please upload files with file names of format "Module_#.txt, where # is 0-13. An example is Module_11.txt')
			break

	if len(output['inputFiles']) != 28:
		#Ensure all 28 files for each module are provided
		output['errors'].append("Wrong amount of files provided. Need all 28 files for the stave")
		return output
	return output

def fileCheckErrors_saveToDir(allFiles,output, dir):
	print("Now in fileCheckErrors")
	print(allFiles,output, dir)
	filePattern = re.compile(r"/tmp/Module_(?:[0-9]|1[0-3])\.txt")
	for file in allFiles:
		# Get files that only match pattern "Module_j.txt", where j goes from 0 to 13
		if filePattern.match(file):
			with open(file) as oF:
				lines = oF.readlines()
			foundCorrectText = any("Corner" in line for line in lines)
			# As an additional check, ensure all files contain the word "Corner"
			if foundCorrectText:
				output['inputFiles'].append(file)
				
				# Copy the file to the Side_J directory
				dest_file = os.path.join(dir, os.path.basename(file))
				os.rename(file, dest_file)  
			else: 
				output['errors'].append("File does not contain the required text.")
				break
		else:
			output['errors'].append('Please upload files with file names of format "Module_#.txt, where # is 0-13. An example is Module_11.txt')
			break

	if len(output['inputFiles']) != 14:
		# Ensure all 14 files for this side are provided
		output['errors'].append("Wrong amount of files provided. Need all 14 files for Side J of the stave")
		return output
	return output

# Function to extract the numeric part from the file name for file sorting later on
def extract_number_and_side(filename):
	match = re.search(r'Module_(\d+)', filename)
	if match:
		module_number = int(match.group(1))
		side = 'J' if 'Side_J' in filename else 'L'
		return (side, module_number)
	return (None, None)

# Function that takes in X,Y corners of modules and calculates the centers
def calc_centers(fids_final_module_X,fids_final_module_Y):
	centers = []
	center_x = fids_final_module_X[0] + fids_final_module_X[1] + fids_final_module_X[2] + fids_final_module_X[3]
	center_x = center_x/4
	center_y = fids_final_module_Y[0] + fids_final_module_Y[1] + fids_final_module_Y[2] + fids_final_module_Y[3]
	center_y = center_y/4
	centers.append(center_x)
	centers.append(center_y)
	return centers

# Function that calculates the module angle
def calc_rotation(fids_final_module_X,fids_final_module_Y):
	angles = []
	m = ((fids_final_module_X[3] - fids_final_module_X[0]) / (fids_final_module_Y[3] - fids_final_module_Y[0])) * (-1)
	theta = np.arctan(m) 
	theta = theta*1000
	if np.isnan(theta): theta = -999
	angles.append(theta)
	return angles

# Function that parses the file and creates a fiducial array for each stage (Ideal, after gluing, before bridge removal, after bridge removal)
def parseFile(file_path, X_Stage, Y_Stage, Z_Stage):
	with open(file_path, 'r') as file:
		lines = file.readlines()

	# Initialize dictionaries to store coordinates
	corners = ['CornerA', 'CornerB', 'CornerC', 'CornerD']
	coordinates = {corner: {'X': 0.0, 'Y': 0.0, 'Z': 0.0} for corner in corners}

	# Parse the text file
	current_corner = None
	for line in lines:
		line = line.strip()
		if line.startswith('[') and line.endswith(']'):
			current_corner = line[1:-1]
		elif current_corner in corners:
			if X_Stage in line:
				coordinates[current_corner]['X'] = float(line.split('=')[1].strip())
			elif Y_Stage in line:
				coordinates[current_corner]['Y'] = float(line.split('=')[1].strip())
			elif Z_Stage in line:
				coordinates[current_corner]['Z'] = float(line.split('=')[1].strip())

	# Create numpy array
	xyz_array = np.zeros((4, 3))
	for i, corner in enumerate(corners):
		xyz_array[i, 0] = coordinates[corner]['X']
		xyz_array[i, 1] = coordinates[corner]['Y']
		xyz_array[i, 2] = coordinates[corner]['Z']

	return xyz_array

def generateExcelFile(identifier, allFids_Side_J, allFids_Side_L, output):
	def writeBlock(sheet, stage, startCell, fids):
		# Name of stage at certain cell
		cell = sheet.cell(startCell[0], startCell[1])
		cell.value = stage

		# Header start down one and to the left one
		start_row = startCell[0] + 1
		start_col = startCell[1] - 1

		header = ["Module", "Ax", "Ay", "Bx", "By", "Cx", "Cy", "Dx", "Dy"]
		for col_index, value in enumerate(header):
			cell = sheet.cell(row=start_row, column=start_col + col_index)
			cell.value = value

		# Starts in B6
		start_row += 1
		for moduleNumber in range(0, 14):
			row_to_write = []
			row_to_write.append(moduleNumber)
			for corner in range(4):
				row_to_write.append(fids[moduleNumber][corner][0])
				row_to_write.append(fids[moduleNumber][corner][1])

			# Write the row data to the specified cell
			for col_index, value in enumerate(row_to_write):
				cell = sheet.cell(row=start_row + moduleNumber, column=start_col + col_index)
				cell.value = value
		return

	# Load or create an Excel workbook
	workbook = openpyxl.Workbook()

	# Create a sheet for Side J
	sheet_J = workbook.active
	sheet_J.title = "Side J"
	writeBlock(sheet_J, 'Ideal', [4, 3], allFids_Side_J[0])
	writeBlock(sheet_J, 'After Bridge Removal', [4, 12], allFids_Side_J[3])
	writeBlock(sheet_J, 'After Gluing', [22, 3], allFids_Side_J[1])
	writeBlock(sheet_J, 'Before Bridge Removal', [22, 12], allFids_Side_J[2])

	# Delete column K in Side J sheet
	for row in sheet_J.iter_rows(min_row=2, max_row=sheet_J.max_row, min_col=11, max_col=11):
		for cell in row:
			cell.value = None

	# Create a sheet for Side L
	sheet_L = workbook.create_sheet("Side L")
	writeBlock(sheet_L, 'Ideal', [4, 3], allFids_Side_L[0])
	writeBlock(sheet_L, 'After Bridge Removal', [4, 12], allFids_Side_L[3])
	writeBlock(sheet_L, 'After Gluing', [22, 3], allFids_Side_L[1])
	writeBlock(sheet_L, 'Before Bridge Removal', [22, 12], allFids_Side_L[2])

	# Delete column K in Side L sheet
	for row in sheet_L.iter_rows(min_row=2, max_row=sheet_L.max_row, min_col=11, max_col=11):
		for cell in row:
			cell.value = None

	# Save the workbook to a file
	filename = f'/tmp/fiducialMeasurements_{identifier}_Sides_J_and_L.xlsx'
	workbook.save(filename)
	
	# Attach excel files to results
	output['sendFiles'].append(filename)
	return output


