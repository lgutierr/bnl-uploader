from app import dbActions
from app.bkgTaskMethods import commonFunctions
from datetime import datetime
from datetime import timezone
'''
{
  "component": "...",
  "testType": "XYZ_METROLOGY",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-12-04T22:13:23.971Z",
  "passed": true,
  "problems": false,
  "results": {
    "CONFOCAL_SURVEY": {},
    "SHIELDBOX_HEIGHT": {
      "childParentRelation": "...",
      "value": -1
    }
  }
}'''

def fillTestSchema(testSchema, identifier, comp, testType, stageList, confocal_survey):
	testSchema['component'] = identifier
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, testType, stageList[0])+1)
	testSchema['date'] = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
	testSchema['passed'] = True
	testSchema['problems'] = False
	testSchema['results']['CONFOCAL_SURVEY'] = confocal_survey
	return testSchema