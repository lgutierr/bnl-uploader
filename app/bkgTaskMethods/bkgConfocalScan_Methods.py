import csv

def csv_to_array(files):
	confocal_survey = {}
	confocal_survey['Main_Side'] = []
	confocal_survey['Secondary_Side'] = []
	for f in files:
		with open(f, 'r') as fileobject:
			csv_reader = csv.reader(fileobject)
			for row in csv_reader:
				if 'SideJ' in str(f):
					confocal_survey['Main_Side'].append([row[0],row[1],row[3]])
				if 'SideL' in str(f):
					confocal_survey['Secondary_Side'].append([row[0],row[1],row[3]])
	return confocal_survey