import time
from datetime import datetime
'''DTO Sample
{
  "component": "...",
  "testType": "VIS_INSP_RES_MOD_V2",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-10-03T16:23:50.757Z",
  "passed": true,
  "problems": false,
  "properties": {
    "COMMENTS": null,
    "RUNNUMBER": null
  },
  "results": {
    "URLSCRATCHPAD": "",
    "LOCATION1": "",
    "DAMAGE_TYPE1": "",
    "URLS1": [
      "",
      ""
    ],
    "LOCATION2": "",
    "DAMAGE_TYPE2": "",
    "URLS2": [
      "",
      ""
    ],
    "LOCATION3": "",
    "DAMAGE_TYPE3": "",
    "URLS3": [
      "",
      ""
    ],
    "LOCATION4": "",
    "DAMAGE_TYPE4": "",
    "URLS4": [
      "",
      ""
    ],
    "LOCATION5": "",
    "DAMAGE_TYPE5": "",
    "URLS5": [
      "",
      ""
    ],
    "LOCATION6": "",
    "DAMAGE_TYPE6": "",
    "URLS6": [
      "",
      ""
    ]
  }
}
'''

def fillTestSchema(serialNumber, sensorData, testSchema):
  testSchema['component'] = serialNumber
  testSchema['institution'] = sensorData['Institute']
  testSchema['runNumber'] = sensorData['RunNumber']
  timestamp = int(time.time())
  testSchema['date'] = datetime.fromtimestamp(timestamp).strftime("%Y-%m-%dT%H:%M:%S.000Z")
  if sensorData['Result'] == "Pass":
    testSchema['passed'] = True
  elif sensorData['Result'] == "Fail":
    testSchema['passed'] = False
    print(sensorData)
  for key in sensorData.keys():
    print("Now in keys")
    print(key)
    print(sensorData[key])
    if key == "ScratchPadURL":
      testSchema['results']['URLSCRATCHPAD'] = sensorData[key]
    elif key == "Location1":
      testSchema['results']['LOCATION1'] = sensorData[key]
    elif key == "DamageType1":
      testSchema['results']['DAMAGE_TYPE1'] = sensorData[key]
    elif key == "URLs1":
      testSchema['results']['URLS1'] = sensorData[key]
    elif key == "Location2":
      testSchema['results']['LOCATION2'] = sensorData[key]
    elif key == "DamageType2":
      testSchema['results']['DAMAGE_TYPE2'] = sensorData[key]
    elif key == "URLs2":
      testSchema['results']['URLS2'] = sensorData[key]
    elif key == "Location3":
      testSchema['results']['LOCATION3'] = sensorData[key]
    elif key == "DamageType3":
      testSchema['results']['DAMAGE_TYPE3'] = sensorData[key]
    elif key == "URLs3":
      testSchema['results']['URLS3'] = sensorData[key]
    elif key == "Location4":
      testSchema['results']['LOCATION4'] = sensorData[key]
    elif key == "DamageType4":
      testSchema['results']['DAMAGE_TYPE4'] = sensorData[key]
    elif key == "URLs4":
      testSchema['results']['URLS4'] = sensorData[key]
    elif key == "Location5":
      testSchema['results']['LOCATION5'] = sensorData[key]
    elif key == "DamageType5":
      testSchema['results']['DAMAGE_TYPE5'] = sensorData[key]
    elif key == "URLs5":
      testSchema['results']['URLS5'] = sensorData[key]
    elif key == "Location6":
      testSchema['results']['LOCATION6'] = sensorData[key]
    elif key == "DamageType6":
      testSchema['results']['DAMAGE_TYPE6'] = sensorData[key]
    elif key == "URLs6":
      testSchema['results']['URLS6'] = sensorData[key]
  return testSchema