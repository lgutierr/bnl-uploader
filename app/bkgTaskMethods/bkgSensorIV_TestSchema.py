import app.dbActions as dbActions
from datetime import datetime
from app.bkgTasks import bkgSensorIV_Methods
'''DTO Sample
{
	"component": "...",
	"testType": "ATLAS18_IV_TEST_V1",
	"institution": "...",
	"runNumber": "...",
	"date": "2024-10-22T11:27:39.014Z",
	"passed": true,
	"problems": false,
	"properties": {
		"VBIAS_SMU": null,
		"RSERIES": null,
		"TEST_DMM": null,
		"RSHUNT": null,
		"RUNNUMBER": null,
		"COMMENTS": null,
		"ALGORITHM_VERSION": null
	},
	"results": {
		"TEMPERATURE": 0,
		"I_500V": 0,
		"SHUNT_VOLTAGE": [
			0,
			0
		],
		"HUMIDITY": 0,
		"VBD": 0,
		"CURRENT": [
			0,
			0
		],
		"RMS_STABILITY": 0,
		"VOLTAGE": [
			0,
			0
		]
	}
}
		'''

def fillTestSchema(serialNumber, comp, currentStage, df_header, df_data, testSchema):
	testSchema['component'] = serialNumber
	testSchema['runNumber'] = str(dbActions.getRunNumber(comp, "ATLAS18_IV_TEST_V1", currentStage)+1)
	testSchema['institution'] = df_header.query('key=="Institute"')['value'].values[0]
	dateStr=df_header.query('key=="Date"')['value'].values[0]+"@"+df_header.query('key=="Time"')['value'].values[0]
	testSchema['date']= datetime.strptime(dateStr, "%d %b %Y@%H:%M:%S").strftime("%Y-%m-%dT%H:%M:%S.000Z")
	testSchema['properties']['COMMENTS']=df_header.query('key=="Comments"')['value'].values[0]
	testSchema['properties']['RSERIES']=df_header.query('key=="Rseries"')['value'].values[0]
	testSchema['properties']['RSHUNT']=df_header.query('key=="Rshunt"')['value'].values[0]
	testSchema['properties']['TEST_DMM']=df_header.query('key=="Test_DMM"')['value'].values[0]
	testSchema['properties']['VBIAS_SMU']=df_header.query('key=="Vbias_SMU"')['value'].values[0]
	testSchema['properties']['RUNNUMBER']=df_header.query('key=="RunNumber"')['value'].values[0]

	resDict = bkgSensorIV_Methods.getResDict(df_header, df_data)
	if resDict['PassIV']!="Yes":
			testSchema['passed']=False
	else:
			testSchema['passed']=True
	# update schema with derived results
	testSchema['problems']=False # what to do?
	# properties
	testSchema['properties']['ALGORITHM_VERSION']=resDict['AlgorithmVersion']
	# results (input)
	testSchema['results']['HUMIDITY']=float(df_header.query('key=="Humidity"')['value'].values[0])
	testSchema['results']['TEMPERATURE']=float(df_header.query('key=="Temperature"')['value'].values[0])
	testSchema['results']['VOLTAGE']=df_data['voltage'].astype(float).to_list()
	testSchema['results']['SHUNT_VOLTAGE']=df_data['shunt'].astype(float).to_list()
	# results (derived)
	testSchema['results']['CURRENT']=df_data['current'].astype(float).to_list()
	testSchema['results']['RMS_STABILITY']=resDict['RMS_Stability[nA]']
	testSchema['results']['VBD']=float(resDict['MicroDischargeV'])
	testSchema['results']['I_500V']=resDict['IDataAtTestV']

	return testSchema