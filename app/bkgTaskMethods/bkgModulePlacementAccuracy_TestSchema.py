from app import dbActions
from app.bkgTaskMethods import bkgModulePlacementAccuracy_Methods
from app.bkgTaskMethods import commonFunctions
'''{
  "component": "...",
  "testType": "MODULE_PLACEMENT_ACCURACY_STAVE",
  "institution": "...",
  "runNumber": "...",
  "date": "2024-11-19T14:49:16.688Z",
  "passed": true,
  "problems": false,
  "results": {
    "MODULE_PLACEMENT": {
      "childParentRelation": "...",
      "value": {}
    },
    "MODULE_POSITION_RELATIVE_TO_EXPECTED_FIDUCIALS": {
      "childParentRelation": "...",
      "value": {}
    }
  }
}'''

def fillTestSchema(testSchema, identifier, fids, itkClient, runNumber):
	testSchema['testType'] = "MODULE_PLACEMENT_ACCURACY_STAVE"
	testSchema['passed'] = True
	testSchema['problems'] = False
	stave = dbActions.getCompStatus(identifier, itkClient)
	stave = stave[0]
	[fidsIdeal_all,fidsAfterGluing_all,fidsBeforeBridgeRemoval_all,fidsAfterBridgeRemoval_all] = fids
	#Fill in rest of testSchema (1 per runNumber)
	fids_final = [[],[]]
	cornerNum = 0
	testSchema['component'] = identifier
	testSchema['institution'] = commonFunctions.getInstitute()
	testSchema['runNumber'] = "After_Bridge_Removal"
	# testSchema['runNumber'] = f"ModulePlacement_{runNumber}"
	results = []
	#Loop to fill out results list
	for ith_module in range(28):
		if runNumber == 0: fids = fidsIdeal_all
		if runNumber == 1: fids = fidsAfterGluing_all
		if runNumber == 2: fids = fidsBeforeBridgeRemoval_all
		if runNumber == 3: fids = fidsAfterBridgeRemoval_all

		#Assign final fiducial X,Y to fill in test schema
		fids_final[0].append([])
		fids_final[1].append([])

		fids_final[0][ith_module].append(fids[cornerNum][0])
		fids_final[0][ith_module].append(fids[1 + cornerNum][0])
		fids_final[0][ith_module].append(fids[2 + cornerNum][0])
		fids_final[0][ith_module].append(fids[3 + cornerNum][0])
		fids_final[0][ith_module].append(999)
		fids_final[0][ith_module].append(999)
		
		fids_final[1][ith_module].append(fids[cornerNum][1])
		fids_final[1][ith_module].append(fids[1 + cornerNum][1])
		fids_final[1][ith_module].append(fids[2 + cornerNum][1])
		fids_final[1][ith_module].append(fids[3 + cornerNum][1])
		fids_final[1][ith_module].append(999)
		fids_final[1][ith_module].append(999)
		
		cornerNum += 4
		fids_final_module_X = fids_final[0][ith_module]
		fids_final_module_Y = fids_final[1][ith_module]
		centers = bkgModulePlacementAccuracy_Methods.calc_centers(fids_final_module_X,fids_final_module_Y)
		rotation = bkgModulePlacementAccuracy_Methods.calc_rotation(fids_final_module_X,fids_final_module_Y)

		if ith_module < 14:  
			staveSide = 'Main'
			module_slot = ith_module
		if ith_module >= 14: 
			staveSide = 'Secondary'
			module_slot = ith_module - 14
		
		value = {
				"STAVE_SIDE": staveSide,
				"MODULE_SLOT": [module_slot],
				"FID_X": fids_final[0][ith_module],
				"FID_Y": fids_final[1][ith_module],
				"CENTER": centers,
				"ROTATION": rotation
				}

		childParentRelation = stave['children'][ith_module]['id']
		results.append({'childParentRelation':childParentRelation ,'value':value})
		#end loop over each module
	testSchema['results']={"MODULE_PLACEMENT":results}
	return testSchema