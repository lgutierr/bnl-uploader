from flask_login import current_user
from app import db
from app.models import User
import inspect
import pytz
from datetime import datetime, timezone
import json
import app.dbActions as dbActions

#Common dictionary for each bkgTask
def commonOutputDict():
	commonOutputDict = {
		'identifier': [], #input identifier, usually SN
		'inputFiles': [], #input files for uploading
		'serialNumber': [], #serial number found from identifier
		'testSchema': [], #Test schema for upload
		'compCode': [], #comp code (typically qual to accepted types)
		'institution': [], #user institution
		'createdFiles': [], #.json file path for each file about to be uploaded
		'uploadedFiles': [], #raw file and json file uploaded
		'testRunLink': [], #database link
		'fig': [], #figure for some functions like sensor IV
		'sendFiles': [], #Save a file to be later output on the page
		'errors': [], #Translated errors for user
		'dBerrors': [] #Error from database (exception thrown)
 	}
	return commonOutputDict

def getFileName(serialNumber, compCode, function_name):
	#Get timestamp in UTC time
	utc_timestamp = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
	#Retrieve file name from dictionary below
	fileNameDict = {
		'bkgPullTest': f'{serialNumber}_{compCode}_{utc_timestamp}_PullTest',
		'bkgSensorVisualInspectionReception': f'{serialNumber}_{compCode}_{utc_timestamp}_SensorVisualInspection_Reception',
		'bkgSensorIV': f'{serialNumber}_{compCode}_{utc_timestamp}_Sensor_IV',
		'bkgVisualInspection': f'{serialNumber}_{compCode}_{utc_timestamp}_Visual_Inspection',
		'bkgHVtabShearTest': f'{serialNumber}_{utc_timestamp}_HVTabShearTest',
		'bkgModuleElectricalTests': f'{serialNumber}_{utc_timestamp}_ModuleElectricalTest',
		'bkgModuleIV': f'{serialNumber}_{compCode}_{utc_timestamp}_Module_IV',
		'bkgModulePlacementAccuracy': f'{serialNumber}_{compCode}_{utc_timestamp}_ModulePlacementAccuracyTest',
		'bkgConfocalScan': f'{serialNumber}_{compCode}_{utc_timestamp}_ConfocalScan',
		'bkgHybridMetrology': f'{serialNumber}_{compCode}_{utc_timestamp}_Hybrid_Metrology_Test'
	}

	fileName = fileNameDict.get(function_name)

	return fileName

def getBNLBoxFolderPath(function_name, compType=None):
	#Retrieve file name from dictionary below
	folderPathDict = {
		'bkgSensorVisualInspectionReception': "sensor/visualInspection",
		'bkgSensorIV': "sensor/IV",
		'bkgVisualInspection': {
			'hvtab': "hvtab/visualInspection",
			'powerboard': "powerboard/visualInspection",
			'hybrid': "hybrid/visualInspection",
			'module': "module/visualInspection"
		},
		'bkgHVtabShearTest': "hvtab/shearTest",
		'bkgModuleElectricalTests': "module/electricalTest",
		'bkgModuleIV': "module/IV"
	}
	if function_name == 'bkgVisualInspection' and compType:
		print("Getting accepted comp type for type", compType)
		return folderPathDict['bkgVisualInspection'].get(compType)
	else:
		return folderPathDict.get(function_name)
	
def getAcceptedFileTypes(function_name):
	acceptedFileTypesDict = {
		'bkgPullTest': ['.csv'],
		'bkgSensorVisualInspectionReception': ['.txt'],
		'bkgSensorIV': ['.dat'],
		'bkgModuleElectricalTests': ['.json'],
		'bkgHybridElectricalTests': ['.json'],
		'bkgModuleIV': ['.json', '.dat'],
		'bkgModulePlacementAccuracy': ['.txt'],
		'bkgConfocalScan': ['.csv'],
		'bkgHybridMetrology': ['.txt']

	}

	acceptedFileTypeList = acceptedFileTypesDict.get(function_name)

	return acceptedFileTypeList

def getAcceptedFileNames(function_name):
	acceptedFileNamesDict = {
		'bkgSensorVisualInspectionReception': ['sensor', "visualInspection"],
		'bkgModuleElectricalTests_1': ["USBHX","USBHY"],
		'bkgModuleElectricalTests_2': ["_NO_","PEDESTAL_TRIM","STROBE_DELAY","RESPONSE_CURVE"],
		'bkgHybridElectricalTests_1': ["USBHX","USBHY"],
		'bkgHybridElectricalTests_2': ["_NO_","PEDESTAL_TRIM","STROBE_DELAY","RESPONSE_CURVE"],
		'bkgSensorIV': ["IV"],
		'bkgModuleIV': ["IV"],
		'bkgConfocalScan': ["SideJ", "SideL"],
		'bkgHybridMetrology': ["GPC"]
	}
	acceptedFileNameList = acceptedFileNamesDict.get(function_name)

	return acceptedFileNameList

def getAcceptedFileFormatString(function_name):
	#Note, can only work with one string to check file contents
	acceptedFileFormatString = {
		'bkgSensorIV': "ATLAS18_IV_TEST_V1"
	}
	acceptedFileString = acceptedFileFormatString.get(function_name)

	return acceptedFileString

def getAcceptedCompTypes(function_name, compType=None):
	acceptedCompTypesDict = {
		'bkgPullTest': {
			"asic": ["ABC", "HCC", "AMAC"],
			"cicorel": ["CICOREL_CARD"],
			"halfmoon": ["SENSOR_HALFMOONS"],
			"hybridflexarray": ["HYBRID_FLEX_ARRAY"],
			"powerboard": ["PWB_CARRIER"]
		},
		'bkgSensorVisualInspectionReception': ["SENSOR"],
		'bkgSensorIV': ["SENSOR"],
		'bkgVisualInspection': {
			'hvtab': ["HV_TAB_SHEET"],
			'powerboard': ["PWB"],
			'hybrid': ["HYBRID_ASSEMBLY"],
			'module': ["MODULE"]
		},
		"bkgHVtabShearTest":["HV_TAB_SHEET"],
		'bkgModuleElectricalTests': ["HYBRID_ASSEMBLY"],
		'bkgModuleIV': ["MODULE"],
		'bkgModulePlacementAccuracy': ['STAVE'],
		'bkgConfocalScan': ['STAVE'],
		'bkgHybridMetrology': ["HYBRID_ASSEMBLY"],
		'bkgHybridElectricalTests': ["HYBRID_ASSEMBLY"]
	}

	# Get the accepted types based on function name
	if function_name == 'bkgVisualInspection' and compType:
		return acceptedCompTypesDict['bkgVisualInspection'].get(compType)
	elif function_name == 'bkgPullTest' and compType:
		return acceptedCompTypesDict['bkgPullTest'].get(compType)
	else:
		return acceptedCompTypesDict.get(function_name)

def getInstitute():
	BNLUploaderInstitutions = ['BNL', 'MELB', 'LBNL_STRIP_MODULES']
	[institutionList, exception] = dbActions.getUserInstitutionList()
	institute = None
	for institute in institutionList:
		for BNLInstitute in BNLUploaderInstitutions:
			if institute == BNLInstitute: return institute
	return [institute, exception]

def getUTCTimeStamp():
	#Get UTC timestamp in a format the ITkPD will accept 
	utc_datetime = datetime.now(timezone.utc)
	timestamp = utc_datetime.strftime("%Y-%m-%dT%H:%MZ")
	return timestamp

def getStageList(function_name):
	acceptedStagesDict = {
		'bkgSensorVisualInspectionReception': ["READY_FOR_MODULE", "UNHAPPY","DAMAGED","SPECIAL_USE"],
		'bkgSensorIV': ["READY_FOR_MODULE","SENS_TEST_STAGE","UNHAPPY","DAMAGED","SPECIAL_USE"],
		'bkgHVtabShearTest': ["SHEET_WITH_TABS"],
		'bkgModuleElectricalTests': ["ON_MODULE"],	
		'bkgModuleIV': ['HV_TAB_ATTACHED', 'GLUED', 'STITCH_BONDING','BONDED', 'TESTED', 'FINISHED', 'AT_LOADING_SITE'],
		'bkgModulePlacementAccuracy': ["MODULE_LOADING"],
		'bkgConfocalScan': ["MODULE_LOADING"],
		'bkgHybridMetrology': ["ASIC_ATTACHMENT"],
		'bkgHybridElectricalTests': ["BURN_IN"],		
	}
	acceptedStageList = acceptedStagesDict.get(function_name)
	return acceptedStageList

def getComponentInfo(identifier,function_name):
	#Get component info
	[comp, exception] = dbActions.getCompStatus(identifier)
	serialNumber = comp['serialNumber']
	stageList = getStageList(function_name)
	currentStage = comp['currentStage']['code']
	compCode = comp['componentType']['code']
	return [comp, serialNumber, stageList, currentStage, compCode]

def createJsonFile(testSchema, path):
	# Create json object
	jsonObject = json.dumps(testSchema, indent=2)
	# Save in file
	outputJsonFile = open(path, "w")
	outputJsonFile.write(jsonObject)
	outputJsonFile.close()

################################################################################################################################3

import glob
import os
'''This code deletes files in tmp, not tested yet'''
def delete_tmp_Files():
	tmpFolder = "/tmp"
	allowedExtensions = [".txt", ".dat", ".jpeg", ".JPEG", ".png", ".json", ".pdf", ".jpg", ".JPG", ".xlsx", ".xls", ".csv", ".tar", ".cr2"]

	# Get files in tmp folder with allowed extensions
	filteredFiles = []
	for extension in allowedExtensions:
		filteredFiles.extend(glob.glob(os.path.join(tmpFolder, '*' + extension)))

	# Remove files in /tmp and from output
	for file in filteredFiles:
		try:
			os.remove(file)
			# print(f"Successfully removed {file}")
		except Exception as e:
			print(str(e))
	return

# '''This function is not tested yet '''
# def deleteFilesNightly():
# 	scheduler = BackgroundScheduler(timezone=timezone('US/Pacific'))
# 	scheduler.start()
# 	trigger = CronTrigger(hour=0, minute=0)
# 	scheduler.add_job(delete_tmp_Files, trigger)