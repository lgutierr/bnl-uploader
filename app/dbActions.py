from flask_login import current_user
import itkdb
from app.models import User
import app
from app import app, db
import config
from cryptography.fernet import Fernet
import os
#Uses itkdb and uuCommands to get and post data to the ITk database
#itkdb docs: https://itkdb.docs.cern.ch/latest/examples/
#uuCommands: https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=11425791

############These functions are "deprecated", will remove after refactoring###################################

def checkComponentType(identifier, acceptedTypes):
	app.logger.info('In function dbActions.checkComponentType (deprecated)')
	# Output
	accepted = None
	# Get component
	[comp, exception] = getCompStatus(identifier)
	if comp == None:
		return accepted
	# Get component type
	dbType = comp['componentType']['code']
	if dbType in acceptedTypes:
		accepted = True
	else:
		accepted = False
	return accepted

def checkComponentLocation(identifier, acceptedLocation):
	app.logger.info('In function dbActions.checkComponentLocation (deprecated)')
	# Output
	accepted = None
	# Get component
	comp = getCompStatus(identifier)
	comp = comp[0]
	if comp == None:
		return accepted
	# Get component type
	dbLocation = comp['currentLocation']['code']
	app.logger.info(f'dbLocation is {dbLocation}')
	
	if dbLocation == acceptedLocation:
		accepted = True
	else:
		accepted = False
	return accepted

#########################################################################################################

def getITkClient(current_user):
	try:
		app.logger.info(f'user is {current_user}')
		app.logger.info(f'Current user id is {current_user.id}')
		app.logger.info(f'Current user name is {current_user.get_name()}')
		app.logger.info('In function dbActions.getITkClient')	
		
		db_key = app.config['DB_KEY'].encode("utf-8")
		fernet = Fernet(db_key)
		accessCode1 = fernet.decrypt(current_user.accessCode1_hash)
		accessCode2 = fernet.decrypt(current_user.accessCode2_hash)
		accessCode1 = accessCode1.decode('utf8') 
		accessCode2 = accessCode2.decode('utf8') 
		itkUser = itkdb.core.User(accessCode1, accessCode2)
		app.logger.info(f'Found itkUser {itkUser}')	
		itkUser.authenticate()
		itkClient = itkdb.Client(user=itkUser)
		app.logger.info(f'found itkClient {itkClient}')
	except Exception as itkClientException:
		app.logger.info(f'EXCEPTION OCCURRED: {itkClientException}')	

	return itkClient

def getUserInstitutionList(itkClient = None):
	app.logger.info('In function dbActions.getUserInstitutionList')
	if itkClient == None: itkClient = getITkClient(current_user)
	institutionList = None
	exception = None
	try:
		app.logger.info('trying to get DTOin')
		itkdbOut = itkClient.get("getUser", json={"userIdentity": itkClient.user.identity})
		institutionList = [institution["code"] for institution in itkdbOut["institutions"]]
		app.logger.info('DTOin successful')
		app.logger.info(f'institutionList is {institutionList}')
	except Exception as e:
		exception = e
		app.logger.info(f'EXCEPTION OCCURRED: {exception}')
	return [institutionList, exception]

def getChildren(identifier, itkClient = None):
	app.logger.info('In function dbActions.getChildren')
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	app.logger.info('trying to get DTOin')
	itkdbOut = itkClient.get('getComponent', json={'component': identifier})
	app.logger.info('DTOin successful')
	return itkdbOut['children']

def getCompStatus(identifier, itkClient = None):
	# Connect to ITk DB
	app.logger.info('In function dbActions.getCompStatus')
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	comp = None
	exception = None
	try:
		app.logger.info('trying to get DTOin')
		comp = itkClient.get('getComponent', json={'component': identifier})
		app.logger.info('DTOin successful')
	except Exception as e:
		try:
			app.logger.info(f'EXCEPTION OCCURRED: {e}')
			app.logger.info('trying again to get DTOin')
			tmpOutput = itkClient.get('getComponent', json={'component': identifier, 'alternativeIdentifier': True})
			comp = itkClient.get('getComponent', json={'component': tmpOutput['serialNumber']})
			exception = str(e)
			app.logger.info('DTOin successful')
		except Exception as e2:
			exception = str(e2)
			app.logger.info(f'EXCEPTION OCCURRED: {e2}')
			app.logger.info('DTOin failed')
	return [comp, exception]

def getCompTestRun(testRunID, itkClient = None):
	app.logger.info('In function dbActions.getCompTestRun')
	# Connect to ITk DB
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Get component test run
	testRun = None
	try:
		app.logger.info('trying to get DTOin')
		testRun = itkClient.get('getTestRun', json={'testRun':testRunID})
		app.logger.info('DTOin successful')
	except Exception as e:
		app.logger.info(f'EXCEPTION OCCURRED: {e}')
		app.logger.info('DTOin failed')
	return testRun

def getTestSchema(testType, componentType, itkClient = None):
	print(testType, componentType)
	app.logger.info('In function dbActions.getTestSchema')
	# Output
	testSchema = dict()
	# Get client
	exception = None
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Get schema
	try:
		app.logger.info('trying to get DTOin')
		testSchema = itkClient.get('generateTestTypeDtoSample', json={'project':'S', 'componentType':componentType, 'code':testType, 'requiredOnly':True})
		app.logger.info('DTOin successful')
	except Exception as e:
		exception = str(e)
		app.logger.info(f'EXCEPTION OCCURRED: {e}')
		app.logger.info('DTOin failed')
	return [testSchema, exception]

def setCompStage(serialNumber, stage, itkClient = None):
	app.logger.info('In function dbActions.setCompStage')
	exception = None
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	try:
		app.logger.info('trying to get DTOin')
		setStageRetVal = itkClient.post('setComponentStage', json={'component': serialNumber, 'stage':stage})
		app.logger.info('DTOin successful')
		output = True
	except Exception as e:
		exception = str(e)
		output = False
		app.logger.info(f'EXCEPTION OCCURRED: {e}')
		app.logger.info('DTOin failed')
	return [output, exception]
	
def uploadSchema(testSchema, itkClient = None):
	app.logger.info('In function dbActions.uploadSchema')
	# Output
	testRun = None
	exception = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Upload schema
	try:
		app.logger.info('trying to get DTOin with uploadTestRunResults')
		uploadRetVal = itkClient.post('uploadTestRunResults', json=testSchema)
		testRun = uploadRetVal['componentTestRun']['testRun']
		app.logger.info(f'testRun is {testRun}')
		uploadSchemaOut = True
		app.logger.info('DTOin successful')
	except Exception as e:
		exception = str(e)
		uploadSchemaOut = False
		app.logger.info(f'EXCEPTION OCCURRED: , {e}')
		app.logger.info('DTOin failed')
	return [uploadSchemaOut, testRun, exception]

def uploadFile(file, name, testRun, itkClient = None):
	app.logger.info('In function dbActions.uploadFile')
	# Output
	output = None
	exception = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Setup data
	attachment = dict()
	attachment['data'] = open(file, 'rb')
	attachmentInfo = dict()
	attachmentInfo['testRun'] = testRun
	attachmentInfo['type'] = 'file'
	attachmentInfo['title'] = name
	attachmentInfo['description'] = 'Original data file'
	app.logger.info(f'attachmentInfo = {attachmentInfo}')
	# Upload attachment
	try:

		app.logger.info('trying to create Test Run Attachment')
		if os.path.getsize(file) < 64000: 
			uploadRetVal = itkClient.post('createTestRunAttachment', data=attachmentInfo, files=attachment)
		output = True
		app.logger.info('DTOin successful')
	except Exception as e:
		exception = str(e)
		output = False
		app.logger.info(f'EXCEPTION OCCURRED: , {e}')
		app.logger.info('DTOin failed')
	# Close file
	attachment['data'].close()
	return [output, exception]

def getRegistrationSchema(compType, itkClient = None):
	app.logger.info('In function dbActions.getRegistrationSchema')
	# Output
	output = None
	schema = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Get schema
	try:
		app.logger.info('trying to get DTOin')
		schema = itkClient.get('generateComponentTypeDtoSample', json={'project':'S', 'code':compType, 'requiredOnly':True})
		output = True
		app.logger.info('DTOin successful')
	except:
		output = False
	return output, schema

def registerComponent(schema, itkClient = None):
	app.logger.info('In function dbActions.registerComponent')
	# Output
	output = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Register component
	regRetVal = None
	try:
		app.logger.info('trying to get DTOin')
		regRetVal = itkClient.post('registerComponent', json=schema)
		output = True
		app.logger.info('DTOin successful')
	except Exception as e:
		output = False
		app.logger.info(f'EXCEPTION OCCURRED: , {e}')
		app.logger.info('DTOin failed')
	return output, regRetVal

def assembleComponent(parent, child, properties=None, slot=None, itkClient = None):
	app.logger.info('In function dbActions.assembleComponent')
	# Output
	output = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Assemble child to parent
	try: 
		app.logger.info('trying to get DTOin')
		if properties == None:
			assembleRetVal = itkClient.post('assembleComponent', json={'parent':parent, 'child':child})
			app.logger.info('properties == None, DTOin successful')
		else:
			if slot == None:
				assembleRetVal = itkClient.post('assembleComponent', json={'parent':parent, 'child':child, 'properties':properties})
				app.logger.info('slot == None, DTOin successful')
			else:
				assembleRetVal = itkClient.post('assembleComponentBySlot', json={'parent':parent, 'slot':slot, 'child':child, 'properties':properties})
				app.logger.info('slot and properties were not None, DTOin successful')
		output = True
	except Exception as e:
		output = False
		app.logger.info(f'EXCEPTION OCCURRED: , {e}')
		app.logger.info('DTOin failed')
	return output
		
def disassembleComponent(parent, child, itkClient = None):
	app.logger.info('In function dbActions.disassembleComponent')
	# Output
	output = None
	# Get client
	if itkClient == None: itkClient = getITkClient(current_user)
	app.logger.info(f'itkClient = {itkClient}')
	# Assemble child to parent
	try: 
		app.logger.info('trying to get DTOin')
		disassembleRetVal = itkClient.post('disassembleComponent', json={'parent':parent, 'child':child})
		output = True
		app.logger.info('DTOin successful')
	except Exception as e:
		output = False
		app.logger.info(f'EXCEPTION OCCURRED: , {e}')
		app.logger.info('DTOin failed')
	return output

def getRunNumber(comp, testType, stage, itkClient = None):
	app.logger.info('In function dbActions.getRunNumber')
	app.logger.info(f'itkClient = {itkClient}')
	runNumber = 0
	for test in comp['tests']:
		if test['code'] == testType:
			dbTestRunsID = []
			for testRun in test['testRuns']:
				if testRun['state'] != 'deleted':
					dbTestRunsID.append(testRun['id'])
			for dbID in dbTestRunsID:
				testRun = getCompTestRun(dbID, itkClient)
				if len(testRun['components'])>0 and testRun['components'][0]['testedAtStage']['code'] == stage:
					runNumber += 1
	app.logger.info(f'runNumber is {runNumber}')
	return runNumber
