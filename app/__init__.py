#Flask app setup

from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_executor import Executor
from flask_dropzone import Dropzone
from datetime import timedelta
#Initiate app
app = Flask(__name__)
app.config.from_object(Config)
#Use secret key to sign cookies
app.config["SECRET_KEY"] = app.config['SECRET_KEY']
#Immediately kick all users out upon init
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(seconds=0)
#Define max session lifetime
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(hours=8)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
executor = Executor(app)
dropzone = Dropzone(app)
from app import routes, models

# Create BNL ssh key
import os
import stat

sshKeyFile = open(app.config['BNL_SSHKEY'], "w")
sshKeyFile.write(app.config['SSH_PRIVATE_KEY'])
sshKeyFile.close()
os.chmod(app.config['BNL_SSHKEY'], stat.S_IRUSR|stat.S_IWUSR) 

#Logging config setup
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)