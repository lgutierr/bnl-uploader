from collections import OrderedDict
import app.dbActions as dbActions

class Component():
  dbType = ""
  serialNumber = ""
  localName = ""
  alternativeID = ""
  location = ""
  stage = ""
  tests = OrderedDict() 
  def __init__(self, identifier, fullCheck):
    # Get component information
    compInfo = dbActions.getCompStatus(identifier)
    compInfo = compInfo[0]
    # Get component tests and relevant variables
    if compInfo != None:
      self.set_vars(compInfo)
      if fullCheck:
        self.check_tests(compInfo)
  def __repr__(self):
    return '<Component type={}>'.format(self.dbType)
  def set_vars(self, itkdbOut):
    self.dbType = itkdbOut['componentType']['code']
    self.serialNumber = itkdbOut['serialNumber']
    self.alternativeID = itkdbOut['alternativeIdentifier']
    for p in itkdbOut['properties']:
      if p['code'] == 'LOCALNAME' or p['code'] == 'LOCAL_NAME':
        self.localName = p['value']
    self.location = itkdbOut['currentLocation']['code']
    self.stage = itkdbOut['currentStage']['code']
    self.set_tests()
  def set_tests(self):
    tests = OrderedDict()
    if self.dbType == "ABC" or self.dbType == "AMAC" or self.dbType == "HCC":
      tests['BOND_PULLING'] = ['PULL_TEST']
    elif self.dbType == "CICOREL_CARD":
      tests['NEW'] = ['PULL_TEST']
    elif self.dbType == "SENSOR_HALFMOONS":
      tests['RECEIVED'] = ['PULL_TEST']
    elif self.dbType == "HV_TAB_SHEET":
      tests['SHEET_WITH_TABS'] = ['VISUAL_INSPECTION', 'SHEAR_TEST']
    elif self.dbType == "SENSOR":
      tests['READY_FOR_MODULE'] = ['VIS_INSP_RES_MOD_V2', 'ATLAS18_IV_TEST_V1']
    elif self.dbType == "PWB":
      tests['MODULE_RCP'] = ['VISUAL_INSPECTION', 'HV_ENABLE', 'LV_ENABLE', 'OF', 'PADID', 'TOGGLEOUTPUT', 'CONFIG', 'DCDC_ADJUST', 'TEMPERATURE', 'BER']
      tests['LOADED'] = []
    elif self.dbType == "HYBRID_ASSEMBLY":
      tests['ASIC_ATTACHMENT'] = ['VISUAL_INSPECTION', 'GLUE_WEIGHT', 'ASIC_METROLOGY']
      tests['WIRE_BONDING'] = ['VISUAL_INSPECTION', 'WIRE_BONDING', 'STROBE_DELAY_PPA', 'PEDESTAL_TRIM_PPA', 'RESPONSE_CURVE_PPA', 'NO_PPA']
      tests['BURN_IN'] = ['RESPONSE_CURVE_BURNIN','NO_BURNIN","STROBE_DELAY_BURNIN','PEDESTAL_TRIM_BURNIN']
      tests['FINISHED_HYBRID'] = ['VISUAL_INSPECTION']
      tests['ON_MODULE'] = ['STROBE_DELAY_PPA', 'PEDESTAL_TRIM_PPA', 'RESPONSE_CURVE_PPA', 'NO_PPA']
    elif self.dbType == "MODULE":
      tests['HV_TAB_ATTACHED'] = ['VISUAL_INSPECTION', 'MODULE_IV_PS_V1']
      tests['GLUED'] = ['VISUAL_INSPECTION', 'GLUE_WEIGHT', 'MODULE_METROLOGY', 'MODULE_BOW']
      tests['BONDED'] = ['VISUAL_INSPECTION', 'MODULE_WIRE_BONDING']
      tests['FINISHED'] = ['VISUAL_INSPECTION']
    for stage in tests:
      self.tests[stage]=OrderedDict()
      if self.dbType == "HYBRID_ASSEMBLY" and stage == 'BURN_IN':
        self.tests[stage]['skip'] = True
      else:
        self.tests[stage]['skip'] = False
      for test in tests[stage]:
        self.tests[stage][test]=OrderedDict()
        self.tests[stage][test]['done'] = False
        self.tests[stage][test]['passed'] = False
  def get_stageTests(self, desiredStage):
    itkdbOut = dbActions.getCompStatus(self.serialNumber)
    itkdbOut = itkdbOut[0]
    doneTests = OrderedDict()
    for test in self.tests[desiredStage]:
      if test != 'skip':
        # Look for test
        dbTests = [x for x in itkdbOut['tests'] if x['code']==test]
        dbTestRunsIDs = []
        if len(dbTests) > 0:
          dbTestRunsIDs = [x['id'] for x in dbTests[0]['testRuns'] if x['state'] != 'deleted']
        latestRun = -1
        latestResult = False
        done = False
        for dbID in dbTestRunsIDs:
          testRun = dbActions.getCompTestRun(dbID)
          if len(testRun['components']) > 0 and testRun['components'][0]['testedAtStage']['code'] == desiredStage:
            done = True
            try:
              if int(testRun['runNumber']) > latestRun:
                latestRun = int(testRun['runNumber'])
                latestResult = testRun['passed']
            except:
              if int(testRun['runNumber'].split("-")[0]) > latestRun:
                latestRun = int(testRun['runNumber'].split("-")[0])
                latestResult = testRun['passed']
        doneTests[test] = done
    return doneTests
  def check_tests(self, itkdbOut):
    # Get component latest status
    for stage in self.tests:
      for test in self.tests[stage]:
        if test != 'skip':
          # Look for test
          dbTests = [x for x in itkdbOut['tests'] if x['code']==test]
          dbTestRunsIDs = []
          if len(dbTests) > 0:
            dbTestRunsIDs = [x['id'] for x in dbTests[0]['testRuns'] if x['state'] != 'deleted']
          latestRun = -1
          latestResult = False
          done = False
          for dbID in dbTestRunsIDs:
            testRun = dbActions.getCompTestRun(dbID)
            if len(testRun['components']) > 0 and testRun['components'][0]['testedAtStage']['code'] == stage:
              done = True
              try:
                if int(testRun['runNumber']) > latestRun:
                  latestRun = int(testRun['runNumber'])
                  latestResult = testRun['passed']
              except:
                if int(testRun['runNumber'].split("-")[0]) > latestRun:
                  latestRun = int(testRun['runNumber'].split("-")[0])
                  latestResult = testRun['passed']
          self.tests[stage][test]['done'] = done
          self.tests[stage][test]['passed'] = latestResult
  def ready_nextStage(self, desiredStage):
    # Find the previous stage
    previousStage = None
    for stage, tests in self.tests.items():
      if desiredStage == stage:
        break
      elif not self.tests[stage]['skip']:
        previousStage = stage
    # Get previous stage tests status
    stageTests = self.get_stageTests(previousStage)
    ready = True
    missingTests = []
    if previousStage != None:
      for test, status in stageTests.items():
        if not status:
          ready = False
          missingTests.append(test)
    return ready, missingTests
  def complete(self):
    output = True
    for stage in self.tests:
      if not self.tests[stage]['skip']:
        for test in self.tests[stage]:
          if test != 'skip' and not self.tests[stage][test]['done']:
            output = False
            break
    return output
  def status(self):
    output = dict()
    output['dbType'] = self.dbType
    output['serialNumber'] = self.serialNumber
    output['localName'] = self.localName
    output['alternativeID'] = self.alternativeID
    output['location'] = self.location
    output['stage'] = self.stage
    output['tests'] = self.tests
    output['complete'] = self.complete() 
    return output
