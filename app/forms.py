from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, RadioField, SelectField, HiddenField
from wtforms.validators import DataRequired
import datetime

class LoginForm(FlaskForm):
	accessCode1 = PasswordField('Access Code 1', validators=[DataRequired()])
	accessCode2 = PasswordField('Access Code 2', validators=[DataRequired()])
	submit = SubmitField('Sign In')

class StatusButton(FlaskForm):
	submitStatusButton = SubmitField('Status')

class CompLoadingForm(FlaskForm):
	identifier = StringField('ID (i.e. SN, alternative ID, local name)', validators=[DataRequired()])
	load = SubmitField('Load')

class TestButton(FlaskForm):
	submitTestButton = SubmitField('Test')
	
class ASICsTestButton(FlaskForm):
	submitASICsTestButton = SubmitField('ASICs')

class ASICsPullTestButton(FlaskForm):
	submitASICsPullTestButton = SubmitField('Pull Test')

class ASICsPullTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	upload = SubmitField('Upload')

class ASICsDropboxButton(FlaskForm):
	submitASICsDropboxButton = SubmitField('BNL Dropbox')

class ASICsDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class CicorelTestButton(FlaskForm):
	submitCicorelTestButton = SubmitField('Cicorel')

class CicorelPullTestButton(FlaskForm):
	submitCicorelPullTestButton = SubmitField('Pull Test')

class CicorelPullTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	upload = SubmitField('Upload')

class CicorelDropboxButton(FlaskForm):
	submitCicorelDropboxButton = SubmitField('BNL Dropbox')

class CicorelDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class HalfmoonTestButton(FlaskForm):
	submitHalfmoonTestButton = SubmitField('Halfmoon')

class HalfmoonPullTestButton(FlaskForm):
	submitHalfmoonPullTestButton = SubmitField('Pull Test')

class HalfmoonPullTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	upload = SubmitField('Upload')

class HalfmoonDropboxButton(FlaskForm):
	submitHalfmoonDropboxButton = SubmitField('BNL Dropbox')

class HalfmoonDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class HVtabTestButton(FlaskForm):
	submitHVtabTestButton = SubmitField('HV-tab')

class HVtabShearTestButton(FlaskForm):
	submitHVtabShearTestButton = SubmitField('Shear Test')

class HVtabShearTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	strengths = StringField('Shear strengths', validators=[DataRequired()])
	operator = StringField('Operator', validators=[], default='Chris Musso')
	reason = StringField('Test reason', validators=[], default='Before module tabbing')
	substrate = StringField('Test substrate', validators=[], default='Half-moon')
	upload = SubmitField('Upload')

class HVtabVisualInspectionButton(FlaskForm):
	submitHVtabVisualInspectionButton = SubmitField('Visual Inspection')

class HVtabVisualInspectionForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	upload = SubmitField('Upload')

class HVtabDropboxButton(FlaskForm):
	submitHVtabDropboxButton = SubmitField('BNL Dropbox')

class HVtabDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class SensorTestButton(FlaskForm):
	submitSensorTestButton = SubmitField('Sensor')

class SensorIVButton(FlaskForm):
	submitSensorIVButton = SubmitField('IV')

class SensorIVForm(FlaskForm):
	upload = SubmitField('Upload')

class sensorVisualInspectionReceptionButton(FlaskForm):
	submitSensorVisualInspectionReceptionButton = SubmitField('Visual Inspection')

class sensorVisualInspectionReceptionForm(FlaskForm):
	upload = SubmitField('Upload')

class SensorDropboxButton(FlaskForm):
	submitSensorDropboxButton = SubmitField('BNL Dropbox')

class SensorDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class PowerboardTestButton(FlaskForm):
	submitPowerboardTestButton = SubmitField('Powerboard')

class PowerboardElectricalTestsButton(FlaskForm):
	submitPowerboardElectricalTestsButton = SubmitField('Electrical Tests')

class PowerboardElectricalTestsForm(FlaskForm):
	upload = SubmitField('Upload')

class PowerboardCarrierPullTestButton(FlaskForm):
	submitPowerboardCarrierPullTestButton = SubmitField('Carrier Pull Test')

class PowerboardCarrierPullTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	upload = SubmitField('Upload')
	
class PowerboardVisualInspectionButton(FlaskForm ):
	submitPowerboardVisualInspectionButton = SubmitField('Visual Inspection')

class PowerboardVisualInspectionForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	upload = SubmitField('Upload')

class PowerboardCarrierVisualInspectionButton(FlaskForm):
	submitPowerboardCarrierVisualInspectionButton = SubmitField('Carrier Visual Inspection')

class PowerboardCarrierVisualInspectionForm(FlaskForm):
	identifier = StringField('Carrier identifier', validators=[DataRequired()])
	load = SubmitField('Fetch powerboards')

class PowerboardCarrierVisualInspectionLoadForm(FlaskForm):
	startList1 = HiddenField('startList1')
	powerboard_0 = StringField('Powerboard')
	powerboard_1 = StringField('Powerboard')
	powerboard_2 = StringField('Powerboard')
	powerboard_3 = StringField('Powerboard')
	powerboard_4 = StringField('Powerboard')
	powerboard_5 = StringField('Powerboard')
	powerboard_6 = StringField('Powerboard')
	powerboard_7 = StringField('Powerboard')
	powerboard_8 = StringField('Powerboard')
	powerboard_9 = StringField('Powerboard')
	finishList1 = HiddenField('finishList1')
	startList2 = HiddenField('startList2')
	status_0 = BooleanField('Passed', default='checked')
	status_1 = BooleanField('Passed', default='checked')
	status_2 = BooleanField('Passed', default='checked')
	status_3 = BooleanField('Passed', default='checked')
	status_4 = BooleanField('Passed', default='checked')
	status_5 = BooleanField('Passed', default='checked')
	status_6 = BooleanField('Passed', default='checked')
	status_7 = BooleanField('Passed', default='checked')
	status_8 = BooleanField('Passed', default='checked')
	status_9 = BooleanField('Passed', default='checked')
	finishList2 = HiddenField('finishList2')
	submitUpload = SubmitField('Upload')
		
class PowerboardDropboxButton(FlaskForm):
	submitPowerboardDropboxButton = SubmitField('BNL Dropbox')

class PowerboardDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class HybridFlexArrayTestButton(FlaskForm):
	submitHybridFlexArrayTestButton = SubmitField('Hybrid Flex Array')

class HybridFlexArrayPullTestButton(FlaskForm):
	submitHybridFlexArrayPullTestButton = SubmitField('Pull Test')

class HybridFlexArrayPullTestForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	upload = SubmitField('Upload')

class HybridFlexArrayDropboxButton(FlaskForm):
	submitHybridFlexArrayDropboxButton = SubmitField('BNL Dropbox')

class HybridFlexArrayDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class HybridTestButton(FlaskForm):
	submitHybridTestButton = SubmitField('Hybrid')

class HybridVisualInspectionButton(FlaskForm):
	submitHybridVisualInspectionButton = SubmitField('Visual Inspection')

class HybridVisualInspectionForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	stage = RadioField('Stage', choices=[('ASIC_ATTACHMENT', 'ASICs attachment'), ('WIRE_BONDING', 'Wire bonding'), ('BURN_IN', 'Burn-in'), ('FINISHED_HYBRID', 'Finished hybrid')], default='ASIC_ATTACHMENT')
	upload = SubmitField('Upload')

class HybridGlueWeightButton(FlaskForm):
	submitHybridGlueWeightButton = SubmitField('Glue weight')

class HybridGlueWeightForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	hybridWeight = StringField('Hybrid weight [g]', validators=[DataRequired()])
	hybridAndGlueWeight = StringField('Hybrid and glue weight [g]', validators=[DataRequired()])
	totalWeight = StringField('Total weight [g]', validators=[DataRequired()])
	upload = SubmitField('Upload')

class HybridMetrologyButton(FlaskForm):
	submitHybridMetrologyButton = SubmitField('Metrology')

class HybridMetrologyForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	hybridThickness = StringField('Hybrid thickness [mm]', validators=[DataRequired()], default='0.403')
	check = SubmitField('Check')
	upload = SubmitField('Upload')

class HybridWireBondingButton(FlaskForm):
	submitHybridWireBondingButton = SubmitField('Wire bonding')

class HybridWireBondingForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	operator = StringField('Operator', validators=[], default='Chris Musso')
	bonder = StringField('Bonder', validators=[], default='Hesse BJ820')
	failedAsicBond = StringField('Failed ASICs bonds', validators=[])
	failedPanelBond = StringField('Failed Panel bonds', validators=[])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	upload = SubmitField('Upload')

class HybridElectricalTestsButton(FlaskForm):
	submitHybridElectricalTestsButton = SubmitField('Electrical Tests')

class HybridElectricalTestsForm(FlaskForm):
	upload = SubmitField('Upload')

class HybridDropboxButton(FlaskForm):
	submitHybridDropboxButton = SubmitField('BNL Dropbox')

class HybridDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class ModuleTestButton(FlaskForm):
	submitModuleTestButton = SubmitField('Module')

class ModuleVisualInspectionButton(FlaskForm):
	submitModuleVisualInspectionButton = SubmitField('Visual Inspection')

class ModuleVisualInspectionForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	stage = RadioField('Stage', choices=[('HV_TAB_ATTACHED', 'HV-tab attached'), ('GLUED', 'Glued'), ('BONDED', 'Bonded'), ('FINISHED', 'Finished module')], default='HV_TAB_ATTACHED')
	upload = SubmitField('Upload')

class ModuleIVButton(FlaskForm):
	submitModuleIVButton = SubmitField('IV')

class ModuleIVForm(FlaskForm):
	testType = RadioField('Test Type', choices=[('PS','PS'), ('AMAC', 'AMAC')], default='AMAC')
	stage = RadioField('Test Stage', choices=[('HV_TAB_ATTACHED','HV-tab attached'), ('GLUED', 'Glued'), ('STITCH_BONDING', 'Stitch bonding'), ('BONDED', 'Bonded'), ('TESTED', 'Tested'), ('FINISHED', 'Finished'), ('AT_LOADING_SITE', 'At loading site')], default='HV_TAB_ATTACHED')
	upload = SubmitField('Upload')

class ModuleGlueWeightButton(FlaskForm):
	submitModuleGlueWeightButton = SubmitField('Glue weight')

class ModuleGlueWeightForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	gluetype = RadioField('Glue type', choices=[('POLARIS', 'Polaris'), ('TRUE_BLUE', 'True Blue/False Blue')], default='TRUE_BLUE')
	hybridxWeight = StringField('Hybrid X weight [g]', validators=[DataRequired()])
	hybridyWeight = StringField('Hybrid Y weight [g]', validators=[])
	hybridxTabsWeight = StringField('Hybrid X tabs weight [g]', validators=[DataRequired()])
	hybridyTabsWeight = StringField('Hybrid Y tabs weight [g]', validators=[])
	powerboardWeight = StringField('Powerboard weight [g]', validators=[DataRequired()])
	sensorWeight = StringField('Sensor weight [g]', validators=[DataRequired()])
	sensorAndHybridWeight = StringField('Sensor and hybrid weight [g]', validators=[DataRequired()])
	totalWeight = StringField('Total weight [g]', validators=[DataRequired()])
	upload = SubmitField('Upload')

class ModuleMetrologyButton(FlaskForm):
	submitModuleMetrologyButton = SubmitField('Metrology')

class ModuleMetrologyForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	temperature = StringField('Testing temperature [°F]', validators=[DataRequired()], default='76')
	powerboardThickness = StringField('Powerboard thickness [mm]', validators=[DataRequired()], default='0.385')
	hybridThickness = StringField('Hybrid thickness [mm]', validators=[DataRequired()], default='0.403')
	check = SubmitField('Check')
	upload = SubmitField('Upload')
	afterTC = SubmitField('After TC Upload')

class ModuleWireBondingButton(FlaskForm):
	submitModuleWireBondingButton = SubmitField('Wire bonding')

class ModuleWireBondingForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	operator = StringField('Operator', default='Chris Musso',validators=[])
	bonder = StringField('Bonder', validators=[], default='Hesse BJ820')
	maxUnconChans = StringField('Max unconnected channels', validators=[DataRequired()])
	maxConUnconChansRow1 = StringField('Max consecutive unconnected channels (Row 1)', validators=[])
	FailedRow1 = StringField('Failed wirebonds (Row 1)', validators=[])
	RepairedRow1 = StringField('Repaired wirebonds (Row 1)', validators=[])
	maxConUnconChansRow2 = StringField('Max consecutive unconnected channels (Row 2)', validators=[])
	FailedRow2 = StringField('Failed wirebonds (Row 2)', validators=[])
	RepairedRow2 = StringField('Repaired wirebonds (Row 2)', validators=[])
	maxConUnconChansRow3 = StringField('Max consecutive unconnected channels (Row 3)', validators=[])
	FailedRow3 = StringField('Failed wirebonds (Row 3)', validators=[])
	RepairedRow3 = StringField('Repaired wirebonds (Row 3)', validators=[])
	maxConUnconChansRow4 = StringField('Max consecutive unconnected channels (Row 4)', validators=[])
	FailedRow4 = StringField('Failed wirebonds (Row 4)', validators=[])
	RepairedRow4 = StringField('Repaired wirebonds (Row 4)', validators=[])
	NumUnconChans = StringField('Number unconnected channels', validators=[])
	result = RadioField('Result', choices=[('passed', 'Passed'), ('failed', 'Failed')], default='passed')
	upload = SubmitField('Upload')

class ModuleElectricalTestsButton(FlaskForm):
	submitModuleElectricalTestsButton = SubmitField('Electrical Tests')

class ModuleElectricalTestsForm(FlaskForm):
	thermalCycled = RadioField('Thermal cycling tests?', choices=[('FALSE', 'No'), ('TRUE', 'Yes')], default='FALSE')
	upload = SubmitField('Upload')

class ModuleDropboxButton(FlaskForm):
	submitModuleDropboxButton = SubmitField('BNL Dropbox')

class ModuleDropboxForm(FlaskForm):
	upload = SubmitField('Upload')

class StaveTestButton(FlaskForm):
	submitStaveTestButton = SubmitField('Stave')

class ModulePlacementAccuracyButton(FlaskForm):
	submitModulePlacementAccuracyButton = SubmitField('Module Placement')

class ModulePlacementAccuracy_Side_J_Form(FlaskForm):
	identifier = StringField('Stave Identifier', validators=[DataRequired()])
	goToSideL = SubmitField('Go to Side L')
class ModulePlacementAccuracyForm(FlaskForm):
	upload = SubmitField('Upload')

class ConfocalScanButton(FlaskForm):
	submitConfocalScanButton = SubmitField('Confocal Scan')
class ConfocalScanForm(FlaskForm):
	upload = SubmitField('Upload')

class RegistrationButton(FlaskForm):
	submitRegistrationButton = SubmitField('Registration')

class HVtabSheetRegistrationButton(FlaskForm):
	submitHVtabSheetRegistrationButton = SubmitField('HV-tab Sheet')

class HVtabSheetRegistrationForm(FlaskForm):
	sheetLabel = StringField('Sheet label', validators=[DataRequired()])
	unusableTabs = StringField('Unusable tabs', validators=[DataRequired()], default='0')
	register = SubmitField('Register')

class HybridRegistrationButton(FlaskForm):
	submitHybridRegistrationButton = SubmitField('Hybrid')

class HybridRegistrationForm(FlaskForm):
	localName = StringField('Local name', validators=[DataRequired()])
	asicAssemblyJig = StringField('ASIC assembly jig', validators=[DataRequired()])
	asicAssemblyChipTray = StringField('ASIC assembly chip tray', validators=[DataRequired()])
	asicAssemblyPickupTool = StringField('ASIC assembly pick-up tool', validators=[DataRequired()])
	glueSyringe = StringField('Glue syringe', validators=[DataRequired()])
	hccBondsCrossed = RadioField('HCC bonds crossed', choices=[('yes', 'Yes'), ('no', 'No')], default='yes')
	register = SubmitField('Register')

class HybridTestPanelRegistrationButton(FlaskForm):
	submitHybridTestPanelRegistrationButton = SubmitField('Hybrid Test Panel')

class HybridTestPanelRegistrationForm(FlaskForm):
	panelName = StringField('Panel batch/name', validators=[DataRequired()])
	localName = StringField('Local name', validators=[])
	rfid = StringField('RFID', validators=[])
	register = SubmitField('Register')

class ModuleRegistrationButton(FlaskForm):
	submitModuleRegistrationButton = SubmitField('Module')

class ModuleRegistrationForm(FlaskForm):
	localName = StringField('Local name', validators=[DataRequired()])
	sensorSN = StringField('Sensor SN', validators=[DataRequired()])
	hvtabJig = StringField('HV-tab Jig', validators=[DataRequired()])
	register = SubmitField('Register')

class DisassembleButton(FlaskForm):
	submitDisassembleButton = SubmitField('Disassemble')

class ASICsGelpackDisassembleButton(FlaskForm):
	submitASICsGelpackDisassembleButton = SubmitField('ASICs Gelpack')

class ASICsGelpackDisassembleForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	disassemble = SubmitField('Disassemble')

class AssembleButton(FlaskForm):
	submitAssembleButton = SubmitField('Assemble')

class HybridTestPanelAssembleButton(FlaskForm):
	submitHybridTestPanelAssembleButton = SubmitField('Hybrid Test Panel')

class HybridTestPanelAssembleForm(FlaskForm):
	identifier = StringField('Identifier', validators=[DataRequired()])
	placementJig = StringField('Placement Jig', validators=[DataRequired()])
	location0SN = StringField('Location 0 SN', validators=[])
	location0PickupTool = StringField('Location 0 Pick-up Tool', validators=[], default='009')
	location1SN = StringField('Location 1 SN', validators=[])
	location1PickupTool = StringField('Location 1 Pick-up Tool', validators=[], default='009')
	location2SN = StringField('Location 2 SN', validators=[])
	location2PickupTool = StringField('Location 2 Pick-up Tool', validators=[], default='009')
	location3SN = StringField('Location 3 SN', validators=[])
	location3PickupTool = StringField('Location 3 Pick-up Tool', validators=[], default='009')
	location4SN = StringField('Location 4 SN', validators=[])
	location4PickupTool = StringField('Location 4 Pick-up Tool', validators=[], default='009')
	location5SN = StringField('Location 5 SN', validators=[])
	location5PickupTool = StringField('Location 5 Pick-up Tool', validators=[], default='009')
	assemble = SubmitField('Assemble')

class HybridAssembleButton(FlaskForm):
	submitHybridAssembleButton = SubmitField('Hybrid')

class HybridAssembleForm(FlaskForm):
	panelIdentifier = StringField('Hybrid panel identifier', validators=[DataRequired()])
	assemble = SubmitField('Assemble')

class ModuleAssembleButton(FlaskForm):
	submitModuleAssembleButton = SubmitField('Module')

class ModuleAssembleForm(FlaskForm):
	# Get today's date
	today = datetime.datetime.now()
	year = str(today.year)
	month = str(today.month).rjust(2, '0')
	day = str(today.day).rjust(2, '0')
	formattedDay = '{}-{}-{}'.format(year, month, day)
	# Form
	identifier = StringField('Identifier', validators=[DataRequired()])
	assemblyJig = StringField('Assembly Jig', validators=[DataRequired()])
	glueType = RadioField('Glue Type', choices=[('PO', 'Polaris'), ('SE', 'SE4445'), ('TB', 'True Blue'), ('FB', 'False Blue')], default='PO')
	pbSN = StringField('Powerboard SN', validators=[DataRequired()])
	pbAlignmentJig = StringField('Powerboard Alignment Jig', validators=[DataRequired()])
	pbPickupTool = StringField('Powerboard Pick-up Tool', validators=[])
	pbGlueDate = StringField('Powerboard Gluing Date [YYYY-MM-DD]', validators=[], default=formattedDay)
	pbGlueSample = StringField('Powerboard Glue Day Sample Number', validators=[], default='1')
	hybridxSN = StringField('Hybrid X SN', validators=[DataRequired()])
	hybridxAlignmentJig = StringField('Hybrid X Alignment Jig', validators=[DataRequired()])
	hybridxPickupTool = StringField('Hybrid X Pick-up Tool', validators=[])
	hybridxGlueDate = StringField('Hybrid X Gluing Date [YYYY-MM-DD]', validators=[], default=formattedDay)
	hybridxGlueSample = StringField('Hybrid X Glue Day Sample Number', validators=[], default='1')
	hybridySN = StringField('Hybrid Y SN', validators=[])
	hybridyAlignmentJig = StringField('Hybrid Y Alignment Jig', validators=[])
	hybridyPickupTool = StringField('Hybrid Y Pick-up Tool', validators=[])
	hybridyGlueDate = StringField('Hybrid Y Gluing Date [YYYY-MM-DD]', validators=[], default=formattedDay)
	hybridyGlueSample = StringField('Hybrid Y Glue Day Sample Number', validators=[], default='1')
	assemble = SubmitField('Assemble')

class StaveAssembleButton(FlaskForm):
	submitStaveAssembleButton = SubmitField('Stave')

class StaveAssembleForm(FlaskForm):
	#Form
	identifier = StringField('Stave Identifier', validators=[DataRequired()])
	assembler = StringField('Assembler Names',validators=[])
	glueBatchID = StringField('Glue Batch Code',validators=[])
	glueType = StringField('Glue type and mix',validators=[])
	calibTime = StringField('Stage-to-Stave Calibration Timestamp (mm-dd-yyyy)',validators=[])
	glueTime = StringField('Gluing Timestamp (mm-dd-yyyy)',validators=[])
	mod0identifier = StringField('J side Module 0 SN', validators=[])
	mod1identifier = StringField('J side Module 1 SN', validators=[])
	mod2identifier = StringField('J side Module 2 SN', validators=[])
	mod3identifier = StringField('J side Module 3 SN', validators=[])
	mod4identifier = StringField('J side Module 4 SN', validators=[])
	mod5identifier = StringField('J side Module 5 SN', validators=[])
	mod6identifier = StringField('J side Module 6 SN', validators=[])
	mod7identifier = StringField('J side Module 7 SN', validators=[])
	mod8identifier = StringField('J side Module 8 SN', validators=[])
	mod9identifier = StringField('J side Module 9 SN', validators=[])
	mod10identifier = StringField('J side Module 10 SN', validators=[])
	mod11identifier = StringField('J side Module 11 SN', validators=[])
	mod12identifier = StringField('J side Module 12 SN', validators=[])
	mod13identifier = StringField('J side Module 13 SN', validators=[])
	mod14identifier = StringField('L side Module 0 SN', validators=[])
	mod15identifier = StringField('L side Module 1 SN', validators=[])
	mod16identifier = StringField('L side Module 2 SN', validators=[])
	mod17identifier = StringField('L side Module 3 SN', validators=[])
	mod18identifier = StringField('L side Module 4 SN', validators=[])
	mod19identifier = StringField('L side Module 5 SN', validators=[])
	mod20identifier = StringField('L side Module 6 SN', validators=[])
	mod21identifier = StringField('L side Module 7 SN', validators=[])
	mod22identifier = StringField('L side Module 8 SN', validators=[])
	mod23identifier = StringField('L side Module 9 SN', validators=[])
	mod24identifier = StringField('L side Module 10 SN', validators=[])
	mod25identifier = StringField('L side Module 11 SN', validators=[])
	mod26identifier = StringField('L side Module 12 SN', validators=[])
	mod27identifier = StringField('L side Module 13 SN', validators=[])
	coreidentifier = StringField('Stave Core SN', validators=[])
	EOSJidentifier = StringField('J side EOS SN', validators=[])
	EOSLidentifier = StringField('L side EOS SN', validators=[])
	DCDCJidentifier = StringField('J side EOS DCDC SN', validators=[])
	DCDCLidentifier = StringField('L side EOS DCDC SN', validators=[])
	assemble = SubmitField('Assemble')

class ResultsButton(FlaskForm):
	submitResultsButton = SubmitField('Comparison Plots')

class IVCurveButton(FlaskForm):
	submitIVCurveButton = SubmitField('IV Curves')

class ResponseCurveButton(FlaskForm):
	submitResponseCurveButton = SubmitField('Electrical Tests')

class AMACIVButton(FlaskForm):
	submitAMACIVButton = SubmitField('AMAC IV')

class SensorIVButton(FlaskForm):
	submitSensorIVButton = SubmitField('Sensor IV')

class AMACIVForm(FlaskForm):
	identifier = StringField('ID (SN)', validators=[DataRequired()])
	load = SubmitField('Load')

class ElectricalTestForm(FlaskForm):
	identifier = StringField('ID (SN)', validators=[DataRequired()])
	testType = RadioField('Test Type', choices=[('Gain','Gain'), ('Noise', 'Noise'),('vt50', 'vt50'),('All', 'All')], default='All')
	upload = SubmitField('Load')

class IVSensorUploadOptionsForm(FlaskForm):
	PassFail = RadioField('Pass or Fail', choices=[('Pass','Pass'), ('Fail', 'Fail')], default='Pass')
	ToUpload = RadioField('Do you want to upload to the database?',choices=[('Upload','Upload'),('Delete','Delete')],default='Upload')
	submitSensorIVUploadOptions = SubmitField('Submit')

class IVModuleUploadOptionsForm(FlaskForm):
	PassFail = RadioField('Pass or Fail', choices=[('Pass','Pass'), ('Fail', 'Fail')], default='Pass')
	ToUpload = RadioField('Do you want to upload to the database?',choices=[('Upload','Upload'),('Delete','Delete')],default='Upload')
	submitModuleIVUploadOptions = SubmitField('Submit')

class GlueWeightRetrievalButton(FlaskForm):
	submitGlueWeightRetrievalButton = SubmitField('Glue Weights')

class GlueWeightRetrievalForm(FlaskForm):
	upload = SubmitField('Upload')

class MenyModIVButton(FlaskForm):
	submitMenyModIVButton = SubmitField("Multiple Module IV's")

class MenyModIVForm(FlaskForm):
	upload = SubmitField('Upload')

class TestUploaderForm(FlaskForm):
	testSubmit = SubmitField('Test')

class TestUploaderButton(FlaskForm):
	submitTestUploaderButton = SubmitField('Test Uploader')