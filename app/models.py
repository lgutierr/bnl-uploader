from datetime import datetime
from datetime import timezone
from app import app, db, login, executor
from flask_login import UserMixin
from collections import OrderedDict
from cryptography.fernet import Fernet
class User(UserMixin, db.Model):
  #Primary key = true means id is unique
  id = db.Column(db.Integer, primary_key=True)
  # name = db.Column(db.String(128), index=True, unique=True)
  name_hash = db.Column(db.LargeBinary, index=True, unique=True)
  accessCode1_hash = db.Column(db.LargeBinary, index=True, unique=True)
  accessCode2_hash = db.Column(db.LargeBinary, index=True, unique=True)
  submittedFiles = db.Column(db.String(1000), index=True)
  
  #When current_user is printed in a function, it returns this string
  def __repr__(self):
    name_hash = self.name_hash
    db_key = app.config['DB_KEY'].encode("utf-8")
    fernet = Fernet(db_key)
    name = fernet.decrypt(name_hash)
    name = name.decode('utf8') 
    return '<User {}>'.format(name)

  def set_name(self, name):
    self.name = name 

  def check_name(self, name):
    return self.name == name

  def get_name(self):
    db_key = app.config['DB_KEY'].encode("utf-8")
    fernet = Fernet(db_key)
    name = fernet.decrypt(self.name_hash).decode("utf-8")
    return name
  
  def check_task(self, task):
    return executor.futures.done(task)

  def set_submittedFiles(self, filesString):
    self.submittedFiles = filesString

@login.user_loader
def load_user(id):
  return User.query.get(int(id))
