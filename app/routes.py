from flask import render_template, flash, redirect, url_for, request, send_file, send_from_directory, session, current_app
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import current_user, login_user, logout_user, login_required
from cryptography.fernet import Fernet
from app import app, db, executor
from app.forms import *
from app.bkgTasks import *
from app.models import User
from werkzeug.urls import url_parse
import itkdb
import os
import logging
import mpld3
import pickle
import pytz

#######################
###General functions###
#######################

def clickButtonGoToURL(button, form, endpoint):
	if button.data and form.validate():
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for(endpoint)
		return redirect(next_page)
	return None

def saveDropzoneFile():
	aFPaths = []
	for f in request.files:
		aF = request.files[f]
		filename = aF.filename.replace(" ","")
		path = os.path.join("/tmp", filename)
		aF.save(path)
		pathParts = path.split("/")
		fileNameParts = pathParts[-1].split(".")
		fileNameExt = fileNameParts[-1]
		rootName = ".".join(fileNameParts[:-1])
		newRootName = rootName.replace(".","-")
		newFileName = newRootName+"."+fileNameExt
		newPath = os.path.join("/".join(pathParts[:-1]), newFileName)
		if path != newPath:
			os.system("mv "+path+" "+newPath)
		aFPaths.append(newPath)
	user = current_user
	app.logger.info("The current user is {user}")
	# user = User.query.get(current_user.id)
	currentSubmittedFiles = user.submittedFiles.split(";")
	uniqueSubmittedFiles = list(set([x for x in currentSubmittedFiles + aFPaths if x!='']))
	user.submittedFiles = ";".join(uniqueSubmittedFiles)
	db.session.commit()
	app.logger.info("The db was committed")
	user = User.query.get(current_user.id)
	app.logger.info(f"User was requeried, got user {user}")
	return

def getResult():
	output_file = request.args.get('output_file')
	output_dir = os.path.join('/tmp', 'output_files')
	output_filepath = os.path.join(output_dir, output_file)
	with open(output_filepath, 'r') as json_file:
		results = json.load(json_file)
	return results

@app.route('/download_log')
def download_log():
	# In the case that the error is unknown, and that if this link is clicked on in the error page, 
	# this function goes through the log directory and retrieves the latest log.
	log_directory = '/tmp'
	log_files = [f for f in os.listdir(log_directory) if f.startswith('error_log_')]
	if log_files:
		log_files.sort(reverse=True)
		latest_log_file = log_files[0]
		file_path = os.path.join(log_directory, latest_log_file)
		if os.path.exists(file_path):
			return send_from_directory('/tmp', latest_log_file, as_attachment=True)
		else:
			return render_template('error.html', title='Error', outputErrors='Log file not found', hasException=False)
	else:
		return render_template('error.html', title='Error', outputErrors='Log file not found', hasException=False)

def handleBackgroundTask(formInstance, bkgFunction, bkgFunctionCustomParams, webpage, title, endPoint, functionType):
	'''
	Gathers parameters from form inputs and appends custom parameters. Stores a "future" which runs in the background 
	while it goes to the loading page.

	args: 
	1) formInstance = Instance of form class
	2) bkgFunction = background function that uploads the data
	3) bkgFunctionCustomParams = custom parameters not on the form that are specific to the function
	4) webpage =  renders the webpage template/form
	5) title = Title of webpage (seen on the tab of the website)
	6) endPoint = which route it goes to after loading
	7) functionType = Specifies whether files are being uploaded or not
	returns:
	1) If form is not submitted yet, render the webpage specified.
	2) If form is submitted, go to loading.
	'''
	# Create form
	form = formInstance()
	# Look for submitted files
	if request.method == "POST":
		app.logger.info('Now going to saveDropzoneFile()')
		saveDropzoneFile()
	# Submit job to function
	# if not form.validate_on_submit():
	# 	print(form.errors)
	if form.validate_on_submit():
		app.logger.info('Form was submitted')
		# user = User.query.get(current_user.id)
		app.logger.info(f'The user before submitting the task is {current_user.get_name()}')
		task = f"{current_user.id}_{webpage}_{endPoint}"
		app.logger.info(f'The task is {task}')
		# Reset the stored future in case the user goes back a page
		try: executor.futures.pop(task)
		except KeyError: pass
		# Gather form data and create functionParams
		functionParams = []
		subFunctionParams = []
		listBoolean = 0
		for field in form:
			field_type = type(field).__name__
			if field_type == 'SubmitField' or field.name == 'csrf_token': 
				continue
			elif field_type == 'BooleanField': 
				field_data = field.data
			elif field_type == 'HiddenField':
				if 'startList' in field.name:
					listBoolean = 1
					continue
				else:
					functionParams.append(subFunctionParams)
					listBoolean = 0
					subFunctionParams = []
					continue
			elif isinstance(field.data, str): 
				field_data = field.data.strip() 
			else: 
				continue
			if listBoolean == 1:
				subFunctionParams.append(field_data)
			else:
				functionParams.append(field_data)
		# Append custom params to functionParams
		for customParam in bkgFunctionCustomParams:
			functionParams.append(customParam)
		app.logger.info(f'functionParams are {functionParams}')
		# Get submitted files if there are files - if they are blank, load the same webpage
		if functionType == 'with_files':
			# user = User.query.get(current_user.id)
			files = current_user.submittedFiles
			app.logger.info('Now getting submitted files.')
			#Return same template if no files were submitted
			if files == '':
				app.logger.info('No files were submitted')
				return render_template(webpage, title=title, form=form)
			app.logger.info(f'Files submitted were {files}')
			# Add files to functionParams
			functionParams.append(files)
			# Clear files for user since appended
			current_user.submittedFiles = ''
			app.logger.info("user's files were cleared")	
			currentUsers = User.query.all()
			app.logger.info(f"users are {currentUsers}")

			db.session.commit()
			app.logger.info("The db was committed")
			user = User.query.get(current_user.id)
			app.logger.info(f"User was requeried, got user {user}")


		app.logger.info("Form was submitted")
		app.logger.info("The function used is: ")
		app.logger.info(str(bkgFunction))
		app.logger.info("The function parameters are:")
		app.logger.info(functionParams)

		#Submit the job to the background, but also go to loading
		if task in executor.futures:
			app.logger.info(f"Task '{task}' is already in progress, skipping re-submission.")
		else:
			app.logger.info(f"Submitting new task '{task}'.")
			future = executor.submit_stored(task, bkgFunction, *functionParams)

		# future = executor.submit_stored(task, bkgFunction, *functionParams)
		app.logger.info(f"The future that was submitted on the form was {future}")
		app.logger.info(f"Task, bkgFunction, and functionParams submitted were {task}, {bkgFunction}, {functionParams}")
		next_page = url_for('loading', task=task)
		return redirect(next_page)

	#Special case where webpage needs to render with a result (like a plot with SensorIV)
	result = None
	file_path = '/tmp/saved_output'
	if os.path.exists(file_path):
		app.logger.info("The file path exists")
		with open('/tmp/saved_output', 'rb') as f:
			result = pickle.load(f)
	
	return render_template(webpage, title=title, form=form, result = result)

###############################################
######### MAIN PAGE, GENERAL ROUTING ##########
###############################################

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
	if (clickThenGoToNextPage := clickButtonGoToURL(StatusButton().submitStatusButton, StatusButton(), 'loadComponent')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(TestButton().submitTestButton, TestButton(), 'testComponent')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(RegistrationButton().submitRegistrationButton, RegistrationButton(), 'registration')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(DisassembleButton().submitDisassembleButton, DisassembleButton(), 'disassemble')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(AssembleButton().submitAssembleButton, AssembleButton(), 'assemble')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ResultsButton().submitResultsButton, ResultsButton(), 'Results')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(TestUploaderButton().submitTestUploaderButton, TestUploaderButton(), 'testUploader')): 
		return clickThenGoToNextPage
	return render_template('index.html', title='Home', statusButton=StatusButton(), testButton=TestButton(), registrationButton=RegistrationButton(), assembleButton=AssembleButton(), disassembleButton=DisassembleButton(), testUploaderButton=TestUploaderButton(), resultsButton=ResultsButton())

@app.route('/login', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		app.logger.info('User was authenticated')
		return redirect(url_for('index'))
	form = LoginForm()
	app.logger.info('On login form')
	if form.validate_on_submit():
		#Verify ITkClient. If not found, tell user to try to log in again
		try:
			itkUser = itkdb.core.User(form.accessCode1.data, form.accessCode2.data)
			itkUser.authenticate()
			itkClient = itkdb.Client(user=itkUser)
			app.logger.info(f'itkUser found in itkpd, {itkUser}')
			app.logger.info(f'itkClient is, {itkClient}')
		except:
			# Request login
			flash('User not found in the ITk database')
			return redirect(url_for('login'))
		#Query for user in database and verify password. If found, login user
		db_key = app.config['DB_KEY'].encode("utf-8")
		fernet = Fernet(db_key)
		name_encrypted = fernet.encrypt(itkUser.name.encode("utf-8"))
		#Search db and decrypt until a match is found. Inefficient but it works
		db_users = User.query.all()
		user = None
		for db_user in db_users:
			db_user_name = fernet.decrypt(db_user.name_hash)
			db_user_name = db_user_name.decode('utf8') 
			if db_user_name == itkUser.name:
				user = db_user
				app.logger.info(f"Searched db, user {db_user_name} was found")
				break
		#If user is not found
		if user is None:
			#Convert access codes to bytes for ITkClient, then encrypt
			accessCode1_encypted = fernet.encrypt(form.accessCode1.data.encode("utf-8"))
			accessCode2_encrypted = fernet.encrypt(form.accessCode2.data.encode("utf-8"))
			user = User(name_hash=name_encrypted,  accessCode1_hash = accessCode1_encypted,  accessCode2_hash = accessCode2_encrypted, submittedFiles = "")
			db.session.add(user)
			app.logger.info("user was added to db")
			db.session.commit()
			app.logger.info("The db was committed")
			currentUsers = User.query.all()
			app.logger.info(f'Users before logging in are {currentUsers}')
		login_user(user)
		currentUsers = User.query.all()
		app.logger.info(f'Users after logging in are {currentUsers}')
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for('index')
		return redirect(next_page)
	return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
	app.logger.info(f"User {current_user.get_name()} logged out")
	db.session.delete(current_user)  
	db.session.commit()
	logout_user()
	return redirect(url_for('index'))

@app.route('/loading', methods=['GET', 'POST'])
@login_required
def loading():
	task = request.args['task']
	print("The task being loaded is", task)
	return render_template('loading.html', title='Executing task', userTask=task, endpoint='checkOutputErrors')

@app.route('/checkOutputErrors', methods=['GET', 'POST'])
@login_required
def checkOutputErrors():
	try:
		task = request.args['task']
		future = executor.futures.pop(task)
		output = future.result()  
		print("Got to checkoutputerrors")
		print("The task, future, and output are below:")
		print(task)
		print(future)
		print(output)

		# If there are no errors, save the output in /tmp for later loading and route to endPoint
		if 'errors' not in output or output['errors'] == []:
			output_dir = os.path.join('/tmp', 'output_files')
			if not os.path.exists(output_dir): os.makedirs(output_dir)
			output_filename = f"{task}_output.json"
			output_filepath = os.path.join(output_dir, output_filename)

			# Dump the output to a json file
			with open(output_filepath, 'w') as json_file:
				json.dump(output, json_file)
			endPoint = task.split('_')[-1]
			next_page = url_for(endPoint, task=task, output_file=output_filename)
			return redirect(next_page)
		else:
			# If there are known errors, route to error page without logs
			outputErrors = output['errors']
			dBerrorFlag = False
			output_dBErrors = None
			if ('dBerrors' in output) and (output['dBerrors'] != []):
				dBerrorFlag = True
				#Get prettier output from the db with for loop by just getting those details
				for index, output_dBError in enumerate(output['dBerrors']):
					start = output_dBError.find("The following details may help")
					if start != -1:
						# Extract the json string
						json_part = output_dBError[start + len("The following details may help:"):].strip()
						# Parse it as json
						try:
							json_data = json.loads(json_part)
							output_dBError = json.dumps(json_data, indent=4)
						except json.JSONDecodeError:
							print("The extracted part is not valid JSON.")
					else:
						print("Could not make the string nicer")
					output['dBerrors'][index] = output_dBError
				output_dBErrors = output['dBerrors']
				print("outputDBErrors is ", output_dBError)
			return render_template('error.html', title='Error', outputErrors=outputErrors, output_dBErrors = output_dBErrors, dBerrorFlag = dBerrorFlag, hasException=False)
	except Exception as e:
		# Create unique log by timestamping it
		log_directory = '/tmp'
		ny_tz = pytz.timezone('America/New_York')
		ny_time = datetime.now(ny_tz)
		timestamp = ny_time.strftime("%Y-%m-%d_%H-%M-%S")
		log_file_name = f'error_log_{timestamp}.txt'
		log_file_path = os.path.join(log_directory, log_file_name)
		logging.basicConfig(filename=log_file_path, level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
		logging.exception(str(e))
		# Output the general error and log
		outputErrors = 'An unknown error occurred with the uploader. Please see attached log:'
		return render_template('error.html', title='Error', outputErrors=outputErrors, hasException=True)

@app.route('/successEndpoint')
@login_required
def successEndpoint():
	result = getResult()
	return render_template('successEndpoint.html', title = 'Successful Upload', result=result)

###############################
########### ROUTES ############
###############################

@app.route('/loadComponent', methods=['GET', 'POST'])
@login_required
def loadComponent():
	return handleBackgroundTask(CompLoadingForm, bkgComponentStatus, [], 'loadComponent.html', 'Get component info', 'loadComponentEndpoint', 'no_files')

@app.route('/loadComponentEndpoint')
@login_required
def loadComponentEndpoint():
	result = getResult()
	return render_template('loadComponentEndpoint.html', title='Component Endpoint', component=result)

@app.route('/testComponent', methods=['GET', 'POST'])
@login_required
def testComponent():
	if (clickThenGoToNextPage := clickButtonGoToURL(ASICsTestButton().submitASICsTestButton, ASICsTestButton(), 'asicsTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(CicorelTestButton().submitCicorelTestButton, CicorelTestButton(), 'cicorelTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HalfmoonTestButton().submitHalfmoonTestButton, HalfmoonTestButton(), 'halfmoonTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HVtabTestButton().submitHVtabTestButton, HVtabTestButton(), 'hvtabTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(SensorTestButton().submitSensorTestButton, SensorTestButton(), 'sensorTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardTestButton().submitPowerboardTestButton, PowerboardTestButton(), 'powerboardTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridFlexArrayTestButton().submitHybridFlexArrayTestButton, HybridFlexArrayTestButton(), 'hybridFlexArrayTests')): 
		return clickThenGoToNextPage	
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridTestButton().submitHybridTestButton, HybridTestButton(), 'hybridTests')): 
		return clickThenGoToNextPage	
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleTestButton().submitModuleTestButton, ModuleTestButton(), 'moduleTests')): 
		return clickThenGoToNextPage	
	if (clickThenGoToNextPage := clickButtonGoToURL(StaveTestButton().submitStaveTestButton, StaveTestButton(), 'staveTests')): 
		return clickThenGoToNextPage	
	return render_template('test.html', title='Test', asicsTestButton = ASICsTestButton(), cicorelTestButton=CicorelTestButton(), halfmoonTestButton=HalfmoonTestButton(), hvtabTestButton=HVtabTestButton(), sensorTestButton=SensorTestButton(), powerboardTestButton=PowerboardTestButton(), hybridFlexArrayTestButton=HybridFlexArrayTestButton(), hybridTestButton=HybridTestButton(), moduleTestButton=ModuleTestButton(),  staveTestButton=StaveTestButton())

@app.route('/asicsTests', methods=['GET', 'POST'])
@login_required
def asicsTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(ASICsPullTestButton().submitASICsPullTestButton, ASICsPullTestButton(), 'asicsPullTest')): 
		return clickThenGoToNextPage	
	if (clickThenGoToNextPage := clickButtonGoToURL(ASICsDropboxButton().submitASICsDropboxButton, ASICsDropboxButton(), 'asicsDropbox')): 
		return clickThenGoToNextPage	
	return render_template('asicsTests.html', title='ASICs Tests', asicsPullTestButton=ASICsPullTestButton(), asicsDropboxButton=ASICsDropboxButton())

@app.route('/asicsPullTest', methods=['GET', 'POST'])
@login_required
def asicsPullTest():
	return handleBackgroundTask(ASICsPullTestForm, bkgPullTest, ['asic'], 'asicsPullTest.html', 'ASICs Pull Test', 'successEndpoint', 'with_files')

@app.route('/asicsDropbox', methods=['GET', 'POST'])
@login_required
def asicsDropbox():
	return handleBackgroundTask(ASICsDropboxForm, bkgDropbox, ['asic/dropbox'], 'asicsDropbox.html', 'ASICs Dropbox', 'successEndpoint', 'with_files')

@app.route('/cicorelTests', methods=['GET', 'POST'])
@login_required
def cicorelTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(CicorelPullTestButton().submitCicorelPullTestButton, CicorelPullTestButton(), 'cicorelPullTest')): 
		return clickThenGoToNextPage	
	if (clickThenGoToNextPage := clickButtonGoToURL(CicorelDropboxButton().submitCicorelDropboxButton, CicorelDropboxButton(), 'cicorelDropbox')): 
		return clickThenGoToNextPage	
	return render_template('cicorelTests.html', title='Cicorel Tests', cicorelPullTestButton=CicorelPullTestButton(), cicorelDropboxButton=CicorelDropboxButton())

@app.route('/cicorelPullTest', methods=['GET', 'POST'])
@login_required
def cicorelPullTest():
	return handleBackgroundTask(CicorelPullTestForm, bkgPullTest, ['cicorel'], 'cicorelPullTest.html', 'Cicorel Pull Test', 'successEndpoint', 'with_files')

@app.route('/cicorelDropbox', methods=['GET', 'POST'])
@login_required
def cicorelDropbox():
	return handleBackgroundTask(CicorelDropboxForm, bkgDropbox, ["cicorel/dropbox"], 'cicorelDropbox.html', 'Cicorel Dropbox', 'successEndpoint', 'with_files')

@app.route('/halfmoonTests', methods=['GET', 'POST'])
@login_required
def halfmoonTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(HalfmoonPullTestButton().submitHalfmoonPullTestButton, HalfmoonPullTestButton(), 'halfmoonPullTest')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HalfmoonDropboxButton().submitHalfmoonDropboxButton, HalfmoonDropboxButton(), 'halfmoonDropbox')): 
		return clickThenGoToNextPage
	return render_template('halfmoonTests.html', title='Halfmoon Tests', halfmoonPullTestButton=HalfmoonPullTestButton(), halfmoonDropboxButton=HalfmoonDropboxButton())

@app.route('/halfmoonPullTest', methods=['GET', 'POST'])
@login_required
def halfmoonPullTest():
	return handleBackgroundTask(HalfmoonPullTestForm, bkgPullTest, ["halfmoon"], 'halfmoonPullTest.html', 'Halfmoon Pull Test', 'successEndpoint', 'with_files')

@app.route('/halfmoonDropbox', methods=['GET', 'POST'])
@login_required
def halfmoonDropbox():
	return handleBackgroundTask(HalfmoonDropboxForm, bkgDropbox, ["halfmoon/dropbox"], 'halfmoonDropbox.html', 'Halfmoon Dropbox', 'successEndpoint', 'with_files')

@app.route('/hvtabTests', methods=['GET', 'POST'])
@login_required
def hvtabTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(HVtabShearTestButton().submitHVtabShearTestButton, HVtabShearTestButton(), 'hvtabShearTest')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HVtabDropboxButton().submitHVtabDropboxButton, HVtabDropboxButton(), 'hvtabDropbox')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HVtabVisualInspectionButton().submitHVtabVisualInspectionButton, HVtabVisualInspectionButton(), 'hvtabVisualInspection')): 
		return clickThenGoToNextPage
	return render_template('hvtabTests.html', title='HVtab Tests', hvtabShearTestButton=HVtabShearTestButton(), hvtabVisualInspectionButton=HVtabVisualInspectionButton(), hvtabDropboxButton=HVtabDropboxButton())

@app.route('/hvtabShearTest', methods=['GET', 'POST'])
@login_required
def hvtabShearTest():
	return handleBackgroundTask(HVtabShearTestForm, bkgHVtabShearTest, [], 'hvtabShearTest.html', 'HVtab Shear Test', 'successEndpoint', 'no_files')

@app.route('/hvtabVisualInspection', methods=['GET', 'POST'])
@login_required
def hvtabVisualInspection():
	return handleBackgroundTask(HVtabVisualInspectionForm, bkgVisualInspection, ['SHEET_WITH_TABS','false','hvtab'], 'hvtabVisualInspection.html', 'HVtab Visual Inspection', 'successEndpoint', 'no_files')

@app.route('/hvtabDropbox', methods=['GET', 'POST'])
@login_required
def hvtabDropbox():
	return handleBackgroundTask(HVtabDropboxForm, bkgDropbox, ["hvtab/dropbox"], 'hvtabDropbox.html', 'HVtab Dropbox', 'successEndpoint', 'with_files')

@app.route('/sensorTests', methods=['GET', 'POST'])
@login_required
def sensorTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(SensorIVButton().submitSensorIVButton,SensorIVButton(), 'sensorIV')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(sensorVisualInspectionReceptionButton().submitSensorVisualInspectionReceptionButton,sensorVisualInspectionReceptionButton(), 'sensorVisualInspectionReception')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(SensorDropboxButton().submitSensorDropboxButton,SensorDropboxButton(), 'sensorDropbox')): 
		return clickThenGoToNextPage
	return render_template('sensorTests.html', title='Sensor Tests', sensorIVButton=SensorIVButton(), sensorVisualInspectionReceptionButton=sensorVisualInspectionReceptionButton(), sensorDropboxButton=SensorDropboxButton())

@app.route('/sensorIV', methods=['GET', 'POST'])
@login_required
def sensorIV():
	return handleBackgroundTask(SensorIVForm, bkgSensorIV, [], 'sensorIV.html', 'Sensor IV', 'sensorIV2', 'with_files')

@app.route('/sensorIV2', methods=['GET', 'POST'])
@login_required
def sensorIV2():
	result = getResult()
	if result:
		result = bkgIVGraph(result)
		with open('/tmp/saved_output', 'wb') as f:
			pickle.dump(result, f)
	else:
		with open('/tmp/saved_output', 'rb') as f:
			result = pickle.load(f)	
	form = IVSensorUploadOptionsForm()
	if form.validate_on_submit() and form.ToUpload.data.strip() == 'Upload':
		return handleBackgroundTask(IVSensorUploadOptionsForm, bkgUploadSensorIV, [result], 'sensorIV2.html', 'Sensor IV Status', 'successEndpoint', 'no_files')
	if form.validate_on_submit() and form.ToUpload.data.strip() == 'Delete':
		return handleBackgroundTask(IVSensorUploadOptionsForm, bkgUploadSensorIV, [result], 'sensorIV2.html', 'Sensor IV Status', 'sensorTests', 'no_files')
	return render_template('sensorIV2.html', title='Sensor IV Status', form=form, result=result)

@app.route('/sensorVisualInspectionReception', methods=['GET', 'POST'])
@login_required
def sensorVisualInspectionReception():
	return handleBackgroundTask(sensorVisualInspectionReceptionForm, bkgSensorVisualInspectionReception, [], 'sensorVisualInspectionReception.html', 'Sensor Visual Inspection (Module Reception)', 'successEndpoint', 'with_files')

@app.route('/sensorDropbox', methods=['GET', 'POST'])
@login_required
def sensorDropbox():
	return handleBackgroundTask(SensorDropboxForm, bkgDropbox, ["sensor/dropbox"], 'sensorDropbox.html', 'Sensor Dropbox', 'successEndpoint', 'with_files')

@app.route('/powerboardTests', methods=['GET', 'POST'])
@login_required
def powerboardTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardElectricalTestsButton().submitPowerboardElectricalTestsButton,PowerboardElectricalTestsButton(), 'powerboardElectricalTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardCarrierPullTestButton().submitPowerboardCarrierPullTestButton,PowerboardCarrierPullTestButton(), 'powerboardCarrierPullTest')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardVisualInspectionButton().submitPowerboardVisualInspectionButton,PowerboardVisualInspectionButton(), 'powerboardVisualInspection')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardCarrierVisualInspectionButton().submitPowerboardCarrierVisualInspectionButton,PowerboardCarrierVisualInspectionButton(), 'powerboardCarrierVisualInspection')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(PowerboardDropboxButton().submitPowerboardDropboxButton,PowerboardDropboxButton(), 'powerboardDropbox')): 
		return clickThenGoToNextPage
	return render_template('powerboardTests.html', title='Powerboard Tests', powerboardElectricalTestsButton=PowerboardElectricalTestsButton(), powerboardVisualInspectionButton=PowerboardVisualInspectionButton(), powerboardCarrierVisualInspectionButton=PowerboardCarrierVisualInspectionButton(), powerboardDropboxButton=PowerboardDropboxButton(), PowerboardCarrierPullTestButton=PowerboardCarrierPullTestButton())

@app.route('/powerboardElectricalTests', methods=['GET', 'POST'])
@login_required
def powerboardElectricalTests():
	return handleBackgroundTask(PowerboardElectricalTestsForm, bkgPowerboardElectricalTests, [], 'powerboardElectricalTests.html', 'Powerboard Electrical Tests', 'successEndpoint', 'with_files')

@app.route('/powerboardVisualInspection', methods=['GET', 'POST'])
@login_required
def powerboardVisualInspection():
	return handleBackgroundTask(PowerboardVisualInspectionForm, bkgVisualInspection, ['MODULE_RCP','false','powerboard'], 'powerboardVisualInspection.html', 'Powerboard Visual Inspection', 'successEndpoint', 'no_files')

@app.route('/powerboardCarrierVisualInspection', methods=['GET', 'POST'])
@login_required
def powerboardCarrierVisualInspection():
	return handleBackgroundTask(PowerboardCarrierVisualInspectionForm, bkgPowerboardCarrierVisualInspectionFetchChildren, [], 'powerboardCarrierVisualInspection.html', 'Powerboard Carrier Visual Inspection', 'powerboardCarrierVisualInspectionLoad', 'no_files')

@app.route('/powerboardCarrierVisualInspectionLoad', methods=['GET', 'POST'])
@login_required
def powerboardCarrierVisualInspectionLoad():
	form = PowerboardCarrierVisualInspectionLoadForm()
	powerBoardChildren = getResult()
	for i in range(10):
		getattr(form, f'powerboard_{i}').data = powerBoardChildren[i] if i < len(powerBoardChildren) else ''
	if form.validate_on_submit():
		return handleBackgroundTask(PowerboardCarrierVisualInspectionLoadForm, bkgMultipleVisualInspection, ['MODULE_RCP','powerboard'], 'powerboardCarrierVisualInspectionLoad.html', 'Powerboard Carrier Visual Inspection', 'powerboardCarrierVisualInspectionEndpoint', 'no_files')
	return render_template('powerboardCarrierVisualInspectionLoad.html', title='Powerboard Carrier Visual Inspection', form=form)

@app.route('/powerboardCarrierVisualInspectionEndpoint', methods=['GET', 'POST'])
@login_required
def powerboardCarrierVisualInspectionEndpoint():
	results = getResult()
	print("Finally in the endpoint")
	return render_template('powerboardCarrierVisualInspectionEndpoint.html', title='Powerboard Carrier Visual Inspection Status', results=results)

@app.route('/powerboardCarrierPullTest', methods=['GET', 'POST'])
@login_required
def powerboardCarrierPullTest():
	return handleBackgroundTask(PowerboardCarrierPullTestForm, bkgPullTest, ["powerboard"], 'powerboardCarrierPullTest.html', 'Powerboard Pull Test', 'successEndpoint', 'with_files')

@app.route('/powerboardDropbox', methods=['GET', 'POST'])
@login_required
def powerboardDropbox():
	return handleBackgroundTask(PowerboardDropboxForm, bkgDropbox, ["powerboard/dropbox"], 'powerboardDropbox.html', 'Powerboard Visual Inspection', 'successEndpoint', 'with_files')

@app.route('/hybridFlexArrayTests', methods=['GET', 'POST'])
@login_required
def hybridFlexArrayTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridFlexArrayPullTestButton().submitHybridFlexArrayPullTestButton,HybridFlexArrayPullTestButton(), 'hybridFlexArrayPullTest')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridFlexArrayDropboxButton().submitHybridFlexArrayDropboxButton,HybridFlexArrayDropboxButton(), 'hybridFlexArrayDropbox')): 
		return clickThenGoToNextPage
	return render_template('hybridFlexArrayTests.html', title='Hybrid Flex Array Tests', hybridFlexArrayPullTestButton=HybridFlexArrayPullTestButton(), hybridFlexArrayDropboxButton=HybridFlexArrayDropboxButton())

@app.route('/hybridFlexArrayPullTest', methods=['GET', 'POST'])
@login_required
def hybridFlexArrayPullTest():
	return handleBackgroundTask(HybridFlexArrayPullTestForm, bkgPullTest, ["hybridflexarray"], 'hybridFlexArrayPullTest.html', 'Powerboard Visual Inspection', 'successEndpoint', 'with_files')

@app.route('/hybridFlexArrayDropbox', methods=['GET', 'POST'])
@login_required
def hybridFlexArrayDropbox():
	return handleBackgroundTask(HybridFlexArrayDropboxForm, bkgDropbox, ["hybridFlexArray/dropbox"], 'hybridFlexArrayDropbox.html', 'Hybrid Flex Array Dropbox', 'successEndpoint', 'with_files')

@app.route('/hybridTests', methods=['GET', 'POST'])
@login_required
def hybridTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridVisualInspectionButton().submitHybridVisualInspectionButton,HybridVisualInspectionButton(), 'hybridVisualInspection')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridGlueWeightButton().submitHybridGlueWeightButton,HybridGlueWeightButton(), 'hybridGlueWeight')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridMetrologyButton().submitHybridMetrologyButton,HybridMetrologyButton(), 'hybridMetrology')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridWireBondingButton().submitHybridWireBondingButton,HybridWireBondingButton(), 'hybridWireBonding')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridElectricalTestsButton().submitHybridElectricalTestsButton,HybridElectricalTestsButton(), 'hybridElectricalTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridDropboxButton().submitHybridDropboxButton,HybridDropboxButton(), 'hybridDropbox')): 
		return clickThenGoToNextPage
	return render_template('hybridTests.html', title='Hybrid Tests', hybridVisualInspectionButton=HybridVisualInspectionButton(), hybridGlueWeightButton=HybridGlueWeightButton(), hybridMetrologyButton=HybridMetrologyButton(), hybridWireBondingButton=HybridWireBondingButton(), hybridElectricalTestsButton=HybridElectricalTestsButton(), hybridDropboxButton=HybridDropboxButton())

@app.route('/hybridVisualInspection', methods=['GET', 'POST'])
@login_required
def hybridVisualInspection():
	return handleBackgroundTask(HybridVisualInspectionForm, bkgVisualInspection, ['false', 'hybrid'], 'hybridVisualInspection.html', 'Hybrid Visual Inspection', 'successEndpoint', 'no_files')

@app.route('/hybridGlueWeight', methods=['GET', 'POST'])
@login_required
def hybridGlueWeight():
	return handleBackgroundTask(HybridGlueWeightForm, bkgHybridGlueWeight, [], 'hybridGlueWeight.html', 'Hybrid Glue Weight', 'successEndpoint', 'no_files')

@app.route('/hybridWireBonding', methods=['GET', 'POST'])
@login_required
def hybridWireBonding():
	return handleBackgroundTask(HybridWireBondingForm, bkgHybridWireBonding, [], 'hybridWireBonding.html', 'Hybrid Wire Bonding', 'successEndpoint', 'no_files')

@app.route('/hybridMetrology', methods=['GET', 'POST'])
@login_required
def hybridMetrology():
	form = HybridMetrologyForm()
	if request.method == "POST":
		saveDropzoneFile()
	if form.check.data and form.validate():
		return handleBackgroundTask(HybridMetrologyForm, bkgHybridMetrology, [True], 'hybridMetrology.html', 'Hybrid Metrology', 'hybridMetrologyCheckEndpoint', 'with_files')
	if form.upload.data and form.validate():
		return handleBackgroundTask(HybridMetrologyForm, bkgHybridMetrology, [False], 'hybridMetrology.html', 'Hybrid Metrology', 'hybridMetrologyEndpoint', 'with_files')	
	return render_template('hybridMetrology.html', title='Hybrid Metrology', form=form)

@app.route('/hybridMetrologyCheckEndpoint', methods=['GET', 'POST'])
@login_required
def hybridMetrologyCheckEndpoint():
	result = getResult()
	tarFile = result['sendFiles'][0]
	return render_template('hybridMetrologyCheckEndpoint.html', title='Hybrid Metrology Check Status', userTarFile=tarFile, result=result)

@app.route('/hybridMetrologyCheck', methods=['GET', 'POST'])
@login_required
def hybridMetrologyCheck():
	tarFile = request.args['tarFile']
	return send_file(tarFile, as_attachment = True)

@app.route('/hybridMetrologyEndpoint', methods=['GET', 'POST'])
@login_required
def hybridMetrologyEndpoint():
	result = getResult()
	tarFile = result['sendFiles'][0]
	return render_template('hybridMetrologyEndpoint.html', title='Hybrid Metrology Endpoint', result=result, userTarFile=tarFile)

@app.route('/hybridElectricalTests', methods=['GET', 'POST'])
@login_required
def hybridElectricalTests():
	return handleBackgroundTask(HybridElectricalTestsForm, bkgHybridElectricalTests, [], 'hybridElectricalTests.html', 'Hybrid Electrical Tests', 'successEndpoint', 'with_files')

@app.route('/hybridDropbox', methods=['GET', 'POST'])
@login_required
def hybridDropbox():
	return handleBackgroundTask(HybridDropboxForm, bkgDropbox, ["hybrid/dropbox"], 'hybridDropbox.html', 'Hybrid Dropbox Status', 'successEndpoint', 'with_files')

@app.route('/moduleTests', methods=['GET', 'POST'])
@login_required
def moduleTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleVisualInspectionButton().submitModuleVisualInspectionButton,ModuleVisualInspectionButton(), 'moduleVisualInspection')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleIVButton().submitModuleIVButton,ModuleIVButton(), 'moduleIV')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleGlueWeightButton().submitModuleGlueWeightButton,ModuleGlueWeightButton(), 'moduleGlueWeight')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleMetrologyButton().submitModuleMetrologyButton,ModuleMetrologyButton(), 'moduleMetrology')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleWireBondingButton().submitModuleWireBondingButton,ModuleWireBondingButton(), 'moduleWireBonding')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleElectricalTestsButton().submitModuleElectricalTestsButton,ModuleElectricalTestsButton(), 'moduleElectricalTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleDropboxButton().submitModuleDropboxButton,ModuleDropboxButton(), 'moduleDropbox')): 
		return clickThenGoToNextPage
	return render_template('moduleTests.html', title='Module Tests', moduleVisualInspectionButton=ModuleVisualInspectionButton(), moduleIVButton=ModuleIVButton(), moduleGlueWeightButton=ModuleGlueWeightButton(), moduleMetrologyButton=ModuleMetrologyButton(), moduleWireBondingButton=ModuleWireBondingButton(), moduleElectricalTestsButton=ModuleElectricalTestsButton(), moduleDropboxButton=ModuleDropboxButton())

@app.route('/moduleVisualInspection', methods=['GET', 'POST'])
@login_required
def moduleVisualInspection():
	return handleBackgroundTask(ModuleVisualInspectionForm, bkgVisualInspection, ['module'], 'moduleVisualInspection.html', 'Module Visual Inspection', 'successEndpoint', 'no_files')

@app.route('/moduleIV', methods=['GET', 'POST'])
@login_required
def moduleIV():
	return handleBackgroundTask(ModuleIVForm, bkgModuleIV, [], 'moduleIV.html', 'Module IV', 'moduleIV2', 'with_files')

@app.route('/moduleIV2', methods=['GET', 'POST'])
@login_required
def moduleIV2():
	result = getResult()
	if result:
		result = bkgIVGraph(result)
		with open('/tmp/saved_output', 'wb') as f:
			pickle.dump(result, f)
	else:
		with open('/tmp/saved_output', 'rb') as f:
			result = pickle.load(f)	
	print("Now in routing")
	form = IVModuleUploadOptionsForm()
	if form.validate_on_submit() and form.ToUpload.data.strip() == 'Upload':
		print("Made it to validate on submit, upload")
		return handleBackgroundTask(IVModuleUploadOptionsForm, bkgUploadModuleIV, [result], 'moduleIV2.html', 'Module IV Status', 'successEndpoint', 'no_files')
	if form.validate_on_submit() and form.ToUpload.data.strip() == 'Delete':
		print("Made it to validate on submit, delete")
		return handleBackgroundTask(IVModuleUploadOptionsForm, bkgUploadModuleIV, [result], 'moduleIV2.html', 'Module IV Status', 'moduleTests', 'no_files')
	return render_template('moduleIV2.html', title='Module IV Status', form=form, result=result)

@app.route('/moduleGlueWeight', methods=['GET', 'POST'])
@login_required
def moduleGlueWeight():
	return handleBackgroundTask(ModuleGlueWeightForm, bkgModuleGlueWeight, [], 'moduleGlueWeight.html', 'Module Glue Weight', 'successEndpoint', 'no_files')

@app.route('/moduleMetrology', methods=['GET', 'POST'])
@login_required
def moduleMetrology():
	form = ModuleMetrologyForm()
	if request.method == "POST":
		saveDropzoneFile()
	if form.check.data and form.validate():
		return handleBackgroundTask(ModuleMetrologyForm, bkgModuleMetrology, [True], 'moduleMetrology.html', 'Module Metrology', 'moduleMetrologyCheckEndpoint', 'with_files')
	if form.upload.data and form.validate():
		return handleBackgroundTask(ModuleMetrologyForm, bkgModuleMetrology, [False], 'moduleMetrology.html', 'Module Metrology', 'moduleMetrologyEndpoint', 'with_files')	
	return render_template('moduleMetrology.html', title='Module Metrology', form=form)

@app.route('/moduleMetrologyCheckEndpoint', methods=['GET', 'POST'])
@login_required
def moduleMetrologyCheckEndpoint():
	result = getResult()
	tarFile = result['sendFiles'][0]
	return render_template('moduleMetrologyCheckEndpoint.html', title='Module Metrology Check Status', userTarFile=tarFile, result=result)

@app.route('/moduleMetrologyCheck', methods=['GET', 'POST'])
@login_required
def moduleMetrologyCheck():
	tarFile = request.args['tarFile']
	return send_file(tarFile, as_attachment = True)

@app.route('/moduleMetrologyEndpoint', methods=['GET', 'POST'])
@login_required
def moduleMetrologyEndpoint():
	result = getResult()
	tarFile = result['sendFiles'][0]
	return render_template('moduleMetrologyEndpoint.html', title='Module Metrology Endpoint', result=result, userTarFile=tarFile)

@app.route('/moduleWireBonding', methods=['GET', 'POST'])
@login_required
def moduleWireBonding():
	return handleBackgroundTask(ModuleWireBondingForm, bkgModuleWireBonding, [], 'moduleWireBonding.html', 'Module Wire Bonding', 'successEndpoint', 'no_files')

@app.route('/moduleElectricalTests', methods=['GET', 'POST'])
@login_required
def moduleElectricalTests():
	return handleBackgroundTask(ModuleElectricalTestsForm, bkgModuleElectricalTests, [], 'moduleElectricalTests.html', 'Module Electrical Tests', 'successEndpoint', 'with_files')

@app.route('/moduleDropbox', methods=['GET', 'POST'])
@login_required
def moduleDropbox():
	return handleBackgroundTask(ModuleDropboxForm, bkgDropbox, ["module/dropbox"], 'moduleDropbox.html', 'Module Dropbox', 'successEndpoint', 'with_files')

@app.route('/staveTests', methods=['GET', 'POST'])
@login_required
def staveTests():
	if (clickThenGoToNextPage := clickButtonGoToURL(ModulePlacementAccuracyButton().submitModulePlacementAccuracyButton, ModulePlacementAccuracyButton(), 'modulePlacementAccuracy_Side_J')):
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ConfocalScanButton().submitConfocalScanButton, ConfocalScanButton(), 'confocalScan')):
		return clickThenGoToNextPage
	return render_template('staveTests.html', title='Stave Tests', modulePlacementAccuracyButton=ModulePlacementAccuracyButton(), confocalScanButton=ConfocalScanButton())

@app.route('/confocalScan', methods=['GET', 'POST'])
@login_required
def confocalScan():
	return handleBackgroundTask(ConfocalScanForm, bkgConfocalScan, [], 'confocalScan.html', 'Confocal Scan', 'successEndpoint', 'with_files')

@app.route('/modulePlacementAccuracy_Side_J', methods=['GET', 'POST'])
@login_required
def modulePlacementAccuracy_Side_J():
	return handleBackgroundTask(ModulePlacementAccuracy_Side_J_Form, bkgModulePlacementAccuracy_Side_J, [], 'modulePlacementAccuracy_Side_J.html', 'Module Placement Accuracy', 'modulePlacementAccuracy', 'with_files')

@app.route('/modulePlacementAccuracy', methods=['GET', 'POST'])
@login_required
def modulePlacementAccuracy():
	return handleBackgroundTask(ModulePlacementAccuracyForm, bkgModulePlacementAccuracy, [], 'modulePlacementAccuracy.html', 'Module Placement Accuracy', 'modulePlacementAccuracyEndpoint', 'with_files')

@app.route('/modulePlacementAccuracyEndpoint', methods=['GET', 'POST'])
@login_required
def modulePlacementAccuracyEndpoint():
	result = getResult()
	excelFile = result['sendFiles']
	return render_template('modulePlacementAccuracyEndpoint.html', title='Module Placement Accuracy Status', excelFile=excelFile, result = result)

@app.route('/modulePlacementAccuracyEndpoint2', methods=['GET', 'POST'])
@login_required
def modulePlacementAccuracyEndpoint2():
	excelFile = request.args['excelFile']
	return send_file(excelFile, as_attachment = True)

@app.route('/registration', methods=['GET', 'POST'])
@login_required
def registration():
	if (clickThenGoToNextPage := clickButtonGoToURL(HVtabSheetRegistrationButton().submitHVtabSheetRegistrationButton,HVtabSheetRegistrationButton(), 'hvtabSheetRegistration')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridRegistrationButton().submitHybridRegistrationButton,HybridRegistrationButton(), 'hybridRegistration')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridTestPanelRegistrationButton().submitHybridTestPanelRegistrationButton,HybridTestPanelRegistrationButton(), 'hybridTestPanelRegistration')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleRegistrationButton().submitModuleRegistrationButton,ModuleRegistrationButton(), 'moduleRegistration')): 
		return clickThenGoToNextPage
	return render_template('registration.html', title='Registration', hvtabSheetRegistrationButton=HVtabSheetRegistrationButton(), hybridRegistrationButton = HybridRegistrationButton(), hybridTestPanelRegistrationButton=HybridTestPanelRegistrationButton(), moduleRegistrationButton=ModuleRegistrationButton())

@app.route('/hvtabSheetRegistration', methods=['GET', 'POST'])
@login_required
def hvtabSheetRegistration():
	return handleBackgroundTask(HVtabSheetRegistrationForm, bkgHVtabSheetRegistration, [], 'hvtabSheetRegistration.html', 'HVtab Sheet registration', 'successEndpoint', 'no_files')

@app.route('/hybridRegistration', methods=['GET', 'POST'])
@login_required
def hybridRegistration():
	form = HybridRegistrationForm()
	if form.validate_on_submit():
		localName = form.localName.data.strip()
		hType = localName.split("-")[-1]
		return handleBackgroundTask(HybridRegistrationForm, bkgHybridRegistration, [hType], 'hybridRegistration.html', 'Hybrid registration', 'successEndpoint', 'no_files')
	return render_template('hybridRegistration.html', title='Hybrid registration', form=form)

@app.route('/hybridTestPanelRegistration', methods=['GET', 'POST'])
@login_required
def hybridTestPanelRegistration():
	return handleBackgroundTask(HybridTestPanelRegistrationForm, bkgHybridTestPanelRegistration, [], 'hybridTestPanelRegistration.html', 'HybridTestPanel registration', 'successEndpoint', 'no_files')

@app.route('/moduleRegistration', methods=['GET', 'POST'])
@login_required
def moduleRegistration():
	form = ModuleRegistrationForm()
	if form.validate_on_submit():
		localName = form.localName.data.strip()
		if 'SS' in localName: mType = 'SS'
		else: mType = 'LS'
		return handleBackgroundTask(ModuleRegistrationForm, bkgModuleRegistration, [mType], 'moduleRegistration.html', 'Module registration', 'successEndpoint', 'no_files')
	return render_template('moduleRegistration.html', title='Module registration', form=form)

@app.route('/disassemble', methods=['GET', 'POST'])
@login_required
def disassemble():
	if (clickThenGoToNextPage := clickButtonGoToURL(ASICsGelpackDisassembleButton().submitASICsGelpackDisassembleButton,ASICsGelpackDisassembleButton(), 'asicsGelpackDisassemble')): 
		return clickThenGoToNextPage
	return render_template('disassemble.html', title='Disassemble', asicsGelpackDisassembleButton=ASICsGelpackDisassembleButton())
	 
@app.route('/asicsGelpackDisassemble', methods=['GET', 'POST'])
@login_required
def asicsGelpackDisassemble():
	return handleBackgroundTask(ASICsGelpackDisassembleForm, bkgASICsGelpackDisassemble, [],'asicsGelpackDisassemble.html', 'ASICsGelpack disassemble', 'successEndpoint', 'no_files')

@app.route('/assemble', methods=['GET', 'POST'])
@login_required
def assemble():
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridTestPanelAssembleButton().submitHybridTestPanelAssembleButton,HybridTestPanelAssembleButton(), 'hybridTestPanelAssemble')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(HybridAssembleButton().submitHybridAssembleButton,HybridAssembleButton(), 'hybridAssemble')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ModuleAssembleButton().submitModuleAssembleButton,ModuleAssembleButton(), 'moduleAssemble')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(StaveAssembleButton().submitStaveAssembleButton,StaveAssembleButton(), 'staveAssemble')): 
		return clickThenGoToNextPage
	return render_template('assemble.html', title='Assemble', hybridTestPanelAssembleButton=HybridTestPanelAssembleButton(), hybridAssembleButton=HybridAssembleButton(), moduleAssembleButton=ModuleAssembleButton(),staveAssembleButton=StaveAssembleButton())

@app.route('/hybridTestPanelAssemble', methods=['GET', 'POST'])
@login_required
def hybridTestPanelAssemble():
	return handleBackgroundTask(HybridTestPanelAssembleForm, bkgHybridTestPanelAssemble, [],'hybridTestPanelAssemble.html', 'Hybrid Test Panel assemble', 'successEndpoint', 'no_files')

@app.route('/hybridAssemble', methods=['GET', 'POST'])
@login_required
def hybridAssemble():
	return handleBackgroundTask(HybridAssembleForm, bkgHybridAssemble, [],'hybridAssemble.html', 'Hybrid assemble', 'successEndpoint', 'with_files')

@app.route('/moduleAssemble', methods=['GET', 'POST'])
@login_required
def moduleAssemble():
	return handleBackgroundTask(ModuleAssembleForm, bkgModuleAssemble, [], 'moduleAssemble.html', 'Module assemble', 'successEndpoint', 'no_files')

@app.route('/staveAssemble', methods=['GET', 'POST'])
@login_required
def staveAssemble():
	return handleBackgroundTask(StaveAssembleForm, bkgStaveAssemble, [], 'staveAssemble.html', 'Stave assemble', 'successEndpoint', 'no_files')

@app.route('/Results', methods=['GET', 'POST'])
@login_required
def Results():
	if (clickThenGoToNextPage := clickButtonGoToURL(IVCurveButton().submitIVCurveButton,IVCurveButton(), 'IVCurves')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(ResponseCurveButton().submitResponseCurveButton,ResponseCurveButton(), 'electricalTests')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(GlueWeightRetrievalButton().submitGlueWeightRetrievalButton,GlueWeightRetrievalButton(), 'glueWeightRetrieval')): 
		return clickThenGoToNextPage
	return render_template('results.html', title='Results', responseCurveButton=ResponseCurveButton(), ivCurveButton=IVCurveButton(),glueWeightRetrievalButton = GlueWeightRetrievalButton())

@app.route('/IVCurves', methods=['GET', 'POST'])
@login_required
def IVCurves():
	if (clickThenGoToNextPage := clickButtonGoToURL(AMACIVButton().submitAMACIVButton,AMACIVButton(), 'amacIV')): 
		return clickThenGoToNextPage
	if (clickThenGoToNextPage := clickButtonGoToURL(MenyModIVButton().submitMenyModIVButton,MenyModIVButton(), 'menyModIV')): 
		return clickThenGoToNextPage
	return render_template('IVCurves.html', title='IV Curves', amacIVButton=AMACIVButton(), menyModIVButton=MenyModIVButton())

@app.route('/amacIV', methods=['GET', 'POST'])
@login_required
def amacIV():
	return handleBackgroundTask(AMACIVForm, bkgAMACIVScans, [], 'amacIV.html', 'AMAC IV', 'amacDisplay', 'no_files')

@app.route('/amacDisplay')
@login_required
def amacDisplay():
	result = getResult()
	html_str = result['fig']
	return render_template('amacDisplay.html', html_str=html_str)

@app.route('/menyModIV', methods=['GET', 'POST'])
@login_required
def menyModIV():
	print('menyModIV accessed')
	return handleBackgroundTask(MenyModIVForm, bkgMenyModIV, [], 'menyModIV.html', 'Multiple module IVs', 'menyModIVEndpoint','with_files')

@app.route('/menyModIVEndpoint', methods=['GET', 'POST'])
@login_required
def menyModIVEndpoint():
	result = getResult()
	tarFile = result['zipfile']
	print(tarFile)
	return render_template('menyModIVEndPoint.html', title='Glue Weight Retrieval end', userTarFile=tarFile, result=result)

@app.route('/electricalTests', methods=['GET', 'POST'])
@login_required
def electricalTests():
	return handleBackgroundTask(ElectricalTestForm, bkgElectricalTestGraph, [], 'electricalTests.html', 'Electrical Tests', 'electricalTestDisplay', 'no_files')

@app.route('/electricalTestDisplay')
@login_required
def electricalTestDisplay():
	result = getResult()
	html_str = result['fig']
	return render_template('electricalTestDisplay.html', html_str=html_str)

@app.route('/glueWeightRetrieval', methods=['GET', 'POST'])
@login_required
def glueWeightRetrieval():
	return handleBackgroundTask(GlueWeightRetrievalForm, bkgGlueWeightRetrieval, [], 'glueWeightRetrieval.html', 'Glue Weight Retrieval', 'glueWeightRetrievalCheck','with_files')

@app.route('/glueWeightRetrievalCheck', methods=['GET', 'POST'])
@login_required
def glueWeightRetrievalCheck():
	tarFile = request.args['tarFile']
	return send_file(tarFile, as_attachment = True)

@app.route('/glueWeightRetrievalEndpoint', methods=['GET', 'POST'])
@login_required
def glueWeightRetrievalEndpoint():
	result = getResult()
	tarFile = result['createdFiles'][0]
	print(tarFile)
	return render_template('glueWeightRetrievalEndpoint.html', title='Glue Weight Retrieval end', userTarFile=tarFile, result=result)

@app.route('/testUploader', methods=['GET', 'POST'])
@login_required
def testUploader():
	return handleBackgroundTask(TestUploaderForm, bkgTestUploader, [], 'testUploader.html', 'Test Uploader Functions', 'testUploaderEndpoint', 'no_files')

@app.route('/testUploaderEndpoint', methods=['GET', 'POST'])
@login_required
def testUploaderEndpoint():
	result = getResult()
	return render_template('testUploaderEndpoint.html', title='Test Uploader Functions', result=result)

