from app import app
import os
from fabric import Connection
import inspect
from app.bkgTaskMethods import commonFunctions

def connect():
  gateway = app.config['BNL_GATEWAY']
  username = app.config['BNL_USER']
  keyPath = app.config['BNL_SSHKEY']
  spar = app.config['BNL_SPAR']
  # gatewayConnection = Connection(gateway, user=username, connect_kwargs={"key_filename":keyPath})
  # connection = Connection(spar, user=username, gateway=gatewayConnection, connect_kwargs={"key_filename":keyPath})
  passphrase = app.config['BNL_PASSWORD']
  gatewayConnection = Connection(gateway, user=username, connect_kwargs={"key_filename": keyPath, "password": passphrase})
  connection = Connection(spar, user=username, gateway=gatewayConnection, connect_kwargs={"key_filename": keyPath, "password": passphrase})
  ###############
  try:
    result = connection.run("whoami", hide=True)
    print(f"Connected successfully!")
  except Exception as e:
    print(f"Connection failed: {e}")
    import traceback
    traceback.print_exc()

  #################
  return connection

def renameFile(connection, fileName, location):
  newName = ""
  fileNameParts = fileName.split(".")
  fileExt = "."+fileNameParts[-1]
  fileNameNoExt = ".".join(fileNameParts[:-1])
  lsOut = connection.run("ls "+location, hide=True)
  filesInDir = lsOut.stdout.split("\n")
  if fileName in lsOut.stdout:
    command = "mv "+os.path.join(location,fileName)+" "+os.path.join(location,fileNameNoExt+"_1"+fileExt)
    command = command.replace('&','\&')
    connection.run(command, hide=True)
    newName = fileNameNoExt+"_2"+fileExt
  else:
    newName = fileName
  print(f'newName is {newName}')
  # Make sure same file with new order does not exist
  filesOfInterest = []
  for f in filesInDir:
    if fileNameNoExt in f and fileExt in f:
      filesOfInterest.append(f)
  lastIndex = 0
  for f in filesOfInterest:
    try:
      ind = int(f.split("_")[-1].split(".")[0])
      if lastIndex < ind:
        lastIndex = ind
    except:
      continue
    newName = fileNameNoExt+"_"+str(lastIndex+1)+fileExt
  if newName == "":
    return fileName
  else:
    return newName

def uploadFile(connection, filePath, destination):
  app.logger.info(f'Now in uploadFile function')
  location = app.config['BNL_STORAGE_PATH']
  print(location)
  app.logger.info(f'location = {location}')
  for folder in destination.split("/"):
    if folder != "":
      location = os.path.join(location, folder)
  fileName = filePath.split("/")[-1]
  app.logger.info(f'location = {location}')
  app.logger.info("GOT TO THIS POINT 1")
  fileName = renameFile(connection, fileName, location)
  app.logger.info("GOT TO THIS POINT 2")
  app.logger.info(f'fileName = {fileName}')
  print(filePath)
  print(os.path.join(location, fileName))

  try:
    connection.put(filePath, os.path.join(location, fileName))
  except Exception as e:
    print(f"Connection failed: {e}")
    import traceback
    traceback.print_exc()
  print(os.path.join(location, fileName))
  app.logger.info("GOT TO THIS POINT 3")
  app.logger.info(f'connection = {connection}')
  return

def uploadToDisk(output, specificLocation):

    # Iterate through the stack to see the last functions that were called.
    # If the test uploader function is found, don't upload anything to the BNL disk.   
    app.logger('Now in uploadToDisk')
    stack = inspect.stack()
    for frame in stack:
        function_name = frame.function
        if function_name == 'bkgTestUploader':
            return True

    notUploadedFiles = []
    toUploadFiles = []
    
    for f in output['inputFiles']:
        if f not in output['uploadedFiles']:
            notUploadedFiles.append(f)
    for f in output['createdFiles']:
        if f not in output['uploadedFiles']:
            notUploadedFiles.append(f)
    if len(output['errors']) > 0:
        toUploadFiles = output['inputFiles']

    # Establish connection to disk
    diskConnection = connect()
    
    # Upload files based on their status
    for f in notUploadedFiles:
        print("notUploadedFile")
        app.logger.info('Going to BNL box - folder = notUploaded')
        uploadFile(diskConnection, f, os.path.join("notUploaded", specificLocation))
    for f in output['uploadedFiles']:
        app.logger.info('Going to BNL box - folder = uploaded')
        uploadFile(diskConnection, f, os.path.join("uploaded", specificLocation))
    for f in toUploadFiles:
        app.logger.info('Going to BNL box - folder = toUpload')
        uploadFile(diskConnection, f, os.path.join("toUpload", specificLocation))


